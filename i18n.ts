import i18n from "i18next";
import { initReactI18next } from "react-i18next";

// the translations
// (tip move them in a JSON file and import them)
// const resources = { en, fr };

let init = (locale, resources) => i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    fallbackLng: 'en_US',
    lng: locale,

    keySeparator: ".", // keys in form messages.welcome

    interpolation: {
      escapeValue: false // react already safes from xss
    },
    debug: false
  });

// export default i18n;
export default init;