import { computeAnyTour, TourType } from "./order-product-compute"
import fs from "fs"
import Big from 'big.js'

const adjustment = {
  "name": "New adjustment",
  "adjustment_1": "30",
  "adjustment_2": "60",
  "adjustment_3": "0",
  "adjustment_4": "0",
  "adjustment_5": "0",
  "adjustment_6": "0",
  "adjustment_7": "0",
  "adjustment_8": "0",
  "adjustment_9": "0",
  "adjustment_10": "0",
  "adjustment_11": "0",
  "adjustment_12": "0",
  "adjustment_13": "0",
  "adjustment_14": "0",
  "adjustment_15": "0",
  "adjustment_16": "0",
  "adjustment_17": "0",
  "adjustment_18": "0",
  "adjustment_19": "0",
  "adjustment_20": "0",
  "condition_1_include_dates": "",
  "condition_2_from_date": "2020-01-01",
  "condition_2_to_date": "2030-12-31",
  "condition_2_exclude_dates": "",
  "condition_2_include_monday": 1,
  "condition_2_include_tuesday": 1,
  "condition_2_include_wednesday": 1,
  "condition_2_include_thursday": 1,
  "condition_2_include_friday": 1,
  "condition_2_include_saturday": 1,
  "condition_2_include_sunday": 1
}

test('computeAnyTour hasChildDiscount && flat rate', () => {
  const tourStr = fs.readFileSync('./test_data/group-tour-flat-rate.json', 'utf8')
  const tour = JSON.parse(tourStr).result

  const adult = 1
  const child = 2

  const result = computeAnyTour(tour, {
    adult,
    child,
    departure_date: '2022-12-28',
    tour_id: 'UNIONGOLF'
  }, TourType.GroupTour, 1668372542738)

  const adultPrice = Big(tour.price_1).times(adult)
  const childPrice = (Big(tour.price_1).minus(tour.price_child_discount)).times(child)
  expect(result.total).toBe(adultPrice.add(childPrice).toFixed(2));

  const adult2 = 2
  const child2 = 1

  const result2 = computeAnyTour(tour, {
    adult,
    child,
    adult2,
    child2,
    departure_date: '2022-12-28',
    tour_id: 'UNIONGOLF'
  }, TourType.GroupTour, 1668372542738)

  const adult2Price = Big(tour.price_1).times(adult2)
  const child2Price = (Big(tour.price_1).minus(tour.price_child_discount)).times(child2)
  expect(result2.total).toBe(adultPrice.add(childPrice).add(adult2Price).add(child2Price).toFixed(2));

  const extension = 5

  const result3 = computeAnyTour(tour, {
    adult,
    child,
    adult2,
    child2,
    departure_date: '2022-12-28',
    tour_id: 'UNIONGOLF',
    extension
  }, TourType.GroupTour, 1668372542738)

  const extensionPrice = Big(tour.price_ext_1).times(3).add(Big(tour.price_ext_1).times(3))
  expect(result3.total).toBe(adultPrice.add(childPrice).add(adult2Price).add(child2Price).add(extensionPrice).toFixed(2));

})

test('computeAnyTour hasChildDiscount && not flat rate', () => {
  const tourStr = fs.readFileSync('./test_data/group-tour-flat-rate.json', 'utf8')
  const tour = JSON.parse(tourStr).result
  tour.max_pax = 2

  const adult = 1
  const child = 1

  const result = computeAnyTour(tour, {
    adult,
    child,
    departure_date: '2022-12-28',
    tour_id: 'UNIONGOLF'
  }, TourType.GroupTour, 1668372542738)

  const price = Big(tour.price_2).minus(tour.price_child_discount)
  expect(result.total).toBe(price.toFixed(2));

  const adult2 = 2
  const child2 = 0

  const result2 = computeAnyTour(tour, {
    adult,
    child,
    adult2,
    child2,
    departure_date: '2022-12-28',
    tour_id: 'UNIONGOLF'
  }, TourType.GroupTour, 1668372542738)

  const price2 = price.add(tour.price_2)

  expect(result2.total).toBe(price2.toFixed(2));

})

test('computeAnyTour noChildDiscount && flat rate', () => {
  const tourStr = fs.readFileSync('./test_data/group-tour-flat-rate.json', 'utf8')
  const tour = JSON.parse(tourStr).result

  const adult = 1

  const result = computeAnyTour(tour, {
    adult,
    child: 0,
    departure_date: '2022-12-28',
    tour_id: 'UNIONGOLF'
  }, TourType.GroupTour, 1668372542738)

  const price = Big(tour.price_1).times(adult)
  expect(result.total).toBe(price.toFixed(2));

  const adult2 = 5

  const result2 = computeAnyTour(tour, {
    adult: adult2,
    child: 0,
    departure_date: '2022-12-28',
    tour_id: 'UNIONGOLF'
  }, TourType.GroupTour, 1668372542738)

  const price2 = Big(tour.price_1).times(adult2)
  expect(result2.total).toBe(price2.toFixed(2));

})

test('computeAnyTour flat rate departure_date adjustment', () => {
  const tourStr = fs.readFileSync('./test_data/group-tour-flat-rate.json', 'utf8')
  const tour = JSON.parse(tourStr).result
  tour['adjustment_departure_dates'] = [adjustment];

  const adult = 1

  const result = computeAnyTour(tour, {
    adult,
    child: 0,
    departure_date: '2022-12-28',
    tour_id: 'UNIONGOLF'
  }, TourType.GroupTour, 1668372542738)

  const price = Big(tour.price_1).times(adult).add(adjustment.adjustment_1)
  expect(result.total).toBe(price.toFixed(2));

  const adult2 = 2

  const result2 = computeAnyTour(tour, {
    adult,
    child: 0,
    adult2,
    departure_date: '2022-12-28',
    tour_id: 'UNIONGOLF'
  }, TourType.GroupTour, 1668372542738)

  const price2 = Big(tour.price_1).times(adult2).add(Big(adjustment.adjustment_1).times(2))
  expect(result2.total).toBe(price.add(price2).toFixed(2));
})

test('computeAnyTour not flat rate travel_date adjustment', () => {
  const tourStr = fs.readFileSync('./test_data/group-tour-flat-rate.json', 'utf8')
  const tour = JSON.parse(tourStr).result

  tour.max_pax = 2
  tour['adjustment_travel_dates'] = [adjustment]

  const adult = 1

  const result = computeAnyTour(tour, {
    adult,
    child: 0,
    departure_date: '2022-12-28',
    tour_id: 'UNIONGOLF'
  }, TourType.GroupTour, 1668372542738)

  const price = Big(tour.price_1).times(adult).add(Big(adjustment.adjustment_1).times(2))
  expect(result.total).toBe(price.toFixed(2));

  const adult2 = 2

  const result2 = computeAnyTour(tour, {
    adult,
    child: 0,
    adult2,
    departure_date: '2022-12-28',
    tour_id: 'UNIONGOLF'
  }, TourType.GroupTour, 1668372542738)

  const price2 = Big(tour.price_2).add(Big(adjustment.adjustment_2).times(2))
  expect(result2.total).toBe(price.add(price2).toFixed(2));
})
