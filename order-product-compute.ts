import Big from 'big.js'
import moment from 'moment'
import rulesCompute from './rules-compute'
import {Tour, Generic, FlightDetail, Order, PriceRules, Customer, PriceRule, v2_PrivateTour, v2_GroupTour} from './types'
import bcrypt from 'bcryptjs'
import { NDC_OfferPriceRes1, NDC_OrderReshopRes, NDC_ServiceListRes } from './custom'

export type Item = {
  [P in keyof Order['products'][0]['items'][0]]? : Order['products'][0]['items'][0][P]
}

export type Product = {
  [P in keyof Pick<Order['products'][0], Exclude<keyof Order['products'][0], 'items'>>]? : Order['products'][0][P]
} & {
  items: Item[]
}

export type ComputeResult = {
  product?: Product,
  error?: string,
  total?: string
}

export type FlightFormValue = {
  od1_id: string
  od2_id?: string
  pricing_id: string
  ptc_adt: number
  ptc_cnn?: number
  ptc_inf?: number
  prime_workflow?: boolean
  search_id?: string
  seats?: {
    traveler_ref: string,
    row_number: string,
    column_id: string,
    offer_item_id: string
  }[]
  services?: {
    traveler_ref: string,
    // orig_offer_item_id: string,
    offer_item_id: string,
    quantity: number
  }[]
}

export type FlightChangeFormValue = {
  od1_id: string
  od2_id?: string
  pricing_id: string
  product_source_ref: string
  seats?: {
    traveler_ref: string,
    row_number: string,
    column_id: string,
    offer_item_id: string
  }[]
}

export type FlightChangeServiceFormValue = {
  product_source: string
  product_source_ref: string
  services?: {
    traveler_ref: string,
    // orig_offer_item_id: string,
    offer_item_id: string,
    quantity: number
  }[],
  seats?: {
    segment_id: string,
    traveler_ref: string,
    row_number: string,
    column_id: string,
    offer_item_id: string
  }[]
}

export type TourFormValue = {
  tour_id?: string,
  rooms?: {
    adult: number,
    child_with_bed?: number,
    child_no_bed?: number,
    infant?: number
  }[],
  option_id?: string,
  departure_date?: string,
  tour_addons?: {
    id: string,
    option_id: string
  }[],
  generic_addons?: {
    id: string,
    options: {
      id: string,
      quantity: string | number
    }[]
  }[]
}

export type GlobaltixFormValue = {
  id: number,
  quantity: number,
  fromResellerId: number | null,
  visitDate?: string,
  questionList?: {
    id: number,
    answer: string
  }[]
}

export type LegacyHotelFormValue = {
  room_id: string,
  price_code: string,
  params: {
    hotelId: string
    checkInDate: string
    duration: number | string
    adults: number | string
    children: number | string
  }}

export type HotelbedsFormValue = {
  
  sourceMarket?: string,

  stay: {
    checkIn: string 
    checkOut: string
    shiftDays: number | string
  },

  occupancies: {
    rooms: number
    adults: number
    children: number
    paxes: {
      type: string
      age: number
    }[]
  }[],

  hotels: {
    hotel: number[]
  },

  destinationCode ?: string,
  rateKey ?: string,
  hotelCode ?: string
}

// fixme: to be removed
export type HotelbedsQuery = HotelbedsFormValue & { rateKey: string, hotelCode?: string, destionationCode?: string }


export type HotelbedsStaticData = {
  code: number,
}

export const decodePriceCode = {
  SGL: 'Adult (SGL)',
  TWN: 'Adult (TWN)',
  TRP: 'Adult (TRP)',
  QUAD: 'Adult (QUAD)',
  CWB: 'Child (with bed)',
  CNB: 'Child (without bed)',
  CHT: 'Child (half twin)',
  INF: 'Infant'
}

export function computeBookableTimeLeft(tour: {
  stop_booking_days_before: number,
  stop_booking_time: string,
  utc_offset: string
}, iso_date: string, currentTs?: number): number {
  if (currentTs == null) currentTs = (new Date()).getTime()

  var targetTS = Date.parse(iso_date + "T00:00:00.000" + tour.utc_offset);
  if (isNaN(targetTS)) targetTS = Date.parse(iso_date + "T00:00:00.000+08:00");
  var stopBookingTimeSplit = (tour.stop_booking_time ?? '').split(':')
  var hour = parseInt(stopBookingTimeSplit[0])
  if (isNaN(hour)) hour = 0

  var min = parseInt(stopBookingTimeSplit[1])
  if (isNaN(min)) min = 0

  var maxAllowedTs = targetTS - (tour.stop_booking_days_before ?? 0) * (24*60*60*1000) + hour * 60*60*1000 + min * 60*1000

  return maxAllowedTs - currentTs
}

// as convention, we assume all string dates are in local timezone
// new Date('2020-01-01') // treats as 00:00 UTC (so we add "T00:00" to convert into local) 
// new Date('2020-01-01T00:00') // treats as local
// new Date(year, monthIndex) // treats as local
export function newDateLocal(a1: string | number = undefined, a2: number = undefined, a3: number = undefined): Date {
  if (a2 == null && a3 == null) {
    if (typeof a1 === "string" && a1.length === 10) return new Date(a1 + "T00:00")
    if (a1 == null) return new Date()
    return new Date(a1)
  }
  if (typeof a1 === "number") return new Date(a1, a2, a3)

  throw new Error("Unable to create Date from provided parameters")
}

export function computeTour(tour: Tour, tourFormValue: TourFormValue): ComputeResult {
  const optionSelected = tour.options.find(option => option.id === tourFormValue.option_id)
  if (optionSelected == null) {return {
    error: 'option_id ' + tourFormValue.option_id + ' not found'
  }}

  const departureSelected = optionSelected.departures.find(departure => departure.date === tourFormValue.departure_date)

  // mutate tourFormValue to ensure everything is number
  // there is no way to enforce types at runtime
  tourFormValue.rooms = tourFormValue.rooms.map(room => ({
    adult: parseInt(String(room.adult || 0), 10),
    child_with_bed: parseInt(String(room.child_with_bed || 0), 10),
    child_no_bed: parseInt(String(room.child_no_bed || 0), 10),
    infant: parseInt(String(room.infant || 0), 10),
  }))

  const adult = tourFormValue.rooms.reduce((acc, room) => (room.adult || 0) + acc, 0)
  const child = tourFormValue.rooms.reduce((acc, room) => (room.child_with_bed || 0) + (room.child_no_bed || 0) + acc, 0)
  const infant = tourFormValue.rooms.reduce((acc, room) => (room.infant || 0) + acc, 0)
  const traveler = adult + child
  const tmpDepartureDate = newDateLocal(tourFormValue.departure_date)

  const notPricingByRoom = tour.price_type.indexOf('ALL') === -1 && tour.price_type.indexOf('SGL') === -1

  const counter = {
    SGL: 0,
    TWN: 0,
    TRP: 0,
    QUAD: 0,
    CWB: 0,
    CNB: 0,
    CHT: 0,
    INF: 0
  }

  const context = {
    adult, child, infant, traveler,
    slots_taken: departureSelected == null ? [] : Array(traveler).fill(null).map((_, idx) => departureSelected.slots_taken + idx),
    departure_date: [tourFormValue.departure_date],
    travel_dates: Array(parseInt(optionSelected.duration, 10)).fill(null).map((_, idx) => {
      const isoDate = dateToIsoDate(tmpDepartureDate)
      tmpDepartureDate.setDate(tmpDepartureDate.getDate() + 1)
      return isoDate}),
    coupon: '',
    days_before_departure: Math.ceil(((newDateLocal(tourFormValue.departure_date)).getTime() - (newDateLocal()).getTime()) / 24 / 60 / 60 / 1000),
    room: tourFormValue.rooms.length
  }

  const rulesResult = rulesCompute(optionSelected.rule_parsed, context)

  const effectivePrice: any = Object.assign({}, optionSelected)
  const currentPrice = rulesResult.prices.pop()
  if (currentPrice != null) {
    effectivePrice.name += ' (' + currentPrice.name + ')'
    for (const j in currentPrice.price) {
      if (currentPrice.price.hasOwnProperty(j)) {
        const k = currentPrice.price[j]
        effectivePrice[k.code] = k.price
      }
    }
  }

  const pushLines = (adultPriceType: string, price: any, subHeader: string, room: any, acc: any[]) => {
    counter[adultPriceType] += room.adult
    acc.push({
      sub_header: subHeader,
      name: 'Adult',
      price: price[adultPriceType],
      deposit: price['fixed_deposit'],
      price_type: adultPriceType,
      quantity: room.adult
    })

    let childBed = room.child_with_bed || 0
    if (room.adult <= 1 && childBed > 0) {
      childBed--
      counter.CHT += 1
      acc.push({
        sub_header: subHeader,
        name: decodePriceCode.CHT,
        price: effectivePrice['CHT'],
        deposit: price['fixed_deposit'],
        price_type: 'CHT',
        quantity: 1
      })
    }

    if (room.adult > 1 && childBed > 0) {
      counter.CWB += childBed
      acc.push({
        sub_header: subHeader,
        name: decodePriceCode.CWB,
        price: effectivePrice['CWB'],
        deposit: price['fixed_deposit'],
        price_type: 'CWB',
        quantity: childBed
      })
    }

    if ((room.child_no_bed || 0) > 0) {
      counter.CNB += room.child_no_bed
      acc.push({
        sub_header: subHeader,
        name: decodePriceCode.CNB,
        price: effectivePrice['CNB'],
        deposit: price['fixed_deposit'],
        price_type: 'CNB',
        quantity: room.child_no_bed
      })
    }

    if ((room.infant || 0) > 0) {
      counter.INF += room.infant
      acc.push({
        sub_header: subHeader,
        name: decodePriceCode.INF,
        price: effectivePrice['INF'],
        deposit: price['fixed_deposit'],
        price_type: 'INF',
        quantity: room.infant
      })
    }
  }

  const invoiceItems = tourFormValue.rooms.reduce((acc, room, index) => {
    const subHeader = tourFormValue.rooms.length > 1 ? 'Room ' + (index + 1) : 'Base price'
    const beds = room.adult + (room.child_with_bed || 0)
    if (tour.price_type.indexOf('ALL') === -1 && tour.price_type.indexOf('SGL') === -1 && tour.price_type.indexOf('CWB') === -1) {
      const totalTravellers = (room.adult || 0) + (room.child_no_bed || 0) + (room.child_with_bed || 0)
      if (totalTravellers > 0) {
        counter.TWN += totalTravellers
        acc.push({
          sub_header: subHeader,
          name: 'Traveller',
          price: effectivePrice['TWN'],
          deposit: effectivePrice['fixed_deposit'],
          price_type: 'TWN',
          quantity: totalTravellers
        })}

      return acc
    }

    if (notPricingByRoom) {
      if ((room.adult || 0) > 0) {
        counter.TWN += room.adult
        acc.push({
          sub_header: subHeader,
          name: 'Adult',
          price: effectivePrice['TWN'],
          deposit: effectivePrice['fixed_deposit'],
          price_type: 'TWN',
          quantity: room.adult
        })}
      const totalChildren = (room.child_no_bed || 0) + (room.child_with_bed || 0)
      if (totalChildren > 0) {
        counter.CWB += totalChildren
        acc.push({
          sub_header: subHeader,
          name: 'Child',
          price: effectivePrice['CWB'],
          deposit: effectivePrice['fixed_deposit'],
          price_type: 'CWB',
          quantity: totalChildren
        })}

      return acc
    }

    if (beds === 1) {
      pushLines('SGL', effectivePrice, subHeader, room, acc)
    }
    else if (beds === 2) {
      pushLines('TWN', effectivePrice, subHeader, room, acc)
    }
    else if (beds === 3) {
      pushLines('TRP', effectivePrice, subHeader, room, acc)
    }
    else {
      pushLines('QUAD', effectivePrice, subHeader, room, acc)
    }
    return acc
  }, [])

  let totalPrice = invoiceItems.reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))

  const items = rulesResult.surcharges.reduce((acc, surcharge) => {
    const price = Big(surcharge.price['fixed']).plus(Big(surcharge.price['percentage']).times(totalPrice).div(100)).toFixed(2)
    const line = {
      sub_header: 'Adjustments',
      name: surcharge.name,
      price: price.toString(),
      deposit: "0",
      price_type: 'ADJUST',
      quantity: surcharge.qty
    }
    acc.push(line)
    totalPrice = totalPrice.add(Big(price).times(surcharge.qty))
    return acc
  }, invoiceItems)

  if (tourFormValue.tour_addons != null) {
    for (var value of tourFormValue.tour_addons) {
      const tour_addons = tour.tour_addons.find((x) => x.id === value.id)

      if (tour_addons == null) continue
      const option = tour_addons.options.find((x) => x.id === value.option_id)

      if (option == null) {
        items.push({
          sub_header: tour_addons.name,
          name: tour_addons.default_option,
          price: '0',
          deposit: '0',
          price_type: 'DEFAULT',
          quantity: '0'
        })
      }
      else if (notPricingByRoom) {
        if (counter.TWN > 0) {
          items.push({
            sub_header: tour_addons.name,
            name: option.name + ' (TWN)',
            price: option.TWN,
            deposit:  "0",
            price_type: 'TWN',
            quantity: counter.TWN
          })}
        if (counter.CWB > 0) {
          items.push({
            sub_header: tour_addons.name,
            name: option.name + ' (CWB)',
            price: option.CWB,
            deposit:  "0",
            price_type: 'CWB',
            quantity: counter.CWB
          })}
      }
      else {
        for (let price_type in counter) {
          const quantity = counter[price_type]
          if (quantity === 0) continue
          items.push({
            sub_header: tour_addons.name,
            name: option.name + ' (' + price_type + ')',
            price: option[price_type],
            deposit:  "0",
            price_type: price_type,
            quantity: quantity
          })
        }
      }
    }
  }

  if (tourFormValue.generic_addons != null) {
    for (let generic_addon_form of tourFormValue.generic_addons) {
      const generic_addon = tour.generic_addons.find(x => x.id === generic_addon_form.id)
      if (generic_addon == null) continue
      for (let option_form of generic_addon_form.options) {
        const quantity = parseInt("" + option_form.quantity)
        if (quantity === 0) continue;
        const option = generic_addon.options.find((x) => x.id === option_form.id)
        items.push({
          sub_header: generic_addon.name,
          name: option.name,
          price: option.price,
          deposit:  "0",
          price_type: '',
          quantity: quantity
        })
      }
    }
  }

  const toDate = newDateLocal(tourFormValue.departure_date)
  toDate.setDate(toDate.getDate() + parseInt(optionSelected.duration, 10))

  const product = {
    'product_source': '',
    'product_source_ref': '',
    'product_type': 'tour_package',
    'product_id': tour.id,
    'product_code': optionSelected['product_code'],
    'product_name': tour.name + ' > ' + optionSelected.name,
    'product_category_ids': tour.categories.map((cat) => cat.id).join(','),
    'quantity': traveler + "",
    'deposit_percentage': effectivePrice['percentage_deposit'],
    'from_date': tourFormValue.departure_date,
    'to_date': dateToIsoDate(toDate),
    'product_status': 'Unconfirmed',
    adult, child, infant, items
  }

  return {
    product,
    total: totalPrice.toString()
  }
}

export type LegacyHotelForm = {
  hotelId: string
  checkInDate: string
  duration: number
  adults: number
  children: number
}

export function computeGeneric(
  generic: Generic,
  params: {
    generic_id: string,
    option_id: string,
    quantity: number,
    from_date?: string,
    from_time?: string
  }): ComputeResult {

  params.quantity = parseInt(String(params.quantity || 0), 10)

  const option = generic.options.find(optionx => optionx.id === params.option_id)
  if (option == null) {return {
    error: 'option_id ' + params.option_id + ' not found'
  }}

  const product: any = {
    product_name: generic.name,
    product_type: 'nontour_product',
    product_status: 'Unconfirmed',
    product_source: '',
    product_id: generic.id + '/' + option.id,
    product_code: option.code,
    product_category_ids: generic.categories.map((cat) => cat.id).join(','),
    quantity: params.quantity + '',
    deposit_percentage: '100',
    items: [{
      sub_header : '',
      name : option.name,
      price : option.price,
      price_type : '',
      deposit : '0',
      quantity : params.quantity + ''
  }]}

  if (params.from_date != null) {
    product.from_date = params.from_date
    if (params.from_time != null) {
      product.from_time = params.from_time
    }
  }

  return {
    product,
    total: Big(option.price).times(params.quantity).toFixed(2)
  }
}

export type HotelbedsRoomRate = {
  rateKey: string,
  rateClass: string,
  rateType: string,
  net: string,
  travelcloud_price: string,
  allotment: number,
  rateCommentId: string,
  paymentType: string,
  
  packaging: boolean,
  boardCode: string,
  boardName: string,


  rooms: number,
  adults: number,
  children: number,

  cancellationPolicies: {
    amount: string,
    from: string
  }[]

  // rateComments
  // taxes
  // rateBreakDown
  // offers
  // totalNet
  // currency 
  // paymentDataRequired
}

export type HotelbedsRoom = {
    code: string,
    name: string,
    rates: HotelbedsRoomRate[]
}

export type HotelbedsBookingForm = {

  holder: {
    name: string,
    surname: string
  },

  rooms: {
    rateKey: string,
    paxes: {
      roomId: string|number,
      type: string,
      name: string
    }[]

  }[]

}

export type HotelbedsAvailability = {
  code: number,
  name: string,
  categoryCode: string,
  categoryName: string,
  destinationCode: string,
	destinationName: string,
  zoneCode: number,
  zoneName: string,
  latitude: string,
  longitude: string,
  rooms: HotelbedsRoom[]
}

export type HotelbedsCheckRateResponse = HotelbedsAvailability & {
  checkOut: string,
  checkIn: string,
}

export type OutboundFormValue = {
  tourCode: string,
  landOnly: string,
  roomList: {
    roomNo: number,
    adults: number,
    children: number,
    childrenNoBed: number,
    infants: number
  }[]
}

export type OutboundPriceCheck = {
  "tourCode": string,
  "landOnly": string,
  "numAdultSingle": number,
  "numAdultTwin": number,
  "numAdultTriple": number,
  "numAdultQuad": number,
  "numChildWithoutBed": number,
  "numChildHalfTwin": number,
  "numChildWithBed": number,
  "numInfant": number,
  "singleFare": number,
  "twinFare": number,
  "tripleFare": number,
  "quadFare": number,
  "childWithoutBedFare": number,
  "childHalfTwinFare": number,
  "childWithBedFare": number,
  "infantFare": number,
  "groundSingleFare": number,
  "groundTwinFare": number,
  "groundTripleFare": number,
  "groundQuadFare": number,
  "groundChildWithoutBedFare": number,
  "groundChildHalfTwinFare": number,
  "groundChildWithBedFare": number,
  "groundInfantFare": number,
  "airportTaxPerAdultPax": number,
  "airportTaxPerChildPax": number,
  "totalFare": number,
}

export type OutboundTour = {
  "tourCode": string,
  "briefDescription": string,
  "description1": string,
  "description2": string,
  "departureDate": string,
  "returnDate": string,
  "capacity": number,
  "minPax": number,
  "noOfTourLeader": number,
  "paymentDueDate": string,
  "mainAirline": string,
  "remark1": string,
  "remark2": string,
  "remark3": string,
  "departureMeetTime": number,
  "priceCode": string,
  "singleFare": number,
  "twinFare": number,
  "tripleFare": number,
  "quadFare": number,
  "childWithoutBedFare": number,
  "childHalfTwinFare": number,
  "childWithBedFare": number,
  "infantFare": number,
  "groundSingleFare": number,
  "groundTwinFare": number,
  "groundTripleFare": number,
  "groundQuadFare": number,
  "groundChildWithoutBedFare": number,
  "groundChildHalfTwinFare": number,
  "groundChildWithBedFare": number,
  "groundInfantFare": number,
  "airportTaxPerAdultPax": number,
  "airportTaxPerChildPax": number,
  "discountCode": string,
  "singleFareSurcharge": number,
  "twinFareSurcharge": number,
  "tripleFareSurcharge": number,
  "quadFareSurcharge": number,
  "infantFareSurcharge": number,
  "childWithoutBedFareSurcharge": number,
  "childHalfTwinFareSurCharge": number,
  "childWithBedFareSurcharge": number,
  "groundSingleFareSurcharge": number,
  "groundTwinFareSurcharge": number,
  "groundTripleFareSurcharge": number,
  "groundQuadFareSurcharge": number,
  "groundInfantFareSurcharge": number,
  "groundChildWithoutBedFareSurcharge": number,
  "groundChildHalfTwinFareSurcharge": number,
  "groundChildWithBedFareSurcharge": number,
  "adultDiscountAmount": number,
  "childDiscountAmount": number,
  "priceRemark1": string,
  "priceRemark2": string,
  "tourClosed": string,
  "balanceSeat": number,
}

export type OutboundDepositRule = {
  "Price Code": string,
  "Deposit/PAX": number
}

export function computeOutbound(data : {tourMaster: OutboundTour, priceCheck: OutboundPriceCheck, depositRules: OutboundDepositRule[]}, bookingForm: OutboundFormValue): ComputeResult {
  const {tourMaster, priceCheck} = data;

  let total = Big(0)

  const items = [];

  var priceCode = tourMaster.priceCode;
  console.log(data.depositRules)
  var deposit = data.depositRules?.find((dr) => priceCode.indexOf(dr["Price Code"]) !== -1)?.["Deposit/PAX"]
  if (deposit == null) deposit = data.depositRules?.find((dr) => dr["Price Code"] === "DEFAULT")?.["Deposit/PAX"]
  if (deposit == null) deposit = 1000

  const pushItem = (name, qty, discount, price, surcharge, type) => {
    if (qty > 0) {
      total = total.plus(Big(qty).times(price))
      items.push({
        sub_header: type,
        name : name,
        price: String( price ),
        cost: String( price ),
        price_type: '',
        deposit: deposit.toString(),
        quantity: String( qty )
      })

      if (surcharge > 0) {
        total = total.plus(Big(qty).times(surcharge))
        items.push({
          sub_header: type,
          name : name + " Surcharge",
          price: String( surcharge ),
          cost: String( surcharge ),
          price_type: '',
          deposit: 0,
          quantity: String( qty )
        })
      }

      if (discount > 0) {
        total = total.plus(Big(qty).times(discount * -1))
        items.push({
          sub_header: type,
          name : name + " Discount",
          price: String( price * -1),
          cost: String( price * -1),
          price_type: '',
          deposit: 0,
          quantity: String( qty )
        })
      }
    }
}

  //price check's price excludes "hidden discounts"
  //tourMaster price includes "hidden discounts"
  //hidden discounts is indicated as "Adult discount" on OB invoice but is different from adultDiscountAmount, childDiscountAmount
  //price check is probably not required if we can compute numAdultSingle, numAdultTwin, etc. client side

  if (priceCheck.landOnly === 'Y') {
    pushItem(`Adult Single`, priceCheck.numAdultSingle, tourMaster.adultDiscountAmount, tourMaster.groundSingleFare, tourMaster.groundSingleFareSurcharge, 'Land Tour')
    pushItem(`Adult Twin`, priceCheck.numAdultTwin, tourMaster.adultDiscountAmount, tourMaster.groundTwinFare, tourMaster.groundTwinFareSurcharge, 'Land Tour')
    pushItem(`Adult Triple`, priceCheck.numAdultTriple, tourMaster.adultDiscountAmount, tourMaster.groundTripleFare, tourMaster.groundTripleFareSurcharge, 'Land Tour')
    pushItem(`Adult Quad`, priceCheck.numAdultQuad, tourMaster.adultDiscountAmount, tourMaster.groundQuadFare, tourMaster.groundQuadFareSurcharge, 'Land Tour')
    pushItem(`Child Without Bed`, priceCheck.numChildWithoutBed, tourMaster.childDiscountAmount, tourMaster.groundChildWithoutBedFare, tourMaster.groundChildWithoutBedFareSurcharge, 'Land Tour')
    pushItem(`Child Half Twin`, priceCheck.numChildHalfTwin, tourMaster.childDiscountAmount, tourMaster.groundChildHalfTwinFare, tourMaster.groundChildHalfTwinFareSurcharge, 'Land Tour')
    pushItem(`Child With Bed`, priceCheck.numChildWithBed, tourMaster.childDiscountAmount, tourMaster.groundChildWithBedFare, tourMaster.groundChildWithBedFareSurcharge, 'Land Tour')
  } else {
    pushItem(`Adult Single`, priceCheck.numAdultSingle, tourMaster.adultDiscountAmount, tourMaster.singleFare, tourMaster.singleFareSurcharge, 'Full Package')
    pushItem(`Adult Twin`, priceCheck.numAdultTwin, tourMaster.adultDiscountAmount, tourMaster.twinFare, tourMaster.twinFareSurcharge, 'Full Package')
    pushItem(`Adult Triple`, priceCheck.numAdultTriple, tourMaster.adultDiscountAmount, tourMaster.tripleFare, tourMaster.tripleFareSurcharge, 'Full Package')
    pushItem(`Adult Quad`, priceCheck.numAdultQuad, tourMaster.adultDiscountAmount, tourMaster.quadFare, tourMaster.quadFareSurcharge, 'Full Package')
    pushItem(`Child Without Bed`, priceCheck.numChildWithoutBed, tourMaster.childDiscountAmount, tourMaster.childWithoutBedFare, tourMaster.childWithoutBedFareSurcharge, 'Full Package')
    pushItem(`Child Half Twin`, priceCheck.numChildHalfTwin, tourMaster.childDiscountAmount, tourMaster.childHalfTwinFare, tourMaster.childHalfTwinFareSurCharge, 'Full Package')
    pushItem(`Child With Bed`, priceCheck.numChildWithBed, tourMaster.childDiscountAmount, tourMaster.childWithBedFare, tourMaster.childWithBedFareSurcharge, 'Full Package')

    // pricecheck doesn't return tax for some reason
    const adults = priceCheck.numAdultSingle + priceCheck.numAdultTwin + priceCheck.numAdultTriple + priceCheck.numAdultQuad
    
    if (adults > 0 && tourMaster.airportTaxPerAdultPax > 0) {
      total = total.plus(Big(adults).times(tourMaster.airportTaxPerAdultPax))
      items.push({
        sub_header: '',
        name : `Adult (Airport Tax)`,
        price: String( tourMaster.airportTaxPerAdultPax ),
        cost: String( tourMaster.airportTaxPerAdultPax ),
        price_type: '',
        deposit: '0',
        quantity: String( adults )
      })
    }

    const children = priceCheck.numChildWithoutBed + priceCheck.numChildHalfTwin + priceCheck.numChildWithBed

    if (children > 0 && tourMaster.airportTaxPerChildPax > 0) {
      total = total.plus(Big(children).times(tourMaster.airportTaxPerChildPax))
      items.push({
        sub_header: 'TAX',
        name : `Child (Airport Tax)`,
        price: String( tourMaster.airportTaxPerChildPax ),
        cost: String( tourMaster.airportTaxPerChildPax ),
        price_type: '',
        deposit: '0',
        quantity: String( children )
      })
    }
  }

  const product = {
    product_name: tourMaster.briefDescription + " (" + tourMaster.tourCode.trim() + ")",
    product_type: 'outbound',
    product_status: 'Unconfirmed',
    product_source: 'outbound',
    product_id: null,
    product_code: tourMaster.tourCode,
    product_detail: data,
    from_date: tourMaster.departureDate,
    to_date: tourMaster.returnDate,
    quantity:
      priceCheck.numAdultSingle +
      priceCheck.numAdultTwin +
      priceCheck.numAdultTriple +
      priceCheck.numAdultQuad +
      priceCheck.numChildWithoutBed +
      priceCheck.numChildHalfTwin +
      priceCheck.numChildWithBed + '',
    deposit_percentage: '0',
    items
  }

  return {
    product,
    total: total.toFixed()
  }
}

// according to the best practice described by hotelbeds,
// more than one rateKey shouldn't send per request,
// @param hotel is expected to have only one room having only one rate
// Ref: https://developer.hotelbeds.com/docs/read/apitude_booking/Best_Practices
export function computeHotelbeds( data: {static: any, checkrates: HotelbedsCheckRateResponse}, 
  bookingForm:HotelbedsBookingForm): ComputeResult {

  const hotel = data.checkrates

  if (hotel == null || hotel.rooms.length !== 1 ) return {
    error: 'Only expecting single room and rate for check rate'
  }

  // if (hotel == null || hotel.rooms.length !== 1 || hotel.rooms[ 0 ].rates.length !== 1) return {
  //   error: 'Only expecting single room and rate for check rate'
  // }

  // if (bookingForm == null || bookingForm.rooms.length !== 1) return {
  //   error: 'Only expecting single room and rate for booking form'
  // }

  // if (bookingForm.rooms[0].rateKey !== hotel.rooms[0].rates[0].rateKey) return {
  //   error: 'Ratekey in booking form is different from ratekey in check rate'
  // }

  const room = hotel.rooms[ 0 ]
  const rates = room.rates
  const product_id = rates.map(rate => rate.rateKey).join(',');
  const total = rates.reduce((total: any, rate: any) => total.plus(rate.travelcloud_price), new Big(0))
  const quantities = rates.reduce((total: any, rate: any) => total + rate.rooms, 0);

  const items = rates.map(rate => {
    return {
      sub_header: hotel.name,
      price_rule_id: '',
      name : `${room.name} [${room.code}, ${rate.adults} adults]`,
      price: String( rate.travelcloud_price ),
      cost: String( rate.net ),
      price_type: '',
      deposit: "0",
      quantity: String( rate.rooms )
    }
  })

  const product = {
    product_name: hotel.name,
    product_type: 'hotel_room',
    product_status: 'Unconfirmed',
    product_source: 'hotelbeds',
    product_id,
    product_code: room.code,
    product_detail: data,
    from_date: hotel.checkIn,
    to_date: hotel.checkOut,
    quantity: String( quantities ),
    deposit_percentage: '100',
    items
  }

  return {
    product,
    total: total.toFixed()
  }
}

export function computeGlobaltix(item, form: GlobaltixFormValue): ComputeResult {
  const quantity = parseInt(String(form.quantity || 0), 10)

  if (item.ticketTypes != null) {

    const ticket = item.ticketTypes.find(ticket => String(ticket.id) === String(form.id))
    if (ticket == null) {return {
      error: 'ticketType_id ' + item.ticketType_id + ' not found'
    }}

    var name = [ticket.name]
    if (form.visitDate != null) name.push("- Visit date: " + form.visitDate)
    if (form.questionList != null) {
      for (var answerIndex in form.questionList) {
        const answer = form.questionList[answerIndex]
        name.push("- "  + answer.answer)
      }
    }

    const product = {
      product_name: item.title,
      product_type: 'attraction',
      product_status: 'Unconfirmed',
      product_source: 'globaltix',
      product_id: String(item.id) + '/' + String(form.id),
      product_code: '',
      product_detail: item,
      quantity: quantity + '',
      deposit_percentage: '100',
      items: [{
        sub_header : '',
        name : name.join("\n"),
        price : String(ticket.travelcloud_price),
        cost : String(ticket.travelcloud_cost),
        price_type : '',
        deposit : '0',
        quantity : quantity + ''
      }]
    }

    return {
      product,
      total: Big(ticket.travelcloud_price).times(quantity).toFixed(2)
    }
  }

  else if (item.attraction != null) {

    var name = [item.name]
    if (form.visitDate != null) name.push("- Visit date: " + form.visitDate)
    if (form.questionList != null) {
      for (var answerIndex in form.questionList) {
        const answer = form.questionList[answerIndex]
        name.push("- "  + answer.answer)
      }
    }

    const product = {
      product_name: item.attraction.title,
      product_type: 'attraction',
      product_status: 'Unconfirmed',
      product_source: 'globaltix',
      product_id: String(item.attraction.id) + '/' + String(form.id),
      product_code: '',
      quantity: quantity + '',
      deposit_percentage: '100',
      items: [{
        sub_header : '',
        name : name.join("\n"),
        price : String(item.travelcloud_price),
        cost : String(item.travelcloud_cost),
        price_type : '',
        deposit : '0',
        quantity: quantity + ''
      }]
    }

    return {
      product,
      total: Big(item.travelcloud_price).times(quantity).toFixed(2)
    }

  }

  else {
    return {error: 'Item is in unknown format. Please provide attraction with ticketTypes from attraction/list or ticketType/get'}
  }
}

const range = (count:number) => {
  // return [...Array(count).keys()]
  return Array.apply(null, Array(count)).map((_, index) => index)
}

const decodeDate = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']

export function isAdjustmentEffective(adjustment: v2_PrivateTour['adjustment_travel_dates'][0], date: string): boolean {
  // condition 1
  if (adjustment.condition_1_include_dates.indexOf(date) !== -1) return true;

  // condition 2
  const jsDate = newDateLocal(date)

  /*
  console.log(date, adjustment.condition_2_exclude_dates.indexOf(date) === -1
    , (newDateLocal(adjustment.condition_2_from_date)) <= jsDate
    , (newDateLocal(adjustment.condition_2_to_date)) >= jsDate
    , adjustment['condition_2_include_' + decodeDate[jsDate.getDay()]] === 1)
  */

  if (
    adjustment.condition_2_exclude_dates.indexOf(date) === -1
    && (newDateLocal(adjustment.condition_2_from_date)) <= jsDate
    && (newDateLocal(adjustment.condition_2_to_date)) >= jsDate
    && adjustment['condition_2_include_' + decodeDate[jsDate.getDay()]] === 1) return true
    
  return false
}

export type AnyTourFormValue = {
  adult: number,
  child: number,

  adult2?: number,
  child2?: number,

  adult3?: number,
  child3?: number,

  adult4?: number,
  child4?: number,

  adult5?: number,
  child5?: number,

  child_no_bed?: number,
  tour_id: string,
  departure_date: string,
  extension?: number,
  addon_values?: {[key: string] : number}
}

export enum TourType {
  PrivateTour,
  GroupTour
}

export function computeAnyTour( anyTour: v2_PrivateTour | v2_GroupTour, 
  bookingForm: AnyTourFormValue, tourType: TourType, now: number = null): ComputeResult {

  var product_type = null
  if (tourType === TourType.GroupTour) product_type = 'group_tour'
  else if (tourType === TourType.PrivateTour) product_type = 'private_tour'
  else return {
    error: 'Unknown tour type'
  }

  const totalNumberOfPax = tcParseInt(bookingForm.adult) + tcParseInt(bookingForm.child)
    + tcParseInt(bookingForm.adult2) + tcParseInt(bookingForm.child2)
    + tcParseInt(bookingForm.adult3) + tcParseInt(bookingForm.child3)
    + tcParseInt(bookingForm.adult4) + tcParseInt(bookingForm.child4)
    + tcParseInt(bookingForm.adult5) + tcParseInt(bookingForm.child5)
    + tcParseInt(bookingForm.child_no_bed)
  
  if (bookingForm.departure_date == null) return {error: "departure_date is required"}

  const [dyear, dmonth, dday] = bookingForm.departure_date.split('-')
  if (dday == null) return {error: "departure_date in wrong format"}

  //@ts-ignore
  const capacity = anyTour.capacities.find(x => x.year_month === dyear + dmonth)
  //@ts-ignore
  const booking = anyTour.bookings.find(x => x.year_month === dyear + dmonth)

  if (capacity == null) return {error: "Departure date not found"}

  const availability = capacity['day_' + parseInt(dday, 10)] //parseInt to remove leading 0
  var taken = booking?.['day_' + parseInt(dday, 10)] ?? 0
  const child_no_bed = tcParseInt(bookingForm.child_no_bed)
  if (availability == null) return {error: "No availability on " + bookingForm.departure_date}

  var quantity;

  const number_of_days = tcParseInt(anyTour.number_of_days)
  const extension = tcParseInt(bookingForm.extension)

  if (tourType === TourType.GroupTour) quantity = (totalNumberOfPax + child_no_bed).toString()
  if (tourType === TourType.PrivateTour) quantity = totalRooms?.toString() || 1
  if (quantity == null) return {error: 'Unknown tour type'}

  if (availability - taken < quantity) return {error: "No availability on " + bookingForm.departure_date}
  if (computeBookableTimeLeft(anyTour, bookingForm.departure_date, now) < 0) return {error: "Booking has closed for " + bookingForm.departure_date}

  if (extension > tcParseInt(anyTour.max_extension)) return {error: "Exceeded allowed extension"}

  const from_date = newDateLocal(bookingForm.departure_date)
  const to_date = newDateLocal(bookingForm.departure_date)
  const totalDays = number_of_days + extension
  to_date.setDate(from_date.getDate() + totalDays)

  const runningDate = newDateLocal(bookingForm.departure_date);

  const travelDates = range(totalDays).map(x => {
    const mydate = newDateLocal(runningDate.getTime())
    mydate.setDate(runningDate.getDate() + x)
    return dateToIsoDate(mydate)
  })

  const hasChildDiscount = anyTour.price_child_age > 0 && anyTour.price_child_discount != null && parseFloat(anyTour.price_child_discount) > 0

  var total = Big('0')
  var totalRooms = 0
  var items = []
  const adultChildToItem = (padult, pchild) => {
    totalRooms++
    
    const sub_header = "Room " + totalRooms
    padult = tcParseInt(padult)
    pchild = tcParseInt(pchild)

    // variables used to calculate addon and extension pricing
    // var quantity
    var numberOfPax = padult + pchild

    if (!hasChildDiscount) {
      if (anyTour.max_pax === 1) { //no child discount and flat rate
        var price = anyTour['price_1']
        if (tcParseInt(price) < 1) return {error: "Price is invalid"}

        items.push({
          sub_header,
          price_rule_id: '',
          name : "Guest" + s(numberOfPax),
          price,
          cost: "0",
          price_type: 'BASE',
          deposit: bigZeroIfInvalid(anyTour.deposit_per_pax).toFixed(2),
          quantity: numberOfPax
        })
        total = total.add(lastItemTotal(items))
        
      } else { //no child discount and not flat rate
        var price = anyTour['price_' + numberOfPax]
        if (tcParseInt(price) < 1) return {error: "Price is invalid"}

        items.push({
          sub_header,
          price_rule_id: '',
          name : numberOfPax + " guest"  + s(numberOfPax),
          price,
          cost: "0",
          price_type: 'BASE',
          deposit: bigZeroIfInvalid(anyTour.deposit_per_pax).times(numberOfPax).toFixed(2),
          quantity: "1"
        })
        total = total.add(lastItemTotal(items))

      }
      
    }
    else {
      if (anyTour.max_pax === 1) { //if hasChildDiscount && flat rate
        var price = anyTour['price_1']
        if (tcParseInt(price) < 1) return {error: "Price is invalid"}
        items.push({
          sub_header,
          price_rule_id: '',
          name: "Adult" + s(padult),
          price,
          cost: "0",
          price_type: 'BASE',
          deposit: Big(anyTour.deposit_per_pax).toFixed(2),
          quantity: padult
        })
        total = total.add(lastItemTotal(items))

        if (pchild > 0) {
          var effectiveChildDiscount = anyTour.price_child_discount
          var specificChildDiscount = anyTour['price_' + numberOfPax  + '_child_discount']
          if (specificChildDiscount != null && parseInt(specificChildDiscount) != 0) {
            effectiveChildDiscount = specificChildDiscount
          }

          const childPrice = Big(price).minus(bigZeroIfInvalid(effectiveChildDiscount)).toFixed()
          items.push({
            sub_header,
            price_rule_id: '',
            name: "Child" + ren(pchild),
            price: childPrice,
            cost: "0",
            price_type: 'BASE',
            deposit: Big(anyTour.deposit_per_pax).toFixed(2),
            quantity: pchild
          })
          total = total.add(lastItemTotal(items))
        }


      } else { //if hasChildDiscount && not flat rate
        var price = Big(anyTour['price_' + numberOfPax]).toFixed(2)
        if (tcParseInt(price) < 1) return {error: "Price is invalid"}

        if (pchild > 0) {
          var effectiveChildDiscount = anyTour.price_child_discount
          var specificChildDiscount = anyTour['price_' + numberOfPax  + '_child_discount']
          if (specificChildDiscount != null && parseInt(specificChildDiscount) != 0) {
            effectiveChildDiscount = specificChildDiscount
          }

          items.push({
            sub_header,
            price_rule_id: '',
            name: numberOfPax + " guest" + s(numberOfPax),
            price,
            cost: "0",
            price_type: 'BASE',
            deposit: Big(anyTour.deposit_per_pax).times(numberOfPax).toFixed(2),
            quantity: "1"
          })
          total = total.add(lastItemTotal(items))

          items.push({
            sub_header,
            price_rule_id: '',
            name: "Consisting of " + padult + " adult" + s(padult) + " and " + pchild + " child" + ren(pchild),
            price: "-" + effectiveChildDiscount,
            cost: "0",
            price_type: 'CHILD DISCOUNT',
            deposit: "0",
            quantity: pchild
          })
          total = total.add(lastItemTotal(items))
        } else {
          items.push({
            sub_header,
            price_rule_id: '',
            name: numberOfPax + " adult" + s(numberOfPax),
            price,
            cost: "0",
            price_type: 'BASE',
            deposit: Big(anyTour.deposit_per_pax).times(numberOfPax).toFixed(2),
            quantity: "1"
          })
          total = total.add(lastItemTotal(items))
        }
      }
    }

    const extension = tcParseInt(bookingForm.extension)
    if (extension > 0) {
      if (anyTour.max_pax === 1) { //if flat rate
        const price_ext = anyTour['price_ext_1']
        if (tcParseInt(price_ext) < 1) return {error: "Extension price is invalid"}
        items.push({
          sub_header,
          price_rule_id: '',
          name : "Extension per day per pax",
          price: price_ext,
          cost: "0",
          price_type: 'EXTENSION',
          deposit: "0",
          quantity: String(extension * numberOfPax)
        })
        total = total.add(lastItemTotal(items))
      } else { // if not flat rate
        const price_ext = anyTour['price_ext_' + numberOfPax]
        if (tcParseInt(price_ext) < 1) return {error: "Extension price is invalid"}
        items.push({
          sub_header,
          price_rule_id: '',
          name : "Extension per day for " + numberOfPax + " pax",
          price: price_ext,
          cost: "0",
          price_type: 'EXTENSION',
          deposit: "0",
          quantity: String(extension)
        })
        total = total.add(lastItemTotal(items))
      }
    }

    for (const departureAdjustment of anyTour.adjustment_departure_dates ?? []) {
      if (isAdjustmentEffective(departureAdjustment, bookingForm.departure_date)) {
        if (anyTour.max_pax === 1) { //if flat rate
          const adjustment = departureAdjustment['adjustment_1'];
          if (tcParseInt(adjustment) < 1) return {error: "Adjustment price is invalid"}
          items.push({
            sub_header,
            price_rule_id: '',
            name : departureAdjustment.name,
            price: adjustment,
            cost: "0",
            price_type: 'DEPARTURE ADJUSTMENT',
            deposit: "0",
            quantity: String(numberOfPax)
          })
          total = total.add(lastItemTotal(items))
        } else {//if not flat rate
          const adjustment = departureAdjustment['adjustment_' + numberOfPax];
          if (tcParseInt(adjustment) < 1) return {error: "Adjustment price is invalid"}
          items.push({
            sub_header,
            price_rule_id: '',
            name : departureAdjustment.name,
            price: adjustment,
            cost: "0",
            price_type: 'DEPARTURE ADJUSTMENT',
            deposit: "0",
            quantity: "1"
          })
          total = total.add(lastItemTotal(items))
        }
      }
    }

    if ('adjustment_travel_dates' in anyTour) {
      for (const travelAdjustment of anyTour.adjustment_travel_dates) {
        var effective = []
        for (const travelDate of travelDates) {
          if (isAdjustmentEffective(travelAdjustment, travelDate)) effective.push(travelDate)
        }

        if (anyTour.max_pax === 1) { //if flat rate
          const adjustment = travelAdjustment['adjustment_1'];
          if (tcParseInt(adjustment) < 1) return {error: "Adjustment price is invalid"}

    
          if (effective.length > 0) {
            items.push({
              sub_header,
              price_rule_id: '',
              name : travelAdjustment.name + " (" + effective.join(", ") + ")",
              price: adjustment,
              cost: "0",
              price_type: 'TRAVEL ADJUSTMENT',
              deposit: "0",
              quantity: String(effective.length * numberOfPax)
            })
            total = total.add(lastItemTotal(items))
          }
        } else { //if not flat rate
          const adjustment = travelAdjustment['adjustment_' + numberOfPax];
          if (tcParseInt(adjustment) < 1) return {error: "Adjustment price is invalid"}
    
          if (effective.length > 0) {
            items.push({
              sub_header,
              price_rule_id: '',
              name : travelAdjustment.name + " (" + effective.join(", ") + ")",
              price: adjustment,
              cost: "0",
              price_type: 'TRAVEL ADJUSTMENT',
              deposit: "0",
              quantity: String(effective.length)
            })
            total = total.add(lastItemTotal(items))
          }
        }
      }
    }
  }
  
  if (tcParseInt(bookingForm.adult) + tcParseInt(bookingForm.child) > 0) adultChildToItem(bookingForm.adult, bookingForm.child)
  if (tcParseInt(bookingForm.adult2) + tcParseInt(bookingForm.child2) > 0) adultChildToItem(bookingForm.adult2, bookingForm.child2)
  if (tcParseInt(bookingForm.adult3) + tcParseInt(bookingForm.child3) > 0) adultChildToItem(bookingForm.adult3, bookingForm.child3)
  if (tcParseInt(bookingForm.adult4) + tcParseInt(bookingForm.child4) > 0) adultChildToItem(bookingForm.adult4, bookingForm.child4)
  if (tcParseInt(bookingForm.adult5) + tcParseInt(bookingForm.child5) > 0) adultChildToItem(bookingForm.adult5, bookingForm.child5)

  if (totalRooms === 0) return {error: "Nothing to add"}
  if (totalRooms === 1) {
    items = items.map((item) => {item.sub_header = ""; return item;})
  }

  if (child_no_bed > 0) {
    const price = anyTour.price_child_no_bed_price
    if (price != null) {
      total = total.add(Big(price).times(child_no_bed))
      items.push({
        sub_header: '',
        price_rule_id: '',
        name : "Child no bed",
        price,
        cost: "0",
        price_type: 'CHILD NO BED',
        deposit: "0",
        quantity: String(child_no_bed)
      })
    }
  }

  for (var addon_code in bookingForm.addon_values) {
    const val = bookingForm.addon_values[addon_code]
    if (val > 0) {
      const addon = anyTour.addons.find((addon) => addon.addon_code === addon_code)

      if (addon != null) {
        total = total.add(Big(addon.price).times(val))
        items.push({
          sub_header: '',
          price_rule_id: '',
          name : addon.name,
          price: addon.price,
          cost: "0",
          price_type: 'ADDON',
          deposit: "0",
          quantity: String(val)
        })
      }
    }
  }

  const adult = tcParseInt(bookingForm.adult) + tcParseInt(bookingForm.adult2) + tcParseInt(bookingForm.adult3) + tcParseInt(bookingForm.adult4) + tcParseInt(bookingForm.adult5)
  const child = tcParseInt(bookingForm.child) + tcParseInt(bookingForm.child2) + tcParseInt(bookingForm.child3) + tcParseInt(bookingForm.child4) + tcParseInt(bookingForm.child5)

  const product = {
    product_name: anyTour.name,
    product_type,
    product_status: 'Unconfirmed',
    product_source: '',
    product_id: '',
    product_code: anyTour.product_code,
    product_detail: null,
    from_date: dateToIsoDate(from_date),
    to_date: dateToIsoDate(to_date),
    quantity,
    deposit_fixed: anyTour.deposit_fixed,
    deposit_percentage: anyTour.deposit_percentage,
    items,
    adult: adult,
    child: child + tcParseInt(bookingForm.child_no_bed)
  }

  return {
    product,
    total: total.toFixed(2)
  }
}

function segmentToString(segment: FlightDetail['od1']['segments'][0]): string {

  const flights_info = getFlightsSummary(segment)

  return segment.marketing_carrier.code
    + segment.flight_number + ' '
    + flights_info.origin.code + ' (' + flights_info.origin_datetime.substr(0, 16) + ')'
    + ' ==> '
    + flights_info.destination.code + ' (' + flights_info.destination_datetime.substr(0, 16) + ')'

}

type FlightsSummary = {
  origin: {code: string},
  origin_datetime: string,
  destination: {code: string},
  destination_datetime: string,
}

export function getFlightsSummary(segment: FlightDetail['od1']['segments'][0]): FlightsSummary {
  if (segment.flights_info != null) return segment.flights_info
  return {
    origin: segment.flights[0].departure_airport,
    origin_datetime: segment.flights[0].departure_datetime,
    destination: segment.flights[segment.flights.length - 1].arrival_airport,
    destination_datetime: segment.flights[segment.flights.length - 1].arrival_datetime,
  }
}

function computeItinerarySummary(arr: [FlightsSummary,FlightsSummary][]): {
  product_name: string
  from_date: string
  to_date: string
} {
  var od = arr[0]

  var product_name = od[0].origin.code + ' -> ' + od[1].destination.code
  var from_date = od[0].origin_datetime
  var to_date = od[1].destination_datetime

  for (var x = 1; x < arr.length; x++) {
    var prevOd = arr[x-1]
    var od = arr[x]
    to_date = od[1].destination_datetime

    if (prevOd[1].destination.code === od[0].origin.code) {
      product_name += ' -> ' + od[1].destination.code
    } else {
      product_name += ' / ' + od[1].origin.code + ' -> ' + od[1].destination.code
    }
  }

  return {product_name, from_date, to_date}
}

/*
// UI should use computeOd, code below is untested and not required

export function ignoreTimezone( datetime ) {
  return datetime.replace(/[+-][0-9][0-9]:[0-9][0-9]$/,'')
}

export function getFlightsSummary(segment: FlightDetail['od1']['segments'][0]): FlightDetail['od1']['segments'][0]['flights_info'] {
  if (segment.flights_info != null) return segment.flights_info
  var ans = {
    origin: segment.flights[0].departure_airport,
    origin_terminal: segment.flights[0].departure_terminal,
    origin_datetime: ignoreTimezone(segment.flights[0].departure_datetime),
    destination: segment.flights[segment.flights.length - 1].arrival_airport,
    destination_terminal: segment.flights[segment.flights.length - 1].arrival_terminal,
    destination_datetime: ignoreTimezone(segment.flights[segment.flights.length - 1].arrival_datetime),
    total_minutes: moment.duration(moment(segment.flights[segment.flights.length - 1].arrival_datetime).diff(segment.flights[0].departure_datetime)).asMinutes(),
    equipment: segment.flights[0].equipment,
    stops: []
  }

  for (var x = 1; x < segment.flights.length; x++) {
    const prevflight = segment.flights[x-1]
    const nextflight = segment.flights[x]
    ans.stops.push({
      arrival_datetime: prevflight.arrival_datetime,
      departure_datetime: nextflight.departure_datetime,
      airport: prevflight.arrival_airport,
      layover_minutes: moment.duration(moment(nextflight.departure_datetime).diff(prevflight.arrival_datetime)).asMinutes(),
    })
  }

  return ans
}
*/

export function computeFlight(flight: FlightDetail, form: FlightFormValue, offerPriceRes: NDC_OfferPriceRes1 = null) {
  const selectedFare = flight.pricings.find((x) => x.id === form.pricing_id)
  if (selectedFare == null) return {error: 'fare ' + form.pricing_id + 'not found'}

  const product: any = {}
  product.product_type = 'flight';
  product.product_source = selectedFare.source;
  product.product_status = 'Unconfirmed';
  
  var total = Big(0)
  if (offerPriceRes != null) {
    let paxSegmentList = offerPriceRes.DataLists.PaxSegmentList.PaxSegment;
    let paxList = offerPriceRes.DataLists.PaxList.Pax;

    let mapPaxSeg2 = paxSegmentList.reduce((acc, x) => {
      let key = x.PaxSegmentID._text;
      acc[key] = x
      return acc;
    }, {});

    const flightSummaries: [FlightsSummary,FlightsSummary][] = offerPriceRes.DataLists.PaxJourneyList.PaxJourney.reduce((acc, PaxJourney) => {
      const firstSeg = mapPaxSeg2[PaxJourney.PaxSegmentRefID[0]._text]
      const lastSeg = mapPaxSeg2[PaxJourney.PaxSegmentRefID[PaxJourney.PaxSegmentRefID.length - 1]._text]
      acc.push([{
        origin: {code: firstSeg.Dep.IATA_LocationCode._text},
        origin_datetime: firstSeg.Dep.AircraftScheduledDateTime._text,
        destination: {code: firstSeg.Arrival.IATA_LocationCode._text},
        destination_datetime: firstSeg.Arrival.AircraftScheduledDateTime._text,
      },{
        origin: {code: lastSeg.Dep.IATA_LocationCode._text},
        origin_datetime: lastSeg.Dep.AircraftScheduledDateTime._text,
        destination: {code: lastSeg.Arrival.IATA_LocationCode._text},
        destination_datetime: lastSeg.Arrival.AircraftScheduledDateTime._text,
      }])
      return acc
    }, [])

    const itinerarySummary = computeItinerarySummary(flightSummaries)

    product.product_detail = {flight, offerPriceRes}
    product.items = []
    
    product.adult = paxList.reduce((acc, x: any) => {
      acc = acc + (x.PTC._text === 'ADT' ? 1 : 0) 
      return acc;
    }, 0);
    product.child = paxList.reduce((acc, x: any) => {
      acc = acc + (x.PTC._text === 'CNN' ? 1 : 0) 
      return acc;
    }, 0);
    product.infant = paxList.reduce((acc, x: any) => {
      acc = acc + (x.PTC._text === 'INF' ? 1 : 0) 
      return acc;
    }, 0);
    product.quantity = product.adult + product.child
    product.product_name = itinerarySummary.product_name
    product.from_date = itinerarySummary.from_date
    product.to_date = itinerarySummary.to_date
    product.deposit_percentage = '100'

    let mapPaxSeg = paxSegmentList.reduce((acc, x) => {
      let flightNumber = x.MarketingCarrierInfo.CarrierDesigCode._text + x.MarketingCarrierInfo.MarketingCarrierFlightNumberText._text;
      let dep = x.Dep.IATA_LocationCode._text;
      let depTime = x.Dep.AircraftScheduledDateTime._text;
      let arr = x.Arrival.IATA_LocationCode._text;
      let arrTime = x.Arrival.AircraftScheduledDateTime._text;
      let value = flightNumber + ' ' + dep +  '(' + depTime + ')' + ' ==> ' + arr +  '(' + arrTime + ')';
      let key = x.PaxSegmentID._text;
      if (acc[key] == null) {
        acc[key] = []
      }
      acc[key] = value;
      return acc;
    }, {});

    // function scope
    let mapPaxPtc = paxList.reduce((acc, x) => {
      let value = x.PTC._text;
      let key = x.PaxID._text;
      if (acc[key] == null) {
        acc[key] = []
      }
      acc[key] = value ; 
      return acc;
    }, {});


    var servicesIndexed = [];
    // console.log(offerPriceRes.PricedOffer.Offer.OfferItem);
    for (var x of offerPriceRes.PricedOffer.Offer.OfferItem) {
      if (x.FareDetail != null) {
        for (var eachFareDetail of x.FareDetail) {
          let price = eachFareDetail.Price.TotalAmount.DetailCurrencyPrice.Total._text;
          let cost = price;
          let passengerRefs = eachFareDetail.PassengerRefs[0]._text;
          let quantity = passengerRefs.split(' ').length;
          let fareComponents = eachFareDetail.FareComponent;
          let segmentRefs = null;
          let subheadArray = [];
          for (var fareComponent of fareComponents) {
            segmentRefs = fareComponent.SegmentRefs;
            for (var segmentRef of segmentRefs) {
              subheadArray.push(mapPaxSeg[segmentRef._text]);
            }
          }
          let subhead = subheadArray.join("\n")
          const subTotal = Big(price).times(quantity);
          total = total.add(subTotal);
          product.items.push({
            header : 'Item',
            sub_header : subhead,
            name : mapPaxPtc[passengerRefs.split(' ')[0]],
            price : price,
            cost : cost,
            deposit : '0',
            quantity : quantity
          })
        }
      } 
      if (x.Service != null && x.FareDetail == null) {
        
        let serviceDefinitionList = offerPriceRes.DataLists.ServiceDefinitionList.ServiceDefinition;
        let mapServiceName = serviceDefinitionList.reduce((acc, serviceDefinition) => {
          let key = serviceDefinition.ServiceDefinitionID._text;
          let value = {
            name : serviceDefinition.Name._text,
            total_amount : x.Price.TotalAmount._text
          };
          if (acc[key] == null) {
            acc[key] = [];
          }
          acc[key] = value;
          return acc;
        }, {});
        servicesIndexed = x.Service.reduce((acc, y) => {
          let key = x.OfferItemID._text;
          let value = mapServiceName[y.ServiceAssociations.ServiceDefinitionRef.ServiceDefinitionRefID._text];
          if (acc[key] == null) {
            acc[key] = [];
          }
          acc[key] = value;
          return acc;
        }, servicesIndexed);
      }
    }

    if (form.services != null) {
      for (var serviceForm of form.services) {

        const service = servicesIndexed[serviceForm.offer_item_id]
        if (service == null) return {error: "Cannot find offer_item_id " + serviceForm.offer_item_id}
        const name = service.name;
        const unit_price = Big(service.total_amount as string).div(serviceForm.quantity);
        const subTotal = Big(unit_price).times(serviceForm.quantity);
        total = total.add(subTotal);
        product.items.push({
          sub_header : 'Services',
          name : name,
          price : unit_price.toFixed(2),
          cost : unit_price.toFixed(2),
          deposit : '0',
          quantity : serviceForm.quantity
        })
      }

    }
 
    if (form.seats != null) {
      const seatsPricingIndexed = {}
      for (var fare of selectedFare.fares) {
        for (var segment of fare.segments) {
          if (segment.seats_pricings != null) {
            for (var seatsPricing of segment.seats_pricings) {
              seatsPricingIndexed[seatsPricing.offer_item_id] = {
                segment, seatsPricing
              }
            }
          }
        }
      }
  
      for (var formSeat of form.seats) {
        const pricing = seatsPricingIndexed[formSeat.offer_item_id].seatsPricing
        if (pricing == null) return {error: 'Seat ' + formSeat.row_number + formSeat.column_id + ' pricing not found'}
        product.items.push({
          header : 'Item',
          sub_header : 'Seat selection',
          name : formSeat.traveler_ref + ' - ' + formSeat.row_number + formSeat.column_id,
          price : pricing.unit_price,
          cost : pricing.unit_price,
          deposit : '0',
          quantity : 1
      })
      total = total.add(pricing.unit_price)
      }
    }
    // console.log(product.items);
    return {
      total: total.toFixed(2),
      product}
  }

  else {
    product.adult = selectedFare.ptc_breakdown_map['adt'].quantity
    product.child = selectedFare.ptc_breakdown_map['cnn'] != null ? selectedFare.ptc_breakdown_map['cnn'].quantity : 0
    product.infant = selectedFare.ptc_breakdown_map['inf'] != null ? selectedFare.ptc_breakdown_map['inf'].quantity : 0
    product.quantity = product.adult + product.child
    product.product_detail = flight

    const flightSummaries: [FlightsSummary,FlightsSummary][] = [
      [getFlightsSummary(flight.od1.segments[0]), getFlightsSummary(flight.od1.segments[flight.od1.segments.length - 1])]
    ]

    if (flight.od2) {
      flightSummaries.push([getFlightsSummary(flight.od2.segments[0]), getFlightsSummary(flight.od2.segments[flight.od2.segments.length - 1])])
    }

    if (flight.od3) {
      flightSummaries.push([getFlightsSummary(flight.od3.segments[0]), getFlightsSummary(flight.od3.segments[flight.od3.segments.length - 1])])
    }

    if (flight.od4) {
      flightSummaries.push([getFlightsSummary(flight.od4.segments[0]), getFlightsSummary(flight.od4.segments[flight.od4.segments.length - 1])])
    }

    const itinerarySummary = computeItinerarySummary(flightSummaries)

    product.product_name = itinerarySummary.product_name
    product.from_date = itinerarySummary.from_date
    product.to_date = itinerarySummary.to_date
    product.deposit_percentage = '100'

    let segmentsFlattened = flight.od1.segments

    if (flight.od2 != null) {
      segmentsFlattened = segmentsFlattened.concat(flight.od2.segments)
    }

    const subhead = segmentsFlattened.map((x: any) => segmentToString(x)).join('\n')

    product.items = []

    for (const ptc in selectedFare.ptc_breakdown_map) {
      if (selectedFare.ptc_breakdown_map.hasOwnProperty(ptc)) {
        const breakdown = selectedFare.ptc_breakdown_map[ptc]
        // const tax = formatCurrency(breakdown.tax)
        product.items.push({
            header : 'Item',
            sub_header : subhead,
            name : ptc.toUpperCase(),
            price : breakdown.price,
            cost : breakdown.cost,
            deposit : '0',
            quantity : breakdown.quantity
        })
        const subTotal = Big(breakdown.price).times(breakdown.quantity)
        total = total.add(subTotal)
      }
    }

    // NDC returns negative fares for some reason. However, price is 0
    // Comment out the check below to allow this scenario
    if (total.eq(selectedFare.price) === false) {
      return {error: `Error in fare. Breakdown: ${total.toFixed(2)}. Total: ${selectedFare.price}`}
    }

    if (form.services != null) {
      const servicesIndexed = {}
      for (var fare of selectedFare.fares) {
        for (var segment of fare.segments) {
          for (var service of segment.services) {
            servicesIndexed[service.offer_item_id] = service
          }
        }
      }

      for (var serviceForm of form.services) {
        const service = servicesIndexed[serviceForm.offer_item_id]
        if (service == null) return {error: "Cannot find offer_item_id " + serviceForm.offer_item_id}
        product.items.push({
          sub_header : 'Services',
          name : service.name,
          price : service.unit_price,
          cost : service.unit_price,
          deposit : '0',
          quantity : serviceForm.quantity
        })
      }
    }
  }

  if (form.seats != null) {
    const seatsPricingIndexed = {}
    for (var fare of selectedFare.fares) {
      for (var segment of fare.segments) {
        if (segment.seats_pricings != null) {
          for (var seatsPricing of segment.seats_pricings) {
            seatsPricingIndexed[seatsPricing.offer_item_id] = {
              segment, seatsPricing
            }
          }
        }
      }
    }

    for (var formSeat of form.seats) {
      const pricing = seatsPricingIndexed[formSeat.offer_item_id].seatsPricing
      if (pricing == null) return {error: 'Seat ' + formSeat.row_number + formSeat.column_id + ' pricing not found'}
      product.items.push({
        header : 'Item',
        sub_header : 'Seat selection',
        name : formSeat.traveler_ref + ' - ' + formSeat.row_number + formSeat.column_id,
        price : pricing.unit_price,
        cost : pricing.unit_price,
        deposit : '0',
        quantity : 1
    })
    total = total.add(pricing.unit_price)
    }
  }

  return {
    total: total.toFixed(2),
    product
  }

}

// sqndc compute existing flight and flight change of services
export function computeFlightChangeServices(flight: FlightDetail, form: FlightChangeServiceFormValue, offerPriceRes: NDC_OfferPriceRes1 = null) {
  
  const selectedFare = flight.pricings.find((x) => x.id.includes(form.product_source_ref));
  if (selectedFare == null) return {error: 'fare from product source reference: ' + form.product_source_ref + 'not found'}

  var product: any = {
    items: [],
    product_name: 'Change '+ form.product_source_ref,
    product_type: 'flight/change_services',
    product_source_ref: form.product_source_ref,
    product_source: form.product_source,
    product_status: 'Unconfirmed',
    product_detail: {flight},
    deposit_percentage: '100',
    quantity: 1
  }
  var total = Big(0)
  if (offerPriceRes != null) {
    product.product_detail = {flight, offerPriceRes}
    product.items = []
    let eachSegment = [];
    let paxSegmentList = offerPriceRes.DataLists.PaxSegmentList.PaxSegment;
    let paxList = offerPriceRes.DataLists.PaxList.Pax;
    product.adult = paxList.reduce((acc, x: any) => {
      acc = acc + (x.PTC._text === 'ADT' ? 1 : 0) 
      return acc;
    }, 0);
    product.child = paxList.reduce((acc, x: any) => {
      acc = acc + (x.PTC._text === 'CNN' ? 1 : 0) 
      return acc;
    }, 0);
    product.infant = paxList.reduce((acc, x: any) => {
      acc = acc + (x.PTC._text === 'INF' ? 1 : 0) 
      return acc;
    }, 0);
    product.quantity = product.adult + product.child
    product.product_name = paxSegmentList[0].Dep.IATA_LocationCode._text + ' -> ' + paxSegmentList[0].Arrival.IATA_LocationCode._text;
    product.deposit_percentage = '100'
    let mapPaxSeg = paxSegmentList.reduce((acc, x) => {
      let flightNumber = x.MarketingCarrierInfo.MarketingCarrierFlightNumberText._text;
      let dep = x.Dep.IATA_LocationCode._text;
      let depTime = x.Dep.AircraftScheduledDateTime._text;
      let arr = x.Arrival.IATA_LocationCode._text;
      let arrTime = x.Arrival.AircraftScheduledDateTime._text;
      let value = `OD` + flightNumber + ' ' + dep +  '(' + depTime + ')' + '==> ' + arr +  '(' + arrTime + ')';
      let key = x.PaxSegmentID._text;
      if (acc[key] == null) {
        acc[key] = []
      }
      acc[key] = value;
      return acc;
    }, {});

    // function scope
    let mapPaxPtc = paxList.reduce((acc, x) => {
      let value = x.PTC._text;
      let key = x.PaxID._text;
      if (acc[key] == null) {
        acc[key] = []
      }
      acc[key] = value ; 
      return acc;
    }, {});


    var servicesIndexed = [];
    // console.log(offerPriceRes.PricedOffer.Offer.OfferItem);
    for (var x of offerPriceRes.PricedOffer.Offer.OfferItem) {
      if (x.FareDetail != null) {
        for (var eachFareDetail of x.FareDetail) {
          let price = eachFareDetail.Price.TotalAmount.DetailCurrencyPrice.Total._text;
          let cost = price;
          let passengerRefs = eachFareDetail.PassengerRefs[0]._text;
          let quantity = passengerRefs.split(' ').length;
          let fareComponents = eachFareDetail.FareComponent;
          let segmentRefs = null;
          let subhead = '';
          for (var fareComponent of fareComponents) {
            segmentRefs = fareComponent.SegmentRefs;
            for (var segmentRef of segmentRefs) {
              subhead += mapPaxSeg[segmentRef._text] + ' ';
            }
          }
          const subTotal = Big(price).times(quantity);
          total = total.add(subTotal);
          product.items.push({
            header : 'Item',
            sub_header : subhead,
            name : mapPaxPtc[passengerRefs.split(' ')[0]],
            price : price,
            cost : cost,
            deposit : '0',
            quantity : quantity
          })
        }
      } 
      if (x.Service != null && x.FareDetail == null) {
        
        let serviceDefinitionList = offerPriceRes.DataLists.ServiceDefinitionList.ServiceDefinition;
        let mapServiceName = serviceDefinitionList.reduce((acc, serviceDefinition) => {
          let key = serviceDefinition.ServiceDefinitionID._text;
          let value = {
            name : serviceDefinition.Name._text,
            total_amount : x.Price.TotalAmount._text
          };
          if (acc[key] == null) {
            acc[key] = [];
          }
          acc[key] = value;
          return acc;
        }, {});
        servicesIndexed = x.Service.reduce((acc, y) => {
          let key = x.OfferItemID._text;
          let value = mapServiceName[y.ServiceAssociations.ServiceDefinitionRef.ServiceDefinitionRefID._text];
          if (acc[key] == null) {
            acc[key] = [];
          }
          acc[key] = value;
          return acc;
        }, servicesIndexed);
      }
    }

    if (form.services != null) {
      for (var serviceForm of form.services) {

        const service = servicesIndexed[serviceForm.offer_item_id]
        if (service == null) return {error: "Cannot find offer_item_id " + serviceForm.offer_item_id}
        const name = service.name;
        const unit_price = Big(service.total_amount as string).div(serviceForm.quantity);
        const subTotal = Big(unit_price).times(serviceForm.quantity);
        total = total.add(subTotal);
        product.items.push({
          sub_header : 'Services',
          name : name,
          price : unit_price.toFixed(2),
          cost : unit_price.toFixed(2),
          deposit : '0',
          quantity : serviceForm.quantity
        })
      }

    }
  } else {
    const selectedFare = flight.pricings[0]

    if (form.services != null) {
      const servicesIndexed = {}
      for (var fare of flight.pricings[0].fares) {
        for (var segment of fare.segments) {
          for (var service of segment.services) {
            servicesIndexed[service.offer_item_id] = service
          }
        }
      }

      for (var serviceForm of form.services) {
        const service = servicesIndexed[serviceForm.offer_item_id]
        if (service == null) return {error: "Cannot find offer_item_id " + serviceForm.offer_item_id}
        const subTotal = Big(service.unit_price).times(serviceForm.quantity);
        total = total.add(subTotal)
        product.items.push({
          sub_header : 'Services',
          name : service.name,
          price : service.unit_price,
          cost : service.unit_price,
          deposit : '0',
          quantity : serviceForm.quantity
        })
      }

      if (form.seats != null) {
        const seatsPricingIndexed = {}
        for (var fare of selectedFare.fares) {
          for (var segment of fare.segments) {
            if (segment.seats_pricings != null) {
              for (var seatsPricing of segment.seats_pricings) {
                seatsPricingIndexed[seatsPricing.offer_item_id] = {
                  segment, seatsPricing
                }
              }
            }
          }
        }

        for (var formSeat of form.seats) {
          const pricing = seatsPricingIndexed[formSeat.offer_item_id].seatsPricing
          if (pricing == null) return {error: 'Seat ' + formSeat.row_number + formSeat.column_id + ' pricing not found'}
          product.items.push({
            header : 'Item',
            sub_header : 'Seat selection',
            name : formSeat.traveler_ref + ' - ' + formSeat.row_number + formSeat.column_id,
            price : pricing.unit_price,
            cost : pricing.unit_price,
            deposit : '0',
            quantity : 1
        })
        total = total.add(pricing.unit_price)
        }
      }
    }
  }
  return {
    total: total.toFixed(2),
    product
  }
  
}

// sqndc compute flight change of new flight with new seats
export function computeFlightChange(changeFlight: FlightDetail, form: FlightChangeFormValue) {

  const selectedFare = changeFlight.pricings.find((x) => x.id === form.pricing_id)
  if (selectedFare == null) return {error: 'fare ' + form.pricing_id + 'not found'}
  const product: any = {}
  product.product_type = 'flight/change';
  product.product_source = selectedFare.source;
  product.product_source_ref= form.product_source_ref;
  product.product_status = 'Unconfirmed';
  var total = Big(0);
  product.product_detail = {changeFlight}
  product.items = []
  product.adult = selectedFare.ptc_breakdown_map['adt'].quantity
  product.child = selectedFare.ptc_breakdown_map['cnn'] != null ? selectedFare.ptc_breakdown_map['cnn'].quantity : 0
  product.infant = selectedFare.ptc_breakdown_map['inf'] != null ? selectedFare.ptc_breakdown_map['inf'].quantity : 0
  product.quantity = product.adult + product.child
  const od1FirstSegment = getFlightsSummary(changeFlight.od1.segments[0])
  const od1LastSegment = getFlightsSummary(changeFlight.od1.segments[changeFlight.od1.segments.length - 1])
  product.product_name = od1FirstSegment.origin.code + ' -> ' + od1LastSegment.destination.code
  product.deposit_percentage = '100'

  if (changeFlight.od2) {
    const od2FirstSegment = getFlightsSummary(changeFlight.od2.segments[0])
    const od2LastSegment = getFlightsSummary(changeFlight.od2.segments[changeFlight.od2.segments.length - 1])

    if (od1LastSegment.destination.code === od2FirstSegment.origin.code) {
      product.product_name += ' -> ' + od2LastSegment.destination.code
    } else {
      product.product_name += ' / ' + od2FirstSegment.origin.code + ' -> ' + od2LastSegment.destination.code
    }

    product.from_date = od1FirstSegment.origin_datetime.substr(0, 16)
    product.to_date = od2LastSegment.destination_datetime.substr(0, 16)
  } else {
    product.from_date = od1FirstSegment.origin_datetime.substr(0, 16)
    product.to_date = od1LastSegment.destination_datetime.substr(0, 16)
  }

  let segmentsFlattened = changeFlight.od1.segments

  if (changeFlight.od2 != null) {
    segmentsFlattened = segmentsFlattened.concat(changeFlight.od2.segments)
  }
  const subhead = segmentsFlattened.map((x: any) => segmentToString(x)).join('\n')

  product.items = []

  for (const ptc in selectedFare.ptc_breakdown_map) {
    if (selectedFare.ptc_breakdown_map.hasOwnProperty(ptc)) {
      const breakdown = selectedFare.ptc_breakdown_map[ptc]
      // const tax = formatCurrency(breakdown.tax)
      product.items.push({
          header : 'Item',
          sub_header : subhead,
          name : ptc.toUpperCase(),
          price : breakdown.price,
          cost : breakdown.cost,
          deposit : '0',
          quantity : breakdown.quantity
      })
      const subTotal = Big(breakdown.price).times(breakdown.quantity)
      total = total.add(subTotal)
    }
  }

  // if (total.eq(selectedFare.price) === false) {
  //   return {error: `Error in fare. Breakdown: ${total.toFixed(2)}. Total: ${selectedFare.price}`}
  // }

  if (form.seats != null) {
    const seatsPricingIndexed = {}
    for (var fare of selectedFare.fares) {
      for (var segment of fare.segments) {
        if (segment.seats_pricings != null) {
          for (var seatsPricing of segment.seats_pricings) {
            seatsPricingIndexed[seatsPricing.offer_item_id] = {
              segment, seatsPricing
            }
          }
        }
      }
    }

    for (var formSeat of form.seats) {
      const pricing = seatsPricingIndexed[formSeat.offer_item_id].seatsPricing
      if (pricing == null) return {error: 'Seat ' + formSeat.row_number + formSeat.column_id + ' pricing not found'}
      product.items.push({
        header : 'Item',
        sub_header : 'Seat selection',
        name : formSeat.traveler_ref + ' - ' + formSeat.row_number + formSeat.column_id,
        price : pricing.unit_price,
        cost : pricing.unit_price,
        deposit : '0',
        quantity : 1
    })
    total = total.add(pricing.unit_price)
    }
  }
  return {
    total: total.toFixed(2),
    product}
}

function dateToIsoDate(date: Date): string {
  const pad = (x: number) => x < 10 ? '0' + x : x
  return '' + date.getFullYear() + '-' + pad(date.getMonth() + 1) + '-' + pad(date.getDate())
}

function formatCurrency(x: string | number): string {
  const xNumber = typeof x === 'string' ? parseFloat(x) : x
  const neg = xNumber < 0 ? '-' : ''
  return neg + '$' + Big(x).abs().toFixed(2).toString()
}

// not null not empty (not undefined)
// TravelCloud treats null and empty values identically
function nnne(x: string): boolean {
  return x != null && x.length > 0
}

// for parsing optional inputs
// similar to parseInt except
// 1. doesn't have type error when input is string
// 2. empty string, null, and all NaN returns 0 instead of NaN (desired behavior for optional inputs)
export function tcParseInt(x: string | number): number {
  const result = parseInt("" + x, 10)
  if (isNaN(result)) return 0
  return result
}

export function bigZeroIfInvalid(x): Big {
  try {
    return Big(x)
  } catch(e) {
    return Big("0")
  }
}

function s(x): string {
  if (x > 1) return "s"
  return ""
}

function ren(x): string {
  if (x > 1) return "ren"
  return ""
}

function lastItemTotal(items: any[]): Big {
  const lastItem = items[items.length - 1]
  return Big(lastItem.price).times(lastItem.quantity)
}

// not null not zero (for checking boolean fields)
function nnnz(x: string | number): boolean {
  return (tcParseInt(x) !== 0)
}

export function filterPriceRulesWithCheckoutCode(checkoutCode: string, priceRules: PriceRules, bcryptCache: {[key: string]: boolean}, customer: Customer): PriceRules {
  if (checkoutCode == null) return []

  const priceRulesCombined
    = customer == null || customer.categories == null || customer.categories[0] == null || customer.categories[0].customer_in_any_price_rules == null
    ? priceRules
    : customer.categories.reduce((acc, category) => acc.concat(category.customer_in_any_price_rules), priceRules.slice(0))

  if (priceRulesCombined == null) return []

  return priceRulesCombined.filter((rule) => {
    if (nnne(rule.condition_checkout_code_hash)) {
      const cacheKey = rule.condition_checkout_code_hash + '/' + checkoutCode
      if (bcryptCache[cacheKey] == null) bcryptCache[cacheKey] = bcrypt.compareSync(checkoutCode, rule.condition_checkout_code_hash)

      return bcryptCache[cacheKey]
    }
    return false
  })
}
type AnyPriceRules = PriceRules | v2_PrivateTour["price_rules"]
type AnyPriceRule = PriceRules[0] | v2_PrivateTour["price_rules"][0]
type PriceRuleWithEval = PriceRule & {_fail_reason?: string[], _checkout_code_passed?: boolean, _adjustment?: string}
type MinOrder = {checkout_code: string, limit_code?: string, limit_discount?: string, products: (Product & {_price_rules: any, _price_rules_combined_eval: PriceRuleWithEval[]})[]}

// TODO: split function into
// 1. get valid price rules: priceRules[]
// 2. apply price rules: price -> price
export function computePriceRules(order: MinOrder, priceRules: AnyPriceRules, bcryptCache: {[key: string]: boolean}, customer: Customer, limits: {all?: {[key: string]: string}, customer?: {[key: string]: string}} = {all: {}, customer: {}}, now?: Date): MinOrder {
  if (now == null) now  = new Date()
  // remove previous computations if any
  order.products = order.products.map(product => {
    product.items = product.items.filter(item => !nnne(item.price_rule_id))
    return product
  })

  delete order['limit_code']
  delete order['limit_discount']

  if (priceRules == null) priceRules = []

  const orderTotal = order.products.reduce((acc, product) => {
    const itemsTotal = product.items.reduce((acc2, item) =>  acc2.plus(Big(item.price).times(item.quantity)), Big(0))
    return acc.plus(itemsTotal)}, Big(0)).toFixed(2)

  const priceRulesCombined
    = customer == null || customer.categories == null || customer.categories[0] == null || customer.categories[0].customer_in_any_price_rules == null
    ? priceRules
    : customer.categories.reduce((acc: any, category) => acc.concat(category.customer_in_any_price_rules), priceRules.slice(0))

  // clone limits to prevent affecting recalculation
  limits = JSON.parse(JSON.stringify(limits))
  /*
  var travelersCount

  if (order.travelers == null) {
    const maxAdult = order.products.reduce((acc, product) => (product as any).adult != null ? Math.max(parseInt((product as any).adult), acc) : acc, 0)
    const maxChild = order.products.reduce((acc, product) => (product as any).child != null ? Math.max(parseInt((product as any).child), acc) : acc, 0)
    const maxInfant = order.products.reduce((acc, product) => (product as any).infant != null ? Math.max(parseInt((product as any).infant), acc) : acc, 0)
    travelersCount = maxAdult + maxChild + maxInfant
  } else {
    travelersCount = order.travelers.length
  }
  */

  // compute price attributes for
  for (const product of order.products) {
    const priceRulesCombined2 = ((product as any)._price_rules ?? []).concat(priceRulesCombined)

    var priceRulesSorted: PriceRuleWithEval[] = JSON.parse(JSON.stringify(priceRulesCombined2)).sort((a,b) => parseInt(a.adjustment_sequence, 10) - parseInt(b.adjustment_sequence, 10))

    product._price_rules_combined_eval = priceRulesSorted
    
    /*
    priceRulesSorted = priceRulesSorted.map(val => {
      if (val._order_use_count_quantity == null) val._order_use_count_quantity = 0
      return val
    })
    */

    const mutexDict = {}
    const ruleIdDict = {}
    const total = product.items.reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))
    let running = Big(total) // Clone. Big doesn't seem to mutate it's object so this might not be required.
    let seqTotal = Big(0)
    let lastSeq

    const priceAttributes = Object.assign({
      'order/total': orderTotal,
      'product/total': product.items.reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0)),
      'product/quantity': product.quantity,
      //'travelers/count': travelersCount
    }, product.product_attributes)

    for (const rule of priceRulesSorted) {
      if (rule.id !== '0' && rule.id != null && ruleIdDict[rule.id] != null) continue
      // console.log('processing rule ' + rule.id + ' ' + rule.name)
      ruleIdDict[rule.id] = true

      rule._fail_reason = []

      if (lastSeq != rule.adjustment_sequence) {
        running = running.add(seqTotal)
        seqTotal = Big(0)
      }
      lastSeq = rule.adjustment_sequence

      if (nnne(rule.condition_checkout_code_hash)) {
        if (!nnne(order.checkout_code)) {
          rule._fail_reason.push('condition_checkout_code_hash')
          // console.log('rule ' + rule.id + ' condition_checkout_code_hash check failed')
          // continue
        } else {
          //legacy: server does not convert code to lowercase
          const cacheKey = rule.condition_checkout_code_hash + '/' + order.checkout_code
          if (bcryptCache[cacheKey] == null) bcryptCache[cacheKey] = bcrypt.compareSync(order.checkout_code, rule.condition_checkout_code_hash)

          //current: server converts code to lowercase before hashing
          const cacheKey2 = rule.condition_checkout_code_hash + '/' + order.checkout_code.toLowerCase()
          if (bcryptCache[cacheKey2] == null) bcryptCache[cacheKey2] = bcrypt.compareSync(order.checkout_code.toLowerCase(), rule.condition_checkout_code_hash)

          if (bcryptCache[cacheKey] === false && bcryptCache[cacheKey2] === false) {
            rule._fail_reason.push('condition_checkout_code_hash')
            // console.log('rule ' + rule.id + ' condition_checkout_code_hash check failed')
            // continue
          } else {
            rule._checkout_code_passed = true
          }
        }
      }

      if (nnne(rule.condition_product_source) && rule.condition_product_source !== product.product_source) {
        rule._fail_reason.push('condition_product_source')
        // console.log('rule ' + rule.id + ' condition_product_source check failed')
        // continue
      }

      if (nnne(rule.condition_product_code) && rule.condition_product_code?.trim() !== product.product_code?.trim()) {
        rule._fail_reason.push('condition_product_code')
        // console.log('rule ' + rule.id + ' condition_product_code check failed')
        // continue
      }

      if (nnne(rule.condition_product_type)) {
        if (rule.condition_product_type !== product.product_type) {
          rule._fail_reason.push('condition_product_type')
          // console.log('rule ' + rule.id + ' condition_product_type check failed')
          // continue
        }
      }

      if (nnne(rule.condition_from_datetime)) {
        const condition_from_date = newDateLocal(rule.condition_from_datetime)
        if (isNaN(condition_from_date.getTime()) || condition_from_date > now) {
          rule._fail_reason.push('condition_from_datetime')
          // console.log('rule ' + rule.id + ' condition_from_datetime check failed')
          // continue
        }
      }

      if (nnne(rule.condition_to_datetime)) {
        const condition_to_date = newDateLocal(rule.condition_to_datetime)
        if (isNaN(condition_to_date.getTime()) || condition_to_date < now) {
          rule._fail_reason.push('condition_to_datetime')
          // console.log('rule ' + rule.id + ' condition_to_datetime check failed')
          // continue
        }
      }

      if (nnne(rule.condition_product_from_date_from) && nnne(product.from_date)) {
        if (rule.condition_product_from_date_from < product.from_date) {
          rule._fail_reason.push('condition_product_from_date_from')
          // console.log('rule ' + rule.id + ' condition_product_from_date_from check failed')
          // continue
        }
      }

      if (nnne(rule.condition_product_from_date_to) && nnne(product.from_date)) {
        if (rule.condition_product_from_date_to > product.from_date) {
          rule._fail_reason.push('condition_product_from_date_to')
          // console.log('rule ' + rule.id + ' condition_product_from_date_to check failed')
          // continue
        }
      }

      if (nnnz(rule.condition_minimum_product_quantity) && Big(rule.condition_minimum_product_quantity).gt(priceAttributes['product/quantity'])) {
        rule._fail_reason.push('condition_minimum_product_quantity')
        // console.log('rule ' + rule.id + ' condition_minimum_product_quantity check failed')
        // continue;
      }

      if (nnnz(rule.condition_minimum_product_total) && Big(rule.condition_minimum_product_total).gt(priceAttributes['product/total'])) {
        rule._fail_reason.push('condition_minimum_product_total')
        // console.log('rule ' + rule.id + ' condition_minimum_product_quantity check failed')
        // continue;
      }

      if (nnnz(rule.condition_minimum_product_running_total) && Big(rule.condition_minimum_product_running_total).gt(running)) {
        rule._fail_reason.push('condition_minimum_product_running_total')
        // console.log('rule ' + rule.id + ' condition_minimum_product_quantity check failed')
        // continue;
      }

      if (rule.is_public === '0') {
        if (customer == null) {
          rule._fail_reason.push('customer not logged in')
          // console.log('rule ' + rule.id + ' customer not logged in')
          // continue
        }
        else if (
          rule.condition_customer_in_any_categories?.find(category1 =>
            customer.categories?.find(category2 =>
              category1.id === category2.id)
            != null)
          == null) {
            rule._fail_reason.push('customer not in correct category')
            // console.log('rule ' + rule.id + ' customer does not belong in required category')
            // continue
          }
      }

      if (nnne(rule.condition_customer_openid_iss) && customer?.openid_iss !== rule.condition_customer_openid_iss) {
        rule._fail_reason.push('condition_customer_openid_iss')
      }

      if (rule.condition_product_in_any_categories_enabled === '1') {
        if (product.product_category_ids == null) {
          rule._fail_reason.push('condition_product_in_any_categories')
          // console.log('rule ' + rule.id + ' condition_product_in_any_categories check failed')
          // continue
        }
        else {
          const product_category_ids = product.product_category_ids?.split(',')

          if (
            rule.condition_product_in_any_categories?.find(category1 =>
              product_category_ids?.find(category2 =>
                category1.id === category2)
              != null)
            == null) {
              rule._fail_reason.push('condition_product_in_any_categories')
              // console.log('rule ' + rule.id + ' condition_product_in_any_categories check failed')
              // continue
            }
        }
      }

      // auto fail deprecated condition
      if (nnnz(rule.condition_maximum_use_count_quantity)) {
        rule._fail_reason.push('condition_maximum_use_count_quantity')
        // console.log('rule ' + rule.id + ' condition_maximum_use_count_quantity check failed')
        // continue
      }

      if (nnne(rule.condition_mutex_code)) {
        if (mutexDict[rule.condition_mutex_code] != null) {
          rule._fail_reason.push('condition_mutex_code')
          // console.log('rule ' + rule.id + ' condition_mutex_code check failed')
          // continue
        }
      }

      if (rule._fail_reason.length !== 0) continue;
      // console.log('rule ' + rule.name + ' passed')

      let adjustmentTotal = Big(0)

      // calculate adjustment per unit
      // as product_id is only unique within a product source, we require condition_product_source to be set
      const adjustmentLine
        = !nnne(rule.condition_product_source) || !nnne(rule.adjustment_product_id_final_per_unit_csv)
        ? null
        : rule.adjustment_product_id_final_per_unit_csv.split("\n").find((line) => line.split(',')[0].trim() === product.product_id)

      if (adjustmentLine != null) {
        const split = adjustmentLine.split(",")
        if (split.length > 1) {
          try {
            adjustmentTotal = adjustmentTotal.plus(Big(split[1].trim()).times(product.quantity))
          } catch (e) {}
        }
      }

      else {
        if (nnnz(rule.adjustment_per_unit)) {
          adjustmentTotal = adjustmentTotal.plus(Big(rule.adjustment_per_unit).times(product.quantity))
        }

        if (nnnz(rule.adjustment_fixed)) {
          adjustmentTotal = adjustmentTotal.plus(rule.adjustment_fixed)
        }

        // calculate adjustment percentage
        let priceAttribute = Big(0)

        if (rule.adjustment_percentage_price_attribute === 'product_running_total') {
          priceAttribute = running
        } else if (priceAttributes[rule.adjustment_percentage_price_attribute] != null) {
          priceAttribute = Big(priceAttributes[rule.adjustment_percentage_price_attribute])
        }

        if (!priceAttribute.eq(0)) {
          let adjustmentPercentage
          = nnne(rule.adjustment_percentage)
          ? Big(rule.adjustment_percentage).times(priceAttribute).div(100)
          : Big(0)

          adjustmentTotal = adjustmentTotal.plus(adjustmentPercentage)
        }
      }

      // limit checks
      // this is the last check and we "continue" to next check if something fails
      // makes it easier to update limits
      if (nnne(rule.limit_code)) {
        if (nnne(order.limit_code) && order.limit_code !== rule.limit_code) {
          // only 1 limit code per order
          rule._fail_reason.push("limit code mismatch")
          continue;
        }
        else {
          var limitAllAfterAdjustment = Big(limits?.all?.[rule.limit_code.toLowerCase()] ?? 0).minus(adjustmentTotal)
          var limitCustomerAfterAdjustment = Big(limits?.customer?.[rule.limit_code.toLowerCase()] ?? 0).minus(adjustmentTotal)

          if (nnnz(rule.limit_discount_across_all_orders) && limitAllAfterAdjustment.gt(rule.limit_discount_across_all_orders)) {
            rule._fail_reason.push("limit_discount_across_all_orders")
            continue;
          }
          if (nnnz(rule.limit_discount_per_customer) && customer == null) {
            // there is no way to enforce limits if customer is not logged in
            rule._fail_reason.push("limit_discount_per_customer is set but customer is not logged in")
            continue;
          }
          if (nnnz(rule.limit_discount_per_customer) && limitCustomerAfterAdjustment.gt(rule.limit_discount_per_customer)) {
            rule._fail_reason.push("limit_discount_per_customer")
            continue;
          }

          if (limits == null) limits = {}
          if (limits.all == null) limits.all = {}
          if (limits.customer == null) limits.customer = {}
          limits.all[rule.limit_code.toLowerCase()] = limitAllAfterAdjustment.toFixed(2)
          limits.customer[rule.limit_code.toLowerCase()] = limitCustomerAfterAdjustment.toFixed(2)
          order.limit_code = rule.limit_code
          if (order.limit_discount == null) order.limit_discount = "0"
          order.limit_discount = Big(order.limit_discount).minus(adjustmentTotal).toFixed(2)
        }
      }

      seqTotal = seqTotal.plus(adjustmentTotal)

      if (nnne(rule.condition_mutex_code)) {
        mutexDict[rule.condition_mutex_code] = true
      }

      rule._adjustment = adjustmentTotal.toFixed(2)

      //rule._order_use_count_quantity += parseInt(product.quantity, 10)

      product.items.push({
        'id': undefined,
        'sub_header': 'Adjustments',
        'name': rule.adjustment_name,
        'price': adjustmentTotal.toFixed(2),
        'price_type': "",
        'deposit': "0",
        'quantity': '1',
        'price_rule_id': rule.id ?? '0' //null means not a price rule
      })
    }
  }
  return order
}

function urldecode(str) {
  return JSON.parse(`{"${str.replace(/&/g, '","').replace(/\=/g,'":"')}"}`,
    function(key, value) {
          if (key==="") { return value;
          } else { return decodeURIComponent(value); }
  })
}

export function computeLegacyHotel(details, checkout: LegacyHotelFormValue) {
  const source = 'bedsonline';
  const room = details.rooms.find((room) => room.id === checkout.room_id);

  if (room == null) {
    return {
      error: 'Cannot find room id ' + checkout.room_id
    }
  }

  const param = checkout.params;

  const { duration } = param;
  const { checkInDate } = param;
  const checkOutDate = moment(param.checkInDate, 'YYYY-MM-DD').add(param.duration,'days').format('YYYY-MM-DD');

  const product: any = {};
  product.items = [];
  product.product_name = details.name;
  product.product_type = 'hotel_room';
  product.product_status = 'Unconfirmed';
  product.product_source = source;
  product.product_id = room.id;
  product.to_date = checkOutDate;
  product.from_date = checkInDate;
  product.adult = 1;
  product.quantity = '1';
  product.deposit_percentage = '100'

  const voucherRoom = JSON.parse(JSON.stringify(room));
  voucherRoom.prices = [];

  const price = room.prices.find((price) => price.code === checkout.price_code);

  if (price == null) {
    return {
      error: 'Cannot find price code ' + checkout.price_code
    }
  }

  product.items.push({
      header : 'Item',
      sub_header : room.description,
      name : `Check-in on ${checkInDate}, Check-out on ${checkOutDate}, ${duration} nights`,
      price : price.price,
      price_type : price.code,
      deposit : '0',
      cost : price.cost,
      cost_source : source,
      quantity : '1'
  });

  if (price.cost != null) { delete price.cost; }
  price.quantity = '1';
  voucherRoom.prices.push(price);

  //capture info for voucher
  product.voucher = {
      contact : details.contact,
      latitude : details.latitude,
      longitude : details.longitude,
      remarks : details.remarks,
      room : voucherRoom
  };

  return {product};
};
