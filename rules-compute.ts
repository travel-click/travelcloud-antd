import moment from 'moment';

export default function rules_compute (programStr: string, options) {
    var dates_var = {}
    var surcharges = []
    var prices = []
    var start_date = null
    var end_date = null

    //detect start date and end date
    for (var i in options) {
        //moment isValid() detects integers as valid dates no matter what you do
        //no choice but to test for string length :()
        if ( Object.prototype.toString.call( options[i] ) === '[object Array]' && options[i].length > 0 && options[i][0].length==10) {
            //for dates
            for (var ii in options[i]) {
                var current = moment(options[i][ii], 'YYYY-MM-DD')
                if (start_date == null || start_date.isAfter(current))
                    start_date = current.clone()
                if (end_date == null || end_date.isBefore(current))
                    end_date = current.clone()
            }
        }
    }


    var dates_by_day_of_week = {
        0 : {
            description: "Sundays",
            dates: {}
        },
        1 : {
            description: "Mondays",
            dates: {}
        },
        2 : {
            description: "Tuesdays",
            dates: {}
        },
        3 : {
            description: "Wednesdays",
            dates: {}
        },
        4 : {
            description: "Thursdays",
            dates: {}
        },
        5 : {
            description: "Fridays",
            dates: {}
        },
        6 : {
            description: "Saturdays",
            dates: {}
        }
    }

    if (start_date && end_date) {
        while (start_date.isAfter(end_date) == false) {
            dates_by_day_of_week[start_date.day()].dates[start_date.format('YYYY-MM-DD')] = true
            start_date.add(1, 'day')
        }
    }

    var function_handlers = {
        add_surcharge : function(name, price, integer_array) {
            var qty = 1
            for (var i in integer_array) {
                qty = qty * render_integer[integer_array[i].type].apply(this, integer_array[i].args)
            }

            if (qty < 1) return false;

            surcharges.push({
                name: name,
                price: price,
                qty: qty
            })
        },
        set_prices: function(name, price, integer_array) {
            var qty;
            if (integer_array.length == 0) qty = 99999
            else {
                qty = 1
                for (var i in integer_array) {
                    qty = qty * render_integer[integer_array[i].type].apply(this, integer_array[i].args)
                }
            }

            if (qty < 1) return false;

            //prices is global
            //price is local
            prices.push ({
                name: name,
                price: price,
                qty: qty
            })
        },
        echo : function(out) {
            if (out.type == "date_result") {
                console.log(render_date.date_result.apply(this, out.args))
            } else {
                console.log(out)
            }
        },
        dates_include : function(var_name, dates) {
            dates_var[var_name] = dates
        },
        if : function(condition, statement, else_statement) {
            var condition = render_boolean[condition.type].apply(this, condition.args)

            if (condition) function_handlers[statement.type].apply(this, statement.args)
            else if (else_statement) function_handlers[else_statement.type].apply(this, else_statement.args)
        }
    }

    var render_string = {
        string_constant: function(val) {
            return val
        },
        string_option: function(val) {
            return options[val]
        },
    }

    var render_integer = {
        integer_constant: function(val) {
            return val
        },
        integer_option: function(val) {
            return parseInt(options[val])
        },
        integer_number_of_dates: function(a) {
            var result = render_date[a.type].apply(this, a.args)
            return Object.keys(result.dates).length
        },
        integer_array_within_range: function(from, int_array, to) {
            from = render_integer[from.type].apply(this, from.args)
            to = render_integer[to.type].apply(this, to.args)
            int_array = render_integer[int_array.type].apply(this, int_array.args)
            var result = 0;
            for (var i in int_array) {
                if (int_array[i] <= to && int_array[i] >= from) result++;
            }

            return result;
        },
        integer_array_compare: function(primary, op, int_array) {
            function compare(val) {
                switch(op) {
                    case ">" : return primary > val
                    case ">=" : return primary >= val
                    case "<" : return primary < val
                    case "<=" : return primary <= val
                    case "=" : return primary == val
                    default: return false
                }
            }

            primary = render_integer[primary.type].apply(this, primary.args)
            int_array = render_integer[int_array.type].apply(this, int_array.args)

            var ans = 0;

            for (var i in int_array) {
                if (compare(int_array[i])) ans++;
            }

            return ans;
        },
        integer_array_compare_inv: function(int_array, op, primary) {
            function compare(val) {
                switch(op) {
                    case ">" : return val > primary
                    case ">=" : return val >= primary
                    case "<" : return val < primary
                    case "<=" : return val <= primary
                    case "=" : return val == primary
                    default: return false
                }
            }

            primary = render_integer[primary.type].apply(this, primary.args)
            int_array = render_integer[int_array.type].apply(this, int_array.args)

            var ans = 0;

            for (var i in int_array) {
                if (compare(int_array[i])) ans++;
            }

            return ans;
        },
        integer_array: function(identifier) {
            return options[identifier];
        }
    }


    var render_boolean = {
        boolean_string_compare: function (a, op, b) {
            a = render_string[a.type].apply(this, a.args)
            b = render_string[b.type].apply(this, b.args)

            //ignore op for now since it needs to be =
            return a.toLowerCase() === b.toLowerCase();
        },
        boolean_integer_compare: function (a, op, b) {
            a = render_integer[a.type].apply(this, a.args)
            b = render_integer[b.type].apply(this, b.args)
            if (op) {
                switch(op) {
                    case ">" : return a > b;
                    case ">=" : return a >= b;
                    case "<" : return a < b;
                    case "<=" : return a <= b;
                    case "=" : return a == b;
                    default: return false;
                }
            } else {
                return a;
            }
        },
        boolean_integer_within_range: function (a,b,c) {
            a = render_integer[a.type].apply(this, a.args)
            b = render_integer[b.type].apply(this, b.args)
            c = render_integer[c.type].apply(this, c.args)

            return a <= b && b <=c;
        },
        boolean_from_integer_result: function(i) {
            var result = render_integer[i.type].apply(this, i.args)
            console.log('integer result')
            return result > 0
        },
        boolean_from_date_result: function(i) {
            var result = render_date[i.type].apply(this, i.args)
            console.log('date result')
            return Object.keys(result.dates).length > 0
        }
    }

    var render_date = {
        date_option: function(val) {
            if (typeof options[val] == "undefined") throw new Error("Compute error: " + val + " is not found in options")
            var dates = {};
            for (var i in options[val]) {
                dates[options[val][i]] = true
            }
            return {
                description : val,
                dates: dates
            }
        },
        date_variable : function (val) {
            if (typeof dates_var[val] == 'undefined') throw new Error("Compute error: @" + val + " is undefined")
            return render_date[dates_var[val].type].apply(this,dates_var[val].args)
        },
        date_constant : function (val) {
            return val
        },
        date_predefined : function (index) {
            return dates_by_day_of_week[index]
        },
        date_result : function(includes, excludes, ons) {
            var dates = {}
            var description = null

            var rendered_includes = []
            for (var i in includes) {
                rendered_includes.push(render_date[includes[i].type].apply(this,includes[i].args))
            }

            var description_include = []
            for (var i in rendered_includes) {
                var current = rendered_includes[i]
                description_include.push(current.description)

                for (var ii in current.dates) {
                    dates[ii] = true
                }
            }

            description = description_include.join(", ")

            if (excludes) {
                description += " exclude "

                var rendered_excludes = []
                for (var i in excludes) {
                    rendered_excludes.push(render_date[excludes[i].type].apply(this,excludes[i].args))
                }

                var description_exclude = []
                for (var i in rendered_excludes) {
                    var current = rendered_excludes[i]
                    description_exclude.push(current.description)

                    for (var ii in current.dates) {
                        delete dates[ii]
                    }
                }

                description += description_exclude.join(", ")
            }

            if (ons) {
                description += " on "

                var rendered_ons = []
                for (var i in ons) {
                    rendered_ons.push(render_date[ons[i].type].apply(this,ons[i].args))
                }

                var description_on = []
                var new_date = {}
                for (var i in rendered_ons) {
                    var current = rendered_ons[i]
                    description_on.push(current.description)

                    for (var ii in current.dates) {
                        if (dates[ii]) new_date[ii] = true
                    }
                }

                description += description_on.join(", ")
                dates = new_date
            }

            return {
                dates: dates,
                description: description
            }
        }
    }

    // empty string is the only invalid json we accept
    // other invalid json should crash by design
    var program = programStr === "" ? [] : JSON.parse(programStr)

    for (var i in program) {
        if (function_handlers[program[i].type] != null)
            function_handlers[program[i].type].apply(this, program[i].args)
        else
            console.log('Unknown type ' + program[i].type)
    }

    return {
        prices: prices,
        surcharges: surcharges
    }
}
