
// Imports the Google Cloud client library
const { Translate } = require('@google-cloud/translate').v2;
const fs = require('fs');
const { assert } = require('console');
const path = require('path')
const process = require('process')
const glob = require('glob')

const configFilePath = path.resolve( process.cwd(), process.argv[ process.argv.length - 1 ])
const config = require(configFilePath)

const outputPath = config.output != null ? config.output.split(path.sep) : 'locales/$LOCALE/$NAMESPACE.json'.split(path.sep)
const localeSegIndex = outputPath.indexOf('$LOCALE')

// todo: arg for main locale file
// todo: arg fro api key
// todo: flag for test utilities mode

const localeFiles = glob.sync(  outputPath[ 0 ] + '/**/*.json' )
const googleTranslateApiKey = 'AIzaSyAMSeFtvrhaaWOZARY_JhowgxNmwqkNouE'
const mainLocale = path.resolve( process.cwd(), './locales/en-US/translation.json' )

const run = async () => {

  let loadJson = path => JSON.parse(fs.readFileSync( path, {encoding: 'utf8' }))

  let mainTree = flattenTree(loadJson( mainLocale ))
  let __translate = getTranslator()

  localeFiles.map(filePath => {
    let lastSegs = filePath.split(path.sep).slice(-2)
    return [ filePath, lastSegs[0], loadJson( filePath )]
  })
  .map(([ path, langCode, json ]) => {

      Promise.all(

        mainTree.map(([ key, value ]) => {
          return (async () => {

            if (langCode === 'en-US' && value === '') {
                if (key.indexOf('.') > -1) {
                  let [ , value ] = key.split('.')
                } else {
                  value = key
                }
            }

            let output = langCode === 'en-US' ? value : await __translate( value === '' ? key : value, langCode )
            setTreeBranch( json, key, output )
          })()

        })

      ).then(() => {
        console.log(`LangCode ${langCode} parsing is done.`)
        fs.writeFileSync( path, JSON.stringify( json, null, 2 ), { encoding: "utf8" }) 
      })


  })

  // flattenTree( mainTree )
  //   .map(([ key, value ]) => {

  //     __translate( value, 'my' )
  //       .then(output => console.log( key, output ))
  //   })
}

run()


/* * * * * * * * * * * *
 * 
 * Utilities 
 * 
 * * * * * * * * * * * */

function getTreeBranch(tree, key) {
  return key.split('.').reduce((acc, cur) => acc[cur], tree)
}

// todo: move to separate test file
// console.assert(JSON.stringify(getTreeBranch( output, "title" )) == JSON.stringify(output.title))
// console.assert(getTreeBranch(output, 'title.Hotel_plural') === "Hotels")

function setTreeBranch(tree, key, value) {

  let keys = key.split('.')

  keys.reduce((acc, cur, index) => { 

    if (index === keys.length - 1) {
      acc[cur] = value
      return acc[cur]
    }

    else {
      if (acc[cur] != null) {
        return acc[cur]
      }

      else {
        acc[cur] = {}
        return acc[cur]
      }
    }

  }, tree)

}

// todo: move to separate test file
// setTreeBranch(output, "title.Hotel", "hola")
// console.assert(output.title.Hotel === "hola")

// setTreeBranch(output, "friend_male", "bf")
// console.assert(output.friend_male === "bf")

// let t = { one: { two: { three: { four: "five" }}} }

// setTreeBranch(t, "one.two.three.four", 5)
// console.assert(t.one.two.three.four === 5)

function flattenTree(tree, parentKey = "") {
  return Object.entries(tree)
    .map(([ key, value ]) => {
      let _key = parentKey != '' ? parentKey + '.' + key : key

      if (typeof value === 'string') 
        return [[ _key, value]]
        // return [[ _key, value ]]
      else if (typeof value === 'object')
        return flattenTree( value, _key )
    }).flat()
}



// let flatten = Object.fromEntries(flattenTree(output))
// console.log(flatten)
// console.assert(flatten['title.Hotel'] === 'Hotel')
// console.assert(flatten['friend_female'] === 'A girlfriend')

function getTranslator() {

  // Instantiates a client
  const translate = new Translate({ key: googleTranslateApiKey });

  // use <span> to escape google translate 
  // todo: enhance if there's better way to have done
  return async ( text, to ) => {
    let escaped = text.replace(/({{[a-zA-Z_]+}})/gm, "<span class='notranslate'>$1</span>")
      .replace(/<([0-9^]+)>/gm, '<span class="$1">')
      .replace(/<\/([0-9^]+)>/gm, '</span>')
    let [translation] = await translate.translate( escaped, { to, format: "html" });

    // return decode(translation.replace(/<[^>]*>?/gm, ''))
    // return decode(translation.replace(/<span class='notranslate'>(.*)<\/span>/gm, '$1'))
    return decode(
      translation.replace(/<span class='notranslate'>(.*)<\/span>/gm, '$1')
        .replace(/<span class="([0-9]+)">(.+?)<\/span>/gm, '<$1>$2</$1>')
    )

  }
}

// ref: https://stackoverflow.com/questions/40263803/native-javascript-or-es6-way-to-encode-and-decode-html-entities
function decode( str ) {
  return str
  .replace(/&lt;/g, '<').replace(/&gt;/g, '>')
  .replace(/&#(\d+);/g, function(match, dec) {
    return String.fromCharCode(dec);
  })
}

