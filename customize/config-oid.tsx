export default {
  authority: "https://preprod-auth.ntuclink.com.sg/",
  client_id: "jKcvLTkPu6oe6NO4nugzRSG34fVKpsK5",
  redirect_uri: "http://localhost:3000/callback",
  post_logout_redirect_uri: "http://localhost:3000/callback",
  response_type: "code",
  scope: "openid profile email",
  automaticSilentRenew: false,
  validateSubOnSilentRenew: true,
  loadUserInfo: true,

  monitorAnonymousSession: true,

  filterProtocolClaims: true,
  revokeAccessTokenOnSignout: true,
}