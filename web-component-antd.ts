
export function getContent(content?: {}): {
  log(): void;
  code(code: any): any;
  blockId(blockId: any): any;
  blockIds(blockIds: any): any;
  uniqueArray(): void;
  tag(tag: any): any;
  sortLTH(item: any): any;
  sortHTL(item: any): any;
  getCode(): any;
  getValue(): any;
  get(): any;
  all(): any[];
} {
    if (content === void 0) { content = {}; }
    var entryContent = Object.entries(content);
    var state = [];
    return {
        log: function () {
            console.log(this);
        },
        code: function (code) {
            state.push(entryContent.find(function (_a) {
                var value: any = _a[1];
                return (value === null || value === void 0 ? void 0 : value.code) == code;
            }) || []);
            this.uniqueArray();
            this.get();
            return this;
        },
        blockId: function (blockId) {
            state.push(entryContent.find(function (_a) {
                var code = _a[0];
                return code === blockId;
            }) || []);
            this.uniqueArray();
            this.get();
            return this;
        },
        blockIds: function (blockIds) {
            var validatedBlockIds = blockIds.map(function (blockId) { return blockId; });
            state.push.apply(state, (entryContent.filter(function (_a) {
                var code = _a[0];
                return validatedBlockIds.indexOf(code) > -1;
            }) || []));
            this.uniqueArray();
            this.get();
            return this;
        },
        uniqueArray: function () {
            state = state.filter(function (_a, index, self) {
                var code = _a[0];
                return self.map(function (_a) {
                    var code = _a[0];
                    return code;
                }).indexOf(code) === index;
            });
        },
        tag: function (tag) {
            state.push.apply(state, (entryContent.filter(function (_a) {
                var _b;
                var content: any = _a[1];
                return ((_b = content === null || content === void 0 ? void 0 : content.tags) === null || _b === void 0 ? void 0 : _b.indexOf(tag)) > -1;
            }) || []));
            this.uniqueArray();
            return this;
        },
        // multiple tags later
        sortLTH: function (item) {
            state = state.sort(function (_a, _b) {
                var a = _a[1];
                var b = _b[1];
                return parseInt((a === null || a === void 0 ? void 0 : a[item]) || 0) - parseInt((b === null || b === void 0 ? void 0 : b[item]) || 1);
            });
            return this;
        },
        sortHTL: function (item) {
            state = state.sort(function (_a, _b) {
                var a = _a[1];
                var b = _b[1];
                return parseInt((b === null || b === void 0 ? void 0 : b[item]) || 0) - parseInt((a === null || a === void 0 ? void 0 : a[item]) || 1);
            });
            return this;
        },
        getCode: function () {
            //need refactor
            if (state.length > 1) {
                return state.map(function (_a) {
                    var code = _a[0];
                    return code;
                });
            }
            var code = state[0][0];
            return code;
        },
        getValue: function () {
            if (state.length > 1) {
                return state.map(function (_a) {
                    var value = _a[1];
                    return value;
                });
            }
            var _a = state[0], value = _a[1];
            return value;
        },
        get: function () {
            if (state === undefined || state.length == 0) {
                return entryContent[0];
            }
            if (state.length > 1) {
                return state;
            }
            var value = state[0];
            return value;
        },
        all: function () {
            if (state === undefined || state.length == 0) {
                return entryContent[0];
            }
            return state;
        },
    };
}