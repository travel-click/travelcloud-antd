import 'isomorphic-unfetch'
import Big from 'big.js'
import moment from 'moment'
import { computeTour, computeLegacyHotel, computeGeneric, computeFlight, Product, computePriceRules, TourFormValue, LegacyHotelFormValue, GlobaltixFormValue, computeGlobaltix, computeHotelbeds, filterPriceRulesWithCheckoutCode, HotelbedsFormValue, HotelbedsStaticData, HotelbedsBookingForm, HotelbedsCheckRateResponse, HotelbedsAvailability, OutboundFormValue, computeOutbound, FlightFormValue, FlightChangeFormValue, computeFlightChange, newDateLocal, computeAnyTour, AnyTourFormValue, FlightChangeServiceFormValue, computeFlightChangeServices, TourType } from './order-product-compute'
import React, { useRef, useEffect, useCallback, useState } from "react"
import {FlightSearch, FlightDetail, PriceRules, Tour, PriceRule, Order, v2_PrivateTour, v2_GroupTours, v2_GroupTour} from './types'
import i18nInit from './i18n'

export interface TcResponse<T> {
  loading?: boolean
  error?: string
  result?: T
}

export interface LegacyHotelsParams {
  cityCode: string
  checkInDate: string
  checkOutDate: string
  adults: number | string
  children: number | string
}

export interface FlightsParams {
  source: string
  'od1.origin_airport.code': string
  'od1.destination_airport.code'?: string
  'od1.origin_datetime': string
  'od2.origin_airport.code': string
  'od2.destination_airport.code'?: string
  'od2.origin_datetime'?: string
  cabin?: string
  ptc_adt: number
  ptc_cnn: number
  ptc_inf: number
}

export interface FlightParams {
  source: string
  'od1.id': string
  'od2.id'?: string
  ptc_adt: number
  ptc_cnn: number
  ptc_inf: number
  'pricing.id'?: string
}

export interface LegacyHotelParams {
  hotelId: string
  checkInDate: string
  checkOutDate: string
  adults: number | string
  children: number | string
}

export interface TourParams {
  'categories.name': string,
  '~boolean': string
}

const getCache = {}

function postRedirect(post: {method?: string, action: string, form: {name: string, value: string}[]}) {

  if (post.form == null) {
    window.location.href = post.action;
  }

  else {
    const f = document.createElement('form');
    f.action=post.action;
    f.method='POST';

    for (let input of post.form) {
      const i=document.createElement('input');
      i.type='hidden';
      i.name=input.name;
      i.value=input.value;
      f.appendChild(i);
    }
    document.body.appendChild(f);
    f.submit();
  }
}

export const setupUserback = (token = '4386|15385|8OWEwPsjAdNcLi51kGOXy3CKGebHwlJ2sEcrPYy9SEPvizoqNF') => {
  if (window.location.hostname.indexOf('.travel.click') === -1) return
  const window2: any = window
  const id = 'userback-sdk'
  const Userback = window2.Userback || {}
  Userback.access_token = token;
  window2.Userback = Userback
  if (document.getElementById(id)) {return}
  var s = document.createElement('script')
  s.id = id
  s.async = true
  s.src = 'https://static.userback.io/widget/v1.js'
  var parent_node = document.head || document.body
  parent_node.appendChild(s)
}

export const setupGtm = (Router, config: {tcUser: string, currency?: string, googleTagMangerId?: string, googleAnalyticsTrackingId?: string, [key: string]: any}) => {
  const window2: any = window
  if (window == null || window2.gtmAlreadySetup) return

  if (config.googleTagMangerId != null) {
    window2.gtmAlreadySetup = true
    window2.dataLayer = window2.dataLayer || []
    window2.dataLayer.push({
      'gtm.start':
        new Date().getTime(), event: 'gtm.js'
    })

    let tag = document.createElement('script')
    tag.src = "https://www.googletagmanager.com/gtm.js?id=" + config.googleTagMangerId
    let firstScriptTag = document.getElementsByTagName('script')[0]
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag)
  }

  setupGtag(Router, config)

}

export const setupGtag = (Router, config: {tcUser: string, currency?: string, googleAnalyticsTrackingId?: string, [key: string]: any}) => {
  const window2: any = window
  const travelClickTrackingId = 'UA-132985038-1'
  if (window == null || window2.gtagAlreadySetup) return

  window2.dataLayer = window2.dataLayer || []
  window2.gtag = function() {window2.dataLayer.push(arguments)}

  // prevent logging localhost but leave the gtag stub there
  if (window.location.hostname === 'localhost') return

  var currency = config.currency
  if (currency == null || currency.length !== 3) currency = 'SGD'
  window2.tcUser = config.tcUser
  window2.tcCurrency = currency
  window2.gtagAlreadySetup = true

  window2.gtag('js', new Date())
  window2.gtag('config', travelClickTrackingId, {
    'dimension1' : config.tcUser,
    'groups' : 'travel_click',
    currency
  })

  if (config.googleAnalyticsTrackingId != null && config.googleAnalyticsTrackingId.trim() !== "") {
    window2.gtag('config', config.googleAnalyticsTrackingId.trim(), {currency})
  }

  let tag = document.createElement('script')
  tag.src = "https://www.googletagmanager.com/gtag/js?id=" + (config.googleAnalyticsTrackingId || 'UA-132985038-1')
  let firstScriptTag = document.getElementsByTagName('script')[0]
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag)

  let lastTrackedUrl = null;

  Router.events.on('routeChangeComplete', (newUrl) => {
    if (newUrl === lastTrackedUrl || window2.gtag == null) {
      return;
    }

    // Don't double track the same URL
    lastTrackedUrl = newUrl;

    window2.gtag('config', travelClickTrackingId, {
      page_path: newUrl,
      'dimension1' : config.tcUser,
      'groups' : 'travel_click',
      currency
    })

    if (config.googleAnalyticsTrackingId != null && config.googleAnalyticsTrackingId.trim() !== "") {
      window2.gtag('config', config.googleAnalyticsTrackingId.trim(), {
        page_path: newUrl,
        currency
      })
    }
  })
}

export const setupFacebookPixel = (Router, config: {tcUser: string, currency?: string, facebookPixelId?: string, [key: string]: any}) => {
  const window2: any = window
  const facebookPixelId = config.facebookPixelId
  if (window == null || window2.fbq != null || facebookPixelId == null) return

  // prevent logging localhost but create fbq stub
  if (window.location.hostname === 'localhost') {
    window2.fbq = function() {}
    return}

  var currency = config.currency
  if (currency == null || currency.length !== 3) currency = 'SGD'
  window2.tcUser = config.tcUser
  window2.tcCurrency = currency

  function x(f, b, e, v, n=null, t=null, s=null) {
    if (f.fbq) return;
    n = f.fbq = function() {
        n.callMethod ?
            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
    };
    if (!f._fbq) f._fbq = n;
    n.push = n;
    n.loaded = !0;
    n.version = '2.0';
    n.queue = [];
    t = b.createElement(e);
    t.async = !0;
    t.src = v;
    s = b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t, s)
  }
  x(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

  const fbq = window2.fbq
  fbq('init', facebookPixelId);
  fbq('track', 'PageView');

  let lastTrackedUrl = null;

  Router.events.on('routeChangeComplete', (newUrl) => {
    if (newUrl === lastTrackedUrl || window2.gtag == null) {
      return;
    }

    // Don't double track the same URL
    lastTrackedUrl = newUrl;

    fbq('track', 'PageView');
  })
}

export async function redirectToPayment(options: {
  orderRef: string,
  tcUser: string,
  paymentProcessor: string,
  amount?: string,
  successPage: string,
  failurePage: string,
}) {

  const client = new TravelCloudClient( {tcUser: options.tcUser} )
  const success = options.successPage.replace(/^\/+/g, '')
  const failure = options.failurePage.replace(/^\/+/g, '')
  const prefix = location.protocol + '//' + location.host

  const payload: any = {
    order_ref: options.orderRef,
    cancel: prefix + location.pathname + location.search,
    success: prefix + '/' + success,
    failure: prefix + '/' + failure,
    payment_processor: options.paymentProcessor,
  }

  if (options.amount != null) payload.amount = options.amount
  
  const post = await client.post('rpc/payment_redirect', null, payload)

  postRedirect(post.result)

  return
}

export function urlParam(params): string {
  if (params == null) return ''
  return Object.keys(params)
  .filter(k => params[k] !== '')
  .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
  .join('&')
}

function hasPrefix(key: string, prefixes: string[], target: string): boolean {
  const firstSplit = key.split('/')[0]
  const result = prefixes.find(prefix =>  firstSplit === prefix)
  if (result == null) console.warn("Invalid param " + key + " for " + target)
  return result != null
}

export function isValidNumericId(id: any): boolean {
  return  id != null && id.toString().length < 6 && /^\d+$/.test(id);
}

type CacheMeta = {
  //response: any,
  requestStarted: boolean,
  cacheAvailable: boolean,
  time: number,
  resolves: any[]
}

//const cacheMetaMap: {[key:string]: CacheMeta} = {}
//const cacheResponseMap: {[key:string]: any} = {}

export type TravelCloudClientConfig = {
  tcUser: string,
  domain?: string,
  defaultTitle?: string,
  companyName?: string,
  googleAPIKey?: string,
  facebookPixelId?: string,
  flightDataSource?: string,
  locale?: string,
  translation?: string
}

export class TravelCloudClient {
  config: TravelCloudClientConfig

  get(endpoint: string, params, disableCache = false, headers = {}): Promise<TcResponse<any>> {
    return new Promise(async (resolve, reject) => {

      if (this.config.translation != null)
        Object.assign( params, { '~translation': this.config.translation })

      const query = params == null ? '' : '?' + urlParam(params)
      //const currentTime = Date.now()
      const isNode = typeof window === 'undefined'
      const key = this.config.tcUser + '/' + endpoint + '/' + query

      /*
      var filename = 'TC-NOFILENAME'

      var cacheMeta: CacheMeta = cacheMetaMap[key]

      // clear stuck/stale cacheMeta
      if (cacheMeta != null && cacheMeta.time + (maxCacheAge * 1000) <= currentTime) {
        for (var i in cacheMeta.resolves) {
          cacheMeta.resolves[i]({error: "Unable to complete request"})
        }
        cacheMeta = null
        delete cacheMetaMap[key]
      }
      
      // check if fresh cache is available
      if (cacheMeta != null && cacheMeta.cacheAvailable === true) {
        var response = null        

        if (isNode) {
          const crypto = eval('require')('crypto')
          const fs = eval('require')('fs')
          filename = '/tmp/TC-' + crypto.createHash('sha512').update(key).digest("hex")
          try{
            const contents = fs.readFileSync(filename, 'utf-8')
            response = JSON.parse(contents)
          } catch (e) {}
        } else {
          response = cacheResponseMap[key]
        }

        if (response != null) {
          //console.log('cache hit ' + key)
          resolve(response)
        } else {
          delete cacheMetaMap[key]
        }
        return}

      // create new cacheMeta
      if (cacheMeta == null) {
        cacheMeta = {
          requestStarted: false,
          cacheAvailable: false,
          time: currentTime,
          resolves: []
        }

        cacheMetaMap[key] = cacheMeta
      }

      // cache not available so we queue
      cacheMeta.resolves.push(resolve)

      // check if we should perform request
      if (cacheMeta.requestStarted === true) {
        //console.log('cache miss coalesce ' + key)
        return}
      else {
        //console.log('cache miss ' + key)
      }
        
      cacheMeta.requestStarted = true
      */

      try {
        //await sleep(5000)
        const payload: RequestInit = {
          headers,
        }

        var cached = getCache[key]
        var res2

        if (cached == null || cached.expiry < Date.now()) {
          var res = fetch(`https://${this.config.tcUser}.travelcloud.app/${endpoint}${query}`, payload)
          //res = fetch(`http://localhost:9000/${endpoint}${query}`, payload)

          if (!isNode && !disableCache) {
            getCache[key] = {res, expiry: Date.now() + 10000}
            res2 = (await res).clone()
          } else {
            // fetch response.clone() does not work as expected on node
            // https://github.com/node-fetch/node-fetch/issues/1131
            res2 = await res
          }          
        } else {
          console.log(key + " is cached")
          res2 = (await cached.res).clone()
        }

        // intentional cache errors
        const result = await res2.json()

        resolve(result)

        /*
        if (res.ok) {
          // make cache available
          //console.log("Complete request stored to cache" + key)
          if (isNode) {
            const crypto = eval('require')('crypto')
            const fs = eval('require')('fs')
            filename = '/tmp/TC-' + crypto.createHash('sha512').update(key).digest("hex")
            try {
              fs.writeFileSync(filename, JSON.stringify(resJson), {encoding: 'utf-8'})
            } catch (e) {}
          } else {
            cacheResponseMap[key] = resJson
          }

          cacheMeta.cacheAvailable = true
        } else {
          // nothing useful produced
          // we clear cacheMetaMap
          //console.log("Complete request failed")
          delete cacheMetaMap[key]
        }

        for (var i in cacheMeta.resolves) {
          cacheMeta.resolves[i](resJson)
        }
        cacheMeta.resolves = []
        */
      } catch (e) {
        //console.log("Unable to complete request")
        resolve({error: e.message})
        /*
        for (var i in cacheMeta.resolves) {
          cacheMeta.resolves[i]({error: "Unable to complete request"})
        }
        delete cacheMetaMap[key]
        */
      }
    })

  }

  cancelPendingGetRequests(endpoint: string) {
    /*
    if (typeof window === 'undefined') {
      throw new Error("Method not implemented server side")
    } else {
      for (const cacheKey in cacheMetaMap) {
        if (cacheKey.indexOf(endpoint) !== 0) continue
        const entry = (window as any).travelCloudClientCache[cacheKey]
        for (const index in entry.resolves) {
          entry.resolves[index]({})
        }
        entry.resolves.length = 0
      }
    }
    */
  }

  async post(endpoint: string, params, data, headers = {}) {
    headers['content-type'] = 'application/json'
    const query = params == null ? '' : '?' + urlParam(params)
    const payload: RequestInit = {
      body: JSON.stringify(data),
      cache: 'no-cache',
      headers,
      method: 'POST',
      mode: 'cors',
    }

    return fetch(`https://${this.config.tcUser}.travelcloud.app/${endpoint}${query}`,
    //return fetch(`http://localhost:9000/${endpoint}${query}`,
      payload)
    .then(response => response.json()) // parses response to JSON
  }

  //todo: add customer token
  constructor(config: TravelCloudClientConfig) {
    this.config = {
      ...config, 
      tcUser: encodeURIComponent(config.tcUser)
    }
  }

  async getDocument(path: string, domain:string = this.config.domain) {
    let url = domain != null ? resolvePath( domain, path ) : path
    let resp = await fetch(`https://asia-east2-headless-cms-292305.cloudfunctions.net/pageDetails?url=${encodeURIComponent(url)}`)
    return await resp.json() 
  }

  async autoComplete(params: {type: string, search?: string, values?: string | string[]}) {
    if (Array.isArray(params.values)) params.values = params.values.join(',')
    return await this.get('rpc/autocomplete', params)
  }

  private autoCompleteTimeout = null

  async hotelbedsAutoComplete(params: { search: string }):Promise<any> {
    if (params.search.length > 0) {
      const p = new Promise((resolve, reject) => {
        var defer = () => {
          this.autoCompleteTimeout = null
          resolve(fetch(`https://static.travelcloud.app/hotelbeds/autocomplete?query=${params.search}`)
            .then(response => response.json()))
        }

        clearTimeout(this.autoCompleteTimeout)
        this.autoCompleteTimeout = setTimeout(defer, 500)
      })

      return await p
    }
  }

  async hotelbedsDefaultCityCodes(params: { search: string[] }): Promise<any> {
    if (params.search != null && params.search.every(s => s.length > 0)){
      const p = new Promise((resolve, reject) => {
        resolve(fetch(`https://static.travelcloud.app/hotelbeds/destinations?codes=${params.search}`)
        .then(response => response.json()))
      })
      return await p;
    }else{
      return [];
    }
  }

  async iataAutoComplete(query: {query: string, airportCodes: string}) {
    return await this.get('iata/autocomplete', query)
  }

  async iataCarriers(query: {query: string, carrierCodes?: string}) {
    return await this.get('iata/carriers', query)
  }

  async priceRules(paramsRaw) {
    const params = Object.keys(paramsRaw)
      .filter( key => ['is_public'].indexOf(key) !== -1 )
      .reduce( (res, key) => (res[key] = paramsRaw[key], res), {} );
    const json = await this.get('api/price_rules', params)
    return json
  }

  async limits(limitCodes: string[], headers = {}) {
    const json = await this.get('rpc/check_limits', {limit_codes: limitCodes.join(',')}, null, headers)
    return json
  }

  async tours(paramsRaw) {
    const params = Object.keys(paramsRaw)
      .filter( key => hasPrefix(key, ['~boolean', 'categories.name', 'categories.parent'], 'tours'))
      .reduce( (res, key) => (res[key] = paramsRaw[key], res), {} );
    const json = await this.get('api/tours', params)
    json.result = json.result.map(tour => {
      tour['_next_departure'] = nextDeparture(tour.options)
      tour['_cheapest'] = cheapest(tour.options)
      tour.options = tour.options.map(option => {
        if (option['_subset_departures'] == null) option['_subset_departures'] = option['departures']
        if (option['departures'] == null) option['departures'] = option['_subset_departures']
        return option
      })
      return tour
    })
    return json
  }

  async privateTours(params = {}) {
    const json = await this.get('api2/private_tours', params)
    return json
  }

  async privateTour(paramsRaw) {
    const json = await this.get('api2/private_tour', paramsRaw)
    return json
  }

  async groupTours(params = {}) {
    const json = await this.get('api2/group_tours', params)
    return json
  }

  async groupTour(paramsRaw) {
    const json = await this.get('api2/group_tour', paramsRaw)
    return json
  }

  async generics(paramsRaw) {
    const params = Object.keys(paramsRaw)
      .filter( key => hasPrefix(key, ['id', 'categories.name', 'categories.parent'], 'generics'))
      .reduce( (res, key) => (res[key] = paramsRaw[key], res), {} );
    const json = await this.get('api/generics', params)
    return json
  }

  async generic(paramsRaw) {
    if (!isValidNumericId(paramsRaw['id'])) {
      console.warn("invalid id for generic")
      return {result: null}}
    const params = Object.keys(paramsRaw)
      .filter( key => ['id'].indexOf(key) !== -1 )
      .reduce( (res, key) => (res[key] = paramsRaw[key], res), {} );
    const json = await this.get('api/generic', params)
    return json
  }

  async documents(paramsRaw) {
    const params = Object.keys(paramsRaw)
      .filter( key => hasPrefix(key, ['categories.name', 'code', 'categories.parent'], 'documents'))
      .reduce( (res, key) => (res[key] = paramsRaw[key], res), {} );
    const json = await this.get('api/documents', params)
    return json
  }

  async document(paramsRaw) {
    if (!isValidNumericId(paramsRaw['id'])) {
      console.warn("invalid id for document")
      return {result: null}}
    const params = Object.keys(paramsRaw)
      .filter( key => ['id'].indexOf(key) !== -1 )
      .reduce( (res, key) => (res[key] = paramsRaw[key], res), {} );
    const json = await this.get('api/document', params)
    return json
  }

  async tour(paramsRaw) {
    if (!isValidNumericId(paramsRaw['id'])) {
      console.warn("invalid id for tour")
      return {result: null}}
    const params = Object.keys(paramsRaw)
      .filter( key => ['id'].indexOf(key) !== -1 )
      .reduce( (res, key) => (res[key] = paramsRaw[key], res), {} );
    const json = await this.get('api/tour', params)
    json.result['_next_departure'] = nextDeparture(json.result.options)
    json.result['_cheapest'] = cheapest(json.result.options)
    return json
  }

  async globaltix(endpoint:string, params = {}) {
    const json = await this.get('globaltix/' + endpoint, params)
    return json
  }

  async outboundGet(endpoint:string, params = {}) {
    const json = await this.get('outbound/' + endpoint, params)
    return json
  }

  async outboundPost(endpoint:string, params = {}, payload) {
    const json = await this.post('outbound/' + endpoint, params, payload)
    return json
  }

  cancelFlights() {
    this.cancelPendingGetRequests('rpc/flight_search_new')
  }

  async flights(params: FlightsParams, sample: boolean = false): Promise<TcResponse<FlightSearch>> {
    const converted = validateFlightsParams(params)
    if (converted == null) return {}

    const path = sample ? 'sample/flight_search.json' : 'rpc/flight_search_new'

    const json = await this.get(path, converted)
    //const json = await this.get('flight.json', converted)
    return json
  }

  async flightsChange(params: FlightsParams, sample: boolean = false): Promise<TcResponse<FlightSearch>> {
    const converted = validateFlightsChangeParams(params)
    if (converted == null) return {}

    const json = await this.get('rpc/flight_change_search', converted)

    return json
  }

  async flight(params: FlightParams, sample: boolean = false): Promise<TcResponse<FlightDetail>> {
    const converted = validateFlightParams(params)
    if (converted == null) return {}

    const path = sample ? 'sample/flight_detail.json' : 'rpc/flight_detail'

    const json = await this.get(path, converted)

    return json
  }

  async fareRules(params): Promise<TcResponse<FlightDetail>> {
    const path = 'rpc/fare_rules'

    const json = await this.get(path, params)

    return json
  }

  async flightAncillaries(params: FlightParams): Promise<TcResponse<FlightDetail>> {
    const converted = validateFlightParams(params)
    if (converted == null) return {}

    const json = await this.get('rpc/flight_ancillaries', converted)

    return json
  }

  async legacyHotels(
    params: LegacyHotelsParams) {

    const converted = validateLegacyHotelsParams(params)
    if (converted == null) return {}

    const json = await this.get('rpc/legacy_hotel_search', converted)

    return json
  }

  async legacyHotel(
    params: LegacyHotelParams) {

    const converted = validateLegacyHotelParams(params)
    if (converted == null) return {}

    const json = await this.get('rpc/legacy_hotel_detail', converted)
    return json
  }

  private removeKeys<T>(pairs: T, keysToRmove: string[]):Partial<T> {
      return Object.entries(pairs)
        .filter(([key, value]) => keysToRmove.indexOf( key ) === -1)
        .reduce((acc, [ key, value ]) => Object.assign(acc, { [key]: value }), {})
  } 

  async hotelbedsCheckRates(rateKeys: string[]): Promise<any> {

    return await Promise.all(rateKeys.map(rateKey => {
      return this.post( 'hotelbeds/1.0/checkrates', null, {
        "rooms": [ {rateKey} ]
      })
    }))
    
    /*
    return await fetch('https://edge2.travelcloud.app/hotelbeds/1.0/checkrates', {
      method: 'POST',
      mode: 'cors',
      headers: { 'Content-type': 'application/json'  },
      body: JSON.stringify({
        "rooms": [ {rateKey} ]
      })
    })
    .then(resp => resp.json())
    .then(resp => new Promise((resolve, reject) => {
      if (typeof resp.result.error !== 'undefined') {
        reject( resp.result.error.message )
      }

      else if (typeof resp.result.hotel !== 'undefined') {
        resolve(resp.result.hotel)
      }
    }))
     */
  }

  // todo: provide types
  async hotelbedsAvailability(formValue: HotelbedsFormValue): Promise<any> {
    const filteredFormValue = this.removeKeys<any>(formValue, ['destinationCode', 'destinationOrHotelName' ]);

    // combine occupancies with the same number of adults and children
    const occupancies = groupBy(filteredFormValue.occupancies, (item: any) => `${item.adults}-${item.children}`)
      .map(group => {
        return group.reduce((acc, { rooms, adults, children, paxes }) => 
          Object.assign({}, acc, { adults, children, rooms: parseInt(acc.rooms) + parseInt(rooms), paxes }), 
          { rooms: 0 })
      })
    return await this.post('hotelbeds/1.0/hotels', null,  { ...filteredFormValue, occupancies })
  }

  async hotelbedsFromStaticDataByDestinationCode(destinationCode:string): Promise<HotelbedsStaticData[]>  {
    return await fetch('https://static.travelcloud.app/hotelbeds/hotels?destinationCode=' + destinationCode)
      .then(resp => resp.json())
    .catch(x => console.error( x ))
}

  async hotelbedsFromStaticData(codes: Array<string>): Promise<HotelbedsStaticData[]>  {
    return await fetch('https://static.travelcloud.app/hotelbeds/hotels?codes=' + codes.join(','))
      .then(resp => resp.json())
    .catch(x => console.error( x ))
  }

  // todo: provide proper type
  async hotelbedsCategories(): Promise<any> {
    return await fetch('https://static.travelcloud.app/hotelbeds/categories')
      .then(resp => resp.json())
    .catch(x => console.error( x ))
  }

  // todo: provide proper type
  async hotelbedsRateComments(hotel: number, codes: number[]): Promise<any> {
    let str = codes.join(',')
    return await fetch(`https://static.travelcloud.app/hotelbeds/ratecomments?hotel=${hotel}&codes=${str}`)
      .then(resp => resp.json())
    .catch(x => console.error( x ))
  }

  async hotelbedsFacilityTypes(): Promise<any> {
    return await fetch('https://static.travelcloud.app/hotelbeds/facilitiyTypes')
      .then(resp => resp.json())
    .catch(x => console.error( x ))
  }

  async hotelbedsTerminalTypes(): Promise<any> {
    return await fetch('https://static.travelcloud.app/hotelbeds/terminalTypes')
      .then(resp => resp.json())
    .catch(x => console.error( x ))
  }

  async hotelbedsImageTypes(): Promise<any> {
    return await fetch('https://static.travelcloud.app/hotelbeds/imageTypes')
      .then(resp => resp.json())
    .catch(x => console.error( x ))
  }

  async getRoomCharacteristicMap(chArr: Array<string>) : Promise<any>{
    if(chArr.length || chArr == null) 
      return null
    else 
      return await fetch('https://static.travelcloud.app/hotelbeds/rooms?chs='+ chArr.join(','))
        .then(resp => resp.json())
        .catch(x => console.error( x ))
  }
  // todo: provide proper type
  async boards(): Promise<any> {
    return await fetch('https://static.travelcloud.app/hotelbeds/boards')
      .then(resp => resp.json())
    .catch(x => console.error( x ))
  }

  async hotelbedsSearch(searchFormValue: HotelbedsFormValue, limit = 2000): Promise<any> {

    let hotelsStaticData
    let formValue = Object.assign({}, searchFormValue)
    let isSearchByHotel = searchFormValue.hotels != null && 
            searchFormValue.hotels.hotel != null && 
            searchFormValue.hotels.hotel.length > 0;

    if (formValue.destinationCode != null && formValue.destinationCode !== '') {
        hotelsStaticData = await this.hotelbedsFromStaticDataByDestinationCode(formValue.destinationCode)

      if (hotelsStaticData != null) {

        let hotelCodes = hotelsStaticData.map(x => x.code).splice( 0, limit )

        if ( isSearchByHotel ) {
              hotelCodes = [ Number(searchFormValue.hotels.hotel[0]) ].concat(
                hotelCodes.filter(code => code != formValue.hotels.hotel[0])
              ).splice(0,limit)
        }

        formValue.hotels = {
          hotel: hotelCodes
        }

      }
    }

    const resp = await this.hotelbedsAvailability(formValue)

    if (typeof resp != 'undefined') {

      if (resp.error != null) {
        return resp
      }

      else if (resp.result != null && resp.result.hotels != null) {
      
        if (resp.result.hotels['total'] != null && resp.result.hotels['total'] > 0 && resp.result.hotels['hotels'] != null) {

          if (hotelsStaticData == null) {
            hotelsStaticData = await this.hotelbedsFromStaticData(resp.result.hotels['hotels'].map(x => x.code));
          } 
          
          let resultant = resp.result.hotels['hotels'].map(hotel => {

            const details = hotelsStaticData.find(x => x.code == hotel.code);
            if (typeof details !== 'undefined'){
                return Object.assign({}, hotel, { details });
            }
          })
          

          if ( isSearchByHotel ) {

            const hotelDisplayOnTop = resultant.find(r => r.code == searchFormValue.hotels.hotel[0])
            if( hotelDisplayOnTop != null ){
              resultant = resultant.filter(r => r.code != searchFormValue.hotels.hotel[0])
              resultant.unshift({...hotelDisplayOnTop, _skipFiltersAndSort: true});
            }

            if ( ! resp.result.hotels['hotels'].some(({code}) => searchFormValue.hotels.hotel[ 0 ] == code)) {
  
              let details = hotelsStaticData.find(x => x.code == searchFormValue.hotels.hotel[ 0 ])

              if ( details != null ) {
                let categories = await this.hotelbedsCategories()

                if (categories != null) {
                  let board = categories.find(x => x.code == details.categoryCode)

                  board != null && resultant.unshift({ 
                    code: searchFormValue.hotels.hotel[0],
                    rooms: [],
                    details,
                    categoryName: board.description.content,
                    _skipFiltersAndSort: true
                  })
                }
              }

            }
          }

          resp.result.hotels[ 'hotels' ] = resultant
        }

        console.log('result about to return', resp.result.hotels)

        return resp.result.hotels
        
      }
    }
  }

  async order(params) {
    if (params['ref'] == null) {
      console.warn("order requires ref")
      return {result: null}}
    return this.get('api/order', params, true)
  }

  async categories(params) {
    return this.get('api/categories', params)
  }

  async submitContactForm(structured: {
    referral_site: string,
    subject: string,
    customer_name: string,
    customer_email: string
  }, unstructured: {[key: string]: string}) {
    const {referral_site, subject, customer_name, customer_email} = structured
    const payload = {
      referral_site, subject,
      '~customer_add_unstructured_message': {
        unstructured, customer_name, customer_email
      }
    }
    return await this.post('api/conversation', {}, payload)
  }

  async emailAdmins(structured: {
    referral_site?: string,
    subject?: string,
    customer_name?: string,
    customer_email: string,
    admin_emails?: string[]
  }, unstructured: {[key: string]: string}) {
    //const {referral_site, subject, customer_name, customer_email} = structured
    const payload = JSON.parse(JSON.stringify(structured))
    payload.unstructured = unstructured
    return await this.post('rpc/email_admins', {}, payload)
  }

  async subscribeEdm(structured: {
    email: string,
    mailchimp_list_id?: string,
    sendinblue_list_ids?: string[],
    hubspot_list_id?: number,
    attributes: {[key: string]: string}
  }) {
    if (structured.mailchimp_list_id == null && structured.sendinblue_list_ids == null && structured.hubspot_list_id == null) {
      throw new Error("list id missing")
    }
    if (validateEmail(structured.email) === false) {
      throw new Error("Email is invalid")
    }
    const payload = JSON.parse(JSON.stringify(structured))
    return await this.post('rpc/subscribe_edm', {}, payload)
  }
}

// Source: https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript
export function validateEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

export const validateFlightsParams = (state) => {
  const isValidCode = (x) => x != null && x.length === 3
  const nullOrEmpty = (x) => x == null || x === ""
  const nullOrZero = (x) => x == null || parseInt(x) <= 0
  if (state == null) {
    console.log('state is null')
    return null
  }
  if (!isValidCode(state['od1.origin_airport.code']) && !isValidCode(state['od1.origin_city.code'])) {
    console.log('od1 is invalid')
    return null
  }
  if (!isValidCode(state['od2.origin_airport.code']) && !isValidCode(state['od2.origin_city.code'])) {
    console.log('od2 is invalid')
    return null
  }
  if (nullOrEmpty(state['od1.origin_datetime'])) {
    console.log('od1 date is invalid')
    return null
  }
  if (nullOrZero(state.ptc_adt)) {
    console.log('zero adults')
    return null
  }

  const filtered = {
    source: state.source,
    "od1.origin_airport.code": state["od1.origin_airport.code"],
    "od1.origin_city.code": state["od1.origin_city.code"],
    "od1.origin_datetime": state["od1.origin_datetime"],
    "od1.destination_airport.code": state["od1.destination_airport.code"],
    "od1.destination_city.code": state["od1.destination_city.code"],

    "od2.origin_airport.code": state["od2.origin_airport.code"],
    "od2.origin_city.code": state["od2.origin_city.code"],
    "od2.origin_datetime": state["od2.origin_datetime"],
    "od2.destination_airport.code": state["od2.destination_airport.code"],
    "od2.destination_city.code": state["od2.destination_city.code"],
    
    "od3.origin_airport.code": state["od3.origin_airport.code"],
    "od3.origin_city.code": state["od3.origin_city.code"],
    "od3.origin_datetime": state["od3.origin_datetime"],
    "od3.destination_airport.code": state["od3.destination_airport.code"],
    "od3.destination_city.code": state["od3.destination_city.code"],

    "od4.origin_airport.code": state["od4.origin_airport.code"],
    "od4.origin_city.code": state["od4.origin_city.code"],
    "od4.origin_datetime": state["od4.origin_datetime"],
    "od4.destination_airport.code": state["od4.destination_airport.code"],
    "od4.destination_city.code": state["od4.destination_city.code"],
    ptc_adt: state.ptc_adt,
    ptc_cnn: state.ptc_cnn,
    ptc_inf: state.ptc_inf,
    cabin: state.cabin || 'Y'
  }

  if (filtered['od2.origin_datetime'] == null) delete filtered['od2.origin_datetime']
  if (!isValidCode(filtered['od1.destination_airport.code'])) delete filtered['od1.destination_airport.code']
  if (!isValidCode(filtered['od1.destination_city.code'])) delete filtered['od1.destination_city.code']
  if (!isValidCode(filtered['od1.origin_airport.code'])) delete filtered['od1.origin_airport.code']
  if (!isValidCode(filtered['od1.origin_city.code'])) delete filtered['od1.origin_city.code']

  if (!isValidCode(filtered['od2.destination_airport.code'])) delete filtered['od2.destination_airport.code']
  if (!isValidCode(filtered['od2.destination_city.code'])) delete filtered['od2.destination_city.code']
  if (!isValidCode(filtered['od2.origin_airport.code'])) delete filtered['od2.origin_airport.code']
  if (!isValidCode(filtered['od2.origin_city.code'])) delete filtered['od2.origin_city.code']
  if (nullOrEmpty(filtered['od2.origin_datetime'])) delete filtered['od2.origin_datetime']

  if (!isValidCode(filtered['od3.destination_airport.code'])) delete filtered['od3.destination_airport.code']
  if (!isValidCode(filtered['od3.destination_city.code'])) delete filtered['od3.destination_city.code']
  if (!isValidCode(filtered['od3.origin_airport.code'])) delete filtered['od3.origin_airport.code']
  if (!isValidCode(filtered['od3.origin_city.code'])) delete filtered['od3.origin_city.code']
  if (nullOrEmpty(filtered['od3.origin_datetime'])) delete filtered['od3.origin_datetime']

  if (!isValidCode(filtered['od4.destination_airport.code'])) delete filtered['od4.destination_airport.code']
  if (!isValidCode(filtered['od4.destination_city.code'])) delete filtered['od4.destination_city.code']
  if (!isValidCode(filtered['od4.origin_airport.code'])) delete filtered['od4.origin_airport.code']
  if (!isValidCode(filtered['od4.origin_city.code'])) delete filtered['od4.origin_city.code']
  if (nullOrEmpty(filtered['od4.origin_datetime'])) delete filtered['od4.origin_datetime']
  
  if (nullOrZero(filtered.ptc_cnn)) delete filtered['ptc_cnn']
  if (nullOrZero(filtered.ptc_inf)) delete filtered['ptc_inf']

  return filtered
}

export const validateFlightsChangeParams = (state) => {
  const isValidCode = (x) => x != null && x.length === 3
  const nullOrEmpty = (x) => x == null || x === ""
  if (state == null) return null
  if (!isValidCode(state['od1.origin_airport.code']) && !isValidCode(state['od1.origin_city.code'])) return null
  if (!isValidCode(state['od2.origin_airport.code']) && !isValidCode(state['od2.origin_city.code'])) return null
  if (nullOrEmpty(state['od1.origin_datetime'])) return null
  if (nullOrEmpty(state['product_source_ref'])) return null

  const filtered = {
    source: state.source,
    "od1.origin_airport.code": state["od1.origin_airport.code"],
    "od1.origin_city.code": state["od1.origin_city.code"],
    "od1.origin_datetime": state["od1.origin_datetime"],
    "od1.destination_airport.code": state["od1.destination_airport.code"],
    "od1.destination_city.code": state["od1.destination_city.code"],

    "od2.origin_airport.code": state["od2.origin_airport.code"],
    "od2.origin_city.code": state["od2.origin_city.code"],
    "od2.origin_datetime": state["od2.origin_datetime"],
    "od2.destination_airport.code": state["od2.destination_airport.code"],
    "od2.destination_city.code": state["od2.destination_city.code"],
    
    "od3.origin_airport.code": state["od3.origin_airport.code"],
    "od3.origin_city.code": state["od3.origin_city.code"],
    "od3.origin_datetime": state["od3.origin_datetime"],
    "od3.destination_airport.code": state["od3.destination_airport.code"],
    "od3.destination_city.code": state["od3.destination_city.code"],

    "od4.origin_airport.code": state["od4.origin_airport.code"],
    "od4.origin_city.code": state["od4.origin_city.code"],
    "od4.origin_datetime": state["od4.origin_datetime"],
    "od4.destination_airport.code": state["od4.destination_airport.code"],
    "od4.destination_city.code": state["od4.destination_city.code"],
    cabin: state.cabin || 'Y',
    product_source_ref: state.product_source_ref,
  }

  if (filtered['od2.origin_datetime'] == null) delete filtered['od2.origin_datetime']
  if (!isValidCode(filtered['od1.destination_airport.code'])) delete filtered['od1.destination_airport.code']
  if (!isValidCode(filtered['od1.destination_city.code'])) delete filtered['od1.destination_city.code']
  if (!isValidCode(filtered['od1.origin_airport.code'])) delete filtered['od1.origin_airport.code']
  if (!isValidCode(filtered['od1.origin_city.code'])) delete filtered['od1.origin_city.code']

  if (!isValidCode(filtered['od2.destination_airport.code'])) delete filtered['od2.destination_airport.code']
  if (!isValidCode(filtered['od2.destination_city.code'])) delete filtered['od2.destination_city.code']
  if (!isValidCode(filtered['od2.origin_airport.code'])) delete filtered['od2.origin_airport.code']
  if (!isValidCode(filtered['od2.origin_city.code'])) delete filtered['od2.origin_city.code']
  if (nullOrEmpty(filtered['od2.origin_datetime'])) delete filtered['od2.origin_datetime']

  if (!isValidCode(filtered['od3.destination_airport.code'])) delete filtered['od3.destination_airport.code']
  if (!isValidCode(filtered['od3.destination_city.code'])) delete filtered['od3.destination_city.code']
  if (!isValidCode(filtered['od3.origin_airport.code'])) delete filtered['od3.origin_airport.code']
  if (!isValidCode(filtered['od3.origin_city.code'])) delete filtered['od3.origin_city.code']
  if (nullOrEmpty(filtered['od3.origin_datetime'])) delete filtered['od3.origin_datetime']

  if (!isValidCode(filtered['od4.destination_airport.code'])) delete filtered['od4.destination_airport.code']
  if (!isValidCode(filtered['od4.destination_city.code'])) delete filtered['od4.destination_city.code']
  if (!isValidCode(filtered['od4.origin_airport.code'])) delete filtered['od4.origin_airport.code']
  if (!isValidCode(filtered['od4.origin_city.code'])) delete filtered['od4.origin_city.code']
  if (nullOrEmpty(filtered['od4.origin_datetime'])) delete filtered['od4.origin_datetime']

  return filtered
}

export const validateFlightParams = (state) => {
  if (state == null) return null
  if (state['od1.id'] == null) return null
  if (state.ptc_adt == null) return null

  const filtered = {
    source: state.source,
    'od1.id': state['od1.id'],
    'od2.id': state['od2.id'],
    'pricing.id' : state['pricing.id'],
    ptc_adt: state.ptc_adt,
    ptc_cnn: state.ptc_cnn,
    ptc_inf: state.ptc_inf,
  }

  if (filtered['od2.id'] == null) delete filtered['od2.id']
  if (filtered['pricing.id'] == null) delete filtered['pricing.id']
  return filtered
}

export const validateHotelbedsParams = (state) => {
  if (state == null) return null
  if (state.stay == null || state.stay.checkIn == null || state.stay.checkOut == null) return null
  if ((state.hotels == null || state.hotels.hotel.length == 0) && state.destinationCode == null) {
    return null
  }

  if (state.occupancies == null || state.occupancies.length == 0) return null
  
  const output = state.occupancies.map(elem => {
    if (elem.adults == 0) return null
    if (elem.children > 0) {
      if (typeof elem.paxes == 'undefined') return null
      if (elem.children != elem.paxes.length) return null
      if (elem.paxes.filter(px => typeof px.age == 'undefined' || px.age == 0 || px.age == null).length > 0) return null
    }
    return true;
  })

  if (output.filter(x => x == null).length > 0)
    return null

  return true
}

export const validateLegacyHotelParams = (state) => {
  if (state == null) return null
  if (state.hotelId == null) return null
  if (state.checkInDate == null) return null
  if (state.checkOutDate == null) return null
  if (state.adults == null) return null

  return {
    hotelId: state.hotelId,
    checkInDate: moment(state.checkInDate).format('YYYY-MM-DD'),
    duration: moment(state.checkOutDate).diff(state.checkInDate, 'days'),
    adults: state.adults,
    children: state.children || 0
  }}

export const validateLegacyHotelsParams = (state) => {
  if (state == null) return null
  if (state.cityCode == null) return null
  if (state.checkInDate == null) return null
  if (state.checkOutDate == null) return null
  if (state.adults == null) return null

  return {
    cityCode: state.cityCode,
    checkInDate: moment(state.checkInDate).format('YYYY-MM-DD'),
    duration: moment(state.checkOutDate).diff(state.checkInDate, 'days'),
    adults: state.adults,
    children: state.children || 0
  }}

// mutates each option to include _next_departure
export function nextDeparture(options) {
  if (options.length === 0) return null;
  for (var index in options) {
    const option = options[index]
    const departuresOrSubset = option.departures || option._subset_departures
    if (option['on_demand_advance_booking'] === '0') {
      option['_next_departure'] = departuresOrSubset.reduce(
        (acc2, departure) =>
          parseInt(departure.slots_total) > parseInt(departure.slots_taken)
            ? (acc2 == null || acc2 > departure.date ? departure.date : acc2)
            : acc2, null)

      option['_last_departure'] = departuresOrSubset.reduce(
        (acc2, departure) =>
          parseInt(departure.slots_total) > parseInt(departure.slots_taken)
            ? (acc2 == null || acc2 < departure.date ? departure.date : acc2)
            : acc2, null)
    } else {
      const todayDate = (newDateLocal()).getDate()
      const minDate = newDateLocal()
      const maxDate = newDateLocal()
      minDate.setDate(todayDate + parseInt(option['min_lead_time']))
      maxDate.setDate(todayDate + parseInt(option['on_demand_advance_booking']))
      option['_next_departure'] = dateToIsoDate(minDate)
      option['_last_departure'] = dateToIsoDate(maxDate)
    }
  }

  return options.reduce((acc, option) =>
    acc === false || acc > option['_next_departure'] ? option['_next_departure'] : acc, false)
}

function cheapest(options) {
  return options.reduce((acc, price) => acc == null|| acc > price.TWN ? price.TWN : acc, null);
}

export function dateToIsoDate(date: Date): string {
  const pad = (x: number) => x < 10 ? '0' + x : x
  return "" + date.getFullYear() + '-' + pad(date.getMonth() + 1) + '-' + pad(date.getDate())
}

export function extractValueFromFormState(form): any {
  const acc = {}
  for (var key in form) {
    if (form.hasOwnProperty(key)) {
      const val = form[key].value
      // typeof Array is also object
      if(Array.isArray(val)) {
        // assume array of objects
        // not sure how to distinguish between array of objects and array of primitives
        acc[key] = val.map(item => extractValueFromFormState(item))
      }
      else if (typeof val === 'object') {
        acc[key] = extractValueFromFormState(val)
      }
      else {
        acc[key] = val
      }
    }
  }
  return acc
}

// mutate existing to avoid re-rendering form in pure components
// only works for values and array of objects
export function mergeFormState(existing, updates) {
  if (updates == null) return existing
  if (existing == null) existing = {}

  for (let key in updates) {
    const val = updates[key]
    if (Array.isArray(val)) {
      for (let index in val) {
        existing[key][index] = mergeFormState(existing[key][index], val[index])
      }
    } else {
      existing[key] = val
    }
  }

  return existing
}

export const updateFormState = (rc: React.Component, key: string) =>
  (changes) => {
    rc.setState({[key]: mergeFormState(rc.state[key], changes)})}

export function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// Probably should reimplement as context provider when it is fixed
// https://github.com/zeit/next.js/issues/4194
export class Cart {
  static localStorageTestKey = '__LOCAL_STORAGE_TEST__'
  static localStorageOrderKey = '-travelcloud-order'
  static localStorageCustomerKey = '-travelcloud-customer'
  static localStoragePriceRulesKey = '-travelcloud-priceRules'
  static localStorageTokenKey = '-travelcloud-token'
  public demo: boolean = false

  public limits
  public customer
  public order
  public lastComputed
  private token
  private openIdToken
  private checkoutInProgreass = []
  public priceRules: TcResponse<PriceRules> = {}
  private bcryptCache = {}

  private client: TravelCloudClient
  private onOrderChange: (order: any) => any
  private onCustomerChange: (customer: any) => any
  private onPriceRulesChange: (priceRules: TcResponse<PriceRules>) => any

  private getStateFromLocalStorage() {
    var order = {}
    var priceRules: any = null
    var customer: any = null
    var token = null
    var localStorageAvailable = false

    try {
      localStorage.setItem(this.client.config.tcUser + Cart.localStorageTestKey, Cart.localStorageTestKey)
      localStorage.removeItem(this.client.config.tcUser + Cart.localStorageTestKey)
      order = JSON.parse(localStorage.getItem(this.client.config.tcUser + Cart.localStorageOrderKey))
      priceRules = JSON.parse(sessionStorage.getItem(this.client.config.tcUser + Cart.localStoragePriceRulesKey))
      customer = JSON.parse(localStorage.getItem(this.client.config.tcUser + Cart.localStorageCustomerKey))
      token = localStorage.getItem(this.client.config.tcUser + Cart.localStorageTokenKey)
      localStorageAvailable = true

    } catch(e) {}

    const result = {
      order: {result: order}, customer: {result: customer}, token, priceRules: {result: priceRules},
      localStorageAvailable,
    }

    return result
  }

  // for order/customer
  // null - server-side, not initialized
  // {} - nothing to show/empty
  constructor(
    options:
      { client: TravelCloudClient,
        priceRules?: any,
        onOrderChange: (order: any) => any,
        onCustomerChange: (customer: any) => any,
        onPriceRulesChange?: (priceRules: TcResponse<PriceRules>) => any,
      }) {
    this.client = options.client
    if (options.onOrderChange != null) this.onOrderChange = (x) => options.onOrderChange(Object.assign({}, x))
    if (options.onCustomerChange != null) this.onCustomerChange = (x) => options.onCustomerChange(Object.assign({}, x))
    if (options.onPriceRulesChange != null) this.onPriceRulesChange = (x) => options.onPriceRulesChange(Object.assign({}, x))

    const restored = this.getStateFromLocalStorage()

    if (options.priceRules?.result != null) {
      this.priceRules = options.priceRules
      this.setPriceRules(this.priceRules)
    } else {
      if (restored?.priceRules?.result != null) {
        this.priceRules = restored.priceRules
        this.setPriceRules(this.priceRules)
      }
    }
    
    this.token = restored.token
    this.updateCustomer(restored['customer'])
    this.updateOrder(restored['order'])
  }

  // for testing
  saveOrderSnapshot() {
    const str = JSON.stringify(this.order)
    localStorage.setItem(this.client.config.tcUser + 'orderSnapshotStr', str)
  }

  restoreOrderSnapshot() {
    const str = localStorage.getItem(this.client.config.tcUser + 'orderSnapshotStr')
    this.updateOrder(JSON.parse(str))
  }

  async getPassword(email: string) {
    email = email.trim()
    const json = await this.client.post('api/customer', {email}, {'~send_login_password': {method: 'email'}})
    return json
  }

  async login(email: string, login_password: string) {
    email = email.trim()
    login_password = login_password.trim()
    const json = await this.client.post('api/customer', {email}, {'~get_login_token': {login_password}})

    if (json.result != null) {
      //json.result['_discounts'] = json.result.categories.reduce((acc, cat) => Object.assign(acc, parseCategoryDescription(cat.description)), {})
      this.updateCustomer(json)
      this.token = json.result['_get_login_token']
      try {
        localStorage.setItem(this.client.config.tcUser + Cart.localStorageTokenKey, json.result['_get_login_token'])
        localStorage.setItem(this.client.config.tcUser + Cart.localStorageCustomerKey, JSON.stringify(json.result))
      } catch (e) {}
    } else {
      this.updateCustomer({})
      try {
        localStorage.removeItem(this.client.config.tcUser + Cart.localStorageTokenKey)
        localStorage.removeItem(this.client.config.tcUser + Cart.localStorageCustomerKey)
      } catch (e) {}
    }
    return json
  }

  async openIdLogin(id_token: string) {
    this.openIdToken = id_token
    this.token = null
    const id_token_split = id_token.split('.')
    const payload = JSON.parse(atob(id_token_split[1].replace(/_/g, '/').replace(/-/g, '+')))

    const json = {
      result: {
        email: payload.email,
        name: payload.name,
        openid_aud: payload.aud,
        openid_iss: payload.iss,
        openid_sub: payload.sub
      }
    }

    // we don't persist data from openID by design
    // allow the openID library to manage token expiry
    this.updateCustomer(json, true)

    return json
  }

  priceRulesWithCheckoutCode(code: string): PriceRules {
    if (code == null) return []
    return filterPriceRulesWithCheckoutCode(code, this.priceRules.result, this.bcryptCache, this.customer)
  }

  setCheckoutCode(checkoutCode: string) {
    const order = Object.assign({}, this.order.result || {})
    if (checkoutCode == null) delete order.checkout_code
    else order.checkout_code = checkoutCode.trim()
    this.updateOrder({result: order})
  }

  async payOrderWithCustomerCredit(ref: string, amount: string = null) {
    const customer = this.customer
    const login_token = this.token
    if (login_token == null || customer == null || customer.result == null) return

    const payload = {
      '~authenticate_with_login_token': {login_token}, '~pay_order': {ref}
    }

    //@ts-ignore
    if (amount != null) payload['~pay_order'].amount = amount

    const json = await this.client.post
      ('api/customer'
      , { email: customer.result.email }
      , payload)

    if (json.result != null) {
      this.updateCustomer(json)
      localStorage.setItem(this.client.config.tcUser + Cart.localStorageCustomerKey, JSON.stringify(json.result))
    }

    return json
  }

  logout() {
    this.updateCustomer({})
    this.updateOrder({})
    this.token = null
    this.openIdLogin = null
    localStorage.removeItem(this.client.config.tcUser + Cart.localStorageTokenKey)
  }

  async refreshCustomer() {
    const customer = this.customer
    const login_token = this.token
    if (login_token == null || customer == null || customer.result == null) return

    const json = await this.client.post('api/customer', {email: customer.result.email}, {'~authenticate_with_login_token': {login_token}})

    if (json.result != null) {
      this.updateCustomer(json)
      try {
        localStorage.setItem(this.client.config.tcUser + Cart.localStorageCustomerKey, JSON.stringify(json.result))
      } catch (e) {}
    }
  }

  addTour(tour, tourFormValue: TourFormValue, mock = false): Product | null {
    const computed: any = computeTour(tour, tourFormValue)
    this.lastComputed = computed
    if (computed.product == null) return null
    const tourFormValueClone: any = {...tourFormValue}
    // todo: filter all unknown properties
    delete tourFormValueClone.rooms_selected
    computed.product.form = tourFormValueClone
    if (computed.total == null) return null
    return this.addProduct(computed.product, mock)
  }

  addAnyTour(privateTour: v2_PrivateTour | v2_GroupTour, AnyTourFormValue: AnyTourFormValue, tourType: TourType, mock = false): Product | null {
    const computed: any = computeAnyTour(privateTour, AnyTourFormValue, tourType)
    this.lastComputed = computed
    if (computed.product == null) return null
    const tourFormValueClone: any = {...AnyTourFormValue}
    computed.product.form = tourFormValueClone
    computed.product._price_rules = privateTour.price_rules
    if (computed.total == null) return null
    return this.addProduct(computed.product, mock)
  }

  addGlobaltix(item, globaltixFormValue: GlobaltixFormValue, mock = false): Product | null {
    const computed: any = computeGlobaltix(item, globaltixFormValue)
    this.lastComputed = computed
    if (computed.product == null) return null
    computed.product.form = globaltixFormValue
    return this.addProduct(computed.product, mock)
  }

  addLegacyHotel(details, legacyHotelFormValue: LegacyHotelFormValue, mock = false) {
    const computed: any = computeLegacyHotel(details, legacyHotelFormValue)
    this.lastComputed = computed
    if (computed.product == null) return null
    computed.product.form = legacyHotelFormValue
    return this.addProduct(computed.product, mock)
  }

  addOutboundTour(details, outboundFormValue: OutboundFormValue, mock = false) {
    const computed: any = computeOutbound(details, outboundFormValue)
    this.lastComputed = computed
    if (computed.product == null) return null
    computed.product.form = outboundFormValue
    computed.product._price_rules = details.price_rules
    return this.addProduct(computed.product, mock)
  }

  // according to the best practice described by hotelbeds,
  // more than one rateKey shouldn't send per request,
  // @param hotel is expected to have only one room having only one rate
  // Ref: https://developer.hotelbeds.com/docs/read/apitude_booking/Best_Practices
  addHotelbedsHotel(data: {static: any, checkrates: HotelbedsCheckRateResponse},
      bookingForm: HotelbedsBookingForm, 
      mock = false) {
    
    const computed: any = computeHotelbeds( data, bookingForm )
    this.lastComputed = computed

    if (computed.product == null) return null
    computed.product.form = bookingForm;

    return this.addProduct(computed.product, mock)
  }
  addFlight(flight: FlightDetail, pricing_id: string, mock = false) {
    return this.addFlightWithAncillaries(flight, {pricing_id}, mock)
  }

  addFlightWithAncillaries(flight: FlightDetail, ancillaries: {pricing_id: string, seats?: any, services?: any, prime_workflow?: boolean, search_id?: string}, mock = false) {
    const pricingId = ancillaries.pricing_id
    const selectedFare = flight.pricings.find((x) => x.id === pricingId)
    if (selectedFare == null) return null

    const ptc_adt = selectedFare.ptc_breakdown_map['adt'].quantity
    const ptc_cnn = selectedFare.ptc_breakdown_map['cnn'] != null ? selectedFare.ptc_breakdown_map['cnn'].quantity : 0
    const ptc_inf = selectedFare.ptc_breakdown_map['inf'] != null ? selectedFare.ptc_breakdown_map['inf'].quantity : 0

    const form: FlightFormValue
      = {
        od1_id: flight.od1.id,
        pricing_id: pricingId,
        ptc_adt, ptc_cnn, ptc_inf}

    if (flight.od2 != null) form['od2_id'] = flight.od2.id
    if (flight.od3 != null) form['od3_id'] = flight.od3.id
    if (flight.od4 != null) form['od4_id'] = flight.od4.id
      
    if (ancillaries != null && ancillaries.seats != null) {
      form.seats = ancillaries.seats
    }

    if (ancillaries != null && ancillaries.services != null) {
      form.services = ancillaries.services
    }

    if (ancillaries != null && ancillaries.prime_workflow != null) {
      form.prime_workflow = ancillaries.prime_workflow
    }

    const search_id = ancillaries?.search_id
    if (search_id != null) form.search_id = search_id

    const computed: any = computeFlight(flight, form)
    this.lastComputed = computed

    computed.product.form = form
    if (computed.total == null) return null
    return this.addProduct(computed.product, mock)
  }

  addFlightChangeWithAncillaries(flight: FlightDetail, pricingId: string, product_source_ref: string, ancillaries: {seats?: any}, mock = false) {
    const selectedFare = flight.pricings.find((x) => x.id === pricingId)
    if (selectedFare == null) return null

    const form: FlightChangeFormValue = {
      od1_id: flight.od1.id,
      pricing_id: pricingId,
      product_source_ref
    }

    if (flight.od2 != null) form['od2_id'] = flight.od2.id
    if (flight.od3 != null) form['od3_id'] = flight.od3.id
    if (flight.od4 != null) form['od4_id'] = flight.od4.id

    if (ancillaries != null && ancillaries.seats != null) {
      form.seats = ancillaries.seats
    }

    const computed: any = computeFlightChange(flight, form)
    this.lastComputed = computed
    if (computed.product == null) return null
    computed.product.form = form
    return this.addProduct(computed.product, mock)
  }

  addFlightChangeServices(flight: FlightDetail, product_source: string, product_source_ref: string, ancillaries: {services?: any, seats?: any}, mock = false) {
    const form: FlightChangeServiceFormValue
      = {product_source, product_source_ref, services: ancillaries.services, seats: ancillaries.seats}

    const computed: any = computeFlightChangeServices(flight, form)
    this.lastComputed = computed
    if (computed.product == null) return null
    computed.product.form = form
    return this.addProduct(computed.product, mock)
  }

  addGeneric(generic, params: {generic_id: string, option_id: string, quantity: number, unstructured?}, mock = false) {
    const computed: any = computeGeneric(generic, params)
    this.lastComputed = computed
    computed.product.form = params
    computed.product.product_detail = generic
    if (computed.total == null) return null
    return this.addProduct(computed.product, mock)
  }

  checkout(checkoutFormValue): Promise<TcResponse<Order>> {
    return new Promise(async (resolve, reject) => {
      this.checkoutInProgreass.push(resolve)
      if (this.checkoutInProgreass.length !== 1) return

      const order = Object.assign({}, this.order.result || {}, checkoutFormValue)

      // server format is not the same
      const oc = JSON.parse(JSON.stringify(order))
      const headers = {}
      oc['~set_email'] = oc['email']

      delete oc['email']
      delete oc['payment_required']
      delete oc['deposit_required']
      delete oc['from_date']
      delete oc['to_date']
      delete oc['limit_code']
      delete oc['limit_discount']

      const customer = this.customer
      const login_token = this.token
      const openIdToken = this.openIdToken

      if (customer != null && customer.result != null) {
        if (login_token != null)
        oc['~customer_authenticate_with_login_token'] = {
          email : customer.result.email,
          login_token: login_token
        }
        
        else if (openIdToken != null) {
          headers['authorization'] = "Bearer " + openIdToken
        }
      }

      const addRemoveProducts = oc['products'].map(product => {
        if (product.product_type === 'tour_package') {
          return {
            tour: product.form
          }
        } else if (product.product_type === 'private_tour') {
          return {
            private_tour: product.form
          }
        } else if (product.product_type === 'group_tour') {
          return {
            group_tour: product.form
          }
        } else if (product.product_type === 'flight') {
          return {
            flight: product.form
          }
        } else if (product.product_type === 'flight/change') {
          return {
            "flight/change": product.form
          }
        } else if (product.product_type === 'flight/change_services') {
          return {
            "flight/change_services": product.form
          }
        } else if (product.product_type === 'nontour_product') {
          return {
            generic: product.form
          }
        } else if (product.product_source === 'hotelbeds') {
          return {
            hotelbeds: product.form
          }
        } else if (product.product_type === 'hotel_room') {
          return {
            legacy_hotel: product.form
          }
        } else if (product.product_source === 'globaltix') {
          return {
            globaltix: product.form
          }
        } else if (product.product_source === 'outbound') {
          return {
            outbound: product.form
          }
        }
        
        throw new Error("Unknown product type " + product.product_type)
      })

      delete oc['products']
      oc['~add_remove_products'] = addRemoveProducts

      if (addRemoveProducts.length === 0) throw new Error("Cart is empty")

      const json = await this.client.post(`api/order`, null, oc, headers)

      if (json.result != null) {
        this.updateOrder({})
        try {
          localStorage.removeItem(this.client.config.tcUser + Cart.localStorageOrderKey)
        } catch(e) {
          console.warn("Cannot save order details to localStorage")
        }
      }

      for (var res of this.checkoutInProgreass) {
        res(json)
      }
      this.checkoutInProgreass = []
    })
  }

  reset() {
    const order: TcResponse<any> = {}
    if (this.order != null && this.order.result != null) {
      order.result = {}
      if (this.order.result.checkout_code != null) order.result.checkout_code = this.order.result.checkout_code
    }
    this.updateOrder(order)
    return this
  }

  addProduct(product: Product, mock: boolean = false): Product | null {
    const order = JSON.parse(JSON.stringify(this.order.result || {}))

    order['products'] = order['products'] || []

    if (mock) {
      order['products'].push(product)
      computePriceRules(order, this.priceRules.result, this.bcryptCache, this.customer.result)
    } else {
      // if we find that an existing product already exists, we overwrite it
      // merging products is too complex due to pricing and availability
      const existingProductIndex = order['products'].findIndex(existingProduct =>
        product.product_id === existingProduct.product_id
        && product.product_type === existingProduct.product_type
        && product.product_code === existingProduct.product_code
        && product.product_source === existingProduct.product_source
        && product.product_name === existingProduct.product_name)

      const quantity = parseInt(product.quantity, 10)
      if (!isNaN(quantity) && quantity > 0) {
        const window2: any = window

        var source = product.product_source
        const tcUser = window2.tcUser || 'unknown'
        const currency = window2.tcCurrency || 'SGD'
        const price = product.items.reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0)).toFixed(2)

        if (source === 'db' || source === 'self' || source === '' || source == null) source = 'travelcloud'

        const gtag = window2.gtag
        if (gtag != null) {
          gtag('event', 'add_to_cart', {
            'send_to' : 'travel_click',
            "items": [
              {
                "id": [tcUser, source, product.product_type, product.product_id].join('/'),
                "name": product.product_name,
                "list_name": tcUser,
                "brand": source,
                "category": product.product_type,
                "quantity": product.quantity,
                price
              }
            ]
          });

          gtag('event', 'add_to_cart', {
            "items": [
              {
                "id": [source, product.product_type, product.product_id].join('/'),
                "name": product.product_name,
                "brand": source,
                "category": product.product_type,
                "quantity": product.quantity,
                price
              }
            ]
          });
        }

        const fbq = window2.fbq
        if (fbq != null) {
          fbq('track', 'AddToCart', {
            value: price,
            currency
            });
        }
      }

      if (existingProductIndex == -1) {
        if (parseInt(product.quantity, 10) > 0) order['products'].push(product)
        else return null
      } else {
        if (parseInt(product.quantity, 10) <= 0) {
          this.removeProductByIndex(existingProductIndex)
          return null}
        else order['products'][existingProductIndex] = product
      }

      this.updateOrder({result: order})
    }

    // product will be mutated by computePriceRules
    return product
  }

  removeProductByIndex(index) {
    const order = Object.assign({}, this.order.result || {})
    order['products'] = order['products'] || []
    if (order['products'][index] != null) order.products.splice(index, 1)
    this.updateOrder({result: order})
  }

  // utility for invoice where we group products by name
  removeProductByName(name) {
    const order = Object.assign({}, this.order.result || {})
    const products = order['products'] || []
    order['products'] = products.filter(product => product.product_name !== name)
    this.updateOrder({result: order})
  }

  setPriceRules(priceRules) {
    this.priceRules = priceRules
    this.updateOrder(this.order)
    if (this.onPriceRulesChange != null) this.onPriceRulesChange(this.priceRules)

    if (priceRules.result != null) {
      sessionStorage.setItem(this.client.config.tcUser + Cart.localStoragePriceRulesKey, JSON.stringify(priceRules.result))
    } else {
      sessionStorage.removeItem(this.client.config.tcUser + Cart.localStoragePriceRulesKey)
    }
  }

  async refreshPriceRules(force = false) {
    if (this.priceRules?.result != null && force === false) {
      console.log("Using cached price rules")
      return
    }

    this.setPriceRules({loading: true})

    this.client.priceRules({is_public: '1'})
      .then(priceRulesResults => {
        this.setPriceRules(priceRulesResults)
      })
      .catch(err => {
        console.warn("Failed to fetch price rules", err)
      })
  }

  async refreshLimits(limit_codes: string[]) {
    if (limit_codes == null || limit_codes.length === 0) {
      if (this.order?.result?.limit_code != null && this.order?.result?.limit_code !== "") {
        limit_codes = [this.order?.result?.limit_code]
      } else {
        this.limits = null
        this.updateOrder(this.order)
        return
      }
    }

    const headers = {}
    if (this.openIdToken != null) headers['authorization'] = "Bearer " + this.openIdToken
    this.limits = await this.client.limits(limit_codes, headers)
    this.updateOrder(this.order)
  }

  public getCombinedPriceRules(): PriceRule[] {
    const customerPriceRules
    = this.customer == null || this.customer.categories == null || this.customer.categories[0] == null || this.customer.categories[0].customer_in_any_price_rules == null
    ? []
    : this.customer.categories.reduce((acc, category) => acc.concat(category.customer_in_any_price_rules), [])

    const publicPriceRules
    = this.priceRules == null || this.priceRules.result == null
    ? []
    : this.priceRules.result

    return publicPriceRules.concat(customerPriceRules)
  }

  private updateOrder(order) {
    // when there are updates to customer, price rules, or limits,
    // the order statys the same but we trigger updateOrder to recalculate price
    // we clone order to ensure that we trigger react's change detection
    if (order === this.order) {
      order = {...order}
    }
    if (order.result != null && order.result.products != null) {
      // the dates used in flights are without timezone
      // it is better to use string comparison
      // sample flight date: 2019-08-31T18:25
      // example tour date: 2019-08-31 (without time)
      const extractDate = (date) => {
        if (date == null) return null
        if (date.length === 10) return date + 'T00:00'
        if (date.length === 16) return date
        return null
      }

      computePriceRules(order.result, this.priceRules.result, this.bcryptCache, this.customer.result, this.limits?.result)

      order.result['payment_required'] = order.result['products'].reduce((acc, product) =>
        product.items.reduce((acc2, item) => acc2.add(Big(item.price).times(item.quantity)), acc), Big(0))

      order.result['deposit_required'] = order.result['products'].reduce((acc, product) => {
        const total = product.items.reduce((acc2, item) => acc2.add(Big(item.price).times(item.quantity)), Big(0))
        const depositFixed = product.items.reduce((acc2, item) => acc2.add(Big(item.deposit).times(item.quantity)), Big(product["deposit_fixed"] ?? "0"))
        const depositPercentage = Big(product["deposit_percentage"] ?? "0").mul(total).div(100)
        return depositPercentage.add(depositFixed).add(acc)
      }, Big(0))

      order.result['from_date'] = order.result['products'].reduce((acc, product) => {
        const productDate = extractDate(product.from_date)
        if (productDate == null) return acc
        if (acc == null) return productDate
        if (acc > productDate) return productDate
        return acc
      }, null)

      order.result['to_date'] = order.result['products'].reduce((acc, product) => {
        const productDate = extractDate(product.to_date)
        if (productDate == null) return acc
        if (acc == null) return productDate
        if (acc < productDate) return productDate
        return acc
      }, null)
    }

    this.order = order
    this.onOrderChange(order)

    try {
      if (order.result)
        localStorage.setItem(this.client.config.tcUser + Cart.localStorageOrderKey, JSON.stringify(order.result))
      else
        localStorage.removeItem(this.client.config.tcUser + Cart.localStorageOrderKey)
    } catch(e) {
      console.warn("Cannot save order details to localStorage")
    }
  }

  private async updateCustomer(customer, doNotPersist = false) {
    this.customer = customer
    this.onCustomerChange(customer)

    try {
      if (customer.result) {
        if (this.order != null  && this.order.result != null && this.order.result.products != null) {
        this.updateOrder(this.order)
      }

        if (doNotPersist === false) localStorage.setItem(this.client.config.tcUser + Cart.localStorageCustomerKey, JSON.stringify(customer.result))
        else localStorage.removeItem(this.client.config.tcUser + Cart.localStorageCustomerKey)
      } else {
      localStorage.removeItem(this.client.config.tcUser + Cart.localStorageCustomerKey)
    }
    } catch(e) {
      console.warn("Cannot save order details to localStorage")
      console.error(e)
    }
  }
}

export const useCart = (tcUser: string, priceRulesDefault = {}): {customer, order, cart, client, initCart, priceRules} => {
  const client = new TravelCloudClient({
    tcUser
  })

  const [order, setOrder] = useState({})
  const [customer, setCustomer] = useState({})
  const [priceRules, setPriceRules] = useState(priceRulesDefault)
  const [cart, setCart] = useState(null)

  const initCart = () => {
    const cart = new Cart({
      priceRules: priceRulesDefault,
      client,
      onOrderChange: setOrder,
      onCustomerChange: setCustomer,
      onPriceRulesChange: setPriceRules
    })
    setCart(cart)
    return cart
  }

  return {customer, order, cart, client, initCart, priceRules}
}

/************
* Utilities *
*************/

export function formatCurrency(x, decimalPlace=2) {
  if (x == null) return ''
  const neg = parseFloat(x) < 0 ? '-' : ''
  return neg + '$' + Big(x).abs().toFixed(decimalPlace).toString()
}

// not sure how to enable Object.entries in babel
export function objectEntries(obj) {
  const groups = []
  for (var i in obj) {
    groups.push([i, obj[i]])
  }
  return groups
}

// will only work for array of forms, need some modifications to work for array of inputs
export function formArrayWrapper({RenderForm, name, form}: {RenderForm, name: string, form}) {
  form.getFieldDecorator(name)
  var value = form.getFieldValue(name)
  if (value == null) value = []
  if (!Array.isArray(value)) {
    console.warn(`Expecting form state of ${name} to be array`, value)
    value = []
  }
  const onChange = (index) => (change) => {
    value[index] = Object.assign(value[index], change)
    form.setFieldsValue({[name]: value})
  }

  const deleteSelf = (index) => () => {
    value.splice(index, 1)
    form.setFieldsValue({[name]: value})
  }

  return value.map((item, index) => <RenderForm key={index} index={index} numberOfSiblings={value.length} value={item} deleteSelf={deleteSelf(index)} onChange={onChange(index)} />)
}

export function mapTourOptionDepartures<T>(option: Tour["options"][0], func: (departure: Tour["options"][0]["departures"][0], departureDate: string) => T): T[] {
  if (option['on_demand_advance_booking'] === '0') {
    return option.departures.map((departure) => func(departure, departure.date))
  } else {
    const todayDate = (newDateLocal()).getDate()
    const runningDate = newDateLocal()
    runningDate.setDate(todayDate + parseInt(option['min_lead_time']))

    const endDate = newDateLocal()
    endDate.setDate(todayDate +  parseInt(option['on_demand_advance_booking']))

    const result = []
    while (runningDate < endDate) {
      result.push(func(null, dateToIsoDate(runningDate)))
      runningDate.setDate(runningDate.getDate() + 1)
    }

    return result
  }
}

export const isEmptyObject = obj => typeof obj == 'undefined' || obj == null || Object.keys(obj).length == 0

export function groupBy (arr: Array<any>, callback: (any) => any, returnType: string = 'array'):any {

  const pairs = arr.reduce((acc, cur) => {
    const key = callback( cur )
    if (acc[key] == null) acc[key] = []
    acc[key].push(cur)
    return acc
  }, {});

  return returnType == 'array' ? Object.entries(pairs).map(([ key, items ]) => items) : pairs
}
const useTimeout = (callback, delay) => {
  const timeoutId = useRef()
  const savedCallback = useRef()

  useEffect(() => { savedCallback.current = callback }, [callback])

  const reset = useCallback(() => {
    clearTimeout(timeoutId.current)
    const id = setTimeout(savedCallback.current, delay)
  }, [ delay ])

  useEffect(() => {
    if (delay !== null) {
      reset()
      return () => clearTimeout(timeoutId.current)
    }
  }, [ delay, reset ])

  return { reset }
}

const useDelayNextChildren = (children, delay) => {
  const [finalChildren, setFinalChildren] = useState(children);

  const { reset } = useTimeout(() => {
    setFinalChildren(children);
  }, delay);

  useEffect(
    () => {
      reset();
    },
    [reset, children],
  );

  return finalChildren || children || null;
};

const Deferred = ({ delay, children }) => {
  return useDelayNextChildren(children, delay)
}

export function identity(val: any) { return val }

export const range = (count:number) => {
  // return [...Array(count).keys()]
  return Array.apply(null, Array(count)).map((_, index) => index)
}

export function sortBy (arr, fn:(input:any) => any, sortOrder?: string): any[] {
  return arr.sort((val1, val2) => {
    return (fn(val1) - fn(val2)) * (sortOrder === 'asc' ? 1 : -1)
  })
}

export function getHeadAndTail<T>(arr: Array<T>, fn:(input:T) => any): [T, T] {
  let sortedArr = sortBy(arr, fn)  
  return [sortedArr[0], sortedArr[ sortedArr.length - 1 ]]
}

export function overlap (arr): Array<Array<any|null>> {
  return arr.map((x, index) => {
    let next = arr[ index + 1 ] != null ? arr[ index + 1 ] : null
    return [ x, next ]
  })
}

export function chain(input: any, fns: Array<(arr) => any>): any {
  return fns.reduce((acc, cur, index) => cur(acc), input)
}

// export function isDateInRange(date: Date, range: [Date, Date]) {

// }

export function isDateRangeOverlap(range1: [Date, Date], range2: [Date, Date]) {

  let isOverlap = (range1[0] >= range2[0] && range1[0] <= range2[1]) ||
    (range1[1] >= range2[0] && range1[1] <= range2[1])

  let isWithin = (range1[0] < range2[0] && range1[1] > range2[1]) ||
    (range1[0] > range2[0] && range1[1] < range2[1]) 

  return isOverlap && isWithin
}

export function firstCapitalLastLower(name:string) {
  return name.charAt(0) + name.substring(1).toLowerCase()
}

export function parseCookie(rawCookie: string) {
  // ref for regexp
  // https://stackoverflow.com/questions/4607745/split-string-only-on-first-instance-of-specified-character
  return rawCookie
    .split(';')
    .map(token => token.split(/=(.+)/).map(x => x.trim()))
    .map(([ key, value,  ]) => [key, value])
    .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {})
}

export function setCookie(key, value, response = null) {

  if (response != null) {
    response.setHeader('Set-Cookie', [ `${key}=${value}` ])
  }

  else if (typeof window != 'undefined') {
    window.document.cookie = `${key}=${value}`
  }

}

export function loadi18n( resources, defaultLocale, cookieSupport = false, headers = null ) {

  let cookieStr = ''
  let locale = defaultLocale
  
  if ( cookieSupport ) {

    if (headers != null) {
      cookieStr = headers.cookie ?? ''
    } 
    
    else if (typeof window != 'undefined') {
      cookieStr = window.document.cookie
    }

    let cookies = parseCookie( cookieStr )
    locale = cookies['__locale'] != null ? cookies['__locale'] : defaultLocale
  }

  return i18nInit(locale, resources)
}
export function omit( pairs, keys: string[] ): any {
  return Object.entries(pairs)
    .filter(([key, value]) => keys.indexOf(key) === -1)
    .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {})
}

export function purify(pairs): any {
  return Object.entries(pairs)
    .filter(([key, value]) => value != null)
    .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {})
}

export function resolvePath(...args: string[]): string {
  return args.map((token: any) => token.slice(0,1) === '/' ? token.slice(1) : token)
    .map((token: any) => token.slice(-1) === '/' ? token.slice(0, -1) : token)
    .join('/')
}

export type ProductBreakdown = {
  product_breakdown: {[price_type: string]: string},
  price_rule_items: any[],
  product_total: Big,
  grand_total: Big
}

export function computeProductBreakdown(product: Product): ProductBreakdown{
  if (product == null) return null
  var product_breakdown = {}
  var price_rule_items = []
  var product_total = Big(0)
  var grand_total = Big(0)

  for (var item of product.items) {
    const sub_total = Big(item.price).times(item.quantity)
    grand_total = grand_total.add(sub_total)

    if (item.price_rule_id == null || item.price_rule_id === "") {
      product_total = product_total.add(sub_total)
      if (product_breakdown[item.price_type] == null) product_breakdown[item.price_type] = Big(0)
      product_breakdown[item.price_type] = product_breakdown[item.price_type].add(sub_total)
    } else {
      price_rule_items.push(item)
    }
  }

  return {
    product_breakdown,
    price_rule_items,
    product_total,
    grand_total
  }

}