import React, { Fragment } from "react";
import { formatCurrency, TcResponse, Cart, TravelCloudClient } from "../travelcloud";
import { Button, Icon, Row, Col, Collapse, Popover } from "antd";
import moment from "moment";
import { NoResult } from "../components/no-result";
import { FlightSearch, FlightDetail } from "../types";
import Big from "big.js";
import { Element, Events, scroller } from "react-scroll";
import {
  getBestCabinFromSegments,
  getOriginDestinationFromFareSegments,
  computeOd,
  computePrice,
  ignoreTimezoneExtractTime,
  ignoreTimezoneExtractDate,
  renderFares,
  computeBaggage
} from "../components/flights-result";

export const Od = ({od, source}: {od: FlightSearch[0]['od1'], source: string}) => {
    const odInfo = computeOd(od)

    return <section className="flightinfo border">
        <Row>
        <Col lg={6}>
            <img
            src={`//cdn.net.in/static/airlines/${odInfo.departure_marketing_carrier.code}.gif`}
            />
        </Col>
        <Col lg={7}>
            {odInfo.departure_local_date}
        </Col>
        <Col lg={11}>
            {odInfo.departure_operating_carrier != null && odInfo.departure_operating_carrier.name != null && odInfo.departure_marketing_carrier.code !== odInfo.departure_operating_carrier.code
                && <span className="flight-opr-full-name">
                Operated by:
                {odInfo.departure_operating_carrier.name.substring(0,15)}
            </span>
            }
        </Col>
        </Row>
        <ul className="timing">
        <li className="long">
            <h6>Depart</h6>
            <h2>
            <strong>
                {odInfo.departure_airport.code}
            </strong>
            &nbsp;
            {odInfo.departure_local_time}
            </h2>
        </li>
        <li className="long2">
            <div className="stop-num">
            <h2>
                <span className="direct">
                {odInfo.stops.display_with_airport_codes}
                <br />
                {odInfo.total_time.display}
                </span>
            </h2>
            </div>
        </li>
        <li className="long">
            <h6>Arrive</h6>
            <h2>
            <strong>
                {odInfo.arrival_airport.code}
            </strong>
            &nbsp;
            {odInfo.arrival_local_time}
            </h2>
        </li>
        </ul>
        {od.segments[0].flights != null &&
        <Collapse bordered={false} className="details">
        <Collapse.Panel header="Show details" key={"i"}>
            {od.segments.map((segment, key) => (
            <Row
                key={key}
                type="flex"
                justify="space-between"
                align="middle"
            >
                <Col xs={24} md={4} className="flightno">
                {segment.marketing_carrier.code}
                {segment.flight_number}
                </Col>
                <Col xs={24} md={20}>
                {segment.flights.map((flight, key) => (
                    <div key={`ds1` + key}>
                    <p>
                        Depart{" "}
                        <strong>
                        {flight.departure_airport.code}
                        </strong>{" "}
                        {" - " + flight.departure_airport.name} at{" "}
                        {ignoreTimezoneExtractTime(
                        flight.departure_datetime
                        )}
                        ,{" "}
                        {ignoreTimezoneExtractDate(
                        flight.departure_datetime
                        )}
                        <br />
                        Arrive{" "}
                        <strong>
                        {flight.arrival_airport.code}
                        </strong>{" "}
                        {" - " + flight.arrival_airport.name} at{" "}
                        {ignoreTimezoneExtractTime(
                        flight.arrival_datetime
                        )}
                        ,{" "}
                        {ignoreTimezoneExtractDate(
                        flight.arrival_datetime
                        )}
                    </p>
                    </div>
                ))}
                </Col>
            </Row>
            ))}
        </Collapse.Panel>
        </Collapse>}
    </section>
}

export const FlightsResult: React.StatelessComponent<{
  flights: FlightSearch;
  flightMap: { [key: string]: TcResponse<FlightDetail> };
  onFlightClick?: (index: number, flight: any) => any;
  onFareClick?: (index: number, flight: FlightDetail, fareId: string) => any;
  onFareRulesClick?: (flight: FlightDetail, pricing_id: string) => any;
  cart: Cart;
}> = ({ flights, flightMap, onFlightClick, onFareClick, onFareRulesClick, cart }) => {
  return (
    <>
      {flights.map((flight, index) => {
        const pricing_id = flight.pricings[0].id
        const computed = computePrice(cart, flight, pricing_id)
        const baggage = computeBaggage(flight.pricings[0].fares[0])
        const hasFareRules = flight.pricings[0].fare_rules != null

        return (
          <Fragment key={index}>
            <div className={"ant-card flight-result"}>
              <div className="filter-result-section">
                <Row type="flex" justify="start">
                  <Col xs={24} sm={24} lg={18}>
                    <Od od={flight.od1} source={flight.pricings[0].source} />
                    {flight.od2 != null && <Od od={flight.od2} source={flight.pricings[0].source} />}
                    {flight.od3 != null && <Od od={flight.od3} source={flight.pricings[0].source} />}
                    {flight.od4 != null && <Od od={flight.od4} source={flight.pricings[0].source} />}
                  </Col>
                  <Col xs={24} sm={24} lg={6}>
                    <section className="travel-fair">
                      <h2 className="total-price">
                        {" "}
                        {computed.beforeDiscount ===
                        computed.afterDiscount ? (
                          computed.beforeDiscount
                        ) : (
                          <>
                            <span style={{ textDecoration: "line-through" }}>
                              {computed.beforeDiscount}
                            </span>
                            &nbsp;
                            <b>{computed.afterDiscount}</b>
                          </>
                        )}
                      </h2>
                      <div className="query-and-info">
                        <Popover
                            content={
                                <table className="fare-breakdown-table">
                                    {Object.entries(flight.pricings[0].ptc_breakdown_map).map((val, n) => {
                                    
                                      if (val[1].base_fare) return <Fragment key={n}>
                                          <tr>
                                              <td>{val[0].toUpperCase()} Base Fare</td>
                                              <td>x{val[1].quantity}</td>
                                              <td>${Big(val[1].base_fare).toFixed(2)}</td>
                                          </tr>
                                          <tr>
                                              <td>{val[0].toUpperCase()} Taxes and Charges</td>
                                              <td>x{val[1].quantity}</td>
                                              <td>${Big(val[1].price).minus(val[1].base_fare).toFixed(2)}</td>
                                          </tr>
                                      </Fragment>                                        

                                      return <tr key={n}>
                                            <td>{val[0].toUpperCase()}</td>
                                            <td>x{val[1].quantity}</td>
                                            <td>${Big(val[1].price).toFixed(2)}</td>
                                        </tr>
                                    })}
                                </table>}
                            trigger="click">
                            <p className="hasinfo">
                            <Icon type="question-circle" /> Fare Breakdown
                            </p>
                        </Popover>
                        
                        {hasFareRules && <p className="hasinfo" onClick={() => {
                            onFareRulesClick(flight, pricing_id)
                          }}>
                          <Icon type="info-circle" /> Fare Rules
                        </p>}
                        {baggage
                            && (
                          <p>
                            <Icon type="shopping" /> {baggage}
                          </p>
                        )}
                      </div>
                      {/*<small className="tax">(inclusive of $80.2 tax)</small>
           <p><img src="../static/images/icon-bag.png"/> 30kg</p>*/}
                      {flightMap[index] != null ? (
                        <Button className="select sel-deal open" type="primary">
                          {" "}
                          Select <Icon type="right" />
                        </Button>
                      ) : (
                        <Button
                          className="select sel-deal"
                          type="primary"
                          onClick={() => {
                            onFlightClick(index, flight);
                          }}
                        >
                          {" "}
                          Select <Icon type="right" />
                        </Button>
                      )}
                    </section>
                  </Col>
                </Row>
              </div>
              {flightMap[index] != null && (
                <Element name="options">
                  {flightMap[index].result == null ? (
                    <div className="options">
                      <NoResult
                        response={flightMap[index]}
                        type="fares"
                        loadingMessage="Verifying prices and availability..."
                      />{" "}
                    </div>
                  ) : (
                    <div className="options">
                      <Collapse
                        bordered={false}
                        defaultActiveKey={[`op${index}`]}
                      >
                        <Collapse.Panel header="" key={`op${index}`}>
                          {flightMap[index].result &&
                            renderFares(
                              index,
                              cart,
                              flightMap[index].result,
                              onFareClick
                            )}
                        </Collapse.Panel>
                      </Collapse>
                    </div>
                  )}
                </Element>
              )}
            </div>
          </Fragment>
        );
      })}
    </>
  );
};
