import React from 'react'

export class PrintDiv extends React.Component<{printDivId?: string, hideOriginalDiv?: boolean, children: React.ReactNode}> {
  myRef: React.RefObject<HTMLDivElement>
  myElem: HTMLElement
  constructor(props) {
    super(props)
    this.myRef = React.createRef()
  }
  render() {
    return <div className="originalDiv" ref={this.myRef} style={{display: this.props.hideOriginalDiv === true ? 'none' : 'block'}}>{this.props.children}</div>
  }

  componentDidMount() {
    this.myElem = document.createElement("div")
    this.myElem.className = "printDiv"
    if (this.props.printDivId != null) this.myElem.id = this.props.printDivId
    document.body.appendChild(this.myElem)
    this.componentDidUpdate()
  }

  componentDidUpdate() {
    if (this.myRef != null && this.myRef.current != null && this.myElem != null) {
      this.myElem.innerHTML = this.myRef.current.innerHTML
    }
  }

  componentWillUnmount() {
    this.myElem.parentNode.removeChild(this.myElem)
  }
}
