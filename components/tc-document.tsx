import React, { useRef } from 'react'
import Head from 'next/head'
import {Spin, Icon} from 'antd'
import {TravelCloudClient} from '../travelcloud'
import {useTcDocument} from './hooks/tc-document'
import {Parser} from 'html-to-react'

interface iTcDocument extends Partial<HTMLElement> {
  blockId: string;
  contents: any;
}

export const TcDocument = ({ contents, blockId, id, className }: iTcDocument) => {
  
  let parser = useRef(new Parser)  

  if (blockId === '__head') {
    return parser.current.parse(contents?.[blockId]?? '')
  }

  return contents != null ? 
    contents?.[blockId]?.type === 'BlockEditor' && <div id={id} className={className} dangerouslySetInnerHTML={{ __html: contents?.[blockId]?.content ?? '' }} /> :
    <Spin indicator={<Icon type='loading' />} />;
}

export default TcDocument