import React from "react";
import { Modal, Icon, Row, Col, Divider } from "antd";

const FareBreakdownModal = ({ data, handleClose, show }) => {
  var total = 0;
  if (data.adt) total = total + data.adt.price * data.adt.quantity;
  if (data.cnn) total = total + data.cnn.price * data.cnn.quantity;
  if (data.inf) total = total + data.inf.price * data.inf.quantity;
  const travellerRows = [];
  if (data.adt)
    for (let i = 0; i < parseInt(data.adt.quantity); i++) {
      travellerRows.push({
        amt: parseFloat(data.adt.price),
        for: "Adult"
      });
    }
  if (data.cnn)
    for (let i = 0; i < parseInt(data.cnn.quantity); i++) {
      travellerRows.push({
        amt: parseFloat(data.cnn.price),
        for: "Child"
      });
    }
  if (data.inf)
    for (let i = 0; i < parseInt(data.inf.quantity); i++) {
      travellerRows.push({
        amt: parseFloat(data.inf.price),
        for: "Infant"
      });
    }
  return (
    <Modal
      className="customize-your-holiday-modal fare-breakdown-modal"
      visible={show}
      onCancel={handleClose}
      closable={false}
      footer={null}
    >
      <div className="modal-box">
        <div className="modal-header">
          <h4
            className="font-heading color-primary"
            style={{ marginBottom: 20 }}
          >
            <strong>Fare Breakdown</strong>
          </h4>
          <Icon type="close" className="modal-close" onClick={handleClose} />
        </div>
        <div className="modal-body">
          {travellerRows.map((item, i) => (
            <Row key={i} type="flex" gutter={12}>
              <Col span={14}>
                Traveller {i + 1}: {item.for}
              </Col>
              <Col span={10} style={{ textAlign: "right" }}>
                S$ {item.amt.toFixed(2)}
              </Col>
            </Row>
          ))}
          <Divider />
          <Row type="flex" gutter={12} style={{ fontWeight: "bold" }}>
            <Col span={14}>Total: SGD</Col>
            <Col span={10} style={{ textAlign: "right" }}>
              S$ {total.toFixed(2)}
            </Col>
          </Row>
        </div>
      </div>
    </Modal>
  );
};

export default FareBreakdownModal;
