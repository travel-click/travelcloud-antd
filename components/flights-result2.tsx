import React , { CSSProperties }from 'react'
import Big from 'big.js'
import SvgIcon from './svg-icon'
import scrollToElement from 'scroll-to-element'
import moment from 'moment'
import { Button, Icon, Row, Col, Carousel, Card, Divider, Tabs, Popover, Tooltip } from 'antd'
import { formatCurrency, TcResponse, Cart } from '../travelcloud'
import { NoResult } from './no-result'
import { FlightSearch, FlightDetail } from '../types'
import { getBestCabinFromSegments } from './flights-result'

export class CarouselArrow extends React.Component<{
  iconElement: any;
  className?: any;
  style?: CSSProperties;
  onClick?: any;
}> {
  render() {
    const { iconElement, className, style, onClick } = this.props;
    return (
      <div className={className} style={style} onClick={onClick}>
        {iconElement}
      </div>
    );
  }
}


function ignoreTimezoneExtractTime(datetime) {
  return moment(datetime.replace(/[+-][0-9][0-9]:[0-9][0-9]$/, '')).format('HH:mm')
}

function ignoreTimezoneExtractDate(datetime) {
  return moment(datetime.replace(/[+-][0-9][0-9]:[0-9][0-9]$/, '')).format('ddd, D MMM YYYY')
}

function computePrice(cart: Cart, flight: FlightDetail, fareId: string): {
  beforePriceRules: string,
  afterPriceRules: string
}{
  const fare = flight.pricings.find((fare) => fare.id == fareId)
  if (fare === null) {
    return {
      afterPriceRules: 'Invalid fare id',
      beforePriceRules: 'Invalid fare id'
    }
  }
  if (cart === null) {
    return {
      afterPriceRules: formatCurrency(fare.price),
      beforePriceRules: formatCurrency(fare.price)
    }
  }
  const product = cart.addFlight(flight, fareId, true)
  if (product == null) {
    return {
      afterPriceRules: formatCurrency(fare.price),
      beforePriceRules: formatCurrency(fare.price)
    }
  }
  // more like 'before discounts' and 'after discounts'
  const beforePriceRules = product.items.filter((item) => parseInt(item['price']) > 0).reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))
  const afterPriceRules = product.items.reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))
  return {
    afterPriceRules: formatCurrency(afterPriceRules),
    beforePriceRules: formatCurrency(beforePriceRules)
  }
}

function customPriceDisplay(defaultPriceDisplay) {
  return 'S$'+Math.ceil(Number(defaultPriceDisplay.replace('$',''))).toLocaleString()
}

export class RenderFares extends React.Component<{flightIndex: number, cart: Cart, flight: FlightDetail, onFareClick?: (index: number, flight: FlightDetail, fareId: string) => any, windowWidth:any}> {
  state = {
    expanded: false
  }
  backToTopOfFlightItem(id,offset) {
    scrollToElement(id, {
      offset: offset,
      ease: 'in-out-circ',
      duration: 1000
    })
  }
  render() {
    const { flightIndex, cart, flight, onFareClick, windowWidth } = this.props
    const indexServices = (acc2, fare, fareDetailIndex) => {
      if (acc2[fareDetailIndex] == null) acc2[fareDetailIndex] = {
        origin: fare.segments[0].origin,
        destination: fare.segments[fare.segments.length - 1].destination,
        servicesSet: {}
      }
      const brand = fare.brand
      if (brand == null) return acc2
      const services = brand.services
      if (services == null) return acc2
      acc2[fareDetailIndex].servicesSet = services.reduce((acc3, service) => {
        acc3[service.name] = true
        return acc3
      }, acc2[fareDetailIndex].servicesSet)
      return acc2
    }
    let fares = flight.pricings
    var fareServicesIndexed = fares.reduce((acc, fare) => {
      return fare.fares.reduce(indexServices, acc)
    }, new Array(fares[0].fares.length))
    fareServicesIndexed = fareServicesIndexed.map(x => {
      x.servicesSet = Object.keys(x.servicesSet)
      return x
    })
    const flightOptionsCarousel = {
      arrows: true,
      prevArrow: <CarouselArrow className='slick-arrow slick-prev' style={{top:50}} iconElement={<Icon style={{fontSize:30,background:'#fff',color:'#f47117',margin:'-5px auto auto -5px'}} theme='filled' type='left-square' />} />,
      nextArrow: <CarouselArrow className='slick-arrow slick-next' iconElement={<Icon style={{fontSize:30,background:'#fff',color:'#f47117',margin:'-5px auto auto -5px'}} theme='filled' type='right-square' />} />,
      dots: false,
      draggable: true,
      infinite: false
    }
    return <div className='flight-options' style={{padding:10}}>
      <Row type='flex' style={{overflow:'hidden',height: this.state.expanded === true ? 'auto' : 150}}>
        <Col xs={12} lg={6} style={{textAlign:'left'}}>
          <Card>
            {fareServicesIndexed.map((info, index) => {
              return <div key={index}>
                {index < 1 &&
                <div className='th' style={{paddingBottom:10}}>
                  <div className='price'><strong>&nbsp;</strong></div>
                  <div><Button type='primary' disabled style={{opacity:0}}>&nbsp;</Button></div>
                </div>}
                <div className='th'><strong>{info.origin.code} → {info.destination.code}</strong></div>
                {info.servicesSet.map(serviceName =>
                  <div key={serviceName} className='td'><span>{serviceName}</span></div>
                )}
              </div>
            })}
          </Card>
        </Col>
        <Col xs={12} lg={18}>
          <div id={'scroll_'+flightIndex} style={{paddingRight: windowWidth < 992 ? 0 : 80,overflow:'hidden'}}>
            <Carousel className={windowWidth < 992 ? 'carousel-arrow-top' : 'carousel-arrow-top carousel-overflow'} {...flightOptionsCarousel} slidesToShow={windowWidth < 992 ? 1 : fares.length > 2 ? 3 : 2}>
              {fares.map((detail, i1) => {
                const computed = computePrice(cart, flight, detail.id)
                return <Card hoverable key={i1}>
                  <div className='th align-center' style={{paddingBottom:10}}>
                    <div className='price'><strong>{computed.afterPriceRules}</strong></div>
                    <div><Button type='primary' onClick={() => onFareClick(flightIndex, flight, detail.id)}>Select</Button></div>
                  </div>
                  {detail.fares.map((fare, i2) => {
                    const servicesIndexed = fare.brand == null || fare.brand.services == null ? {} : fare.brand.services.reduce((acc, service) => {
                      acc[service.name] = service
                      return acc
                    }, {})
                    return <div key={i2}>
                      <div className='th align-center'>{fare.brand != null && fare.brand.name != null ? fare.brand.name : getBestCabinFromSegments(fare.segments)}</div>
                        {fareServicesIndexed[i2].servicesSet.map((serviceName, index) => {
                          return <div key={index} className='td align-center'>
                            {servicesIndexed[serviceName] != null && servicesIndexed[serviceName].included === true ? <Icon style={{color:'#7C7',display:'block',margin:'auto'}} type='check' /> : <Icon style={{color:'#C77',display:'block',margin:'auto'}} type='close' />}
                          </div>
                        })}
                    </div>
                  })}
                </Card>
              })}
            </Carousel>
          </div>
        </Col>
      </Row>
      <a style={{display:'block',textAlign:'center',padding:'5px 0',marginBottom:5}}
      onClick={() => {
        const offset = this.state.expanded === true ? -200 : -20
        this.setState({ expanded: !this.state.expanded })
        this.backToTopOfFlightItem('#scroll_'+flightIndex,offset)
      }}>
        {this.state.expanded === true ? <span>Collapse</span> : <span>Expand</span>}
      </a>
    </div>
  }
}

function scrollToFlight(id) {
  scrollToElement(id,{
    offset: -90,
    ease: 'in-out-circ',
    duration: 400
  })
}

// got it from price breaker repo
export const FlightsResult2: React.StatelessComponent<{
  flights: FlightSearch
  flightMap: { [key: string]: TcResponse<FlightDetail> }
  onFlightClick?: (index: number, flight: any) => any
  onFareClick?: (index: number, flight: FlightDetail, fareId: string) => any
  onFareBreakdownClick?: (fareBreakdownDetail: any) => any
  onFareRulesClick?: (fareRulesDetail: any) => any
  showFlightModal:any
  showFlightMapModal?: (detail: any) => any
  type:any
  cart: Cart,
  windowWidth?: any,
  cabin?: any
}>
= ({ flights, flightMap, onFlightClick, 
    onFareClick, onFareBreakdownClick, onFareRulesClick, 
    showFlightModal, showFlightMapModal, 
    type, cart, windowWidth, cabin }) => {
  return <>{flights.map((flight, index) => {
    const combine_segments_flights = segments => {
      let results = []
      for (let i=0, len=segments.length; i<len; i++) {
        const flights = segments[i].flights
        for (let j=0, len2=flights.length; j<len2; j++) {
          const flight = segments[i].flights[j]
          const departure = flight.departure_datetime
          const arrival = flight.arrival_datetime
          results.push({
            departure: {
              datetime: departure,
              airport: flight.departure_airport
            },
            arrival: {
              datetime: arrival,
              airport: flight.arrival_airport
            },
            duration: moment.duration(moment(arrival).diff(departure)).asMilliseconds(),
            cabin: cabin,
            carrier_code: segments[i].marketing_carrier.code,
            carrier_name: segments[i].marketing_carrier.name,
            flight_number: segments[i].flight_number,
            equipment_name: flight.equipment.name,
          })
        }
      }
      return results
    }
    const outbound_flights = combine_segments_flights(flight.od1.segments)
    const return_flights = flight.od2 ? combine_segments_flights(flight.od2.segments) : []

    const measure_travel_time = flights => {
      return moment.duration(
        moment(flights[flights.length-1].arrival.datetime).diff(flights[0].departure.datetime)
      ).asMilliseconds()
    }
    const outbound_time = measure_travel_time(outbound_flights)
    const return_time = return_flights.length > 0 ? measure_travel_time(return_flights) : null
    const outbound_travel_time = {
      days: Number(moment.duration(outbound_time).days()),
      hours: Number(moment.duration(outbound_time).hours()),
      minutes: Number(moment.duration(outbound_time).minutes())
    }
    const return_travel_time = return_time
    ? {
        days: Number(moment.duration(outbound_time).days()),
        hours: Number(moment.duration(outbound_time).hours()),
        minutes: Number(moment.duration(outbound_time).minutes())
      }
    : null

    const measure_stops_duration = flights => {
      let total = moment.duration()
      for (let i=0, len=flights.length; i<len; i++) {
        if (flights[i+1]) {
          total = total.add(moment.duration(
            moment(flights[i+1].departure.datetime).diff(flights[i].arrival.datetime)
          ))
        }
      }
      return total.asMilliseconds()
    }
    const outbound_stops = {
      days: moment.duration(measure_stops_duration(outbound_flights)).days(),
      hours: moment.duration(measure_stops_duration(outbound_flights)).hours(),
      minutes: moment.duration(measure_stops_duration(outbound_flights)).minutes()
    }
    const return_stops = {
      days: moment.duration(measure_stops_duration(return_flights)).days(),
      hours: moment.duration(measure_stops_duration(return_flights)).hours(),
      minutes: moment.duration(measure_stops_duration(return_flights)).minutes()
    }

    const departingFlight = flight.od1
    const departingFlightFirstSegment = departingFlight.segments[0]
    const departingFlightLastSegment = departingFlight.segments[departingFlight.segments.length - 1]
    const departingFlightFirstLeg = departingFlightFirstSegment.flights[0]
    const departingFlightLastLeg = departingFlightLastSegment.flights[departingFlightLastSegment.flights.length - 1]
    const departingFlightDuration = moment.duration(moment(departingFlightLastLeg.arrival_datetime).diff(departingFlightFirstLeg.departure_datetime))
    //const departingFlightAdtFirstFare = flight.od1_ptc_fare_map.adt[0]
    //const departingFlightHasBaggage = departingFlightAdtFirstFare.baggage_pieces != null || departingFlightAdtFirstFare.baggage_weight_amount != null

    const returningFlight = flight.od2
    const returningFlightFirstSegment = returningFlight && returningFlight.segments[0]
    const returningFlightLastSegment = returningFlight && returningFlight.segments[returningFlight.segments.length - 1]
    const returningFlightFirstLeg = returningFlight && returningFlightFirstSegment.flights[0]
    const returningFlightLastLeg = returningFlight && returningFlightLastSegment.flights[returningFlightLastSegment.flights.length - 1]
    const returningFlightDuration = returningFlight && moment.duration(moment(returningFlightLastLeg.arrival_datetime).diff(returningFlightFirstLeg.departure_datetime))
    //const returningFlightAdtFirstFare = returningFlight && flight.od2_ptc_fare_map.adt[0]
    //const returningFlightHasBaggage = returningFlight && (returningFlightAdtFirstFare.baggage_pieces != null || returningFlightAdtFirstFare.baggage_weight_amount != null)

    const computed = computePrice(cart, flight, flight.pricings[0].id)
    let isExist = 0
    const airline = departingFlightFirstSegment.marketing_carrier.name

    const outbound_departure = ignoreTimezoneExtractTime(departingFlightFirstLeg.departure_datetime)
    const outbound_arrival = ignoreTimezoneExtractTime(departingFlightFirstLeg.arrival_datetime)
    var outbound_dep = outbound_departure.split(':')
    var outbound_arrv =  outbound_arrival.split(':')
    const return_departure = returningFlight && ignoreTimezoneExtractTime(returningFlightFirstLeg.departure_datetime)
    const return_arrival = returningFlight && ignoreTimezoneExtractTime(returningFlightFirstLeg.arrival_datetime)
    var return_dep = returningFlight && return_departure.split(':')
    var return_arrv = returningFlight && return_arrival.split(':')

    var baggage = true
    /*if(typeof flight.pricings[0].fares[0].baggage_weight_amount==='undefined'){
      baggage = false
    }*/
    if (flight.pricings[0].fares[0].baggage_pieces === undefined || flight.pricings[0].fares[0].baggage_pieces === 0) {
      baggage = false
    }

    return <div key={index}>
      <div className='flight-card' id={`flight${index}`}>
        <div className='flight-card-row'>
          <div className='flight-content'>
            <Tabs onChange={() => scrollToFlight(`#flight${index}`)} defaultActiveKey='1' className='flight-card-tab' tabPosition='bottom' animated={false} tabBarStyle={{textAlign:'right',marginTop:0}}>
              <Tabs.TabPane key='1' tab={<a><small><strong>HIDE FLIGHT DETAILS</strong> &nbsp; <SvgIcon name='up' width='10' height='10' fill='#D02C2F' /></small></a>}>
                <Row type='flex' gutter={50}>
                  <Col xs={24} lg={12} className='flight-travel flight-depart'>
                    <div><strong>Outbound</strong> &nbsp;&nbsp; {moment(outbound_flights[0].departure.datetime).format('ddd, D MMM YYYY')}</div>
                    <div className='flight-simple'>
                      <div className='flight-logo'><Popover placement='topLeft' content={<div style={{padding:'5px 10px'}}>{airline}</div>}><a><img src={`//cdn.net.in/static/airlines/${departingFlightFirstSegment.marketing_carrier.code}.gif`} /></a></Popover></div>
                      <div className='flight-route'>
                        <div className='flight-from'>
                          <div><small>&nbsp;</small></div>
                          <div><strong>{moment(outbound_flights[0].departure.datetime).format('HH:mm')}</strong></div>
                          <div>{outbound_flights[0].departure.airport.code}</div>
                        </div>
                        <div className={outbound_flights.length > 1 ? 'flight-stops' : 'flight-stops direct'}>
                          <div>
                            <small>
                              {outbound_stops.days > 0 && (
                                outbound_stops.days > 1
                                ? <span>{outbound_stops.days} days </span>
                                : <span>{outbound_stops.days} day </span>)}
                              {outbound_stops.hours > 0 && (
                                outbound_stops.hours > 1
                                ? <span>{outbound_stops.hours} hrs </span>
                                : <span>{outbound_stops.hours} hr </span>)}
                              {outbound_stops.minutes > 0 && (
                                outbound_stops.minutes > 1
                                ? <span>{outbound_stops.minutes} mins</span>
                                : <span>{outbound_stops.minutes} min</span>)}
                            </small>
                          </div>
                          <div>-----------------------------</div>
                          <div>
                            {outbound_flights.length > 1
                            ? outbound_flights.length > 2
                              ? <small>{outbound_flights.length-1} stops</small>
                              : <small>{outbound_flights.length-1} stop</small>
                            : <small>Direct</small>}
                          </div>
                        </div>
                        <div className='flight-to'>
                          <div><small>&nbsp;</small></div>
                          <div><strong>{moment(outbound_flights[outbound_flights.length-1].arrival.datetime).format('HH:mm')}</strong></div>
                          <div>{outbound_flights[outbound_flights.length-1].arrival.airport.code}</div>
                        </div>
                      </div>
                      <div className='flight-duration'>
                        <small>
                          Travel Time: &nbsp;
                          {outbound_travel_time.days > 0 && (
                            outbound_travel_time.days > 1
                            ? <span>{outbound_travel_time.days} days </span>
                            : <span>{outbound_travel_time.days} day </span>)}
                          {outbound_travel_time.hours > 0 && (
                            outbound_travel_time.hours > 1
                            ? <span>{outbound_travel_time.hours} hrs </span>
                            : <span>{outbound_travel_time.hours} hr </span>)}
                          {outbound_travel_time.minutes > 0 && (
                            outbound_travel_time.minutes > 1
                            ? <span>{outbound_travel_time.minutes} mins </span>
                            : <span>{outbound_travel_time.minutes} min </span>)}
                        </small>
                      </div>
                    </div>
                  </Col>
                  {//returningFlight && ((return_dep[0]>=returntime.departure_start && return_dep[0]<=returntime.departure_end) && (return_arrv[0]>=returntime.arrival_start && return_arrv[0]<=returntime.arrival_end)) &&
                  return_flights.length > 0 &&
                    <Col xs={24} lg={12} className='flight-travel'>
                      <div><strong>Return</strong>&nbsp;&nbsp;{moment(return_flights[0].departure.datetime).format('ddd, D MMM YYYY')}</div>
                      <div className='flight-simple'>
                        <div className='flight-logo'><Popover placement='topLeft' content={<div style={{padding:'5px 10px'}}>{airline}</div>}><a><img src={`//cdn.net.in/static/airlines/${returningFlightFirstSegment.marketing_carrier.code}.gif`} /></a></Popover></div>
                        <div className='flight-route'>
                          <div className='flight-from'>
                            <div><small>&nbsp;</small></div>
                            <div><strong>{moment(return_flights[0].departure.datetime).format('HH:mm')}</strong></div>
                            <div>{return_flights[0].departure.airport.code}</div>
                          </div>
                          <div className={return_flights.length > 1 ? 'flight-stops' : 'flight-stops direct'}>
                            <div>
                              <small>
                                {return_stops.days > 0 && (
                                  return_stops.days > 1
                                  ? <span>{return_stops.days} days </span>
                                  : <span>{return_stops.days} day </span>)}
                                {return_stops.hours > 0 && (
                                  return_stops.hours > 1
                                  ? <span>{return_stops.hours} hrs </span>
                                  : <span>{return_stops.hours} hr </span>)}
                                {return_stops.minutes > 0 && (
                                  return_stops.minutes > 1
                                  ? <span>{return_stops.minutes} mins</span>
                                  : <span>{return_stops.minutes} min</span>)}
                              </small>
                            </div>
                            <div>-----------------------------</div>
                            <div>
                              {return_flights.length > 1
                              ? return_flights.length > 2
                                ? <small>{return_flights.length-1} stops</small>
                                : <small>{return_flights.length-1} stop</small>
                              : <small>Direct</small>}
                            </div>
                          </div>
                          <div className='flight-to'>
                            <div><small>&nbsp;</small></div>
                            <div><strong>{moment(return_flights[return_flights.length-1].arrival.datetime).format('HH:mm')}</strong></div>
                            <div>{return_flights[return_flights.length-1].arrival.airport.code}</div>
                          </div>
                        </div>
                        <div className='flight-duration'>
                          <Row type='flex' gutter={40}>
                            <Col xs={24} md={12} lg={24} xl={12}>
                              <small>
                                Travel Time: &nbsp;
                                {return_travel_time.days > 0 && (
                                  return_travel_time.days > 1
                                  ? <span>{return_travel_time.days} days </span>
                                  : <span>{return_travel_time.days} day </span>)}
                                {return_travel_time.hours > 0 && (
                                  return_travel_time.hours > 1
                                  ? <span>{return_travel_time.hours} hrs </span>
                                  : <span>{return_travel_time.hours} hr </span>)}
                                {return_travel_time.minutes > 0 && (
                                  return_travel_time.minutes > 1
                                  ? <span>{return_travel_time.minutes} mins </span>
                                  : <span>{return_travel_time.minutes} min </span>)}
                              </small>
                            </Col>
                            <Col xs={24} md={12} lg={24} xl={12}>
                              <small>Operated by: {airline}</small>
                            </Col>
                          </Row>
                        </div>
                      </div>
                    </Col>}
                </Row>
              </Tabs.TabPane>
              <Tabs.TabPane key='2' tab={<a><small><strong>FLIGHT DETAILS</strong> &nbsp; <SvgIcon name='down' width='10' height='10' fill='#D02C2F' /></small></a>}>
                <div className='flight-detailed'>
                  <Row type='flex' gutter={50}>
                    <Col xs={24} lg={12} className='flight-travel flight-depart'>
                      <div><strong>Outbound</strong>&nbsp;&nbsp;{moment(outbound_flights[0].departure.datetime).format('ddd, D MMM YYYY')}</div>
                      <Divider style={{margin:'20px 0'}} />
                      <div className='flight-logo'><img src={`//cdn.net.in/static/airlines/${outbound_flights[0].carrier_code}.gif`} /> &nbsp; <strong>{outbound_flights[0].carrier_name.substring(0,25)}</strong></div>
                      {outbound_flights.map((f,i) => {
                        const layover = outbound_flights[i+1] ? moment.duration(moment(outbound_flights[i+1].departure.datetime).diff(f.arrival.datetime)).asMilliseconds() : null
                        const layover_time = {
                          days: moment.duration(layover).days(),
                          hours: moment.duration(layover).hours(),
                          minutes: moment.duration(layover).minutes()
                        }
                        const duration = moment.duration(moment(f.arrival.datetime).diff(f.departure.datetime)).asMilliseconds()
                        const duration_time = {
                          days: moment.duration(duration).days(),
                          hours: moment.duration(duration).hours(),
                          minutes: moment.duration(duration).minutes()
                        }
                        const flight_service_and_equipment = []
                        f.cabin && flight_service_and_equipment.push(f.cabin)
                        f.carrier_code && f.flight_number && flight_service_and_equipment.push(f.carrier_code+f.flight_number)
                        f.equipment_name && flight_service_and_equipment.push(f.equipment_name)
                        return <div key={i}>
                          <div className='flight-route'>
                            <div className='point point-end'><strong>{moment(f.departure.datetime).format('HH:mm')}</strong> &nbsp; {f.departure.airport.name} ({f.departure.airport.code})</div>
                            <div className='point'>
                              <small>Travel Time: &nbsp;
                                {duration_time.days > 0 && (
                                  duration_time.days > 1
                                  ? <span>{duration_time.days} days </span>
                                  : <span>{duration_time.days} day </span>)}
                                {duration_time.hours > 0 && (
                                  duration_time.hours > 1
                                  ? <span>{duration_time.hours} hrs </span>
                                  : <span>{duration_time.hours} hr </span>)}
                                {duration_time.minutes > 0 && (
                                  duration_time.minutes > 1
                                  ? <span>{duration_time.minutes} mins </span>
                                  : <span>{duration_time.minutes} min </span>)}
                              </small>
                            </div>
                            <div className='point point-end'><strong>{moment(f.arrival.datetime).format('HH:mm')}</strong> &nbsp; {f.arrival.airport.name} ({f.arrival.airport.code})</div>
                          </div>
                          <div className='flight-aircraft'>
                            {flight_service_and_equipment.length > 0 ? <small>{flight_service_and_equipment.join('    •    ')}</small> : <small>&nbsp;</small>}
                          </div>
                          {layover && <div className='flight-stops'>
                            <small>
                              {layover_time.days > 0 && (
                                layover_time.days > 1
                                ? <span>{layover_time.days} days </span>
                                : <span>{layover_time.days} day </span>)}
                              {layover_time.hours > 0 && (
                                layover_time.hours > 1
                                ? <span>{layover_time.hours} hrs </span>
                                : <span>{layover_time.hours} hr </span>)}
                              {layover_time.minutes > 0 && (
                                layover_time.minutes > 1
                                ? <span>{layover_time.minutes} mins </span>
                                : <span>{layover_time.minutes} min </span>)}
                              layover &nbsp;
                              <strong>{f.arrival.airport.name} ({f.arrival.airport.code})</strong>
                            </small>
                          </div>}
                        </div>
                      })}
                      <div className='flight-duration'>
                        <small>
                          Travel Time: &nbsp;
                          {outbound_travel_time.days > 0 && (
                            outbound_travel_time.days > 1
                            ? <span>{outbound_travel_time.days} days </span>
                            : <span>{outbound_travel_time.days} day </span>)}
                          {outbound_travel_time.hours > 0 && (
                            outbound_travel_time.hours > 1
                            ? <span>{outbound_travel_time.hours} hrs </span>
                            : <span>{outbound_travel_time.hours} hr </span>)}
                          {outbound_travel_time.minutes > 0 && (
                            outbound_travel_time.minutes > 1
                            ? <span>{outbound_travel_time.minutes} mins </span>
                            : <span>{outbound_travel_time.minutes} min </span>)}
                        </small>
                      </div>
                    </Col>
                    {//returningFlight && ((return_dep[0]>=returntime.departure_start && return_dep[0]<=returntime.departure_end) && (return_arrv[0]>=returntime.arrival_start && return_arrv[0]<=returntime.arrival_end)) &&
                    flight.od2.segments.length > 0 &&
                      <Col xs={24} lg={12} className='flight-travel'>
                        <div><strong>Return</strong>&nbsp;&nbsp;{moment(return_flights[0].departure.datetime).format('ddd, D MMM YYYY')}</div>
                        <Divider style={{margin:'20px 0'}} />
                        <div className='flight-logo'><img src={`//cdn.net.in/static/airlines/${return_flights[0].carrier_code}.gif`} /> &nbsp; <strong>{outbound_flights[0].carrier_name.substring(0,25)}</strong></div>
                        {return_flights.map((f,i) => {
                          const layover = return_flights[i+1] ? moment.duration(moment(return_flights[i+1].departure.datetime).diff(f.arrival.datetime)).asMilliseconds() : null
                          const layover_time = {
                            days: moment.duration(layover).days(),
                            hours: moment.duration(layover).hours(),
                            minutes: moment.duration(layover).minutes()
                          }
                          const duration = moment.duration(moment(f.arrival.datetime).diff(f.departure.datetime)).asMilliseconds()
                          const duration_time = {
                            days: moment.duration(duration).days(),
                            hours: moment.duration(duration).hours(),
                            minutes: moment.duration(duration).minutes()
                          }
                          const flight_service_and_equipment = []
                          f.cabin && flight_service_and_equipment.push(f.cabin)
                          f.carrier_code && f.flight_number && flight_service_and_equipment.push(f.carrier_code+f.flight_number)
                          f.equipment_name && flight_service_and_equipment.push(f.equipment_name)
                          return <div key={i}>
                            <div className='flight-route'>
                              <div className='point point-end'><strong>{moment(f.departure.datetime).format('HH:mm')}</strong> &nbsp; {f.departure.airport.name} ({f.departure.airport.code})</div>
                              <div className='point'>
                                <small>Travel Time: &nbsp;
                                  {duration_time.days > 0 && (
                                    duration_time.days > 1
                                    ? <span>{duration_time.days} days </span>
                                    : <span>{duration_time.days} day </span>)}
                                  {duration_time.hours > 0 && (
                                    duration_time.hours > 1
                                    ? <span>{duration_time.hours} hrs </span>
                                    : <span>{duration_time.hours} hr </span>)}
                                  {duration_time.minutes > 0 && (
                                    duration_time.minutes > 1
                                    ? <span>{duration_time.minutes} mins </span>
                                    : <span>{duration_time.minutes} min </span>)}
                                </small>
                              </div>
                              <div className='point point-end'><strong>{moment(f.arrival.datetime).format('HH:mm')} <Tooltip placement='right' title={moment(f.arrival.datetime).format('HH:mm')+' Singapore time'}><Icon type='info-circle' style={{fontSize:12}} /></Tooltip></strong> &nbsp; {f.arrival.airport.name} ({f.arrival.airport.code})</div>
                            </div>
                            <div className='flight-aircraft'>
                              {flight_service_and_equipment.length > 0 ? <small>{flight_service_and_equipment.join('    •    ')}</small> : <small>&nbsp;</small>}
                            </div>
                            {layover && <div className='flight-stops'>
                              <small>
                                {layover_time.days > 0 && (
                                  layover_time.days > 1
                                  ? <span>{layover_time.days} days </span>
                                  : <span>{layover_time.days} day </span>)}
                                {layover_time.hours > 0 && (
                                  layover_time.hours > 1
                                  ? <span>{layover_time.hours} hrs </span>
                                  : <span>{layover_time.hours} hr </span>)}
                                {layover_time.minutes > 0 && (
                                  layover_time.minutes > 1
                                  ? <span>{layover_time.minutes} mins </span>
                                  : <span>{layover_time.minutes} min </span>)}
                                layover &nbsp;
                                <strong>{f.arrival.airport.name} ({f.arrival.airport.code})</strong>
                              </small>
                            </div>}
                          </div>
                        })}
                        <div className='flight-duration'>
                          <small>
                            Travel Time: &nbsp;
                            {return_travel_time.days > 0 && (
                              return_travel_time.days > 1
                              ? <span>{return_travel_time.days} days </span>
                              : <span>{return_travel_time.days} day </span>)}
                            {return_travel_time.hours > 0 && (
                              return_travel_time.hours > 1
                              ? <span>{return_travel_time.hours} hrs </span>
                              : <span>{return_travel_time.hours} hr </span>)}
                            {return_travel_time.minutes > 0 && (
                              return_travel_time.minutes > 1
                              ? <span>{return_travel_time.minutes} mins </span>
                              : <span>{return_travel_time.minutes} min </span>)}
                          </small>
                        </div>
                      </Col>}
                  </Row>
                </div>
              </Tabs.TabPane>
            </Tabs>
          </div>
          <div className='flight-action'>
            <div className='flight-price'>
              {computed.beforePriceRules === computed.afterPriceRules
              ? <span>{customPriceDisplay(computed.beforePriceRules)}</span>
              : <>
                  <span style={{textDecoration:'line-through'}}>{customPriceDisplay(computed.beforePriceRules)}</span> &nbsp;
                  <span>{customPriceDisplay(computed.afterPriceRules)}</span>
                </>
              }
            </div>
            <div onClick={(fares)=>onFareBreakdownClick(flight.pricings[0].ptc_breakdown_map)}><a><small className='flight-pricing-icon'><SvgIcon name='fare' width='16' height='16' fill='#D02C2F' /> &nbsp; fare breakdown</small></a></div>
            <Button type='primary' size='large' disabled={flightMap[index] != null} onClick={() => {onFlightClick(index, flight) }}>SELECT</Button>
          </div>
        </div>
        {flightMap[index] != null &&
          <div id='options' className='flight-options'>
            <div>
              {flightMap[index].result == null
              ? <div className='pad'>
                  <NoResult response={flightMap[index]} type='fares' loadingMessage='Verifying prices and availability...' />
                </div>
              : <RenderFares flightIndex={index} cart={cart} flight={flightMap[index].result} onFareClick={onFareClick} windowWidth={windowWidth} />}
            </div>
          </div>
        }
      </div>
    </div>
  })}</>
}