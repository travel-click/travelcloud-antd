import React from "react";
import { Collapse, Icon, Row, Col, Button, List, Avatar } from "antd";
import Router from 'next/router'

export const GlobalTixSearchResult: React.StatelessComponent<{
  data: any;
}> = ({ data }) => {
  return (
    <div key={"main-container"} className="globaltix-result">
      {data == null || data.length === 0 ? (
        <h4>No records found!</h4>
      ) : (
          <List
            size="large"
            itemLayout="horizontal"
            dataSource={data}
            className="main-collaspe-relative"
            renderItem={(item: any) => (
              <List.Item>
                <List.Item.Meta
                  avatar={<Avatar src={
                                "https://uat-api.globaltix.com/api/image?name=" +item.imagePath} />}
                  title={<a onClick={() => Router.push({
                    pathname: '/globaltix-details2',
                    query: {'attractionId': item.id },
                  })}>{item.title}</a>}
                />
              </List.Item>
            )}
          />
        )}
    </div>
  );
};

