import React, { useState, useEffect } from 'react';
import { Element, scroller } from 'react-scroll';
import { HotelbedsResult } from '../components/hotelbeds-results';
import { List, Collapse, Tabs, Row, Col, Tag, Typography, Button, Icon, Statistic, Modal, Tooltip } from 'antd';
import { IoMdCamera } from 'react-icons/io';
import { FaBuilding, FaMapMarkerAlt, FaStar, FaStarHalf, FaBed } from 'react-icons/fa';
import { Map } from '../components/hotelbeds/utilities';
import moment from 'moment';
import { TravelCloudClient, groupBy } from '../travelcloud';
import { mergeImageTypes, prepareCancellationPolicy, RateClassInfo, mergeFacilityTypes, isFacilityAvailable, getHotelResultUtilities, useRoomCharacteristicMap } from '../components/hooks/hotelbeds';
import Lightbox from 'react-image-lightbox';
import Big from "big.js";
import "react-image-lightbox/style.css"


const { Panel } = Collapse;

//customized
export const Facility = ({ facility, checkIn, checkOut }: any) => {
  const isAvailable = isFacilityAvailable(facility, checkIn, checkOut)

  return (
    (isAvailable && facility.number !== 0) && (
      <li>
        <Row>
          <Col span={2}>
            <Icon type="check-circle" />
          </Col>
          <Col span={22}>
            <Typography.Text delete={!isAvailable} style={{ paddingLeft: 5 }}>
              {facility.description.content}
            </Typography.Text>

            {facility.number != null && facility.number !== 0 ? (
              <Typography.Text>: {facility.number}</Typography.Text>
            ) : null}

            {facility.indFee != null && facility.indFee === true ? (
              <Tag color="red" style={{ marginLeft: 5 }}>
                Extra Fees
                {facility.amount != null && (
                  <span>
                    {facility.currency} {facility.amount}
                  </span>
                )}
              </Tag>
            ) : null}

            {facility.dateFrom != null ? (
              <Typography.Text>From: {facility.dateFrom}</Typography.Text>
            ) : null}
            {facility.dateTo != null ? (
              <Typography.Text>To: {facility.dateTo}</Typography.Text>
            ) : null}

            <br />
            {facility.fromAge != null || facility.toAge != null ? (
              <Typography.Text>
                (Age Requirement:
                {facility.fromAge != null ? (
                  <span>From: {facility.fromAge}</span>
                ) : null}
                {facility.toAge != null ? (
                  <span>To: {facility.toAge}</span>
                ) : null}
                )
              </Typography.Text>
            ) : null}
          </Col>
        </Row>
      </li>
    )
  );
};

//customized
export const Facilities = ({
  facilities,
  facilityTypes,
  checkIn,
  checkOut,
  windowWidth
}: {
  facilities: Array<any>;
  facilityTypes: any;
  checkIn: any;
  checkOut: any;
  windowWidth: any;
}): any => {
  let groups = mergeFacilityTypes(facilities, facilityTypes);

  return (
    <Row type="flex" justify="space-between" className="facility">
      {groups.map(
        (group: any, index: number) =>
          !group.description.content.includes("Hotel type") &&
          !group.description.content.includes("payment") && (
            <Col xs={24} sm={24} md={12} lg={8} key={`fac${index}`}>
              <h4>{group.description.content}</h4>
              <div className="facility-box" key={`fbox${index}`}>
                <ul>
                  {group.facilities.map(
                    (facility, index) =>
                      facility["indYesOrNo"] == undefined && (
                        <Facility
                          key={index}
                          facility={facility}
                          checkIn={checkIn}
                          checkOut={checkOut}
                        />
                      )
                  )}
                </ul>
              </div>
            </Col>
          )
      )}
    </Row>
  );
};

//customized
const ImagesTab = ({
  images,
  imageTypes,
  client,
  current,
  windowWidth
}: {
  images: Array<any>;
  imageTypes;
  client: TravelCloudClient;
  current;
  windowWidth: any;
}): any => {
  const [showModal, setShowModal] = useState(false);
  const [path, setPath] = useState("");
  const [id, setId] = useState("");

  const thumbnailUrlPrefix = "http://photos.hotelbeds.com/giata/medium/";
  const urlPrefix = "http://photos.hotelbeds.com/giata/bigger/";

  let groupedImages = !!images && mergeImageTypes(
    images.filter((x) => x.imageTypeCode !== "HAB"),
    imageTypes,
  );

  const [lightBoxImages, setLightBoxImages] = useState(groupedImages.map(i => i[0]));
  const [image, setImage] = useState({path: '', description: { content: '' }});

  const prevImage = (photo) => {
    let index = lightBoxImages.indexOf(photo);
    if (index === 0) return lightBoxImages[lightBoxImages.length - 1];
    if (index < 0 ) return photo;
    return lightBoxImages[index - 1];
  };

  const nextImage = (photo) => {
    let index = lightBoxImages.indexOf(photo);
    if (index === lightBoxImages.length - 1) return lightBoxImages[0];
    if (index < 0) return photo;
    return lightBoxImages[index + 1];
  };

  let roomImages = !!images && groupBy(
    images.filter((x) => x.imageTypeCode === "HAB"),
    (x) => x.characteristicCode
  );

  let roomTypes = !!images && roomImages
    .map((room) => {
      return room[0].characteristicCode;
    })
    .filter((x) => x !== undefined);

  const map = useRoomCharacteristicMap(roomTypes, client)
  // console.log(prevImage, nextImage)

  return groupedImages.length > 0 ? (
    <Row className="gallery">
      <Modal
        closable={true}
        footer={false}
        visible={showModal}
        destroyOnClose={true}
        centered
        bodyStyle={{ width: "auto", height: "auto" }}
        onCancel={() => setShowModal(false)}
      >
        <img src={path} width="85%" height="auto" />
      </Modal>

      <Col span={24}>
        <Tabs defaultActiveKey="1" tabPosition={windowWidth > 1024 ? 'left' : 'top'}>
          {groupedImages.map((images, index: number) => (
            <Tabs.TabPane
              tab={images[0].description.content}
              key={`other-${index}`}
            >
              <List
                grid={{
                  gutter: 8,
                  xs: 2,
                  sm: 3,
                  md: 4,
                  lg: 6,
                }}
                dataSource={images}
                renderItem={(image: any, index) => {
                  return <List.Item>
                    <div
                      key={`images${index}`}
                      className="gallery-pic"
                      style={{
                        cursor: 'pointer',
                        backgroundImage: `url(${thumbnailUrlPrefix + image.path})`,
                      }}
                      onClick={() => {
                        setImage(image)
                      }}
                    />
                  </List.Item>
                }}
              />
              <div></div>
              {image.path != '' && (
                <Lightbox
                  mainSrc={urlPrefix + image.path}
                  // nextSrc={urlPrefix + prevImage(image).path}
                  // prevSrc={urlPrefix + nextImage(image).path}
                  onCloseRequest={() => setImage({ path: '', description: {content: ''} })}
                  // onMovePrevRequest={() => setImage(prevImage(image))}
                  // onMoveNextRequest={() => setImage(nextImage(image))}
                  imageCaption={<div className="center-block">{image.description.content}</div>}
                />
              )}
            </Tabs.TabPane>
          ))}
        </Tabs>
      </Col>
    </Row>
  ) : null;
};

//customized
const RoomRates = ({
  hotel,
  onHotelRoomRateClick,
}: {
  hotel: any;
  onHotelRoomRateClick: any;
}) => {
  if (hotel.rooms != null && hotel.rooms.length > 0) {
    const groupedRooms = hotel.rooms.map((room: any) => {
      const rates = groupBy(
        room.rates,
        (x) => `${x.boardCode} ${x.rateClass}`,
        "pairs"
      );

      return { ...room, rates: rates };
    });
    const roomInfo = (rate, index) => (
      <>
        {rate.offers != null && rate.offers.length > 0 ? (
          <>
            <b>Offers</b>
            <ul>
              {rate.offers.map((p, index) => (
                <li key={`off` + index}>
                  {p.name} {hotel.currency} {p.amount}
                </li>
              ))}
            </ul>
          </>
        ) : null}
        {rate.promotions != null && rate.promotions.length > 0 ? (
          <>
            <ul>
              {rate.promotions.map((p, idx) => (
                <li key={`promo` + idx}>{p.name}</li>
              ))}
            </ul>
          </>
        ) : null}
        {rate.offers != null && rate.offers.length > 0 && <b>Cancellation</b>}
        {prepareCancellationPolicy(rate, hotel.currency) !== null && (
          <>
            <ul>
              {prepareCancellationPolicy(rate, hotel.currency).map(
                (policy, n) => (
                  <li key={`policy${n}`}>{policy}</li>
                )
              )}
            </ul>
          </>
        )}
      </>
    );

    return groupedRooms.map((room: any, idx: number) => (
      <div className="room-row" key={`groom${idx}`}>
        <h3>
          <span>
            <FaBed />
          </span>{" "}
          {room.name}
        </h3>
        {Object.keys(room.rates).map((key) => {
          return (
            <Row key={`rates` + key} className="type-row">
              <Col sm={24} md={14} lg={14}>
                {room.rates[key][0]["rateClass"] !== "" ? (
                  <h5>
                    {room.rates[key][0]["boardName"]}{" "}
                    <sup>
                      <Tooltip
                        placement="rightTop"
                        title={RateClassInfo[room.rates[key][0]["rateClass"]]}
                      >
                        <Icon type="info-circle" />
                      </Tooltip>
                    </sup>
                  </h5>
                ) : (
                    <>
                      <h5>{room.rates[key][0]["boardName"]}</h5>
                    </>
                  )}

                {room.rates[key].map((rate, index) =>
                  prepareCancellationPolicy(rate, hotel.currency) == null ? (
                    <Tag color="cyan" key={`tag_${index}`}> Free Cancellation</Tag>
                  ) : (
                      <Collapse
                        bordered={false}
                        key={`pol${index}`}
                        className="price-info"
                      >
                        <Panel
                          key={`r${key}`}
                          header={
                            <>
                              {rate.offers != null && rate.offers.length > 0 && (
                                <Tag color="magenta">Discount</Tag>
                              )}
                              {rate.rateClass == "NRF" ? (
                                <Tag color="red">Non-refundable</Tag>
                              ) : (
                                  <Tag color="orange">Cancellation Policy</Tag>
                                )}
                            </>
                          }
                          showArrow={false}
                        >
                          {roomInfo(rate, hotel.currency)}
                        </Panel>
                      </Collapse>
                    )
                )}
              </Col>
              <Col xs={24} sm={24} md={10} lg={10} className="right">
                {room.rates[key].some(
                  (rate: any) => rate._view.hasDiscount
                ) && (
                    <Typography.Text delete>
                      {hotel.currency}
                      {room.rates[key]
                        .reduce(
                          (acc, cur) => acc.add(cur._view.beforeDiscount),
                          Big(0)
                        )
                        .toString()}
                    </Typography.Text>
                  )}
                <Button
                  className="btn-details"
                  onClick={() =>
                    onHotelRoomRateClick({
                      hotel,
                      room,
                      rates: room.rates[key],
                    })
                  }
                >
                  <sup>for </sup> {hotel.currency}
                  {room.rates[key]
                    .reduce(
                      (acc, cur) => acc.add(cur._view.afterDiscount),
                      Big(0)
                    )
                    .toString()}{" "}
                  <Icon type="right" />
                </Button>
              </Col>
            </Row>
          );
        })}
      </div>
    ));
  } else {
    return <div>No Room Available at the moment.</div>;
  }
};

const CustomizeHotelbedsResult = ({ hotels, types, checkIn, checkOut, client, onHotelRoomRateClick, windowWidth, googleAPIKey }) => {
  const [current, setCurrent] = useState("tab-price")

  function callback(key) {
    if (key.length > 0) {
      scroller.scrollTo(key[0], {
        duration: 800,
        delay: 5,
        offset: -10,
        smooth: "easeInOutQuart",
      });
    }
  }
  const { findStars, countNights } = getHotelResultUtilities;
  const nights = countNights(checkIn, checkOut)

  const listNights = nights > 0 ? `${nights} Nights` : `1 Night`;

  return (
    <HotelbedsResult
      hotels={hotels}
      types={types}
      checkIn={checkIn}
      checkOut={checkOut}
      client={client}
      onHotelRoomRateClick={onHotelRoomRateClick}
      googleAPIKey={googleAPIKey}
    >
      {(params) => {
        const { hotels, imageUrlPrefix, rateClasses, types } = params;

        const hotelOverview = (hotel) => (
          <Row gutter={{ lg: 24 }} type="flex" justify="space-between">
            <Col xs={24} sm={24} md={24} lg={8}>
              {hotel.details.images != null && hotel.details.images.length > 0 ? (
                <div className="hotel-cover">
                  {hotel.details.images[0] && (
                    <img src={imageUrlPrefix + hotel.details.images[0].path} />
                  )}
                </div>
              ) : (
                  <div className="placeholder-image">
                    <FaBuilding />
                  </div>
                )}
            </Col>
            <Col xs={24} sm={24} md={24} lg={16}>
              <div className="hotel-info">
                <h2>
                  {hotel.details.name.content}{" "}
                  {[...Array(findStars(hotel.categoryName))].map((i, index) => (
                    <FaStar key={`s${hotel.code + index}`} />
                  ))}
                  {hotel.categoryName.includes("HALF") && <FaStarHalf />}
                </h2>

                <Row type="flex" className="address" align="middle">
                  <Col xs={2} sm={2} lg={1}>
                    <FaMapMarkerAlt />
                  </Col>
                  <Col xs={22} sm={22} lg={23}>
                    {hotel.details.address.content} {hotel.details.city.content}{" "}
                    {hotel.details.postalCode}
                    <a onClick={() => setCurrent("tab-map")}> (View Map)</a>
                  </Col>
                </Row>

                <Row justify="space-between" align="top" className="details-btn">
                  <Col xs={24} sm={24} md={24} lg={8}>
                    {!!rateClasses[hotel.rooms[0]] &&
                      rateClasses[hotel.rooms[0].rates[0].rateClass].includes(
                        "Non-"
                      ) && (
                        <Tag color="red">
                          {rateClasses[hotel.rooms[0].rates[0].rateClass]}
                        </Tag>
                      )}
                  </Col>
                  <Col xs={24} sm={24} md={24} lg={16} className="right">
                    {!!hotel.rooms[0] && (
                      <div className="price">
                        <sup> {listNights} frm </sup>

                        {(((hotel.rooms[0] || {}).rates || {})[0] || {})._view
                          .hasDiscount && (
                            <Typography.Text delete>
                              {hotel.currency}
                              {(
                                ((hotel.rooms[0] || {}).rates || {})[0] || {}
                              )._view.beforeDiscount.toString()}
                            </Typography.Text>
                          )}

                        {hotel.currency}
                        {(
                          ((hotel.rooms[0] || {}).rates || {})[0] || {}
                        )._view.afterDiscount.toString()}
                      </div>
                    )}

                    <Element name={`${hotel.code}`}>
                      <Button>
                        View Details <Icon type="down" />
                      </Button>
                    </Element>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        );

        const occupantDetails = (hotel) => !!hotel.rooms[0] &&
          hotel.rooms
            .slice(0, 1)[0]
          ["rates"].slice(0, 1)
            .map((i, index) => (
              <div key={`occ${index}`} className="right book">
                Booking for {listNights},
                <Statistic
                  title=""
                  value={i["rooms"]}
                  suffix={i["rooms"] > 1 ? " Rooms" : " Room"}
                />
                <Statistic
                  title=""
                  value={i["adults"]}
                  suffix={i["adults"] > 1 ? " Adults" : " Adult"}
                />
                {i["children"] > 1 && (
                  <Statistic
                    title=""
                    value={i["children"]}
                    suffix={
                      i["children"] > 1 ? " Children" : " Child"
                    }
                  />
                )}
              </div>
            ))

        return (
          <div id="hotelbeds-result2">
            <List
              dataSource={hotels}
              renderItem={(hotel: any) => (
                <List.Item key={hotel.code}>
                  <Collapse className="hotel" onChange={(e) => callback(e)}>
                    <Panel
                      header={hotelOverview(hotel)}
                      key={hotel.code}
                      showArrow={false}
                    >
                      <Tabs className="info" onChange={(key) => { setCurrent(key) }} defaultActiveKey={current}>
                        <Tabs.TabPane tab="Room Types" key="tab-price">
                          {occupantDetails(hotel)}
                          <RoomRates
                            hotel={hotel}
                            onHotelRoomRateClick={onHotelRoomRateClick}
                          />
                        </Tabs.TabPane>
                        <Tabs.TabPane
                          tab="Description"
                          key="tab-desc"
                          className="tab-desc"
                        >
                          <p>{hotel?.details?.description?.content}</p>
                          {hotel.details.images != null && (
                            <>
                              <h3>
                                <IoMdCamera /> Gallery
                              </h3>
                              <ImagesTab windowWidth={windowWidth}
                                images={hotel.details.images} current={current}
                                imageTypes={types.imageTypes}
                                client={client}
                              />
                            </>
                          )}
                          {!!hotel.details.facilities &&
                            hotel.details.facilities.length > 1 && (
                              <>
                                {" "}
                                <h3>
                                  <FaBuilding /> Facilities
                                </h3>
                                <Facilities
                                  key={"fac-list"}
                                  windowWidth={windowWidth}
                                  facilities={hotel.details.facilities}
                                  checkIn={checkIn}
                                  checkOut={checkOut}
                                  facilityTypes={types.facilityTypes}
                                />
                              </>
                            )}
                        </Tabs.TabPane>

                        <Tabs.TabPane tab="Map" key="tab-map" className="tab-map">
                          <Row
                            type="flex"
                            gutter={{ md: 16, lg: 16 }}
                            justify="space-between"
                          >
                            <Col xs={24} sm={14} md={15} lg={15}>
                              <Map googleAPIKey={googleAPIKey} {...hotel.details.coordinates} />
                            </Col>
                            <Col xs={24} sm={24} md={9} lg={9}>
                              {typeof hotel.details.interestPoints !==
                                "undefined" ? (
                                  <React.Fragment>
                                    <h4>Tourist Attraction</h4>
                                    <List
                                      dataSource={hotel.details.interestPoints}
                                      renderItem={(item: any, index) => (
                                        <List.Item key={index} className="map-row">
                                          <Row
                                            gutter={{ lg: 8 }}
                                            justify="space-between"
                                            align="top"
                                          >
                                            <Col span={19}>{item.poiName}</Col>
                                            <Col span={5}>{item.distance} m</Col>
                                          </Row>
                                        </List.Item>
                                      )}
                                    />
                                  </React.Fragment>
                                ) : null}
                              <h3>Address</h3>
                              <p>{hotel.details.address.content} {hotel.details.city.content}{" "}
                                {hotel.details.postalCode}</p>
                              {typeof hotel.details.terminals !== "undefined" ? (
                                <React.Fragment>
                                  <h3>Transportation</h3>
                                  <List
                                    dataSource={hotel.details.terminals}
                                    renderItem={(item: any, index) =>
                                      types.terminalTypes[item.terminalCode] !=
                                        null ? (
                                          <List.Item key={index} className="map-row">
                                            <Row
                                              gutter={{ lg: 8 }}
                                              justify="space-between"
                                              align="top"
                                            >
                                              <Col span={19}>
                                                {" "}
                                                {
                                                  types?.terminalTypes[
                                                    item.terminalCode
                                                  ]?.name?.content
                                                }{" "}
                                                <br />
                                                <Tag color="blue">
                                                  {
                                                    types?.terminalTypes[
                                                      item.terminalCode
                                                    ]?.description?.content
                                                  }
                                                </Tag>
                                              </Col>
                                              <Col span={5}>{item.distance} km</Col>
                                            </Row>
                                          </List.Item>
                                        ) : null
                                    }
                                  />
                                </React.Fragment>
                              ) : null}
                            </Col>
                          </Row>
                        </Tabs.TabPane>
                      </Tabs>
                    </Panel>
                  </Collapse>
                </List.Item>
              )}
            />
          </div>
        )
      }}
    </HotelbedsResult>
  )
}

export default CustomizeHotelbedsResult;