import React from 'react'
import { newDateLocal } from '../order-product-compute';
import { FlightDetail } from '../types'

function formatDate(str: string) {
  const stripped = str.replace(/[+-][0-9][0-9]:[0-9][0-9]$/,'')
  var options: Intl.DateTimeFormatOptions = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };
  var myDate  = newDateLocal(stripped);
  return myDate.toLocaleDateString("en-US", options)
}

const OdInfo = ({od}: {od: FlightDetail['od1']}) => {
  return <>{od.segments.map((segment, index) => {
    const flights_info = segment.flights_info
    const stops = flights_info.stops == null ? [] : flights_info.stops
    return <div key={index}>
      <div><b>{segment.marketing_carrier.code}{segment.flight_number}</b></div>
      <table style={{width: '100%'}}>
        <tbody>
          <tr><td>&raquo;</td><td className="list_label">Origin:</td><td>{flights_info.origin.code} {flights_info.origin_terminal != null && "Terminal " + flights_info.origin_terminal}</td></tr>
          <tr><td>&nbsp;</td><td className="list_label">Departure:</td><td>{formatDate(flights_info.origin_datetime)}</td></tr>
          <tr><td>&nbsp;</td></tr>
        </tbody>
        {stops.map((stop, index2) => <tbody key={index2}>
          <tr><td>&raquo;</td><td className="list_label">Stopover:</td><td>{stop.airport.code}</td></tr>
          {stop.arrival_datetime && <tr><td>&nbsp;</td><td className="list_label">Arrival</td><td>{stop.arrival_datetime}</td></tr>}
          {stop.departure_datetime && <tr><td>&nbsp;</td><td className="list_label">Departure</td><td>{formatDate(stop.departure_datetime)}</td></tr>}
          <tr><td>&nbsp;</td></tr>
        </tbody>)}
        <tbody>
          <tr><td>&raquo;</td><td className="list_label">Destination:</td><td>{flights_info.destination.code} {flights_info.destination_terminal != null && "Terminal " + flights_info.destination_terminal}</td></tr>
          <tr><td>&nbsp;</td><td className="list_label">Arrival:</td><td>{formatDate(flights_info.destination_datetime)}</td></tr>
        </tbody>
      </table>
    </div>
  })}</>
}

export const Flight = (
  {order}
  : {order}) => {
  const products = order.products || []

  const productsFiltered = products.filter(product => product.product_type === 'flight')

  if (productsFiltered.length === 0) return null

  // properties in the outermost dive may not be rendered when
  // nextjs replaces client rendered dom with server rendered dom
  // add additional div outside to workaround this
  return <div><div className="tc-invoice">
    {productsFiltered.map((product, index) => {      
      const hasVoucher = product.voucher != null && product.voucher.flight_tickets != null
      return <div key={index}>
        <table className="invoice_table_style"><tbody>
          <tr className="item_name">
            <td className="col1">Flight Booking</td>
            {hasVoucher && <td>{product.product_source_ref}</td>}
          </tr>
          <tr className="header">
            <td className="col1" style={{width: "50%"}}>Itinerary</td>
            {hasVoucher && <td className="col1" style={{width: "50%"}}>Passengers</td>}
          </tr>
          <tr>
            <td className="col1" style={{verticalAlign: 'top', paddingTop: '10px'}}>
              <OdInfo od={product.product_detail.od1} />
              {product.product_detail.od2 != null && 
                <>
                  <hr style={{margin: '10px 5px'}} />
                  <OdInfo od={product.product_detail.od2} />
                </>}
            </td>
            {hasVoucher && <td className="col1" style={{verticalAlign: 'top', paddingTop: '10px'}}>
                {product.voucher.flight_tickets.map((ticket, index2) => <div key={index2}>
                  <div><b>{ticket.first_name} {ticket.last_name}</b></div>
                  <table style={{width: '100%'}}><tbody>
                    <tr><td>&raquo;</td><td className="list_label">Passport:</td><td>{ticket.passport}</td></tr>
                    <tr><td>&nbsp;</td><td className="list_label">Ticket Num:</td><td>{ticket.ticket_number}</td></tr>
                    <tr><td>&nbsp;</td></tr>
                    </tbody></table></div>
                )}
            </td>}
          </tr>
        </tbody></table>
      </div>
    })}

  </div></div>}