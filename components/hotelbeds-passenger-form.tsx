import React, { Fragment, useState, useEffect, createContext, useRef } from 'react'
import { Row, Col, Icon, Card, Button, Tabs, Popover, Carousel, Radio, Form, Input } from 'antd'
import { TravelCloudClient, formatCurrency, Cart, validateLegacyHotelParams, groupBy, range } from '../travelcloud'
import { HotelbedsSelectedRateInfo } from './hotelbeds-results'
import {useHotelbedsPassengerHook} from './hooks/hotelbeds-passenger-form';
const {Item} = Form

// fixme: provide types
const PassengerTypeSelector = ({ onChange, value }) => {
  return (
    <Radio.Group onChange={onChange} 
      value={value}
      style={{marginBottom: '20px'}}>
      <Radio value={true}>Enter the lead passenger data only</Radio>
      <Radio value={false}>Enter the data for all passenger</Radio>
    </Radio.Group>
  )
}

// fixme: any return type ?
const PassengerForm = (
{
  type,
  onChange,
  roomIndex,
  paxIndex,
  value
} : {
  type ?: string,
  onChange: (value:any) => void,
  roomIndex?: number,
  paxIndex?: number,
  value: string[]
}) => {
  
  const [name, setName] = useState(value[ 0 ])
  const [surname, setSurname] = useState(value[ 1 ])
  
  const firstRender = useRef(true)
  useEffect(() => { 

    // skip on first render
    if (firstRender.current) {
      firstRender.current = false;
      return 
    }

    onChange({
      name: [ name, surname ],
      paxIndex,
      roomIndex
    })

  }, [ name, surname ])

  return (
    <Row>
      <Col span={4}>
        <Item>
          <Input onChange={e => setName(e.target.value)}
            value={name}
            placeholder={type == 'AD' ? 'Name - Adult' : 'Name - Child'} />
        </Item>
      </Col>
      <Col span={4}>
        <Item>
          <Input onChange={e => setSurname(e.target.value)}
            value={surname}
            placeholder={type == 'AD' ? 'Surname - Adult' : 'Name - Child'} />
        </Item>
      </Col>
    </Row>
  )
}


const HotelbedsPassenger = (
{
  title,
  children,
  onUpdate,
  isFormValidated,
  item
} : {
  title ?: string,
  children ?: any,
  onUpdate: (any) => void,
  isFormValidated: (any) => void,
  item: HotelbedsSelectedRateInfo
}) :any => {

  const {
    isLeadPassengerOnly,
    setPassengerFlag,
    rooms,
    setRooms,
    leadPassengerRooms,
    setLeadPassengerRooms,
    onChange,
  } = useHotelbedsPassengerHook({item});

  return (
    <Card style={{marginTop: '5px'}} title={title != null ? title : "Passenger details"}>
      <Row>
        <Col span={24}>
          <PassengerTypeSelector 
            onChange={(e) => { onChange({ isLeadPassengerOnly: e.target.value }, onUpdate, isFormValidated) }}
            value={isLeadPassengerOnly}
            />
        </Col> 
      </Row>
      {isLeadPassengerOnly ? (
        <Row>
          <Col span={24}>
            <h3>Lead Passengers</h3>
            <Form layout="inline">
              {rooms.map((room,idx) => (
                  groupBy(room.paxes, x => x.roomId).map((group,gIdx) => (
                    <React.Fragment key={gIdx}>
                      <h5>
                        {room._display.hotelName}, {room._display.roomName}, &nbsp;
                        {room._display.adults} adults, {room._display.children} children
                      </h5>
                      <PassengerForm key={gIdx} 
                        paxIndex={gIdx}
                        roomIndex={idx}
                        value={group[0].name}
                        type={group[0].type}
                        onChange={(value) => onChange(value, onUpdate, isFormValidated)} />
                    </React.Fragment>
                  ))
              ))}
            </Form>
          </Col>
        </Row>
      ) : (
        <Row>
          <Col span={24}>
            <h3>All Passengers' Data</h3>
            <Form layout="inline">
              {rooms.map((room,idx) => (
                  groupBy(room.paxes, x => x.roomId).map((group,gIdx) => (
                    <React.Fragment key={gIdx}>
                      <h5>
                        {room._display.hotelName}, {room._display.roomName}, &nbsp;
                        {room._display.adults} adults, {room._display.children} children
                      </h5>
                      {group.map((pax,index) => (
                        <PassengerForm key={index} 
                          paxIndex={index}
                          roomIndex={idx}
                          value={pax.name}
                          type={pax.type}
                          onChange={(value) => onChange(value, onUpdate, isFormValidated)} />
                      ))}
                    </React.Fragment>
                  ))
              ))}
            </Form>
          </Col>
        </Row>
      )}
    </Card>
  )
}

export default HotelbedsPassenger