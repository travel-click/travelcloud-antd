import React, { useEffect, useState, createContext, useContext, useMemo, useCallback } from 'react'
import { List, Row, Col, Rate, Icon, Button, Typography, Tabs, Card, Table, Drawer, Carousel, Statistic, Tag, Modal, Spin } from 'antd'
import { formatCurrency, TravelCloudClient, Cart, groupBy, sortBy, overlap, chain, isDateRangeOverlap, updateFormState, firstCapitalLastLower } from '../travelcloud';
import moment from 'moment';
import { GlobalTixSearchResultAndAddToCart } from './globaltix-serach-result-cart';
import Router from 'next/router'
import qs from 'qs'
import { HotelbedsFormValue } from '../order-product-compute';
import CarouselArrow from './carousel-arrow';
import HotelbedsPassenger from '../components/hotelbeds-passenger-form'
import Big from 'big.js'
import { dateDiffInDays } from './hooks/tour-booking';
import { mergeImageTypes, mergeFacilityTypes, getRoomCharacteristicMap, RateClassInfo, prepareCancellationPolicy, useRoomCharacteristicMap, isFacilityAvailable, getHotelResultUtilities } from './hooks/hotelbeds';
import Index from '../pages/hotelbeds-hotels';
import { Lightbox, Map, ImagesTab } from './hotelbeds/utilities';


export const Facility = ({ facility, checkIn, checkOut }: any) => {

  const isAvailable = isFacilityAvailable(facility, checkIn, checkOut)

  // indFee: Boolean
  // Indicator if the facility has cost or not at the establishment

  // distance: Integer
  // Distance in meters to the facility
  // todo: show the distance with unit along with description

  // ageFrom: Integer
  // ageTo: Integer
  // Minimum age to access the facility

  // textValue: String
  // Text value of the facility
  // todo: ?

  // dateFrom: YYYY-MM-DD
  // dateTo: YYYY-MM-DD
  // timeTo: HH:MM:SS
  // timeTo: HH:MM:SS

  // amount: Float
  // currency: String
  // Amount of the facility fee

  // applicationType (PN, PS, UH, UN, US)
  // PN - Per Person & Night
  // PS - Per Person & Stay
  // UH - Per Unit & Hour
  // US - Per Unit & Stay
  // todo: amend age with bracket side note

  // languageCode: String
  // Language in which the facilitily description is shown
  // todo: ?

  return <li>
    {isAvailable ?
      <Icon type="check-circle" className="bullet-icon-yes" theme="filled" twoToneColor="#52c41a" />
      : <Icon type="close-circle" className="bullet-icon-no" theme="filled" twoToneColor="#eb2f96" />}
    <Typography.Text delete={!isAvailable} style={{ paddingLeft: 5 }}>{facility.description.content}</Typography.Text>
    {facility.number != null ? (<Typography.Text strong>: {facility.number}</Typography.Text>) : null}

    {facility.indFee != null && facility.indFee === true ?
      (<Tag color="red" style={{ marginLeft: 5 }}>Extra Fees
        {facility.amount != null && <span>{facility.currency} {facility.amount}</span>}
      </Tag>) : null}

    {facility.dateFrom != null ? <Typography.Text>From: {facility.dateFrom}</Typography.Text> : null}
    {facility.dateTo != null ? <Typography.Text>To: {facility.dateTo}</Typography.Text> : null}

    <br />
    {facility.fromAge != null || facility.toAge != null ? (
      <Typography.Text>
        (Age Requirement:
        {facility.fromAge != null ? <span>From: {facility.fromAge}</span> : null}
        {facility.toAge != null ? <span>To: {facility.toAge}</span> : null})
      </Typography.Text>
    ) : null}
  </li>
}

export const Facilities = ({ facilities, facilityTypes, checkIn, checkOut }:
  { facilities: Array<any>, facilityTypes: any, checkIn: any, checkOut: any }): any => {

  let groups = mergeFacilityTypes(facilities, facilityTypes)

  return (groups.map((group: any, index: number) => (<Row>
    <Col span={24} key={index}>
      <Typography.Text strong>{group.description.content}</Typography.Text>
      <ul style={{ listStyleType: "none" }}>
        {group.facilities.map((facility, index) => (
          <Facility key={index} facility={facility} checkIn={checkIn} checkOut={checkOut} />
        ))}
      </ul>
    </Col>
  </Row>)))
};


export const RoomRates = ({ hotel, onHotelRoomRateClick }: { hotel: any, onHotelRoomRateClick: any }) => {

  console.log('Rendering room rate...')
  // console.log('form value', formValue)

  if (hotel.rooms != null && hotel.rooms.length > 0) {
    const groupedRooms = hotel.rooms.map((room: any) => {
      const rates = groupBy(room.rates, x => `${x.boardCode} ${x.rateClass}`, 'pairs')

      return { ...room, rates: rates }
    })

    return groupedRooms.map((room: any, idx: number) => (
      <React.Fragment key={idx}>
        <Typography.Title level={4}>{room.name}</Typography.Title>
        {Object.keys(room.rates).map(key => {
          return (
            <Row key={key} style={{ marginBottom: 10 }}>
              <Col span={6}>
                <h5>
                  {room.rates[key][0]['boardName']}
                </h5>
                <small>
                  {room.rates[key][0]['rateClass'] !== 'NOR' && <Icon type="exclamation-circle" style={{ paddingRight: 5 }} />}

                  {RateClassInfo[room.rates[key][0]['rateClass']]}
                </small>
              </Col>
              <Col span={13} offset={1}>
                <div style={{}}>
                  {room.rates[key].map((rate, index) => (
                    <div key={index} style={{ paddingBottom: 10 }}>
                      <Row>
                        <Col span={6}>
                          <Statistic title="" value={rate.rooms} suffix=" rooms" />
                        </Col>
                        <Col span={6}>
                          <Statistic title="" value={rate.adults} suffix=" adults" />
                        </Col>
                        <Col span={6}>
                          <Statistic title="" value={rate.children} suffix=" children" />
                        </Col>
                        <Col span={6}>
                          <div>
                            {rate._view.hasDiscount && (
                              <Typography.Text delete>
                                {hotel.currency}
                                {rate._view.beforeDiscount.toString()}
                              </Typography.Text>
                            )}
                          </div>

                          <div>
                            <Typography.Text strong>
                              {hotel.currency}
                              {rate._view.afterDiscount.toString()}
                            </Typography.Text>
                          </div>
                        </Col>
                      </Row>
                      <Typography.Text strong>Cancellation policy</Typography.Text>
                      {prepareCancellationPolicy(rate, hotel.currency) !== null && (
                        <>
                          <ul>
                            {prepareCancellationPolicy(rate, hotel.currency).map(
                              (policy, n) => (
                                <li key={`policy${n}`}>{policy}</li>
                              )
                            )}
                          </ul>
                        </>
                      )}

                      {rate.promotions != null && rate.promotions.length > 0 ? (
                        <React.Fragment>
                          <Typography.Text strong>Promotions</Typography.Text>
                          <ul>
                            {rate.promotions.map((p, idx) => (
                              <li key={idx}>{p.name}</li>
                            ))}
                          </ul>
                        </React.Fragment>
                      ) : null}

                      {rate.offers != null && rate.offers.length > 0 ? (
                        <React.Fragment>
                          <Typography.Text strong>Offers</Typography.Text>
                          <ul>
                            {rate.offers.map((p: any, index: any) => (
                              <li key={index}>{p.name} {hotel.currency} {p.amount}</li>
                            ))}
                          </ul>
                        </React.Fragment>
                      ) : null}
                    </div>
                  ))}
                </div>
              </Col>
              <Col span={4}>
                <div style={{ textAlign: 'right', paddingTop: 5 }}>
                  <div>
                    {room.rates[key].some((rate: any) => rate._view.hasDiscount) && (
                      <Typography.Text delete>
                        {hotel.currency}
                        {room.rates[key].reduce((acc, cur) => acc.add(cur._view.beforeDiscount), Big(0)).toString()}
                      </Typography.Text>
                    )}
                  </div>
                  <div>
                    <Typography.Text strong>
                      {hotel.currency}
                      {room.rates[key].reduce((acc, cur) => acc.add(cur._view.afterDiscount), Big(0)).toString()}
                    </Typography.Text>
                  </div>
                  <div>
                    <Button className="btn-details"
                      style={{ marginLeft: '3px' }}
                      onClick={() => onHotelRoomRateClick({ hotel, room, rates: room.rates[key] })}>
                      Details
                    </Button>
                  </div>

                </div>
              </Col>
            </Row>
          )
        })}
      </React.Fragment>
    ))
  }

  else {
    return <div>No Room Available at the moment.</div>
  }
};

export const HotelbedsResult = ({
  hotels,
  onHotelRoomRateClick,
  types,
  checkIn,
  checkOut,
  client,
  children = null,
  googleAPIKey
}
  : {
    hotels: any,
    onHotelRoomRateClick: (any) => any,
    types: any,
    checkIn: any,
    checkOut: any,
    client: TravelCloudClient,
    children?: (params: { hotels: any, types: any, imageUrlPrefix: string, rateClasses: any }) => any,
    googleAPIKey: string
  }) => {

  const imageUrlPrefix = 'http://photos.hotelbeds.com/giata/';

  const { findStars } = getHotelResultUtilities;

  const rateClasses = {
    NOR: 'Normal',
    NRF: 'Non-refundable',
    SPE: 'Special',
    OFE: 'Offer',
    PAQ: 'Package',
    NRP: 'Non - refundable package'
  };
  const params = {
    hotels,
    types,
    imageUrlPrefix,
    rateClasses
  }
  return useMemo(() => {
    if (children != null) {
      return children(params)
    } else {
      return <div id="hotelbeds-result">
        <List
          dataSource={hotels}
          renderItem={(hotel: any) => (
            <List.Item key={hotel.hotelId}>
              <Card className="hotel" style={{ width: '100%' }}>
                <Row style={{ width: '100%' }}>
                  <Col span={4} style={{ height: 150 }}>
                    {(hotel.details.images != null && hotel.details.images.length > 0) && (
                      <div style={{
                        backgroundImage: `url(${hotel.details.images[0] ? imageUrlPrefix + hotel.details.images[0].path : ''})`,
                        height: '100%',
                        width: '100%',
                        backgroundSize: 'cover',
                        backgroundPosition: 'center'
                      }} />
                    )}
                  </Col>
                  <Col span={14} style={{ height: 150, padding: "16px 32px" }}>
                    <h2>
                      {hotel.details.name.content} {" "}
                      {[...Array(findStars(hotel.categoryName))].map((i, index) => (
                        <img src="/static/star.png" />
                      ))}
                      {hotel.categoryName.includes("HALF") && <img src="/static/half-star.png" />}
                    </h2>
                    <h5>{hotel.categoryName}</h5>
                    <span>{hotel.details.address.content}</span>
                    <span>{hotel.details.city.content}</span>
                    <p>Zip: {hotel.details.postalCode}</p>
                  </Col>
                  <Col span={6} style={{ height: 150 }}>
                    <div>
                      {hotel.rooms != null && hotel.rooms.length > 0 && hotel.rooms[0] ? (
                        <>
                          <Typography.Text type="secondary" strong>{hotel.rooms[0].name}</Typography.Text>
                          <br />
                          <Typography.Text type="warning">{hotel.rooms[0].code}</Typography.Text>
                          <br />
                          <Typography.Text>
                            <Icon type="dollar" /> {rateClasses[hotel.rooms[0].rates[0].rateClass]}
                          </Typography.Text>
                        </>
                      ) : ''}
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col span={24}>
                    <Tabs defaultActiveKey="5" className="info">
                      <Tabs.TabPane tab="Map" key="1">
                        <Col span={14}>
                          <Map {...hotel.details.coordinates} />
                        </Col>
                        <Col span={9} offset={1}>
                          {typeof hotel.details.interestPoints !== 'undefined' ? (
                            <React.Fragment>
                              <h4>Tourist Attraction</h4>
                              <List dataSource={hotel.details.interestPoints}
                                renderItem={(item: any, index) => (
                                  <List.Item key={index}>
                                    <List.Item.Meta title={item.poiName} />
                                    <div>{item.distance} m</div>
                                  </List.Item>
                                )}
                              />
                            </React.Fragment>) : null}

                          {typeof hotel.details.terminals !== 'undefined' ? (
                            <React.Fragment>
                              <h3>Transportation</h3>
                              <List dataSource={hotel.details.terminals}
                                renderItem={(item: any, index) => (
                                  types.terminalTypes[item.terminalCode] != null ? (
                                    <List.Item key={index}>
                                      <List.Item.Meta
                                        title={<Typography.Text>
                                          {types?.terminalTypes[item.terminalCode]?.name?.content} <br />
                                          <Tag>{types?.terminalTypes[item.terminalCode]?.description?.content}</Tag>
                                        </Typography.Text>} />
                                      <div>{item.distance} km</div>
                                    </List.Item>
                                  ) : null
                                )}
                              />
                            </React.Fragment>) : null}
                        </Col>
                      </Tabs.TabPane>
                      <Tabs.TabPane tab="Images" key="2">
                        {hotel.details.images != null && (
                          <ImagesTab images={hotel.details.images}
                            imageTypes={types.imageTypes}
                          />)}
                      </Tabs.TabPane>
                      <Tabs.TabPane tab="Description" key="3">
                        <Typography.Text key={"desc"} style={{ marginBottom: '1rem' }}>{hotel?.details?.description?.content}</Typography.Text>
                        {hotel.details.facilities != null &&
                          <Facilities key={"fac"} facilities={hotel.details.facilities} checkIn={checkIn} checkOut={checkOut} facilityTypes={types.facilityTypes} />}
                      </Tabs.TabPane>
                      <Tabs.TabPane tab="Opinions" key="4">

                      </Tabs.TabPane>
                      <Tabs.TabPane tab="Prices and Boards" key="5">
                        <RoomRates hotel={hotel} onHotelRoomRateClick={onHotelRoomRateClick} />
                      </Tabs.TabPane>
                    </Tabs>
                  </Col>
                </Row>
              </Card>
            </List.Item>
          )} /></div>
    }
  }, [ hotels ])
}

export interface HotelbedsSelectedRateInfo {
  hotel: any,  // todo: provide proper type
  room: any,
  rates: any[]
}
