import React, { useState, useEffect, Children } from "react";
import {
  Form,
  Select,
  Input,
  Button,
  Row,
  Col,
  AutoComplete,
  DatePicker,
  Popover,
  Icon,
  Divider
} from "antd";
import { TravelCloudClient, validateHotelbedsParams } from "../travelcloud";
import moment from "moment";
import SvgIcon from "./svg-icon";
import { generateCountryOptions } from "./country-dropdown";
import { useHotelbedsSearchForm } from "./hooks/hotelbeds-search-form";
import { useHotelBedsOccupancy } from "./hooks/hotelbeds-occupancies";
import { newDateLocal } from "../order-product-compute";
const { Option } = Select;

export const HotelsAndDestinationsName = function ({
  client,
  placeholder,
  style,
  className,
  params,
  onHotelsOrDestinationsChange,
  defaultCityCodes
}: {
  client: TravelCloudClient;
  placeholder?: string;
  style?: React.CSSProperties;
  className?: string;
  params: any;
  onHotelsOrDestinationsChange: Function;
  defaultCityCodes?: string[];
}) {
  const [items, setItems] = useState([]);

  useEffect(() => {
    if(defaultCityCodes != null){
      (async () => {
        const destinations = await client.hotelbedsDefaultCityCodes({ search: defaultCityCodes });
        console.log(destinations);
        const showDestinations = (
          <AutoComplete.OptGroup key={`dest111`} label={"Destinations"}>
            {
              destinations?.map(destination => (
                <AutoComplete.Option
                  key={destination.code}
                  value={destination.code}
                >
                  {destination.name.content}
                </AutoComplete.Option>
              ))
            }
          </AutoComplete.OptGroup>
        )
        setItems([showDestinations]);
      })()
    }
  }, [])

  const onSearch = async (value: string) => {
    if (value.length < 3) return;
    const results = await client.hotelbedsAutoComplete({ search: value });

    if (typeof results != "undefined") {
      const hotels = (
        <AutoComplete.OptGroup key={`hotel111`} label={"Hotels"}>
          {results.hotels.map(x => (
            <AutoComplete.Option key={`${x.code}`} value={x.destination_code + '-' + x.code}>
              {x.name + ', ' + x.destination_name + ', ' + x.country_name}
            </AutoComplete.Option>
          ))}
        </AutoComplete.OptGroup>
      );

      const destinations = (
        <AutoComplete.OptGroup key={`dest111`} label={"Destinations"}>
          {results.destinations.map(x => (
            <AutoComplete.Option
              key={x.code}
              value={x.code}
            >
              {x.name === x.country_name ? x.name : x.name + ', ' + x.country_name}
            </AutoComplete.Option>
          ))}
        </AutoComplete.OptGroup>
      );

      setItems([destinations, hotels]);
    }
  };

  return (
    <AutoComplete
      onSearch={e => onSearch(e)}
      defaultValue={params.destinationOrHotelName}
      onSelect={(val: string, elem: any) => {
        let [destinationCode, hotelCode] = val.split("-");

        onHotelsOrDestinationsChange({
          destinationCode,
          hotelCode,
          name: elem.props.children
        });
      }}
      placeholder={placeholder}
      style={style}
      className={className}
      dataSource={items}
    ></AutoComplete>
  );
};

export const HotelBedsCheckInDatePicker = ({
  placeholder,
  style,
  className,
  params,
  minLeadTime,
  onStayChange
}: {
  className?: string;
  style?: React.CSSProperties;
  placeholder?: string;
  params: any;
  onStayChange: Function;
  minLeadTime?: number
}) => {
  const disabledStartDate = checkInDate => {
    if (checkInDate == null) return false;
    return checkInDate.isBefore(minLeadTime ? moment().add(minLeadTime, "days") : newDateLocal());
  };

  return (
    <DatePicker
      value={
        params.stay.checkIn == null ? null : moment(params.stay.checkIn)
      }
      style={style}
      disabledDate={disabledStartDate}
      placeholder={placeholder}
      className={className}
      dropdownClassName="hide-year-buttons"
      format="YYYY-MM-DD"
      onChange={checkIn =>
        onStayChange({ checkIn: checkIn.format("YYYY-MM-DD") })
      }
    />
  );
};

// export const HotelBedsShiftDaysSelect = ({
//   placeholder,
//   style,
//   className,
//   params,
//   onStayChange
// }: {
//   className?: string;
//   style?: React.CSSProperties;
//   placeholder?: string;
//   params: any;
//   onStayChange: Function;
// }) => {
//   return (
//     <Select
//       onChange={({ shiftDays }) => onStayChange({ shiftDays })}
//       value={params.stay.shiftDays}
//       style={style}
//       placeholder={placeholder}
//       className={className}
//     >
//       <Select.Option value={1}>1</Select.Option>
//       <Select.Option value={2}>2</Select.Option>
//       <Select.Option value={3}>3</Select.Option>
//       <Select.Option value={4}>4</Select.Option>
//       <Select.Option value={5}>5</Select.Option>
//     </Select>
//   );
// };

export const HotelBedsCheckOutDatePicker = ({
  placeholder,
  style,
  className,
  params,
  minLeadTime,
  onStayChange,
}: {
  className?: string;
  style?: React.CSSProperties;
  placeholder?: string;
  params: any;
  onStayChange: Function;
  minLeadTime?: number,
}) => {
  const disabledEndDate = (checkInDate, checkOutDate) => {
    if (checkOutDate == null) return false;

    checkInDate = checkInDate == null ? moment().add(minLeadTime ? minLeadTime : 1, "days") : moment(checkInDate);
    if (!checkOutDate || !checkInDate) {
      return checkOutDate && checkOutDate < moment().endOf("day");
    }

    return (
      checkOutDate.isBefore(moment(checkInDate).add(1, "days")) ||
      checkOutDate.diff(checkInDate, "days") > 30
    );
  };

  const defaultEndDate = checkInDate => {
    return checkInDate == null
      ? moment().add(1, "days")
      : moment(checkInDate).add(1, "days");
  };

  return (
    <DatePicker
      value={
        params.stay.checkOut == null ? null : moment(params.stay.checkOut)
      }
      style={style}
      defaultPickerValue={defaultEndDate(params.stay.checkIn)}
      placeholder={placeholder}
      disabledDate={date => disabledEndDate(params.stay.checkIn, date)}
      className={className}
      dropdownClassName="hide-year-buttons"
      format="YYYY-MM-DD"
      onChange={checkOut =>
        onStayChange({ checkOut: checkOut.format("YYYY-MM-DD") })
      }
    />
  );
};

const range = length => Array.from({ length }, (x, i) => i);

interface BtnOccupancyProps {
  onChange: (value: Number) => any;
  value: Number | String;
  max: Number;
  children?: ({ value, controller }: any) => React.ReactNode | never[];
}

export const BtnOccupancy = (
  {
    onChange,
    value,
    max,
    children
  }: BtnOccupancyProps
) => {
  const handleMinus = () => value > 0 && onChange(Number(value) - 1)
  const handlePlus = () => value < max && onChange(Number(value) + 1)
  const controller = { handleMinus, handlePlus }

  const $nodes = children ? children({ value, controller }) : (
    <div className='number' style={{ fontSize: 16, display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
      <Icon type='minus-circle'
        onClick={handleMinus} />
      <div style={{ margin: '0 16px' }}>{value}</div>
      <Icon type='plus-circle'
        onClick={handlePlus} />
    </div>
  )
  return <React.Fragment>{$nodes}</React.Fragment>
}

export const SelectChildrenAge = ({ value, onSelect }) => {
  return (
    <Select
      showSearch
      style={{ width: 100 }}
      placeholder="Select a person"
      optionFilterProp="children"
      value={value}
      onSelect={onSelect}
    >
      {
        range(12).map((n, index) => (
          <Option key={index} value={n}>{n}</Option>
        ))
      }
    </Select>
  )
}

interface RoomOccupancyProps {
  index: number;
  params: any;
  occupancy: any;
  onOccupanciesChange: Function;
  updateOccupancies: Function;
  updateChildrenPax: Function;
  children?: ({ params, controller }: any) => React.ReactNode | never[]
}

export const RoomOccupancy = ({
  index,
  params,
  occupancy,
  onOccupanciesChange,
  updateOccupancies,
  updateChildrenPax,
  children
}: RoomOccupancyProps) => {
  const controller = { index, occupancy, onOccupanciesChange, updateOccupancies, updateChildrenPax }
  const $nodes = children ? children({ params, controller }) : (
    <Row>
      <Row gutter={24} type="flex" justify="center">
        <Col>
          Room: ({`${index + 1}`})
        </Col>
      </Row>
      <Row gutter={24} type="flex" justify="space-around">
        <Col>
          <Form.Item label="Adult" style={{ textAlign: 'center' }}>
            <BtnOccupancy
              max={4}
              value={occupancy.adults != null ? occupancy.adults : 1}
              onChange={value =>
                onOccupanciesChange(
                  updateOccupancies(params, index, "adults", value)
                )
              }
            />
          </Form.Item>
        </Col>
        <Col>
          <Form.Item label="Children" style={{ textAlign: 'center' }}>
            <BtnOccupancy
              max={3}
              value={occupancy.children != null ? occupancy.children : 0}
              onChange={value =>
                onOccupanciesChange(
                  updateOccupancies(params, index, "children", value)
                )
              }
            />
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24} type="flex" justify="space-around">
        {occupancy.paxes
          ? occupancy.paxes.map((pax, indx) => (
            <React.Fragment key={`children_age_${indx}`}>
              <Col>
                <Form.Item label={`Children: (${indx + 1})`} style={{ textAlign: 'center' }}>
                  <SelectChildrenAge
                    value={pax.age}
                    onSelect={value =>
                      onOccupanciesChange(
                        updateChildrenPax(params, index, indx, value)
                      )
                    }
                  />
                </Form.Item>
              </Col>
            </React.Fragment>
          ))
          : null}
      </Row>
    </Row>
  )
  return <React.Fragment>{$nodes}</React.Fragment>
}

export const HotelBedsOccupancyPopover = ({
  style,
  className,
  params,
  onOccupanciesChange,
  children
}: {
  className?: string;
  style?: React.CSSProperties;
  params: any;
  onOccupanciesChange: Function;
  children?: ({ params, controller }: any) => React.ReactNode | never[];
}) => {
  const [visible, setVisible] = useState(false);

  const hotelBedsOccupancy = useHotelBedsOccupancy();
  const {
    updateOccupancies,
    updateOccupancyElementCount,
    updateChildrenPax
  } = hotelBedsOccupancy;

  const roomTotal = params.occupancies.length;
  let adultTotal = 0;
  params.occupancies.map(occupancy => {
    if (occupancy.adults) {
      adultTotal += Number(occupancy.adults)
    }
  });

  const controller = { ...hotelBedsOccupancy, onOccupanciesChange, roomTotal, adultTotal, visible, setVisible }

  const $nodes = children ? (
    children({ params, controller })
  ) : (
      <Popover
        className={className}
        style={style}
        content={
          <>
            <Row type="flex" justify="center">
              <Col>
                <Form.Item label="Rooms" style={{ textAlign: 'center' }}>
                  <BtnOccupancy
                    max={4}
                    onChange={(value) =>
                      onOccupanciesChange(
                        updateOccupancyElementCount(params, value)
                      )
                    }
                    value={params.occupancies.length}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Divider style={{ margin: 3 }} />
            <Row>
              {
                params.occupancies.map((occupancy, index) => (
                  <React.Fragment key={`room_${index}`}>
                    <RoomOccupancy
                      index={index}
                      occupancy={occupancy}
                      params={params}
                      onOccupanciesChange={onOccupanciesChange}
                      updateOccupancies={updateOccupancies}
                      updateChildrenPax={updateChildrenPax}
                    />
                    <Divider style={{ margin: 3 }} />
                  </React.Fragment>
                ))
              }
            </Row>
          </>
        }
        trigger="click"
        placement="bottom"
        visible={visible}
        onVisibleChange={visible => setVisible(visible)}
      >
        <div style={{ marginBottom: 6 }}>
          <div className='no-border no-radius' style={{ position: 'relative' }}>
            <a className='ant-dropdown-link'>
              <Input size='default' readOnly style={{ width: '100%', cursor: 'pointer' }}
                value={`${roomTotal} Room(s) - ${adultTotal} Adult(s)`} />
              <SvgIcon
                fill='rgba(0,0,0,.15)'
                width='10'
                height='10'
                name='down'
                style={{ position: 'absolute', top: '50%', right: 11, marginTop: -5 }}
              />
            </a>
          </div>
        </div>
      </Popover>
    )
  return <React.Fragment>{$nodes}</React.Fragment>
}


export const HotelBedsOccupancySelects = ({
  placeholder,
  style,
  className,
  params,
  onOccupanciesChange
}: {
  className?: string;
  style?: React.CSSProperties;
  placeholder?: string;
  params: any;
  onOccupanciesChange: Function;
}) => {
  // const [ occupancies, setOccupancies ] = useState([ {} ]);
  const {
    updateOccupancies,
    updateOccupancyElementCount,
    updateChildrenPax
  } = useHotelBedsOccupancy();

  return (
    <>
      <Row>
        <Col>
          <Select
            onChange={(value) => onOccupanciesChange(updateOccupancyElementCount(params, value))}
            value={params.occupancies.length}
            style={style}
            placeholder={placeholder}
            className={className}>
            <Select.Option value={1}>1</Select.Option>
            <Select.Option value={2}>2</Select.Option>
            <Select.Option value={3}>3</Select.Option>
            <Select.Option value={4}>4</Select.Option>
          </Select>
        </Col>
      </Row>
      {params.occupancies.map((occupancy, index) => (
        <React.Fragment key={index}>
          <Row>
            <Col span={12}>Adult:</Col>
            <Col span={12}>Children:</Col>
          </Row>
          <Row>
            <Col span={12}>
              <Select
                style={{ width: "98%" }}
                placeholder="Adult"
                value={occupancy.adults}
                onSelect={e =>
                  onOccupanciesChange(
                    updateOccupancies(params, index, "adults", e)
                  )
                }
              >
                <Select.Option value={1}>1</Select.Option>
                <Select.Option value={2}>2</Select.Option>
                <Select.Option value={3}>3</Select.Option>
                <Select.Option value={4}>4</Select.Option>
              </Select>
            </Col>
            <Col span={12}>
              <Select
                style={{ width: "98%" }}
                placeholder="Child"
                value={occupancy.children}
                onSelect={e =>
                  onOccupanciesChange(
                    updateOccupancies(params, index, "children", e)
                  )
                }
              >
                <Select.Option value={0}>0</Select.Option>
                <Select.Option value={1}>1</Select.Option>
                <Select.Option value={2}>2</Select.Option>
                <Select.Option value={3}>3</Select.Option>
                <Select.Option value={4}>4</Select.Option>
              </Select>
            </Col>
          </Row>
          {occupancy.paxes
            ? occupancy.paxes.map((pax, indx) => (
              <Row key={indx}>
                <Col span={12}>Child ({indx + 1}) age</Col>
                <Col span={12}>
                  <Select
                    style={{ width: "98%" }}
                    value={pax.age}
                    onSelect={e =>
                      onOccupanciesChange(
                        updateChildrenPax(params, index, indx, e)
                      )
                    }
                    placeholder="Age"
                  >
                    <Select.Option value={1}>1</Select.Option>
                    <Select.Option value={2}>2</Select.Option>
                    <Select.Option value={3}>3</Select.Option>
                    <Select.Option value={4}>4</Select.Option>
                    <Select.Option value={5}>5</Select.Option>
                    <Select.Option value={6}>6</Select.Option>
                    <Select.Option value={7}>7</Select.Option>
                    <Select.Option value={8}>8</Select.Option>
                    <Select.Option value={9}>9</Select.Option>
                    <Select.Option value={10}>10</Select.Option>
                    <Select.Option value={11}>11</Select.Option>
                    <Select.Option value={12}>12</Select.Option>
                  </Select>
                </Col>
              </Row>
            ))
            : null}
        </React.Fragment>
      ))}
    </>
  );
};

// source market (AKA nationality)
export const HotelbedsSourceMarket = ({ params, onSourceMarketChange }) => {

  return (
    <Select filterOption={(input, option) => (option.props.children as any).toLowerCase().indexOf(input.toLowerCase()) >= 0
      || (option.props.value as any).toLowerCase().indexOf(input.toLowerCase()) >= 0} value={params.sourceMarket} showSearch onChange={(e) => onSourceMarketChange(e)}>
      {generateCountryOptions()}
    </Select>
  )
}

export const isEmptyObject = obj =>
  typeof obj == "undefined" || obj == null || Object.keys(obj).length == 0;




export const HotelbedsSearchForm: React.StatelessComponent<{
  onSearch?: any;
  client: TravelCloudClient;
  formValue?: {};
  minLeadTime?: number;
  onChange?: (any) => void;
  children?: ({ params, controller, defaultCityCodes }: any) => React.ReactNode | never[];
  defaultCityCodes?: string[]
}> = ({ onSearch, client, children, formValue, onChange, minLeadTime, defaultCityCodes }) => {

  const { params, controller } = useHotelbedsSearchForm({ formValue, onChange, minLeadTime })

  const nodes = children ? (
    children({ params, controller, defaultCityCodes })
  ) : (
      <Form onSubmit={e => e.preventDefault()} key={"SearchForm"} id="hotelbeds-search-form">
        <Row gutter={16} justify="center" align="middle">
          <Col span={20}>
            <Row gutter={16} justify="center" align="middle">
              <Col span={6}>
                <Form.Item key={"formItem1"} label="Destination or Hotel">
                  <HotelsAndDestinationsName
                    client={client}
                    params={params}
                    defaultCityCodes={defaultCityCodes}
                    onHotelsOrDestinationsChange={controller.onHotelsOrDestinationsChange}
                  />
                </Form.Item>
              </Col>
              <Col span={5}>
                <Form.Item label="Nationality">
                  <HotelbedsSourceMarket params={params} onSourceMarketChange={controller.onSourceMarketChange} />
                </Form.Item>
              </Col>

              <Col span={4}>
                <Form.Item label="Check In">
                  <HotelBedsCheckInDatePicker
                    minLeadTime={minLeadTime}
                    style={{ width: "99%" }}
                    placeholder="Check In Date"
                    params={params}
                    onStayChange={controller.onStayChange}
                  />
                </Form.Item>
              </Col>

              <Col span={4}>
                <Form.Item label="Check Out">
                  <HotelBedsCheckOutDatePicker
                    minLeadTime={minLeadTime}
                    style={{ width: "99%" }}
                    placeholder="Check Out Date"
                    params={params}
                    onStayChange={controller.onStayChange}
                  />
                </Form.Item>
              </Col>

              <Col span={5}>
                <Form.Item label="Room(s) - Guest(s)">
                  {/* <HotelBedsOccupancyPopover
                    params={params}
                    onOccupanciesChange={controller.onOccupanciesChange}
                  /> */}
                  <HotelBedsOccupancyPopover
                    params={params}
                    onOccupanciesChange={controller.onOccupanciesChange}
                  >
                    {
                      ({ params, controller }) => (
                        <Popover
                          content={
                            <>
                              <Row type="flex" justify="center">
                                <Col>
                                  <Form.Item label="Rooms" style={{ textAlign: 'center' }}>
                                    <BtnOccupancy
                                      max={4}
                                      onChange={(value) =>
                                        controller.onOccupanciesChange(
                                          controller.updateOccupancyElementCount(params, value)
                                        )
                                      }
                                      value={params.occupancies.length}
                                    >
                                      {({ value, controller }) => (
                                        <div className='number' style={{ fontSize: 16, display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
                                          <Icon type='minus-circle'
                                            onClick={controller.handleMinus} />
                                          <div style={{ margin: '0 16px' }}>{value}</div>
                                          <Icon type='plus-circle'
                                            onClick={controller.handlePlus} />
                                        </div>
                                      )}
                                    </BtnOccupancy>
                                  </Form.Item>
                                </Col>
                              </Row>
                              <Divider style={{ margin: 3 }} />
                              <Row>
                                {
                                  params.occupancies.map((occupancy, index) => (
                                    <React.Fragment key={`room_${index}`}>
                                      <RoomOccupancy
                                        index={index}
                                        occupancy={occupancy}
                                        params={params}
                                        onOccupanciesChange={controller.onOccupanciesChange}
                                        updateOccupancies={controller.updateOccupancies}
                                        updateChildrenPax={controller.updateChildrenPax}
                                      >
                                        {
                                          ({ params, controller }) => (
                                            <Row>
                                              <Row gutter={24} type="flex" justify="center">
                                                <Col>
                                                  Room: ({`${controller.index + 1}`})
                                                </Col>
                                              </Row>
                                              <Row gutter={24} type="flex" justify="space-around">
                                                <Col>
                                                  <Form.Item label="Adult" style={{ textAlign: 'center' }}>
                                                    <BtnOccupancy
                                                      max={4}
                                                      value={occupancy.adults != null ? occupancy.adults : 0}
                                                      onChange={value =>
                                                        controller.onOccupanciesChange(
                                                          controller.updateOccupancies(params, controller.index, "adults", value)
                                                        )
                                                      }
                                                    >
                                                      {({ value, controller }) => (
                                                        <div className='number' style={{ fontSize: 16, display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
                                                          <Icon type='minus-circle'
                                                            onClick={controller.handleMinus} />
                                                          <div style={{ margin: '0 16px' }}>{value}</div>
                                                          <Icon type='plus-circle'
                                                            onClick={controller.handlePlus} />
                                                        </div>
                                                      )}
                                                    </BtnOccupancy>
                                                  </Form.Item>
                                                </Col>
                                                <Col>
                                                  <Form.Item label="Children" style={{ textAlign: 'center' }}>
                                                    <BtnOccupancy
                                                      max={3}
                                                      value={occupancy.children != null ? occupancy.children : 0}
                                                      onChange={value =>
                                                        controller.onOccupanciesChange(
                                                          controller.updateOccupancies(params, controller.index, "children", value)
                                                        )
                                                      }
                                                    >
                                                      {({ value, controller }) => (
                                                        <div className='number' style={{ fontSize: 16, display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
                                                          <Icon type='minus-circle'
                                                            onClick={controller.handleMinus} />
                                                          <div style={{ margin: '0 16px' }}>{value}</div>
                                                          <Icon type='plus-circle'
                                                            onClick={controller.handlePlus} />
                                                        </div>
                                                      )}
                                                    </BtnOccupancy>
                                                  </Form.Item>
                                                </Col>
                                              </Row>
                                              <Row gutter={24} type="flex" justify="space-around">
                                                {occupancy.paxes
                                                  ? occupancy.paxes.map((pax, indx) => (
                                                    <React.Fragment key={`children_age_${indx}`}>
                                                      <Col>
                                                        <Form.Item label={`Children: (${indx + 1})`} style={{ textAlign: 'center' }}>
                                                          <SelectChildrenAge
                                                            value={pax.age}
                                                            onSelect={value =>
                                                              controller.onOccupanciesChange(
                                                                controller.updateChildrenPax(params, controller.index, indx, value)
                                                              )
                                                            }
                                                          />
                                                        </Form.Item>
                                                      </Col>
                                                    </React.Fragment>
                                                  ))
                                                  : null}
                                              </Row>
                                            </Row>
                                          )
                                        }
                                      </RoomOccupancy>
                                      <Divider style={{ margin: 3 }} />
                                    </React.Fragment>
                                  ))
                                }
                              </Row>
                            </>
                          }
                          trigger="click"
                          placement="bottom"
                          visible={controller.visible}
                          onVisibleChange={visible => controller.setVisible(visible)}
                        >
                          <div style={{ marginBottom: 6 }}>
                            <div className='no-border no-radius' style={{ position: 'relative' }}>
                              <a className='ant-dropdown-link'>
                                <Input size='default' readOnly style={{ width: '100%', cursor: 'pointer' }}
                                  value={`${controller.roomTotal} Room(s) - ${controller.adultTotal} Adult(s)`} />
                                <SvgIcon
                                  fill='rgba(0,0,0,.15)'
                                  width='10'
                                  height='10'
                                  name='down'
                                  style={{ position: 'absolute', top: '50%', right: 11, marginTop: -5 }}
                                />
                              </a>
                            </div>
                          </div>
                        </Popover>
                      )
                    }
                  </HotelBedsOccupancyPopover>
                </Form.Item>
              </Col>

            </Row>
          </Col>

          {onSearch != null && <Col span={2}>
            <Form.Item>
              <Button
                style={{ marginTop: 40 }}
                size="default"
                className="search-btn"
                disabled={!validateHotelbedsParams(params)}
                onClick={() => onSearch(params)}
              >
                Search
            </Button>
            </Form.Item>
          </Col>}
        </Row>
      </Form>
    );
  return (
    <div className="hotelbeds-search-form" key={"Searchdiv"}>
      {nodes}
    </div>
  );
};
