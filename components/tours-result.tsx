import React from "react";
import { List, Spin } from 'antd'
import { Cart, dateToIsoDate, mapTourOptionDepartures, TcResponse, nextDeparture as calculateNextDeparture, nextDeparture } from "../travelcloud";
import { Tour } from "../types";
import Big from 'big.js';
import { Room } from "./tour-booking";

// utility function to
// 1. clone tours
// 2. filter tours based on criteria
// 3. add _next_departure and _cheapest_computed to each tour
export function computeTourPricesAndNextDeparture(tours: Tour[], options: {
  rooms?: Room[],
  divisor?: number,
  filterOptionType?: string,
  cart?: Cart
}): any[] {
  let toursResultCloned: Tour[] = JSON.parse(JSON.stringify(tours))
  var priceRulesIndexed = {}

  if (options.cart != null) {
    const priceRulesCombined = options.cart.getCombinedPriceRules()
    priceRulesIndexed = priceRulesCombined.reduce((acc, pr) => {
      acc[pr.id] = pr
      return acc
    }, {})
  }

  const {rooms = [{adult: 2}], divisor = 2, filterOptionType, cart} = options

  return toursResultCloned
    // filter base on filterOptionType
    .map((tour) => {
      if (filterOptionType != null) tour.options = tour.options.filter((option) => option.type === filterOptionType)

      tour['_next_departure'] = nextDeparture(tour.options)

      if (cart == null) return tour

      tour.options = tour.options.map((option) => {
        option['_cheapest_computed'] =
          //compute price for each departure
          mapTourOptionDepartures(option, (departure, departureDate) => {

            if (departure != null && departure.slots_taken >= departure.slots_total) return null

            const product = cart.addTour(tour, {
              tour_id: tour.id,
              option_id: option.id,
              departure_date: departureDate,
              rooms: rooms
            }, true)

            if (product == null) return null

            // a better name for these would be beforeDiscounts/afterDiscounts
            const beforePriceRules = product.items
              .filter((item) => parseInt(item['price']) > 0)
              .reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))
              .div(divisor)
            const afterPriceRules = product.items
              .reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))
              .div(divisor)

            const priceRuleIds = product.items.reduce((acc, item) => {
              if (item.price_rule_id != null) acc.push(item.price_rule_id)
              return acc
            }, [])

            const appliedPriceRules = priceRuleIds.reduce((acc, id) => {
              acc.push(priceRulesIndexed[id])
              return acc
            }, [])

            const result = {
              beforeDiscounts: beforePriceRules,
              afterDiscounts: afterPriceRules,
              beforePriceRules, afterPriceRules, option, departureDate, appliedPriceRules
            }

            if (departureDate === tour['_next_departure']) {
              tour['_next_departure_computed'] = result
            }

            return result
          })

          // filter out invalid departures
          .filter((val) => val != null)

          // find the cheapest
          .reduce((acc, computed) => {
            if (acc == null || computed.afterPriceRules.cmp(acc.afterPriceRules) === -1) {
              return computed
            }
            return acc
          }, null)

          return option
      })

      tour['_cheapest_computed'] = tour.options.reduce((acc: any, option) => {
        if (option['_cheapest_computed'] == null) return acc

        if (acc == null || option['_cheapest_computed'].afterPriceRules.cmp(acc.afterPriceRules) === -1) {
          return option['_cheapest_computed']
        }

        return acc
      }, null)

      return tour;
    })
}

  export const ToursResult = ({ toursResult, onTourClick, style, className, cart, rooms = [{ adult: 2 }], divisor = 2, filterOptionType }: { className?: string, toursResult: any, onTourClick: (any) => any, style?: React.CSSProperties, cart: Cart, rooms?: Room[], divisor?: number, filterOptionType?: string }) => {
    const toursResultComputed = computeTourPricesAndNextDeparture(toursResult, {cart, rooms, divisor, filterOptionType})

    return <div style={style} className={className}>
      <List
        grid={{ gutter: 16, xs: 1, sm: 1, md: 1, lg: 2, xl: 3, xxl: 4 }}
        dataSource={toursResultComputed}
        renderItem={(tour: Tour) => {
          return <List.Item>
            <div className="ant-card ant-card-bordered ant-card-hoverable" onClick={() => onTourClick(tour)}>
              <div style={{
                backgroundImage: `url(${tour.photo_url})`,
                height: '200px',
                backgroundSize: 'cover',
                backgroundPosition: 'center'
              }} />

              <div style={{ padding: '8px' }}>
                <div className="tc-full-width-ellipsis" style={{ fontWeight: 'bold', fontSize: '16px' }}>{tour.name}</div>
                {tour['_next_departure'] == null || tour.options.length === 0
                  ? <div>No departure scheduled</div>
                  : <div>Next departure on: <b>{tour['_next_departure']}</b></div>}

                {tour['_cheapest_computed'] == null && <div>&nbsp;</div>}
                {tour['_cheapest_computed'] != null && (
                  tour['_cheapest_computed'].beforeDiscounts.eq(tour['_cheapest_computed'].afterDiscounts)
                    ? <div>Prices from: <b>${tour['_cheapest_computed'].beforeDiscounts.toFixed(0)}</b></div>
                    : <div>Prices from: <span style={{ textDecoration: 'line-through' }}>${tour['_cheapest_computed'].beforeDiscounts.toFixed(0)}</span> <b>${tour['_cheapest_computed'].afterDiscounts.toFixed(0)}</b></div>
                )}
              </div>
            </div>
          </List.Item>
        }}
      /></div>
  }

