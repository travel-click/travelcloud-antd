import React from 'react'
import { Form, Select, Input, Radio } from 'antd'
import { WrappedFormUtils } from 'antd/lib/form/Form'
import { generateCountryOptions, generateDialOptions } from '../components/country-dropdown'
import { dateToIsoDate, TravelCloudClient } from '../travelcloud'
import { Order } from '../types'
import { newDateLocal, OutboundFormValue } from '../order-product-compute'

function vw(value) {
  return {value}
}

function filterOption(input, option) {
  // for option separator
  if (option.props.disabled === true) return true
  if (option.props.children == null || option.props.value == null) return false
  return (option.props.children as any).toLowerCase().indexOf(input.toLowerCase()) >= 0 ||(option.props.value as any).toLowerCase().indexOf(input.toLowerCase()) >= 0
}

export const initialCheckoutForm = (order, demo = false) => {
  if (order == null || order.products == null) return null
  const maxAdult = order.products.reduce((acc, product) => product.adult != null ? Math.max(parseInt(product.adult), acc) : acc, 0)
  const maxChild = order.products.reduce((acc, product) => product.child != null ? Math.max(parseInt(product.child), acc) : acc, 0)
  const maxInfant = order.products.reduce((acc, product) => product.infant != null ? Math.max(parseInt(product.infant), acc) : acc, 0)

  var types
  var travelerMeta


  const outboundProduct = order.products.find((product) => product.product_source === 'outbound')

  if (outboundProduct != null) {
    const outboundForm = outboundProduct.form as OutboundFormValue
    [types, travelerMeta] = outboundForm.roomList.reduce(([accTypes, accTravelerMeta], room) => {
      accTypes = accTypes.concat(Array(room.adults).fill('adult').concat(Array(room.children).fill('child')).concat(Array(room.childrenNoBed).fill('child')).concat(Array(room.infants).fill('infant')))
      const labelPrefix = 'Room ' + room.roomNo + " - "
      accTravelerMeta = accTravelerMeta
        .concat(
          Array(room.adults).fill(null).map((_, i) => ({label: labelPrefix + 'Adult ' + (i + 1)}))
            .concat(Array(room.children).fill(null).map((_, i) => ({label: labelPrefix + 'Child ' + (i + 1)})))
            .concat(Array(room.childrenNoBed).fill(null).map((_, i) => ({label: labelPrefix + 'Child (no bed) ' + (i + 1)})))
            .concat(Array(room.infants).fill(null).map((_, i) => ({label: labelPrefix + 'Infant ' + (i + 1)}))))
      return [accTypes, accTravelerMeta]
    }, [[], []])

    console.log(travelerMeta)
    

  } else {
    types = Array(maxAdult).fill('adult').concat(Array(maxChild).fill('child')).concat(Array(maxInfant).fill('infant'))
    travelerMeta =Array(maxAdult).fill(null).map((_, i) => ({label: 'Adult ' + (i + 1)}))
      .concat(Array(maxChild).fill(null).map((_, i) => ({label: 'Child ' + (i + 1)})))
      .concat(Array(maxInfant).fill(null).map((_, i) => ({label: 'Infant ' + (i + 1)})))
   }

  if (demo === false) {
    const travelers = types.map(type => ({type: vw(type)}))
    return {travelers, travelerMeta}}

  const travelers = types.map((type, index) => {

    const now = newDateLocal()
    var birth_date = '1990-12-25'
    if (type === 'adult') {
      now.setFullYear(now.getFullYear() - 30)
      birth_date = dateToIsoDate(now)}

    else if (type === 'child') {
      now.setFullYear(now.getFullYear() - 6)
      birth_date = dateToIsoDate(now)}

    else if (type === 'infant') {
      now.setFullYear(now.getFullYear() - 1)
      birth_date = dateToIsoDate(now)}

    return {
      type: vw(type),
      first_name: vw("John" + String.fromCharCode('A'.charCodeAt(0) + index)),
      last_name: vw("Doe"),
      title: vw("Mr"),
      country: vw("SG"),
      residence_country: vw("SG"),
      place_of_birth: vw("Singapore"),
      passport: vw("S1234567A"),
      passport_issue_date: vw("2020-02-02"),
      passport_issue_country: vw("SG"),
      birth_date: vw(birth_date),
      expiry: vw("2025-12-25"),
    }})

  return {
    email: vw("demo@pytheas.travel"),
    phone: vw("12345678"),
    phone_country: vw("65"),
    first_name: vw("John"),
    last_name: vw("Doe"),
    travelers,
    travelerMeta}
}

export const createCheckoutForm = Form.create<{onChange?, formState: any, form, wrappedComponentRef?, order: Order, tcUser: string, defaultCarrierCodes?: string, defaultCountryCodes?: any}>({
  onFieldsChange(props, changedFields) {
    if (props.onChange != null) props.onChange(changedFields)
  },
  mapPropsToFields(props) {
    const {formState, order} = props
    if (formState == null) return {}

    const hasFlight = order.products.find(product => product.product_type === 'flight') != null

    const fields = {
      email: Form.createFormField(
        formState.email || {value: ''}
      ),
      phone_country: Form.createFormField(
        formState.phone_country || {value: ''}
      ),
      phone: Form.createFormField(
        formState.phone || {value: ''}
      ),
      first_name: Form.createFormField(
        formState.first_name || {value: ''}
      ),
      last_name: Form.createFormField(
        formState.last_name || {value: ''}
      ),
    };

    if (Array.isArray(formState.travelers)) {
      for (let k in formState.travelers) {
        fields[`travelers[${k}].type`] = Form.createFormField(
          formState.travelers[k].type || {value: ''}
        )
        fields[`travelers[${k}].first_name`] = Form.createFormField(
          formState.travelers[k].first_name || {value: ''}
        )
        fields[`travelers[${k}].last_name`] = Form.createFormField(
          formState.travelers[k].last_name || {value: ''}
        )
        fields[`travelers[${k}].title`] = Form.createFormField(
          formState.travelers[k].title || {value: ''}
        )
        fields[`travelers[${k}].country`] = Form.createFormField(
          formState.travelers[k].country || {value: ''}
        )
        fields[`travelers[${k}].residence_country`] = Form.createFormField(
          formState.travelers[k].residence_country || {value: ''}
        )
        fields[`travelers[${k}].place_of_birth`] = Form.createFormField(
          formState.travelers[k].place_of_birth || {value: ''}
        )
        fields[`travelers[${k}].passport`] = Form.createFormField(
          formState.travelers[k].passport || {value: ''}
        )
        fields[`travelers[${k}].passport_issue_country`] = Form.createFormField(
          formState.travelers[k].passport_issue_country || {value: ''}
        )
        fields[`travelers[${k}].passport_issue_date`] = Form.createFormField(
          formState.travelers[k].passport_issue_date || {value: ''}
        )
        fields[`travelers[${k}].birth_date`] = Form.createFormField(
          formState.travelers[k].birth_date || {value: ''}
        )
        fields[`travelers[${k}].expiry`] = Form.createFormField(
          formState.travelers[k].expiry || {value: ''}
        )

        if (hasFlight) {
          fields[`travelers[${k}].loyalty_carrier`] = Form.createFormField(
            formState.travelers[k].loyalty_carrier || {value: ''}
          )
          fields[`travelers[${k}].loyalty_number`] = Form.createFormField(
            formState.travelers[k].loyalty_number || {value: ''}
          )
        }
      }
    }

    return fields;
  }
})

class CheckoutFormComponent extends React.Component<{form: WrappedFormUtils, formState: any, onChange?, order: Order, tcUser: string, defaultCarrierCodes?: string, defaultCountryCodes?: any}> {
  render() {
    const {form, formState, order, tcUser, defaultCountryCodes} = this.props
    const {getFieldDecorator} = form;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 12 },
    };

    if (formState == null) return null;

    return (
      <Form layout="horizontal">
        <Form.Item
          {...formItemLayout}
          label="E-mail">
          {getFieldDecorator('email', {
            rules: [{
              type: 'email', message: 'E-mail is not valid',
            }, {
              required: true, message: 'Please input your E-mail!',
            }],
          })(
            <Input />
          )}
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          label="Phone Number">
          {getFieldDecorator(`phone_country`, {
            rules: [{ required: true, message: 'Please input your country dialing code!', whitespace: true }]})(
            <select className="tc-antd-option" style={{width: "50%"}}>
              {defaultCountryCodes?.length > 0 ? generateDialOptions(defaultCountryCodes) : generateDialOptions(['SG', 'MY', 'ID', 'MM', 'KH'])}
            </select>)}
          {getFieldDecorator('phone', {
            rules: [{ required: true, message: 'Please input your phone number!' }],
          })(
            <Input style={{width: "50%"}} />
          )}
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          label="First name">
          {getFieldDecorator('first_name', {
            rules: [{ required: true, message: 'Please input your first name!', whitespace: true }],
          })(
            <Input />
          )}
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          label="Last name">
          {getFieldDecorator('last_name', {
            rules: [{ required: true, message: 'Please input your last name!', whitespace: true }],
          })(
            <Input />
          )}
        </Form.Item>
        {formState.travelers.map((value, index) =>
          <TravelerFormComponent key={index} form={this.props.form} index={index} order={order} tcUser={tcUser} meta={formState.travelerMeta[index]} defaultCarrierCodes="SQ,MH" defaultCountryCodes={defaultCountryCodes?.length > 0 ? defaultCountryCodes : ['SG', 'MY', 'ID', 'MM', 'KH']}
          />
        )}
      </Form>
    )
  }
}

export const CheckoutForm = createCheckoutForm(CheckoutFormComponent)

class TravelerFormComponent extends React.Component<{form: WrappedFormUtils, index: number, order: Order, tcUser: string, defaultCarrierCodes?: string, meta, defaultCountryCodes?: any}> {
  state = {
    carriersResult: {} as any
  }

  client = new TravelCloudClient({tcUser: this.props.tcUser})

  async onSearch(query = null) {
    var param = {} as any
    if (query != null && query.length > 1) param.query = query;
    else if(this.props.defaultCarrierCodes != null && this.props.defaultCarrierCodes.trim() != "") param.carrierCodes = this.props.defaultCarrierCodes.trim();

    if (param.query != null || param.carrierCodes != null) {
      const carriersResult = await this.client.iataCarriers(param)
      this.setState({carriersResult})
    }
  }

  componentDidMount() {
    this.onSearch()
  }

  render() {
    const { getFieldDecorator, getFieldValue } = this.props.form as WrappedFormUtils;
    const { order, meta, defaultCountryCodes } = this.props;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 12 },
    };

    const hasFlight = order.products.find(product => product.product_type === 'flight') != null
    var numberOfRooms = null

    const legacyTour =  order.products.find(product => product.product_type === 'tour_package')
    if (legacyTour != null) {
      //@ts-ignore
      numberOfRooms = legacyTour.form?.rooms?.length
    }
    const loyaltyCarrierValue = getFieldValue(`travelers[${this.props.index}].loyalty_carrier`)
    const hasLoyaltyCarrier = loyaltyCarrierValue != null && loyaltyCarrierValue != ''
    const type = this.props.form.getFieldValue(`travelers[${this.props.index}].type`)

    var minBirthDate = '1901-01-01'

    if (type === 'child') {
      const tmpDate = newDateLocal()
      tmpDate.setFullYear(tmpDate.getFullYear() - 12)
      minBirthDate = dateToIsoDate(tmpDate)
    }
    else if (type === 'infant') {
      const tmpDate = newDateLocal()
      tmpDate.setFullYear(tmpDate.getFullYear() - 2)
      minBirthDate = dateToIsoDate(tmpDate)
    }

    const tmpDate2 = newDateLocal()
    tmpDate2.setMonth(tmpDate2.getMonth() + 6)
    const minPassportExpiry = dateToIsoDate(tmpDate2)
    tmpDate2.setFullYear(tmpDate2.getFullYear() + 20)
    const maxPassportExpiry = dateToIsoDate(tmpDate2)

    const tmpDate3 = newDateLocal()
    tmpDate3.setFullYear(tmpDate3.getFullYear() - 8);
    const minPassportIssueDate = dateToIsoDate(tmpDate3);
    tmpDate3.setFullYear(tmpDate3.getFullYear() + 8);
    tmpDate3.setDate(tmpDate3.getDate() - 1);
    const maxPassportIssueDate = dateToIsoDate(tmpDate3)

    var label = meta?.label
    if (label == null) label = "Traveler "  + (this.props.index + 1) + (type !== 'adult' ? ("(" + type + ")") : "")
    
    return <div>
      <h1>{label}</h1>
      <Form.Item
        {...formItemLayout}
        label="First name">
        {getFieldDecorator(`travelers[${this.props.index}].first_name`, {
          rules: [{ required: true, message: 'Please input your first name!', whitespace: true }],})(
        <Input />)}
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        label="Last name">
        {getFieldDecorator(`travelers[${this.props.index}].last_name`, {
          rules: [{ required: true, message: 'Please input your last name!', whitespace: true }],})(
        <Input />)}
      </Form.Item>
      <Form.Item
        label="Gender"
        {...formItemLayout}>
        {getFieldDecorator(`travelers[${this.props.index}].title`)(
        <Radio.Group>
          <Radio.Button value="Mr">Male</Radio.Button>
          <Radio.Button value="Ms">Female</Radio.Button>
        </Radio.Group>)}
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        label="Date of birth">
        <DropdownDatePickerInputWrapper min={minBirthDate} form={this.props.form} name={`travelers[${this.props.index}].birth_date`} />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        label="Nationality">
        {getFieldDecorator(`travelers[${this.props.index}].country`, {
        rules: [{ required: true, message: 'Please input your nationality!', whitespace: true }]})(
        <Select showSearch
          filterOption={filterOption}>
          {generateCountryOptions(defaultCountryCodes)}
        </Select>)}
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        label="Place of birth">
        {getFieldDecorator(`travelers[${this.props.index}].place_of_birth`, {
          rules: [{ required: true, message: 'Please input your place of birth!', whitespace: true }],})(
        <Input />)}
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        label="Country of residence">
        {getFieldDecorator(`travelers[${this.props.index}].residence_country`, {
        rules: [{ required: true, message: 'Please input your country of residence!', whitespace: true }]})(
        <Select showSearch
          filterOption={filterOption}>
          {generateCountryOptions(defaultCountryCodes)}
        </Select>)}
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        label="Passport number">
        {getFieldDecorator(`travelers[${this.props.index}].passport`, {
          rules: [{ required: true, message: 'Please input your passport number!', whitespace: true }],})(
        <Input />)}
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        label="Passport expiry">
        <DropdownDatePickerInputWrapper min={minPassportExpiry} max={maxPassportExpiry} form={this.props.form} name={`travelers[${this.props.index}].expiry`} />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        label="Passport issue date">
        <DropdownDatePickerInputWrapper min={minPassportIssueDate} max={maxPassportIssueDate} form={this.props.form} name={`travelers[${this.props.index}].passport_issue_date`} />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        label="Passport issue country">
        {getFieldDecorator(`travelers[${this.props.index}].passport_issue_country`, {
        rules: [{ required: true, message: 'Please input your passport issue country!', whitespace: true }]})(
        <Select showSearch
          filterOption={filterOption}>
          {generateCountryOptions(defaultCountryCodes)}
        </Select>)}
      </Form.Item>
      {numberOfRooms != null && numberOfRooms > 1 && <Form.Item
        {...formItemLayout}
        label="Room">
        {getFieldDecorator(`travelers[${this.props.index}].unstructured.room`)(
            <Select>
            {Array.from(Array(numberOfRooms).keys()).map(x => <Select.Option key={x} value={x+1}>Room {x + 1}</Select.Option>)}
          </Select>)}
      </Form.Item>}
      {hasFlight &&<Form.Item
        {...formItemLayout}
        label="Loyalty Carrier">
        {getFieldDecorator(`travelers[${this.props.index}].loyalty_carrier`)(
        <Select
          showSearch
          onSearch={(x) => this.onSearch(x)}
          filterOption={() => true}
          defaultActiveFirstOption={false}>
          <Select.Option key="none" value="">None</Select.Option>
          {
            this.state.carriersResult.result && this.state.carriersResult.result.map((carrier) => <Select.Option key={carrier.vendor_code} value={carrier.vendor_code}>{carrier.name}</Select.Option>)
          }
        </Select>)}
      </Form.Item>}
      {hasFlight && hasLoyaltyCarrier && <Form.Item
        {...formItemLayout}
        label="Loyalty Number">
        {getFieldDecorator(`travelers[${this.props.index}].loyalty_number`)(
        <Input />)}
      </Form.Item>}
    </div>
  }
}

export interface DropdownDatePicker {
  min?: string;
  max?: string;
  value?: number;
  onChange?: (value: number | string | undefined) => void;
  disabled?: boolean;
  size?: 'large' | 'small' | 'default';
  style?: React.CSSProperties;
  className?: string;
  name?: string;
}

// only for parsing iso format string
function parseDateString(str: string) {
  var result = newDateLocal(str)
  if (isNaN(result.getTime()) == false) return result
  return newDateLocal()
}

function getLastDayOfMonth(valDate: Date) {
  const valDateClone = newDateLocal(valDate.getTime())
  valDateClone.setMonth(valDateClone.getMonth() + 1)
  valDateClone.setDate(0)
  return valDateClone.getDate()
}

const DropdownDatePickerInputWrapper = (props: DropdownDatePicker & {form: WrappedFormUtils}) => {
  const { getFieldDecorator } = props.form;

  const val = props.form.getFieldValue(props.name)

  var valDate = parseDateString(val)
  const minDate = parseDateString(props.min)
  const maxDate = parseDateString(props.max)

  // we only clip the year input
  // actual min/max should be validated elsewher
  // this is to avoid changing the user's input as he is entering
  if (valDate.getFullYear() < minDate.getFullYear()) valDate.setFullYear(minDate.getFullYear())
  if (valDate.getFullYear() > maxDate.getFullYear()) valDate.setFullYear(maxDate.getFullYear())

  const validYears = Array(maxDate.getFullYear() - minDate.getFullYear() + 1).fill(null).map((_, index) => minDate.getFullYear() + index)
  const maxValidDate = getLastDayOfMonth(valDate)
  const validDays = Array(maxValidDate).fill(null).map((_, index) => index + 1)

  const year = valDate.getFullYear()
  const month = valDate.getMonth()
  const day = valDate.getDate()

  const yearChanged = (updated) => {
    valDate.setFullYear(updated)
    if (valDate.getDate() !=  day) {
      valDate.setMonth(month)
      valDate.setDate(getLastDayOfMonth(valDate))
    }
    props.form.setFieldsValue({
      [props.name]: dateToIsoDate(valDate)
    })
  }

  const monthChanged = (updated) => {
    valDate.setMonth(updated)
    if (valDate.getDate() !=  day) {
      valDate.setMonth(updated)
      valDate.setDate(getLastDayOfMonth(valDate))
    }
    props.form.setFieldsValue({
      [props.name]: dateToIsoDate(valDate)
    })
  }

  // assume user fill year -> month -> day
  // we don't try to clip invalid day, just overflow
  // also, invalid days shouldn't show in dropdown
  const dayChanged = (updated) => {
    valDate.setDate(updated)
    props.form.setFieldsValue({
      [props.name]: dateToIsoDate(valDate)
    })
  }

  getFieldDecorator(props.name)

  return <div>
    <Select showSearch value={year} onChange={yearChanged} style={{marginRight: '2%', width: '25%'}}>
      {validYears.map(validYear => <Select.Option key={String(validYear)} value={validYear}>{validYear}</Select.Option>)}
    </Select>
    <Select showSearch value={month} onChange={monthChanged} style={{marginRight: '2%', width: '50%'}}
      filterOption={filterOption}>
      <Select.Option key={String(0)} value={0}>January</Select.Option>
      <Select.Option key={String(1)} value={1}>February</Select.Option>
      <Select.Option key={String(2)} value={2}>March</Select.Option>
      <Select.Option key={String(3)} value={3}>April</Select.Option>
      <Select.Option key={String(4)} value={4}>May</Select.Option>
      <Select.Option key={String(5)} value={5}>June</Select.Option>
      <Select.Option key={String(6)} value={6}>July</Select.Option>
      <Select.Option key={String(7)} value={7}>August</Select.Option>
      <Select.Option key={String(8)} value={8}>September</Select.Option>
      <Select.Option key={String(9)} value={9}>October</Select.Option>
      <Select.Option key={String(10)} value={10}>November</Select.Option>
      <Select.Option key={String(11)} value={11}>December</Select.Option>
    </Select>
    <Select showSearch value={day} onChange={dayChanged} style={{width: '20%'}}>
      {validDays.map(validDay => <Select.Option key={String(validDay)} value={validDay}>{validDay}</Select.Option>)}
    </Select>
  </div> as any
}