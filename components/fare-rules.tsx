import React from "react";
import { Modal, Tabs } from "antd";

const FareRulesModal = ({ data, handleClose, show }) => {
  return (
    <Modal
      title="Fare Rules"
      visible={show}
      onCancel={handleClose}
      width={800}
      footer={null}
      className="fares-breakdown"
    >
      <div className="rules-and-tc">
        {data && data.adt && data.adt[0].brand && (
          <Tabs type="card">
            {data.adt.map(function(item, index) {
              return (
                <Tabs.TabPane
                  tab={item.fare_basis}
                  key={item.fare_basis + index}
                >
                  <div className="WRYA1SG1">
                    <h3>RULE APPLICATION AND OTHER CONDITIONS</h3>
                    {item.brand && item.brand.description && (
                      <p>{item.brand.description}</p>
                    )}
                  </div>
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        )}
        {data && data.adt && data.adt[0].brand ? (
          ""
        ) : (
          <div>
            <strong>There is no fare rules.</strong>
          </div>
        )}
      </div>
    </Modal>
  );
};

export default FareRulesModal;
