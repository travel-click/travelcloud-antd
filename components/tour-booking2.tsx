import React from "react";
import { Input, Icon } from "antd";
import { CalendarHorizontal } from "./calendar";
import { dateToIsoDate, formatCurrency } from "../travelcloud";
import { Od } from "./flights-result3";
import {
  TourBookingFormController,
  TourBookingFormState
} from "./hooks/tour-booking";
import { Room } from "./tour-booking";
import { withRouter, SingletonRouter } from "next/router";
import { newDateLocal } from "../order-product-compute";

export const IncDecInput: React.SFC<{
  min;
  max;
  value;
  onAdd;
  onSubtract;
}> = props => {
  return (
    <Input
      className="tc-inc-dec-input"
      type="number"
      addonAfter={<Icon type="plus" onClick={props.onAdd} />}
      addonBefore={<Icon type="minus" onClick={props.onSubtract} />}
      min={props.min}
      max={props.max}
      value={props.value}
      // prevent readonly warning
      onChange={() => {}}
    />
  );
};

export const RoomInput: React.SFC<{
  room: Room;
  bookingFormController: TourBookingFormController;
  index: number;
}> = props => {
  const { room, bookingFormController } = props;
  const key = props.index;
  return (
    <div style={{ width: "120px" }}>
      <div>Adults</div>
      <IncDecInput
        min={1}
        max={3}
        value={room.adult}
        onAdd={() => bookingFormController.adjustRoom(key, "adult", 1)}
        onSubtract={() => bookingFormController.adjustRoom(key, "adult", -1)}
      />
      {bookingFormController.status.allowChildBed && (
        <>
          <div>Children</div>
          <IncDecInput
            min={0}
            max={10}
            value={room.child_with_bed || 0}
            onAdd={() =>
              bookingFormController.adjustRoom(key, "child_with_bed", 1)
            }
            onSubtract={() =>
              bookingFormController.adjustRoom(key, "child_with_bed", -1)
            }
          />
        </>
      )}
      {bookingFormController.status.allowChildNoBed && (
        <>
          <div>Child (without bed)</div>
          <IncDecInput
            min={0}
            max={10}
            value={room.child_no_bed || 0}
            onAdd={() =>
              bookingFormController.adjustRoom(key, "child_no_bed", 1)
            }
            onSubtract={() =>
              bookingFormController.adjustRoom(key, "child_no_bed", -1)
            }
          />
        </>
      )}
      {bookingFormController.status.allowInfant && (
        <>
          <div>Infants</div>
          <IncDecInput
            min={0}
            max={10}
            value={room.infant || 0}
            onAdd={() => bookingFormController.adjustRoom(key, "infant", 1)}
            onSubtract={() =>
              bookingFormController.adjustRoom(key, "infant", -1)
            }
          />
        </>
      )}
    </div>
  );
};

type TourBookingFormProps = {
  bookingFormController: TourBookingFormController;
  bookingFormState: TourBookingFormState;
};

export const TourBookingForm2: React.ComponentType<TourBookingFormProps> = withRouter<
  TourBookingFormProps & { router: SingletonRouter }
>(props => {
  const { bookingFormController, bookingFormState, router } = props;

  if (bookingFormController.tour.options.length === 0) return <div>Tour has no options</div>

  return (
    <div>
      {bookingFormController.tour.options.length > 1 && <div>
        <h2>Select tour option</h2>
        {bookingFormController.tour.options.map((option, key) => {
          const style =
            bookingFormState.option_id === option.id
              ? { outline: "dotted red" }
              : {};
          return (
            <div key={key} style={style}>
              <h3>{option.name}</h3>
              <div>Prices from {option.TWN}</div>
              <button
                onClick={() =>
                  bookingFormController.updateOptionId(option.id)
                }
              >
                Select
              </button>
            </div>
          );
        })}
      </div>}
      {bookingFormState.option_id != null && (
        <div>
          {bookingFormController.status.allowRooms ? (
            <>
              <h2>How many rooms?</h2>
              <IncDecInput
                min={1}
                max={3}
                value={bookingFormState.rooms.length}
                onAdd={() => bookingFormController.adjustNumberOfRooms(1)}
                onSubtract={() => bookingFormController.adjustNumberOfRooms(-1)}
              />
              <h2>Room occupancy</h2>
            </>
          ) : (
            <h2>How many travelers?</h2>
          )}

          {bookingFormState.rooms.map((room, key) => (
            <RoomInput
              bookingFormController={bookingFormController}
              key={key}
              index={key}
              room={room}
            />
          ))}
          <h2>Select departure date</h2>

          <CalendarHorizontal
            className="tour-calendar"
            validRange={[
              newDateLocal(bookingFormState.selectedOption["_next_departure"]),
              newDateLocal(bookingFormState.selectedOption["_last_departure"])
            ]}
            currentViewMonth={bookingFormState.currentViewMonth}
            onNext={() =>
              bookingFormController.nextCurrentViewMonth()
            }
            onPrev={() =>
              bookingFormController.prevCurrentViewMonth()
            }
            CalendarDayGenerator={(date, isPadding, key) => {
              const isoDate = dateToIsoDate(date);

              // if a month does not end on a sat, additional days are drawn to pad out the week
              // if isPadding, we pretned there's no dayInfo and don't display any info
              const dayInfo = isPadding
                ? {}
                : bookingFormState.selectedOptionDateInfoMap[isoDate] ||
                  ({} as any);
              const {
                disabled = true,
                afterDiscounts,
                noDeparture = true,
                slotsRemaining
              } = dayInfo;

              return (
                <td
                  role="gridcell"
                  key={key}
                  className={
                    "ant-fullcalendar-cell" +
                    (disabled ? " ant-fullcalendar-disabled-cell" : "") +
                    (bookingFormState.departure_date === isoDate
                      ? " ant-fullcalendar-selected-day"
                      : "")
                  }
                  onClick={() =>
                    disabled === false &&
                    bookingFormController.updateDepartureDateAndFlightResult(
                      isoDate
                    )
                  }
                >
                  <div className="ant-fullcalendar-date">
                    <div className="ant-fullcalendar-value">
                      {date.getDate()}
                    </div>
                    {noDeparture ? (
                      <div className="ant-fullcalendar-content"></div>
                    ) : (
                      <div className="ant-fullcalendar-content">
                        {/*}
                        <div>{formatCurrency(afterDiscounts)}</div>
                        {slotsRemaining != null && (
                          <div>{slotsRemaining} available</div>
                        )}
                        */}
                      </div>
                    )}
                  </div>
                </td>
              );
            }}
          />
        </div>
      )}

      <div>
        {bookingFormState.flightResult != null &&
          bookingFormState.flightResult.result != null && (
            <div>
              <Od
                od={bookingFormState.flightResult.result.od1}
                source="webconnect"
              />
              <Od
                od={bookingFormState.flightResult.result.od2}
                source="webconnect"
              />
            </div>
          )}
      </div>

      <div>
        {bookingFormState.tourComputed != null &&
          bookingFormState.tourComputed.totalPrice && (
            <button
              onClick={() => {
                bookingFormController.cart
                  .reset()
                  .addTour(
                    bookingFormController.tour,
                    bookingFormController.getBookingForm()
                  );

                router.push("/checkout");
              }}
            >
              Book tour:{" "}
              {formatCurrency(bookingFormState.tourComputed.totalPrice)}
            </button>
          )}

        {bookingFormState.loading && <div>Loading...</div>}
        {bookingFormState.flightResult != null &&
          bookingFormState.flightResult.result != null && (
            <div>
              <button
                onClick={() => {
                  bookingFormController.cart
                    .reset()
                    .addTour(
                      bookingFormController.tour,
                      bookingFormController.getBookingForm()
                    );

                  bookingFormController.cart.addFlight(
                    bookingFormState.flightResult.result,
                    bookingFormState.flightResult.result.pricings[0].id
                  );

                  router.push("/checkout");
                }}
              >
                Book tour &#43; Flight:{" "}
                {formatCurrency(bookingFormState.tourPlusFlightPrice)}{" "}
              </button>
            </div>
          )}
      </div>
    </div>
  );
});
