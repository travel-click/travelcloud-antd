import React from "react"
import { Icon, Button } from "antd"
import { newDateLocal } from "../order-product-compute"
type CalendarDay<T> = (props: T & React.HTMLAttributes<HTMLDivElement>) => JSX.Element

export default function Calendar<T>({validRange, locale = 'en-US', style, className, CalendarDayGenerator}:{CalendarDayGenerator: (Date, isPadding, number) => JSX.Element, validRange: [Date, Date], locale?: string} & React.HTMLAttributes<HTMLDivElement>) {
  const startDate = newDateLocal(validRange[0].getTime())
  startDate.setDate(1)

  const months = []
  while (startDate.getFullYear() * 12 + startDate.getMonth() <= validRange[1].getFullYear() * 12 + validRange[1].getMonth()) {
    months.push(CalendarMonth({date: startDate, locale, CalendarDayGenerator}))

    startDate.setMonth(startDate.getMonth() + 1)
  }

  return <div className={"ant-fullcalendar ant-fullcalendar-full ant-fullcalendar-fullscreen " + (className || '')} style={{position: 'relative', ...style}}>
      <div className="ant-fullcalendar-calendar-body" style={{paddingTop: 23, height: '100%'}}>
        <div style={{paddingRight: 12, position: 'absolute', zIndex: 1, top: 0}}>
          <table className="ant-fullcalendar-table">
            <thead>
              <tr role="row">
                <th role="columnheader" title="Sun" className="ant-fullcalendar-column-header">
                  <span className="ant-fullcalendar-column-header-inner">Sun</span>
                </th>
                <th role="columnheader" title="Mon" className="ant-fullcalendar-column-header">
                  <span className="ant-fullcalendar-column-header-inner">Mon</span>
                </th>
                <th role="columnheader" title="Tue" className="ant-fullcalendar-column-header">
                  <span className="ant-fullcalendar-column-header-inner">Tue</span>
                </th>
                <th role="columnheader" title="Wed" className="ant-fullcalendar-column-header">
                  <span className="ant-fullcalendar-column-header-inner">Wed</span>
                </th>
                <th role="columnheader" title="Thu" className="ant-fullcalendar-column-header">
                  <span className="ant-fullcalendar-column-header-inner">Thu</span>
                </th>
                <th role="columnheader" title="Fri" className="ant-fullcalendar-column-header">
                  <span className="ant-fullcalendar-column-header-inner">Fri</span>
                </th>
                <th role="columnheader" title="Sat" className="ant-fullcalendar-column-header">
                  <span className="ant-fullcalendar-column-header-inner">Sat</span>
                </th>
              </tr>
            </thead>
          </table>
        </div>
        <div style={{flex: 1, overflowY: 'scroll', height: 'inherit'}}>
          <table className="ant-fullcalendar-table" role="grid">
            {months}
          </table>
        </div>
      </div>
    </div>
}

export function CalendarHorizontal<T>({onNext, onPrev, validRange, locale = 'en-US', style, className, CalendarDayGenerator, currentViewMonth}:{currentViewMonth: Date, CalendarDayGenerator: (Date, isPadding, number) => JSX.Element, validRange: [Date, Date], locale?: string, onNext, onPrev} & React.HTMLAttributes<HTMLDivElement>) {
  function dateToNumMonths(date: Date): number {
    return date.getFullYear() * 12 + date.getMonth()
  }

  if (currentViewMonth == null) return null

  const monthHeader = currentViewMonth.toLocaleDateString(locale, { year: 'numeric', month: 'long'});

  return <>
    <div style={{padding: 5, textAlign: 'center', display:'flex'}}>
      <Button type="primary" icon="left" disabled={dateToNumMonths(validRange[0]) >= dateToNumMonths(currentViewMonth)} onClick={() => onPrev()}/>
      <div style={{flexGrow: 1}}>{monthHeader}</div>
      <Button type="primary" icon="right" disabled={dateToNumMonths(validRange[1]) <= dateToNumMonths(currentViewMonth)} onClick={() => onNext()} />
    </div>
    <div className={"ant-fullcalendar ant-fullcalendar-full ant-fullcalendar-fullscreen " + (className || '')} style={{position: 'relative', ...style}}>
      <div className="ant-fullcalendar-calendar-body" style={{paddingTop: 23, height: '100%'}}>
        <div style={{flex: 1, height: 'inherit'}}>
          <table className="ant-fullcalendar-table" role="grid">
            <thead>
              <tr role="row">
                <th role="columnheader" title="Sun" className="ant-fullcalendar-column-header">
                  <span className="ant-fullcalendar-column-header-inner">Sun</span>
                </th>
                <th role="columnheader" title="Mon" className="ant-fullcalendar-column-header">
                  <span className="ant-fullcalendar-column-header-inner">Mon</span>
                </th>
                <th role="columnheader" title="Tue" className="ant-fullcalendar-column-header">
                  <span className="ant-fullcalendar-column-header-inner">Tue</span>
                </th>
                <th role="columnheader" title="Wed" className="ant-fullcalendar-column-header">
                  <span className="ant-fullcalendar-column-header-inner">Wed</span>
                </th>
                <th role="columnheader" title="Thu" className="ant-fullcalendar-column-header">
                  <span className="ant-fullcalendar-column-header-inner">Thu</span>
                </th>
                <th role="columnheader" title="Fri" className="ant-fullcalendar-column-header">
                  <span className="ant-fullcalendar-column-header-inner">Fri</span>
                </th>
                <th role="columnheader" title="Sat" className="ant-fullcalendar-column-header">
                  <span className="ant-fullcalendar-column-header-inner">Sat</span>
                </th>
              </tr>
            </thead>
            <CalendarMonth hideHeader={true} date={currentViewMonth} locale={locale} CalendarDayGenerator={CalendarDayGenerator} />
          </table>
        </div>
      </div>
    </div>
    </>
}

export function CalendarMonth<T>({hideHeader = false, date, locale = 'en-US', CalendarDayGenerator}:{CalendarDayGenerator: (Date, boolean, number) => JSX.Element, date: Date, locale: string, hideHeader?: boolean}) {
  const thisMonth = date.getMonth()

  const startDate = newDateLocal(date.getTime())
  startDate.setDate(1) // get the 1st of month
  startDate.setDate(1 - startDate.getDay()) // get the start of week

  const endDate = newDateLocal(date.getTime())
  endDate.setMonth(endDate.getMonth() + 1) // get next month
  endDate.setDate(0) // get last day of previous month of next month
  endDate.setDate(endDate.getDate() + 6 - endDate.getDay()) // get end of week

  const allDates = []
  for (; startDate <= endDate; startDate.setDate(startDate.getDate() + 1)) {
    allDates.push(newDateLocal(startDate.getTime()))
  }

  const allWeeks = partition(allDates, 7)

  const monthHeader = date.toLocaleDateString(locale, { year: 'numeric', month: 'long'});

  return <tbody className="ant-fullcalendar-tbody" key={"" + date.getFullYear() + date.getMonth()}>
      {(hideHeader === false) &&
        <tr>
          <td className="month-divider" colSpan={7}>{monthHeader}</td>
        </tr>}
      {allWeeks.map((week, index) =>
        <tr role="row" key={index}>
          {week.map((date, index) => {
            return CalendarDayGenerator(date, date.getMonth() !== thisMonth, index)
          })}
        </tr>
      )}
    </tbody>
}

export const CalendarDayDefault: CalendarDay<{date: Date, [x: string]: any}> = (props) =>
  <td role="gridcell" className="ant-fullcalendar-cell">
    <div className="ant-fullcalendar-date">
      <div className="ant-fullcalendar-value">{props.date.getDate()}</div>
      <div className="ant-fullcalendar-content"></div>
    </div>
  </td>

// https://stackoverflow.com/questions/11345296/partitioning-in-javascript
function partition(input, spacing) {
    var output = [];

    for (var i = 0; i < input.length; i += spacing) {
        output[output.length] = input.slice(i, i + spacing);
    }

    return output;
}

