import { TravelCloudClient, TcResponse } from '../travelcloud'
import { Collapse, Spin } from 'antd'
import React, { useState } from 'react'
import { useEffect } from 'react'

const Panel = Collapse.Panel

export const FareRules = ({flight, pricing_id, client, autoOpen}: {flight: any, pricing_id: string, client: TravelCloudClient, autoOpen?: boolean}) => {
  const [ fareRulesText, setFareRulesText ] = useState<{[key:string]: TcResponse<any>}>({}) 
  const [ activeKey, setActiveKey ] = useState<string[] | string>([])
  var pricing

  // this method probably doesn't handle race condition well
  const loadRule = async (y) => {
    if (fareRulesText[y] != null) return
    const fare_rule = pricing.fare_rules[y]
    if (fare_rule.rules != null) {
      fareRulesText[y] = {result: fare_rule.rules}
      setFareRulesText(Object.assign({}, fareRulesText))
      return
    }
    fareRulesText[y] = {loading: true}
    setFareRulesText(Object.assign({}, fareRulesText))
    fareRulesText[y] = await client.fareRules(Object.assign({}, fare_rule, {source: source}))
    setFareRulesText(Object.assign({}, fareRulesText))
  }

  useEffect(() => {
    if (autoOpen) {
      loadRule('0')
      setActiveKey(['0'])
    }
  }, [])

  if (flight == null || pricing_id == null) return null;
  if (flight.pricings != null) {
    pricing = flight.pricings.find(pricing => pricing.id === pricing_id)
  }

  if (pricing == null) return null;

  const source = pricing_id.split('/')[0]

  const fareBasisFilter = {}

  if (pricing.fare_rules == null) return null;

  const filteredFareRules = pricing.fare_rules
    .filter((rule) => {
      // this logic will probably fail for indian fares
      // should filter by prefix instead
      if (rule.fare_basis.toUpperCase().endsWith('CH') || rule.fare_basis.toUpperCase().endsWith('IN')) return false
      const result = fareBasisFilter[rule.fare_basis] == null
      fareBasisFilter[rule.fare_basis] = true
      return result
    })

  if (filteredFareRules.length === 0) return null;

  return <>
    <h1>Fare rules</h1>
    <Collapse
      bordered={false}
      activeKey={activeKey}
      onChange={(x) => {
        if (typeof x === 'string') x = [x]
        x.map(y => loadRule(y))
        setActiveKey(x)
      }}
    >
      {filteredFareRules
        .map((rule, index2) =>
          <Panel header={rule.origin + " -> " + rule.destination + " (" + rule.fare_basis + ")"} key={index2}>
            {fareRulesText[index2]?.loading && <Spin />}
            {fareRulesText[index2]?.result && fareRulesText[index2]?.result.map((rule, index3) => <div key ={index3}>
              <h3>{rule.category_name}</h3>
              <div>{rule.text}</div>
            </div>)}
            {fareRulesText[index2]?.error && <div>Unable to retrieve fare rules</div>}
          </Panel>)}

    </Collapse>
  </>
}