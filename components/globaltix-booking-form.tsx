import React from 'react'
import moment from 'moment'

import { Button, Input, Form, DatePicker, Select } from "antd";
import { IncDecInput } from './inc-dec-input';
import { FormComponentProps } from 'antd/lib/form';

const { Option } = Select;

type MyType = {ticketTypeGet, onAddToCart} & FormComponentProps

export class GlobaltixBooking extends React.Component<MyType> {
  render() {
    const { getFieldDecorator, getFieldsError } = this.props.form;
    const data = this.props.ticketTypeGet.result.data

    const handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          this.props.onAddToCart(values);
        }
      });
    };

    const hasErrors = (fieldsError) => {
      return Object.keys(fieldsError).some(field => fieldsError[field]);
    }

    function disabledDate(days = 0) {
      return (current) =>
        current && current < moment().add(days, 'days').endOf('day');
    }

    const options = {rules: [{ required: true, message: 'This field is required' }]}
    return (
      <Form layout="vertical" onSubmit={handleSubmit}>
        {
          data.questions != null && data.questions
            .filter((question) => question.optional === false)
            .map((question, index) => {
              var input
              const id: string = question.id + ""
  
              if (question.type.name === 'FREETEXT') input = getFieldDecorator(id, options)(<Input />)
              else if (question.type.name === 'DATE') input = getFieldDecorator(id, options)(<DatePicker disabledDate={disabledDate()} style={{width: '100%'}} />)
              else if (question.type.name === 'OPTION') input = 
                getFieldDecorator(id, options)(<Select>
                  {question.options.map((option, index) => <Option key={index} value={option}>{option}</Option>)}
                </Select>)
              else throw new Error("Invalid question type")
  
              return <Form.Item label={question.question} style={{marginBottom: '12px'}} key={index}>
                {input}
              </Form.Item>
            })
        }

        {data.isVisitDateCompulsory && data.isRequestVisitDate &&
          <Form.Item label="Visit date" style={{marginBottom: '12px'}}>
            {getFieldDecorator('visitDate', options)(<DatePicker disabledDate={disabledDate(data.advanceBookingDays || 0)} style={{width: '100%'}} />)}
          </Form.Item>
        }
        <Form.Item label="Quantity" style={{marginBottom: '12px'}}>
          <IncDecInput min={0} max={9} name="quantity" form={this.props.form} style={{width: '60%', float: 'left'}} />
          <Button htmlType="submit" style={{width: '30%', float: 'right'}} disabled={hasErrors(getFieldsError())}>Add to cart</Button>
        </Form.Item>
      </Form>
    );
  }
}

export const GlobaltixBookingForm = Form.create<MyType>({
  mapPropsToFields(props) {
    const data = props.ticketTypeGet.result.data

    const fields = {
      quantity: Form.createFormField({
        value: 1,
      }),
    };

    for (var question of data.questions) {
      const id: string = question.id + ""
      fields[id] = Form.createFormField({
        value: ""
      })
    }
    
    return fields;
  }
})(GlobaltixBooking);

