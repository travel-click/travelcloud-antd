import React, {useState, useEffect, useContext, useCallback, useLayoutEffect, useRef, useMemo} from 'react'
import Router from 'next/router'
import moment from "moment";
import { TravelCloudClient, Cart, validateFlightsParams, TcResponse, FlightsParams, FlightParams, omit, purify, validateFlightsChangeParams } from '../../travelcloud'
import { FlightSearch, FlightDetail } from '../../types'
import { Form, DatePicker, Select, Row, Col, Tooltip, Icon } from "antd";
import { computePriceRules } from '../../order-product-compute';
import { computePrice, computeOd } from '../flights-result'
import { number } from 'prop-types';


const validKeys: string[] = [
  "od1.origin_airport.code",
  "od1.origin_city.code",
  "od1.origin_datetime",
  "od1.destination_airport.code",
  "od1.destination_city.code",

  "od2.origin_airport.code",
  "od2.origin_city.code",
  "od2.origin_datetime",
  "od2.destination_airport.code",
  "od2.destination_city.code",

  "od3.origin_airport.code",
  "od3.origin_city.code",
  "od3.origin_datetime",
  "od3.destination_airport.code",
  "od3.destination_city.code",

  "od4.origin_airport.code",
  "od4.origin_city.code",
  "od4.origin_datetime",
  "od4.destination_airport.code",
  "od4.destination_city.code",

  "source",
  "cabin",
  "ptc_adt",
  "ptc_cnn",
  "ptc_inf"
];

// probably don't need hooks for these state
var selectedFlight = null
var selectedPricingId = null

export function useFlights({
  defaultType,
  query,
  client,
  sources,
  onLoad,
  onLoadStart,
  autoSearch,
  onPricingSelect,
  onSeatSelect,
  autoSelectDateRange=3,
  product_source_ref,
  searchType='search'
}: {
  defaultType?: string;
  query: any;
  client: TravelCloudClient;
  sources: any[],
  onLoadStart?: (flightsParam) => void,
  onLoad?: (flightsResult) => void,
  onPricingSelect?: (flight, pricing_id, search_id?: string) => void,
  onSeatSelect?: (query, flight, pricing_id) => void,
  autoSearch?: boolean,
  autoSelectDateRange?: number,
  product_source_ref?: string,
  searchType?: string
}): any {

  // any better idea for default argument
  if (onLoad == null) onLoad = () => null
  if (onLoadStart == null) onLoadStart = () => null
  if (autoSearch == null) autoSearch = false

  const [ flights, setFlights ]: [TcResponse<FlightSearch> & {search_id?: string}, (any) => void] = useState({}) 
  const [ flightMap, setFlightMap ]: [{[key: string]: TcResponse<FlightDetail>}, (any) => void] = useState({})
  const [ isPrimeWorflowModelVisible, setIsPrimeWorflowModelVisible ] = useState<boolean>(false)

  const { flightsParam, handleParamsChange, handleSwap, type, setType } 
    = useFlightParams({ defaultType, query, sources, autoSelectDateRange })

  // a bit stupid way, but events are annoying
  const [ validatedFlightsParam, updateValidatedFlightsParam ]: [ any, (any) => void ] 
    = useState(Object.assign( {}, flightsParam ))

  // const search = useCallback()
  
  const validateAndSearch = useCallback(() => {
    const validated = validateFlightsParams( flightsParam );
    if (validated == null) return ;

    onLoadStart( validated );
    client.cancelFlights()

    setFlights({ loading: true })

    client.flights(validated)
      .then(value => { 
        setFlights( value ); 
        setFlightMap({}); 
        onLoad( value )
      })

  }, [ flightsParam ])

  const validateAndSearchChange = useCallback(() => {
    flightsParam.product_source_ref = product_source_ref
    const validated = validateFlightsChangeParams( flightsParam );
    if (validated == null) return ;

    onLoadStart( validated );
    client.cancelFlights()

    setFlights({ loading: true })

    // @ts-ignore
    client.flightsChange(validated)
      .then(value => { 
        setFlights( value ); 
        setFlightMap({}); 
        onLoad( value )
      })

  }, [ flightsParam ])
  // fixme: why there is 2 types for flight search

  const onFlightClick = (index, flight) => {
    // future changes
    // 1. flight_search should indicate what to do instead of hardcoding by source
    // 2. if there is only 1 pricing, should check if pricing remains the same and auto onPricingSelect() if so

    if (sources[0] == 'webconnect') {
      onPricingSelect(flight, flight["pricings"][0]["id"], flights.search_id);
    }
    else if (sources[0] == 'sqndc') {
      setFlightMap(currentFlightMap => {
        // goal is to reduce the number of pricings to a more managable level for UI display
        const filteredPricings = {}
        const getFareId = (x) => {
          // we prefer comparing via brand name because this is what the user sees
          if (x.brand != null && x.brand.name != null) return x.brand.name
          return x.fare_basis
        }

        const getPreferredPricing = (pricing1, pricing2) => {
          if (pricing1 == null) return pricing2;
          if (pricing2 == null) return pricing1;
          
          if (pricing1.fares.length > 1 && pricing2.fares.length) {
            // we prefer pricings with identical depart and return fares
            if (getFareId(pricing1.fares[0]) === getFareId(pricing1.fares[1])) return pricing1;
            if (getFareId(pricing2.fares[0]) === getFareId(pricing2.fares[1])) return pricing2;
          }
          
          // prefer cheaper fare if both are not identical
          if (parseFloat(pricing1.price) > parseFloat(pricing2.price)) return pricing2;
          return pricing1;
        }

        for (let pricing of flight.pricings) {
          // the repeated od1 fares are especially confusing so we filter based on that
          const key = getFareId(pricing.fares[0])
          filteredPricings[key] = getPreferredPricing(filteredPricings[key], pricing)
        }

        const newFlight = Object.assign({}, flight)
        //newFlight.pricings = Object.entries(filteredPricings).map(kvp => kvp[1])
        return Object.assign({}, currentFlightMap, { [index]: {result: newFlight} })
      });
    }
    else {
      handleFlightMap(index, flight, [query.source]);
    }
  }

  const onFareClick = (index, flight, pricingId) => {
    if (pricingId.indexOf('sqndc') !== 0) onPricingSelect(flight, pricingId)
    else {
      if (searchType === 'change') {
        const query = {
          source: 'sqndc',
          "od1.id": flight.od1.id,
          "pricing.id": pricingId,
          product_source_ref: flightsParam.product_source_ref
        }
        if (flight.od2 != null) query["od2.id"] = flight.od2.id;
        if (flight.od3 != null) query["od3.id"] = flight.od3.id;
        if (flight.od4 != null) query["od4.id"] = flight.od4.id;
    
        onSeatSelect(query, flight, pricingId)
      } else {
        selectedFlight = flight
        selectedPricingId = pricingId
        setIsPrimeWorflowModelVisible(true)
      }
    }
  }

  const chooseNotPrimeWorkflow = () => {
    onPricingSelect(selectedFlight, selectedPricingId)
  }

  const choosePrimeWorkflow = () => {
    const pricing = selectedFlight.pricings.find((pricing) => pricing.id === selectedPricingId)
    const query = {
      source: 'sqndc',
      "od1.id": selectedFlight.od1.id,
      ptc_adt: pricing.ptc_breakdown_map.adt.quantity,
      ptc_cnn: pricing.ptc_breakdown_map.cnn
        ? pricing.ptc_breakdown_map.cnn.quantity
        : 0,
      ptc_inf: pricing.ptc_breakdown_map.inf
        ? pricing.ptc_breakdown_map.inf.quantity
        : 0,
      "pricing.id": pricing.id
    }
    if (selectedFlight.od2 != null) query["od2.id"] = selectedFlight.od2.id;
    if (selectedFlight.od3 != null) query["od3.id"] = selectedFlight.od3.id;
    if (selectedFlight.od3 != null) query["od3.id"] = selectedFlight.od3.id;

    onSeatSelect(query, selectedFlight, selectedPricingId)
  }

  useEffect(() => {
    if (autoSearch) {
      // we assume that only 1 will have effect
      validateAndSearch()
      validateAndSearchChange()
    }
  }, [ flightsParam ])

  async function handleFlightMap(index, flight: FlightDetail, sources: any[]) {
    setFlightMap(currentFlightMap => (
      Object.assign({}, currentFlightMap, { [index]: { loading: true } })
    ));

    const payload = {
      source: sources[0],
      "od1.id": flight.od1.id,
      ptc_adt: flight.pricings[0].ptc_breakdown_map.adt.quantity,
      ptc_cnn: flight.pricings[0].ptc_breakdown_map.cnn
        ? flight.pricings[0].ptc_breakdown_map.cnn.quantity
        : 0,
      ptc_inf: flight.pricings[0].ptc_breakdown_map.inf
        ? flight.pricings[0].ptc_breakdown_map.inf.quantity
        : 0,
      "pricing.id": flight.pricings[0].id
    }

    if (flight.od2) payload["od2.id"] = flight.od2.id
    // @ts-ignore
    if (flight.od3) payload["od3.id"] = flight.od3.id
    // @ts-ignore
    if (flight.od4) payload["od4.id"] = flight.od4.id

    const detail = await client.flight(payload);

    setFlightMap(currentFlightMap => (
      Object.assign({}, currentFlightMap, { [index]: detail })
    ));
  }

  return { validateAndSearch, validateAndSearchChange,
    flights, 
    flightMap, handleFlightMap,
    flightsParam, handleParamsChange,
    handleSwap, type, setType,
    onFlightClick, onFareClick,
    isPrimeWorflowModelVisible, choosePrimeWorkflow, chooseNotPrimeWorkflow
  }
}

const nullOrEmpty = (x) => x == null || x === "";

export function useFlightParams({
  defaultType,
  query,
  sources,
  autoSelectDateRange=3
}: {
  defaultType: string;
  query: any;
  sources: any[];
  autoSelectDateRange?: number;
}) {

  const [flightsParam, setFlightsParam] = useState(purify({
    source: sources[0],
    "od1.origin_airport.code": query["od1.origin_airport.code"],
    "od1.origin_city.code": query["od1.origin_city.code"],
    "od1.origin_datetime": query["od1.origin_datetime"],
    "od1.destination_airport.code": query["od1.destination_airport.code"],
    "od1.destination_city.code": query["od1.destination_city.code"],

    "od2.origin_airport.code": query["od2.origin_airport.code"],
    "od2.origin_city.code": query["od2.origin_city.code"],
    "od2.origin_datetime": query["od2.origin_datetime"],
    "od2.destination_airport.code": query["od2.destination_airport.code"],
    "od2.destination_city.code": query["od2.destination_city.code"],
    
    "od3.origin_airport.code": query["od3.origin_airport.code"],
    "od3.origin_city.code": query["od3.origin_city.code"],
    "od3.origin_datetime": query["od3.origin_datetime"],
    "od3.destination_airport.code": query["od3.destination_airport.code"],
    "od3.destination_city.code": query["od3.destination_city.code"],

    "od4.origin_airport.code": query["od4.origin_airport.code"],
    "od4.origin_city.code": query["od4.origin_city.code"],
    "od4.origin_datetime": query["od4.origin_datetime"],
    "od4.destination_airport.code": query["od4.destination_airport.code"],
    "od4.destination_city.code": query["od4.destination_city.code"],
    
    ptc_adt: query.ptc_adt || 1,
    ptc_cnn: query.ptc_cnn || 0,
    ptc_inf: query.ptc_inf || 0,
    cabin: query.cabin || "Y"
  }));

  const [ type, setType ] = useState(() => {
    if (
      ! nullOrEmpty(flightsParam["od1.destination_airport.code"]) ||
      ! nullOrEmpty(flightsParam["od2.destination_airport.code"]) ||
      ! nullOrEmpty(flightsParam["od1.destination_city.code"]) ||
      ! nullOrEmpty(flightsParam["od2.destination_city.code"])
    ) {
      return "multi"
    }

    else {
      return defaultType
    }
  });

  const handleParamsChange = value => {
    if (type === 'return') {
      value["od2.origin_datetime"] = 
        value["od1.origin_datetime"] !== flightsParam["od1.origin_datetime"]
        ? moment(value["od1.origin_datetime"]).add(autoSelectDateRange, "days").format("YYYY-MM-DD")
        : value["od2.origin_datetime"];
    }

    setFlightsParam(value);
  };

  const handleSwap = () => {
    const od1Origin = flightsParam['od1.origin_airport.code']
    const od2Origin = flightsParam['od2.origin_airport.code']
    const newValue = Object.assign({}, flightsParam)
    newValue['od1.origin_airport.code'] = od2Origin
    newValue['od2.origin_airport.code'] = od1Origin
    setFlightsParam(newValue);
  }

  useEffect(() => {
    if (type === "return") {

      setFlightsParam(prev => omit(prev, [
        "od1.destination_code",
        "od2.destination_code",
        "od1.destination_airport.code",
        "od2.destination_airport.code"
      ]))

    }

    if (type === "one") {

      setFlightsParam(prev => omit(prev, [
        "od1.destination_code",
        "od2.destination_code",
        "od1.destination_airport.code",
        "od2.destination_airport.code",
        "od2.origin_departure",
        "od2.origin_datetime"
      ]))

    }

    if (type !== "multi") {
      setFlightsParam(prev => omit(prev, [
        "od3.origin_airport.code",
        "od3.origin_city.code",
        "od3.origin_datetime",
        "od3.destination_airport.code",
        "od3.destination_city.code",

        "od4.origin_airport.code",
        "od4.origin_city.code",
        "od4.origin_datetime",
        "od4.destination_airport.code",
        "od4.destination_city.code",
      ]))
    }

  }, [type]);

  return {
    flightsParam, handleParamsChange, handleSwap, type, setType
  }
}

/**
 * 
 * @param filter 
 */
export function parseFilters(filters: string) {
  return filters.split('/')
    .filter((token:string) => token.indexOf('!') > -1)
    .map(token => token.split('!'))
    .map(([key, val]) => {
      return [key, val.split(',').map((x:any) => isNaN( x ) ? x : Number( x ))]
    })
    .reduce((acc, [key, val]: [string, any]) => ({ ...acc, [key]: val }), {})
}

export function encodeFilters(values: any) {
  return Object.entries(values)
    .filter(([key, value]: [ any, any ]) => value.length > 0)
    .map(([key, value]) => `${key}!${value}`)
    .join('/')
}

export function useUrlDispatch(filters) {
  const isFirstRun = useRef( true );
  useEffect(() => {
    if (! isFirstRun.current ) {
      let filterStr = encodeFilters( filters )
      if (filterStr.length > 0) {
        // fixme: is there any api other than Router.push()
        // like Router.append() ?
        Router.push({ 
          pathname: Router.pathname, 
          query: Object.assign( Router.query, {filters: filterStr}) 
        })
      }
    }

    isFirstRun.current = false;
  }, [ filters ])
}


// fixme: provide proper type for filters
export function useFlightFilterResults(flights:any, filters:any, cart: any) {
  return useMemo(() => filterFlights({ flights, filters, cart }), [ flights, filters ])
}

// fixme: provide proper type for filters
export function useFlightFilters(query: any):any {

  const init = {
    stops: [],
    price_range: [],
    outbound_departure: [],
    outbound_arrival: [],
    return_departure: [],
    return_arrival: [],
    airlines: []
  }

  const [filters, setFilters] = useState(Object.assign( {}, init, query.filters == null ? {} : parseFilters(query.filters) ));

  function clearFilter( filterProperty ) {
    setFilters( init )
  }

  function handleFilters( filterProperty, filterValue ){
    setFilters(filters => {
      let currentFilters = {
        stops: filters.stops,
        price_range: filters.price_range,
        outbound_departure: filters.outbound_departure,
        outbound_arrival: filters.outbound_arrival,
        return_departure: filters.return_departure,
        return_arrival: filters.return_arrival,
        airlines: filters.airlines
      };
      if(Object.keys(currentFilters).find(property => filterProperty == property)){
        currentFilters[filterProperty] = filterValue;
      }
      return currentFilters;
    });
  };

  return [ 
    filters, handleFilters, clearFilter
  ]
}

// ERYI
// requiredCityCodes is used in generateOptions() when the city only has one airport, setRequiredCityCodes logic probably bugged
// defaultCityClone is used in generateOptions() to determine what city codes should fetch
// should pass defaultCityCodes to generateOptions() and derieve there instead of storing as state here

const useValidateCity = (defaultCityCodes, value) => {
  const [defaultCityClone, setDefaultCityClone] = useState(
    defaultCityCodes == null ? [] : defaultCityCodes.slice(0)
  );
  const [ requiredCityCodes , setRequiredCityCodes ] = useState({});
  //fix me: solve code duplication
  useEffect(() => {
    if (isValidCode(value["od1.origin_city.code"])) {
      setRequiredCityCodes({
        [value["od1.origin_city.code"]]: true
      });
      setDefaultCityClone(cityCodes => [
         ...cityCodes, 
         value["od1.origin_city.code"] 
      ]);
    }

    if (isValidCode(value["od2.origin_city.code"])) {
      setRequiredCityCodes({
        [value["od2.origin_city.code"]]: true
      });
      setDefaultCityClone(cityCodes => [
         ...cityCodes, 
         value["od2.origin_city.code"] 
      ]);
    }

    if (isValidCode(value["od1.destination_city.code"])) {
      setRequiredCityCodes({
        [value["od1.destination_city.code"]]: true
      });
      setDefaultCityClone(cityCodes => [
        ...cityCodes,
        value["od1.destination_city.code"]
      ]);
    }

    if (isValidCode(value["od2.destination_city.code"])) {
      setRequiredCityCodes({
        [value["od2.destination_city.code"]]: true
      });
      setDefaultCityClone(cityCodes => [
        ...cityCodes,
        value["od2.destination_city.code"]
      ]);
    }
  }, [])
  return [ defaultCityClone , requiredCityCodes ]
}

const useValidateAirport = (defaultAirportCodes, value) => {
  const [ defaultAirportClone, setDefaultAirportClone ] = useState(defaultAirportCodes == null ? [] : defaultAirportCodes.slice(0));
  //solve code duplication
  useEffect(() => {
    if (isValidCode(value["od1.origin_airport.code"])) {
      setDefaultAirportClone(airportCodes => [
        ...airportCodes,
        value["od1.origin_airport.code"]
      ]);
    }
    if (isValidCode(value["od2.origin_airport.code"])) {
      setDefaultAirportClone(airportCodes => [
        ...airportCodes,
        value["od2.origin_airport.code"]
      ]);
    }
    if (isValidCode(value["od1.destination_airport.code"])) {
      setDefaultAirportClone(airportCodes => [
        ...airportCodes,
        value["od1.destination_airport.code"]
      ]);
    }
    if (isValidCode(value["od2.destination_airport.code"])) {
      setDefaultAirportClone(airportCodes => [
        ...airportCodes,
        value["od2.destination_airport.code"]
      ]);
    }
  }, [])
  return defaultAirportClone;
}

const useDefaultOptions = (validCityClone, validAirportClone, requiredCityCodes, client, airportsOnly) => {
  const [ defaultOptions, setDefaultOptions ] = useState(null);

  useEffect(() => {
    fetchOptions();
  }, [ validCityClone, validAirportClone ]);

  async function fetchOptions() {
    let options = await generateOptions(
      airportsOnly,
      {
        airportCodes: validAirportClone.join(","),
        cityCodes: validCityClone
      },
      client,
      requiredCityCodes
    );
    setDefaultOptions(options);
  }
  return defaultOptions;
}

async function generateOptions(airportsOnly, params, client, requiredCityCodes = {}) {
  if (params.query != null && params.query.length < 3) return null;

  const result = await client.iataAutoComplete(params)

  var options
  if (airportsOnly === true) {
    options = result.result.reduce((acc, city) => {
      return city.airports.reduce((acc2, airport) => {
        acc2.push(<Select.Option key={airport.code} value={airport.code}>{city.city_name}, {airport.name} ({airport.code})</Select.Option>)
        return acc2
      }, acc)
    }, [])
  } else {
    options = result.result.reduce((acc, city) => {
      if (city.airports.length === 1 && requiredCityCodes[city.city_code] == null) {
        acc.push(<Select.Option className="tc-icon-select" key={city.airports[0].code} value={city.airports[0].code}>
          <div className="tc-icon maticon-airplanemode_active" /><span>{city.city_name}, {city.airports[0].name} ({city.airports[0].code})</span></Select.Option>)
        return acc
      } else {
        acc.push(<Select.Option className="tc-icon-select" key={"C-" + city.city_code} value={"C-" + city.city_code}>
          <div className="tc-icon maticon-location_city" />{city.city_name}, {city.country_name} (Any)</Select.Option>)
        return city.airports.reduce((acc2, airport) => {
          acc2.push(<Select.Option className="tc-icon-select l2" key={airport.code} value={airport.code}>
            <div className="tc-icon maticon-airplanemode_active" />{airport.name} ({airport.code})</Select.Option>)
          return acc2
        }, acc)
      }
    }, [])
  }

  return options
}

function isValidCode(x){
  return x != null && x.length === 3;
}

const FormValueContext = React.createContext<FlightsParams>(null);
const FormRcContext = React.createContext<FlightsParamsRcForm>(null);
const FormState = React.createContext<any>(null);


interface FlightsParamsRcForm{
  props: FlightsParamsFormProps,
  handle: (key: any, value: any) => void,
  handleCode: (airportKey: any, cityKey: any, value: string) => void,
  disabledStartDate: (value: any) => void,
  //disabledEndDate: (value: any) => void,
  disabledEndDate2: (value: any) => void
}


interface FlightsParamsFormProps {
  onChange: (FlightParams) => void;
  value: FlightsParams;
  client: TravelCloudClient;
  defaultAirportCodes?: string[];
  defaultCityCodes?: string[];
  airportsOnly?: boolean;
  loading?: boolean;
  children?: any;
  minLeadTime?: number;
}


interface StyleAttributeProps{
  className?: string,
  style?: React.CSSProperties,
  placeholder?: string,
  open?: boolean,
  onOpenChange?: CallableFunction,
  minLeadTime?: number
}

export const FlightPtcAdtSelect: React.FC<StyleAttributeProps> = ({ placeholder, style, className }) => {
  const formValue: any = useContext(FormValueContext);
  const rc: any = useContext(FormRcContext);
  return (
    <Select
      onChange={value => rc.handle("ptc_adt", value)}
      value={formValue.ptc_adt || 0}
      style={style}
      placeholder={placeholder}
      className={className}
    >
      <Select.Option value={1}>1</Select.Option>
      <Select.Option value={2}>2</Select.Option>
      <Select.Option value={3}>3</Select.Option>
    </Select>
  );
}

export const FlightPtcCnnSelect: React.FC<StyleAttributeProps> = ({ style, placeholder, className }) => {
  const formValue: any = useContext(FormValueContext);
  const rc: any = useContext(FormRcContext);

  return (
    <Select
      onChange={(value) => rc.handle('ptc_cnn', value)}
      value={formValue.ptc_cnn || 0}
      style={style}
      placeholder={placeholder}
      className={className}>
      <Select.Option value={0}>0</Select.Option>
      <Select.Option value={1}>1</Select.Option>
      <Select.Option value={2}>2</Select.Option>
      <Select.Option value={3}>3</Select.Option>
    </Select>
  )
}

export const FlightPtcInfSelect: React.FC<StyleAttributeProps> = ({ style, placeholder, className }) => {
  const formValue: any = useContext(FormValueContext);
  const rc: any = useContext(FormRcContext);

  return (
    <Select
      onChange={(value) => rc.handle('ptc_inf', value)}
      value={formValue.ptc_inf || 0}
      style={style}
      placeholder={placeholder}
      className={className}>
      <Select.Option value={0}>0</Select.Option>
      <Select.Option value={1}>1</Select.Option>
      <Select.Option value={2}>2</Select.Option>
      <Select.Option value={3}>3</Select.Option>
    </Select>
  )
}

export const FlightCabinSelect: React.FC<StyleAttributeProps> = ({ style, placeholder, className }) => {
  const formValue: any = useContext(FormValueContext);
  const rc: any = useContext(FormRcContext);

  return (
    <Select
      onChange={(value) => rc.handle('cabin', value)}
      value={formValue.cabin}
      style={style}
      placeholder={placeholder}
      className={className}>
      <Select.Option value='Y'>Economy</Select.Option>
      <Select.Option value='S'>Premium Economy</Select.Option>
      <Select.Option value='C'>Business</Select.Option>
      <Select.Option value='F'>First</Select.Option>
    </Select>
  )
}

export const FlightOd1OriginDepartureDatePicker: React.FC<StyleAttributeProps> = ({ style, placeholder, className, open, onOpenChange, minLeadTime }) => {
  const formValue: any = useContext(FormValueContext);
  const rc: any = useContext(FormRcContext);

  const extra:any = open == null ? {} : { open, onOpenChange }

  if (minLeadTime == null)
    minLeadTime = 0

  return (
    <DatePicker
    {...extra}
    value={formValue['od1.origin_datetime'] == null || formValue['od1.origin_datetime'] === '' ? null : moment(formValue['od1.origin_datetime'])}
    disabledDate={date => rc.disabledStartDate(date.clone().subtract(minLeadTime, 'd'))}
    style={style}
    placeholder={placeholder}
    className={className}
    format="YYYY-MM-DD"
    onChange={(value) => {
      rc.handle('od1.origin_datetime', value)
    }} />
  )
}

export const FlightOd2OriginDepartureDatePicker: React.FC<StyleAttributeProps> = ({ style, placeholder, className, open, onOpenChange }) => {
  const formValue: any = useContext(FormValueContext);
  const rc: any = useContext(FormRcContext);
  const extra:any = open == null ? {} :{ open ,onOpenChange} 
  return (
    <DatePicker
    {...extra}
    value={formValue['od2.origin_datetime'] == null || formValue['od2.origin_datetime'] === '' ? null : moment(formValue['od2.origin_datetime'])}
    defaultPickerValue={formValue['od1.origin_datetime'] == null ? null : moment(formValue['od1.origin_datetime']).add(1, 'days')}
    disabledDate={rc.disabledEndDate2(formValue['od1.origin_datetime'])}
    style={style}
    placeholder={placeholder}
    className={className}
    format="YYYY-MM-DD"
    onChange={(value) => rc.handle('od2.origin_datetime', value)} />
  )
}

export const FlightOd3OriginDepartureDatePicker: React.FC<StyleAttributeProps> = ({ style, className, open, onOpenChange }) => {
  const formValue: any = useContext(FormValueContext);
  const rc: any = useContext(FormRcContext);
  const extra:any = open == null ? {} :{ open ,onOpenChange} 
  return (
    <DatePicker
    {...extra}
    value={formValue['od3.origin_datetime'] == null || formValue['od3.origin_datetime'] === '' ? null : moment(formValue['od3.origin_datetime'])}
    defaultPickerValue={null}
    disabledDate={rc.disabledEndDate2(formValue['od2.origin_datetime'])}
    style={style}
    className={className}
    format="YYYY-MM-DD"
    onChange={(value) => rc.handle('od3.origin_datetime', value)} />
  )
}


export const FlightOd4OriginDepartureDatePicker: React.FC<StyleAttributeProps> = ({ style, className, open, onOpenChange }) => {
  const formValue: any = useContext(FormValueContext);
  const rc: any = useContext(FormRcContext);
  const extra:any = open == null ? {} :{ open ,onOpenChange} 
  return (
    <DatePicker
    {...extra}
    value={formValue['od4.origin_datetime'] == null || formValue['od4.origin_datetime'] === '' ? null : moment(formValue['od4.origin_datetime'])}
    defaultPickerValue={null}
    disabledDate={rc.disabledEndDate2(formValue['od3.origin_datetime'])}
    style={style}
    className={className}
    format="YYYY-MM-DD"
    onChange={(value) => rc.handle('od4.origin_datetime', value)} />
  )
}

export function CodeSelect(props: {style?: any, placeholder?: string, className?: string, field: string}) {
  const formValue = useContext(FormValueContext)
  const rc = useContext(FormRcContext)
  const state = useContext(FormState)

  const [stateOptions, setStateOptions] = useState(null);
  const handleSearch = async (rc, searchValue) => {
    const options = await generateOptions(rc.props.airportsOnly, {query: searchValue}, rc.props.client)
    setStateOptions(options)
  }

  const airportKey = props.field + '_airport.code'
  const cityKey = props.field + '_city.code'

  var value = formValue[airportKey]
  if (formValue[cityKey] != null && formValue[cityKey] != '') value = 'C-' + formValue[cityKey]

  return <Select
    showSearch
    onChange={(value) => rc.handleCode(airportKey, cityKey, value)}
    value={value}
    onSearch={(searchValue) => handleSearch(rc, searchValue)}
    filterOption={() => true}
    style={props.style}
    placeholder={props.placeholder}
    className={props.className}
    defaultActiveFirstOption={false}>
    {stateOptions || state.defaultOptions}
  </Select>
}

export function FlightOd1OriginCodeSelect(props: StyleAttributeProps) {
  return CodeSelect({...props, field: 'od1.origin'})
}

export function FlightOd1DestinationCodeSelect(props: StyleAttributeProps) {
  return CodeSelect({...props, field: 'od1.destination'})
}

export function FlightOd2OriginCodeSelect(props: StyleAttributeProps) {
  return CodeSelect({...props, field: 'od2.origin'})
}

export function FlightOd2DestinationCodeSelect(props: StyleAttributeProps) {
  return CodeSelect({...props, field: 'od2.destination'})
}

export function FlightOd3OriginCodeSelect(props: StyleAttributeProps) {
  return CodeSelect({...props, field: 'od3.origin'})
}

export function FlightOd3DestinationCodeSelect(props: StyleAttributeProps) {
  return CodeSelect({...props, field: 'od3.destination'})
}

export function FlightOd4OriginCodeSelect(props: StyleAttributeProps) {
  return CodeSelect({...props, field: 'od4.origin'})
}

export function FlightOd4DestinationCodeSelect(props: StyleAttributeProps) {
  return CodeSelect({...props, field: 'od4.destination'})
}

export function FlightsParamsForm(props: FlightsParamsFormProps) {
  const [endOpen, setEndOpen] = useState(false);
  const [data1, setData1] = useState([]);
  const [data2, setData2] = useState([]);
  const [validCityClone, requiredCityCodes] = useValidateCity(
    props.defaultCityCodes,
    props.value
  );
  const validAirportClone = useValidateAirport(
    props.defaultAirportCodes,
    props.value
  );
  const defaultOptions = useDefaultOptions(
    validCityClone,
    validAirportClone,
    requiredCityCodes,
    props.client,
    props.airportsOnly
  );

  function handle(key, value) {
    // Select passes string onChange
    // Input passes event onChange
    var update;
    if (value != null && value.target != null) value = value.target.value;

    // put ( value != null ) coz empty date throw error
    if (value != null && value.format != null)
      value = value.format("YYYY-MM-DD");
    else props.onChange({});

    update = {
      [key]: value
    };

    const allData = Object.assign({}, props.value, update);

    for (var i in allData) {
      if (allData[i] == null) delete allData[i];
      else if (validKeys.indexOf(i) === -1) delete allData[i];
    }

    props.onChange(allData);
  }

  function handleCode(airportKey, cityKey, value: string) {
    const allData = Object.assign({}, props.value);

    if (value.substr(0, 2) === "C-") {
      delete allData[airportKey];
      allData[cityKey] = value.substr(2);
    } else {
      delete allData[cityKey];
      allData[airportKey] = value;
    }

    props.onChange(allData);
  }

  function disabledStartDate(od1_origin_departure) {
    // not sure why od1_origin_departure might be null
    if (od1_origin_departure == null) return false;
    return od1_origin_departure.isBefore();
  }

  /*
  function disabledEndDate(od2_origin_departure) {
    if (od2_origin_departure == null) return false;
    const od1_origin_departure =
      props.value["od1.origin_datetime"] == null
        ? moment()
        : moment(props.value["od1.origin_datetime"]);
    if (!od2_origin_departure || !od1_origin_departure) {
      return (
        od2_origin_departure && od2_origin_departure < moment().endOf("day")
      );
    }
    return od2_origin_departure.isBefore(
      moment(od1_origin_departure).add(1, "days")
    );
  }
  */

  function disabledEndDate2(prev_origin_departure_str) {
    return (current_origin_departure) => {
      if (current_origin_departure == null) return false;
      const prev_origin_departure =
          prev_origin_departure_str == null
          ? moment()
          : moment(prev_origin_departure_str);
      if (!prev_origin_departure) {
        return (
          current_origin_departure && current_origin_departure < moment().endOf("day")
        );
      }
      
      return current_origin_departure.isBefore(
        moment(prev_origin_departure).add(1, "days")
      );
    }
  }

  const children = props.children || (
    <Form layout="vertical" style={{ marginTop: 16 }}>
      <Form.Item label="From">
        <FlightOd1OriginCodeSelect />
      </Form.Item>
      <Form.Item label="To">
        <FlightOd2OriginCodeSelect />
      </Form.Item>
      <Form.Item label="Departure date">
        <FlightOd1OriginDepartureDatePicker minLeadTime={props.minLeadTime} style={{ width: "100%" }} />
      </Form.Item>
      <Form.Item label="Return Date">
        <FlightOd2OriginDepartureDatePicker style={{ width: "100%" }} />
      </Form.Item>
      <Form.Item label="Adults">
        <FlightPtcAdtSelect />
      </Form.Item>
      <Form.Item label="Children">
        <FlightPtcCnnSelect />
      </Form.Item>
      <Form.Item label="Infants">
        <FlightPtcInfSelect />
      </Form.Item>
    </Form>
  );

  return (
    <FormValueContext.Provider value={props.value}>
      <FormRcContext.Provider
        value={{
          props,
          handle,
          handleCode,
          disabledStartDate,
          //disabledEndDate,
          disabledEndDate2
        }}
      >
        <FormState.Provider
          value={{
            endOpen,
            defaultOptions,
            data1,
            data2
          }}
        >
          {children}
        </FormState.Provider>
      </FormRcContext.Provider>
    </FormValueContext.Provider>
  );
}

function calculateTravelTime(segments) {
  const firstDeparture = segments[0].flights[0].departure_datetime
  const lastArrival = segments[segments.length-1].flights[segments[segments.length-1].flights.length-1].arrival_datetime
  return moment.duration(moment(lastArrival).diff(firstDeparture)).asMilliseconds()
}

// to check time between regardless of timezone
const isTimeBetween = (time, startHour, endHour) => {
  const start = moment(startHour, "H")
  const end = moment(endHour, "H")

  return moment(moment.parseZone(time)
    .format("H:mm"), "H:mm")
    .isBetween(start, end, null, '[]')
}



function groupBy( arr, callback ) {
  const obj = arr.reduce((acc, cur) => {
    const index = callback( cur )
    if (acc[ index ] == null) acc[ index ] = []
    acc[ index ].push( cur ) 
    return acc
  }, {})

  return Object.entries( obj ).map(([ k, v ]) => v)
}

function unique( arr, callback ): any[] {
  const obj = arr.reduce((acc, cur) => {
    const index = callback( cur )
    if (acc[ index ] == null) acc[ index ] = cur
    return acc
  }, {})

  return Object.entries( obj ).map(([ k, v ]) => v)
}

function min(arr, predicate) {
  const sorted = sortBy(arr, predicate)
  return sorted[ 0 ]
}

function max(arr, predicate) {
  const sorted = sortBy(arr, predicate)
  return sorted[ sorted.length - 1 ]
}

function sortBy(arr, predicate) {
  return arr.slice()
    .sort((a, b) => parseFloat(predicate( a )) - parseFloat(predicate( b )))
}

function pluckFlightInfos( flights, cart: Cart ) {
  return flights.result.slice().map(( currentFlight ) => {
    // sort fares by price
    const cheapest = min(currentFlight.pricings, ({ id }) => { computePrice( cart, currentFlight, id ).afterDiscount })
    //const numberOfStops = currentFlight.od1.segments.reduce((ac, segment) => ac + segment.flights.length, -1)
    var numberOfStops = null;
    const od1Computed = computeOd(currentFlight.od1)
    if (currentFlight.od2 != null) {
      const od2Computed = computeOd(currentFlight.od2)
      numberOfStops = Math.max(od1Computed.stops.count, od2Computed.stops.count)
    }
    else {
      numberOfStops = od1Computed.stops.count
    }
    const airline = currentFlight.od1.segments[0].marketing_carrier;

    return { airline, price: computePrice(cart, currentFlight, cheapest.id).afterDiscount, numberOfStops}
  })
}

export function createFlightStopsFilter( flights:any, cart: Cart, customPriceDisplay:CallableFunction, template?: (any) => any ) {
  return useMemo(() => {
    if (flights.result == null) return []

    const flightStops = pluckFlightInfos( flights, cart )

    const _template = template != null ? template : ({ numberOfStops, price }) => {

      let label: any = numberOfStops < 1 ? "Direct" 
        : numberOfStops < 2 ? `${numberOfStops} stop` : `${numberOfStops} stops`;

      return (
        <Row type="flex" align="middle">
          <Col span={12}>{label}</Col>
          <Col span={12} style={{ textAlign: "right" }}>
            {customPriceDisplay(price)}
          </Col>
        </Row>
      )
    }

    const stopsWithCheapestPrice = groupBy(flightStops, ({ numberOfStops }) => numberOfStops)
      .map(stops => min(stops, ({ price }) => price))

    return stopsWithCheapestPrice.map(({ airline, price, numberOfStops }) => {
      let checkboxItem = { /* checkbox item */
        value: numberOfStops,
        label: _template({ numberOfStops, price })
      }

      return checkboxItem
    })
  }, [ flights ])
}

export function createAirlinesFilter(flights:any, cart: Cart, customPriceDisplay: CallableFunction, airlineCharLimit: any, template?: (any) => any) {
  return useMemo(() => {
    if (flights.result == null) return []

    const _template = ({ airline, price, airlineCharLimit }) => {
      return (<div style={{display: "flex", justifyContent: "stretch", alignItems: "center"}}>
        <div style={{ overflow: "hidden", whiteSpace: "nowrap", paddingRight: 20, position: "relative", flexGrow: 1 }} >
          {airline.name.substring(0, airlineCharLimit)}
          <div style={{ width: 20, top: 0, bottom: 0, right: 0, background: "#fff", position: "absolute" }} />
          {airline.name.length > airlineCharLimit && (
            <Tooltip placement="right" title={airline.name}>
              <Icon type="info-circle" style={{ fontSize: 12, verticalAlign: "unset", color: "#b9b9b9", position: "absolute", right: 0, top: "50%", marginTop: -6 }} />
            </Tooltip>
          )}
        </div>
        <div style={{ textAlign: "right", width: 80 }}>
          {customPriceDisplay(price)}
        </div>
      </div>)
    }

    const flightInfos = pluckFlightInfos( flights, cart )

    return unique( flightInfos, ({ airline }) => airline.code)
      .sort((a:any, b:any) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0) // sort alphabetically
      .map(({ airline, price, numberOfStops }) => {
        const limit = 14
        return {
          id: airline.name,
          value: airline.code,
          label: _template({ airline, price, airlineCharLimit })
        }
      })
  }, [ flights ])
}

export function useFlightSorter( flights:any, initialSort:string ) {
  const [ sort, setSort ] = useState( initialSort );
  return [ sortFlights( flights, sort ), sort, setSort ]
}

function sortFlights( flights:any, sort:string ) {
  // todo: refactor more 
  if (sort === "quickest") {
    return flights.slice().sort((a, b) => {
      const a_duration = a.od2
        ? calculateTravelTime(a.od2.segments) + calculateTravelTime(a.od1.segments)
        : calculateTravelTime(a.od1.segments);
      const b_duration = b.od2
        ? calculateTravelTime(b.od2.segments) + calculateTravelTime(b.od1.segments)
        : calculateTravelTime(b.od1.segments);
      return Number(a_duration) - Number(b_duration);
    });
  }

  else if (sort === "cheapest") {
    return flights.slice().sort((a, b) => {
      return (
        parseFloat(a.pricings[0].price) - parseFloat(b.pricings[0].price)
      );
    });
  }

  else if (sort === "default") {
    return flights
  }
}

// fix me: provide proper type

export function filterFlights({ flights, filters, cart }: 
  { flights: any, filters: any, cart: Cart }): any {
  
  let sortedFilter = flights.result != null ? flights.result.concat([]) : []

  if (sortedFilter) {
    // filter flights
    sortedFilter = sortedFilter.filter(flight => {
      // pricing
      const pricing_match = filters.price_range.length == 0 || flight.pricings.filter(pricing => {
          let { afterDiscount } = computePrice(cart, flight, pricing.id, _ => _)
          return parseFloat( afterDiscount ) >= filters.price_range[0] && 
            parseFloat( afterDiscount ) <= filters.price_range[1]
        }).length > 0;
      
      const od1Computed = computeOd(flight.od1);

      const outbound_departure_hour = od1Computed.departure_datetime //flight.od1.segments[0].flights[0].departure_datetime

      const outbound_arrival_hour = od1Computed.arrival_datetime
      /*
      flight.od1.segments[flight.od1.segments.length - 1]
        .flights[
            flight.od1.segments[flight.od1.segments.length - 1].flights.length - 1
          ].arrival_datetime
      */

      const od2Computed = flight.od2 ? computeOd(flight.od2) : null

      const return_departure_hour = flight.od2 ? od2Computed.departure_datetime : null //flight.od2.segments[0].flights[0].departure_datetime : null;

      const return_arrival_hour = flight.od2 ? od2Computed.arrival_datetime : null
      /*
      flight.od2
        ? flight.od2.segments[flight.od2.segments.length - 1].flights[
              flight.od2.segments[flight.od2.segments.length - 1].flights
                .length - 1
            ].arrival_datetime
        : null;
      */
      
      const outbound_departure_match =
        filters.outbound_departure.length == 0 ||
        isTimeBetween(outbound_departure_hour, filters.outbound_departure[0], filters.outbound_departure[1])

      const outbound_arrival_match = 
        filters.outbound_arrival.length == 0 || isTimeBetween(outbound_arrival_hour, filters.outbound_arrival[0], filters.outbound_arrival[1])

      const return_departure_match = 
        filters.return_departure.length == 0 || isTimeBetween(return_departure_hour, filters.return_departure[0], filters.return_departure[1])

      const return_arrival_match = 
        filters.return_arrival.length == 0 || isTimeBetween(return_arrival_hour, filters.return_arrival[0], filters.return_arrival[1])

      let stops_match = false;
      let airline_match = false;
      let outbound_stops = od1Computed.stops.count,
        return_stops = flight.od2 ? od2Computed.stops.count : -1;

      flight.od1.segments.forEach(segment => {
        //outbound_stops += segment.flights.length;
        if (filters.airlines.length > 0) {
          if (
            filters.airlines.find(
              airline => airline === segment.marketing_carrier.code
            )
          )
            airline_match = true;
        }
      });

      if (flight.od2)
        flight.od2.segments.forEach(segment => {
          //return_stops += segment.flights.length;
          if (filters.airlines.length > 0) {
            if (
              filters.airlines.find(
                airline => airline === segment.marketing_carrier.code
              )
            )
              airline_match = true;
          }
        });
      if (
        filters.stops.indexOf(outbound_stops) > -1 ||
        filters.stops.indexOf(return_stops) > -1 ||
        filters.stops.length < 1
      )
        stops_match = true;
      if (filters.airlines.length < 1) airline_match = true;

      if (
        stops_match &&
        pricing_match &&
        airline_match &&
        outbound_departure_match &&
        outbound_arrival_match &&
        return_departure_match &&
        return_arrival_match
      )
        return flight;
    });

    return sortedFilter
  }
}

FlightsParamsForm.Od1OriginCodeSelect = FlightOd1OriginCodeSelect;
FlightsParamsForm.Od1DestinationCodeSelect = FlightOd1DestinationCodeSelect;
FlightsParamsForm.Od1OriginDepartureDatePicker = FlightOd1OriginDepartureDatePicker;

FlightsParamsForm.Od2OriginCodeSelect = FlightOd2OriginCodeSelect;
FlightsParamsForm.Od2DestinationCodeSelect = FlightOd2DestinationCodeSelect;
FlightsParamsForm.Od2OriginDepartureDatePicker = FlightOd2OriginDepartureDatePicker;

FlightsParamsForm.Od3OriginCodeSelect = FlightOd3OriginCodeSelect;
FlightsParamsForm.Od3DestinationCodeSelect = FlightOd3DestinationCodeSelect;
FlightsParamsForm.Od3OriginDepartureDatePicker = FlightOd3OriginDepartureDatePicker;

FlightsParamsForm.Od4OriginCodeSelect = FlightOd4OriginCodeSelect;
FlightsParamsForm.Od4DestinationCodeSelect = FlightOd4DestinationCodeSelect;
FlightsParamsForm.Od4OriginDepartureDatePicker = FlightOd4OriginDepartureDatePicker;

FlightsParamsForm.PtcAdtSelect = FlightPtcAdtSelect;
FlightsParamsForm.PtcCnnSelect = FlightPtcCnnSelect;
FlightsParamsForm.PtcInfSelect = FlightPtcInfSelect;

export function useAirFares () {
  const [ fareBreakdownModal, setFareBreakdownModal ] = useState(false);
  const [ fareRulesModal, setFareRulesModal ] = useState(false);
  const [ flightModal, setFlightModal ] = useState(true);

  const [ fareBreakdown, setFareBreakdown ] = useState([]);
  const [ fareRules, setFareRules ] = useState([]);

  function showFareBreakdownModal(details){
    setFareBreakdown(details);
    setFareBreakdownModal(true);
    setFareRulesModal(false);
  }

  function showFareRulesModal(details){
    setFareRules(details);
    setFareRulesModal(true);
    setFareBreakdownModal(false);
  }

  function showFlightMapModal(details){
    setFlightModal(details);
  }

  function handleClose() {
    setFareRulesModal(false);
    setFareBreakdownModal(false);
  }

  return {
    fareBreakdownModalData: {
      data: fareBreakdown, 
      handleClose: handleClose,
      show: fareBreakdownModal,
    },

    fareRulesModalData: {
      data: fareRules,
      handleClose: handleClose,
      show: fareRulesModal,
    },

    showFareBreakdownModal,
    showFareRulesModal
  }

}


