import React, { useContext, useState } from 'react'
import moment from 'moment'
import { Form, DatePicker, Select, Button, Row, Col, Radio, Icon } from 'antd'
import { TravelCloudClient, FlightsParams } from '../travelcloud'
import { stringify } from 'querystring';

const FormValueContext = React.createContext<FlightsParams>(null);
const FormRcContext = React.createContext<FlightsParamsForm>(null);
const FormState = React.createContext<any>(null);

const validKeys: string[] = [
  "source",
  "od1.origin_airport.code",
  "od1.origin_city.code",
  "od1.origin_datetime",
  "od1.destination_airport.code",
  "od1.destination_city.code",
  "od2.origin_airport.code",
  "od2.origin_city.code",
  "od2.origin_datetime",
  "od2.destination_airport.code",
  "od2.destination_city.code",
  "cabin",
  "ptc_adt",
  "ptc_cnn",
  "ptc_inf"]

export class FlightPtcAdtSelect extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <Select
            onChange={(value) => rc.handle('ptc_adt', value)}
            value={formValue.ptc_adt || 0}
            style={this.props.style}
            placeholder={this.props.placeholder}
            className={this.props.className}>
            <Select.Option value={1}>1</Select.Option>
            <Select.Option value={2}>2</Select.Option>
            <Select.Option value={3}>3</Select.Option>
          </Select>}
      </FormRcContext.Consumer>}
    </FormValueContext.Consumer>

  }
}

export class FlightPtcCnnSelect extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <Select
            onChange={(value) => rc.handle('ptc_cnn', value)}
            value={formValue.ptc_cnn || 0}
            style={this.props.style}
            placeholder={this.props.placeholder}
            className={this.props.className}>
            <Select.Option value={0}>0</Select.Option>
            <Select.Option value={1}>1</Select.Option>
            <Select.Option value={2}>2</Select.Option>
            <Select.Option value={3}>3</Select.Option>
          </Select>}
      </FormRcContext.Consumer>}
    </FormValueContext.Consumer>
  }
}

export class FlightPtcInfSelect extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <Select
            onChange={(value) => rc.handle('ptc_inf', value)}
            value={formValue.ptc_inf || 0}
            style={this.props.style}
            placeholder={this.props.placeholder}
            className={this.props.className}>
            <Select.Option value={0}>0</Select.Option>
            <Select.Option value={1}>1</Select.Option>
            <Select.Option value={2}>2</Select.Option>
            <Select.Option value={3}>3</Select.Option>
          </Select>}
      </FormRcContext.Consumer>}
    </FormValueContext.Consumer>
  }
}

export class FlightCabinSelect extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <Select
            onChange={(value) => rc.handle('cabin', value)}
            value={formValue.cabin}
            style={this.props.style}
            placeholder={this.props.placeholder}
            className={this.props.className}>
            <Select.Option value='Y'>Economy</Select.Option>
            <Select.Option value='S'>Premium Economy</Select.Option>
            <Select.Option value='C'>Business</Select.Option>
            <Select.Option value='F'>First</Select.Option>
          </Select>}
      </FormRcContext.Consumer>}
    </FormValueContext.Consumer>
  }
}

export class FlightOd2OriginDepartureDatePicker extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <DatePicker
            value={formValue['od2.origin_datetime'] == null || formValue['od2.origin_datetime'] === '' ? null : moment(formValue['od2.origin_datetime'])}
            defaultPickerValue={formValue['od1.origin_datetime'] == null ? null : moment(formValue['od1.origin_datetime']).add(1, 'days')}
            disabledDate={rc.disabledEndDate}
            style={this.props.style}
            placeholder={this.props.placeholder || "(one way)"}
            className={this.props.className}
            dropdownClassName="hide-year-buttons"
            format="YYYY-MM-DD"
            onChange={(value) => rc.handle('od2.origin_datetime', value)} />
      }</FormRcContext.Consumer>
    }</FormValueContext.Consumer>
  }
}

export class FlightOd1OriginDepartureDatePicker extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string, minLeadTime?: number}> {
  render() {

    const minLeadTime = this.props.minLeadTime == null ? 0 : this.props.minLeadTime

    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <DatePicker
            value={formValue['od1.origin_datetime'] == null || formValue['od1.origin_datetime'] === '' ? null : moment(formValue['od1.origin_datetime'])}
            disabledDate={date => rc.disabledStartDate(date.clone().subtract(minLeadTime, 'd'))}
            style={this.props.style}
            placeholder={this.props.placeholder}
            className={this.props.className}
            dropdownClassName="hide-year-buttons"
            format="YYYY-MM-DD"
            onChange={(value) => rc.handle('od1.origin_datetime', value)} />
      }</FormRcContext.Consumer>
    }</FormValueContext.Consumer>
  }
}
async function generateOptions(airportsOnly, params, client, requiredCityCodes = {}) {
  if (params.query != null && params.query.trim().length < 3) return null;
  const result = await client.iataAutoComplete(params)

  if (result.result == null) return null

  var options
  if (airportsOnly === true) {
    options = result.result.reduce((acc, city) => {
      return city.airports.reduce((acc2, airport) => {
        acc2.push(<Select.Option key={airport.code} value={airport.code}>{city.city_name}, {airport.name} ({airport.code})</Select.Option>)
        return acc2
      }, acc)
    }, [])
  } else {
    options = result.result.reduce((acc, city) => {
      if (city.airports.length === 1 && requiredCityCodes[city.city_code] == null) {
        acc.push(<Select.Option className="tc-icon-select" key={city.airports[0].code} value={city.airports[0].code}>
          <div className="tc-icon maticon-airplanemode_active" /><span>{city.city_name}, {city.airports[0].name} ({city.airports[0].code})</span></Select.Option>)
        return acc
      } else {
        acc.push(<Select.Option className="tc-icon-select" key={"C-" + city.city_code} value={"C-" + city.city_code}>
          <div className="tc-icon maticon-location_city" />{city.city_name}, {city.country_name} (Any)</Select.Option>)
        return city.airports.reduce((acc2, airport) => {
          acc2.push(<Select.Option className="tc-icon-select l2" key={airport.code} value={airport.code}>
            <div className="tc-icon maticon-airplanemode_active" />{airport.name} ({airport.code})</Select.Option>)
          return acc2
        }, acc)
      }
    }, [])
  }

  return options
}

export function CodeSelect(props: {style?: any, placeholder?: string, className?: string, field: string}) {
  const formValue = useContext(FormValueContext)
  const rc = useContext(FormRcContext)
  const state = useContext(FormState)
  const [stateOptions, setStateOptions] = useState(null);

  const handleSearch = async (rc, searchValue) => {
    const options = await generateOptions(rc.props.airportsOnly, {query: searchValue}, rc.props.client)
    setStateOptions(options)
  }

  const airportKey = props.field + '_airport.code'
  const cityKey = props.field + '_city.code'

  var value = formValue[airportKey]
  if (formValue[cityKey] != null) value = 'C-' + formValue[cityKey]

  return <Select
    showSearch
    onChange={(value) => rc.handleCode(airportKey, cityKey, value)}
    value={value}
    onSearch={(searchValue) => handleSearch(rc, searchValue)}
    filterOption={() => true}
    style={props.style}
    placeholder={props.placeholder}
    className={props.className}
    defaultActiveFirstOption={false}>
    {stateOptions || state.defaultOptions}
  </Select>
}

export function FlightOd1OriginCodeSelect(props: {style?: any, placeholder?: string, className?: string}) {
  return CodeSelect({...props, field: 'od1.origin'})
}

export function FlightOd1DestinationCodeSelect(props: {style?: any, placeholder?: string, className?: string}) {
  return CodeSelect({...props, field: 'od1.destination'})
}

export function FlightOd2OriginCodeSelect(props: {style?: any, placeholder?: string, className?: string}) {
  return CodeSelect({...props, field: 'od2.origin'})
}

export function FlightOd2DestinationCodeSelect(props: {style?: any, placeholder?: string, className?: string}) {
  return CodeSelect({...props, field: 'od2.destination'})
}

export class FlightsParamsForm extends React.PureComponent<{
  onChange: (FlightParams) => void,
  value: FlightsParams,
  client: TravelCloudClient,
  defaultAirportCodes?: string[],
  defaultCityCodes?: string[],
  airportsOnly?: boolean,
  loading?: boolean,
  minLeadTime?: number
}> {

  static Od1OriginCodeSelect = FlightOd1OriginCodeSelect
  static Od1DestinationCodeSelect = FlightOd1DestinationCodeSelect
  static Od2OriginCodeSelect = FlightOd2OriginCodeSelect
  static Od2DestinationCodeSelect = FlightOd2DestinationCodeSelect
  static Od1OriginDepartureDatePicker = FlightOd1OriginDepartureDatePicker
  static Od2OriginDepartureDatePicker = FlightOd2OriginDepartureDatePicker
  static PtcAdtSelect = FlightPtcAdtSelect
  static PtcCnnSelect = FlightPtcCnnSelect
  static PtcInfSelect = FlightPtcInfSelect

  state = {
    endOpen: false,
    defaultOptions: null,
    data1: [],
    data2: []
  }

  async componentDidMount() {
    const isValidCode = (x) => x != null && x.length === 3
    const defaultAirportClone = this.props.defaultAirportCodes == null ? [] : this.props.defaultAirportCodes.slice(0)
    const defaultCityClone = this.props.defaultCityCodes == null ? [] : this.props.defaultCityCodes.slice(0)
    if (isValidCode(this.props.value['od1.origin_airport.code'])) {
      defaultAirportClone.push(this.props.value['od1.origin_airport.code'])
    }
    if (isValidCode(this.props.value['od2.origin_airport.code'])) {
      defaultAirportClone.push(this.props.value['od2.origin_airport.code'])
    }
    if (isValidCode(this.props.value['od1.destination_airport.code'])) {
      defaultAirportClone.push(this.props.value['od1.destination_airport.code'])
    }
    if (isValidCode(this.props.value['od2.destination_airport.code'])) {
      defaultAirportClone.push(this.props.value['od2.destination_airport.code'])
    }
    const requiredCityCodes = {}
    if (isValidCode(this.props.value['od1.origin_city.code'])) {
      requiredCityCodes[this.props.value['od1.origin_city.code']] = true
      defaultCityClone.push(this.props.value['od1.origin_city.code'])
    }
    if (isValidCode(this.props.value['od2.origin_city.code'])) {
      requiredCityCodes[this.props.value['od2.origin_city.code']] = true
      defaultCityClone.push(this.props.value['od2.origin_city.code'])
    }
    if (isValidCode(this.props.value['od1.destination_city.code'])) {
      requiredCityCodes[this.props.value['od1.destination_city.code']] = true
      defaultCityClone.push(this.props.value['od1.destination_city.code'])
    }
    if (isValidCode(this.props.value['od2.destination_city.code'])) {
      requiredCityCodes[this.props.value['od2.destination_city.code']] = true
      defaultCityClone.push(this.props.value['od2.destination_city.code'])
    }

    const options = await generateOptions(this.props.airportsOnly, {airportCodes: defaultAirportClone.join(','), cityCodes: defaultCityClone}, this.props.client, requiredCityCodes)

    this.setState({
      defaultOptions: options
    })
  }

  handle(key, value) {
    // Select passes string onChange
    // Input passes event onChange
    var update
    if (value != null && value.target != null) value = value.target.value

    if (value.format != null) value = value.format('YYYY-MM-DD')
    update = {
      [key]: value
    }

    const allData = Object.assign({}, this.props.value, update)

    for (var i in allData){
      if (allData[i] == null) delete allData[i]
      else if (validKeys.indexOf(i) === -1) delete allData[i]
    }

    this.props.onChange(allData)
  }

  handleCode(airportKey, cityKey, value: string) {
    const allData = Object.assign({}, this.props.value)

    if (value.substr(0,2) === 'C-') {
      delete allData[airportKey]
      allData[cityKey] = value.substr(2)
    } else {
      delete allData[cityKey]
      allData[airportKey] = value
    }

    this.props.onChange(allData)
  }

  disabledStartDate = (od1_origin_departure) => {
    // not sure why od1_origin_departure might be null
    if (od1_origin_departure == null) return false
    return od1_origin_departure.isBefore();
  }
  disabledEndDate = (od2_origin_departure) => {
    if (od2_origin_departure == null) return false
    const od1_origin_departure = this.props.value['od1.origin_datetime'] == null ? moment() : moment(this.props.value['od1.origin_datetime']);
    if (!od2_origin_departure || !od1_origin_departure) {
      return od2_origin_departure && od2_origin_departure < moment().endOf('day');
    }
    return od2_origin_departure.isBefore(moment(od1_origin_departure).add(1, 'days'));
  }

  render() {
    const children = this.props.children || <Form layout="vertical" style={{marginTop: 16}}>
      <Form.Item label="From">
        <FlightOd1OriginCodeSelect />
      </Form.Item>
      <Form.Item label="To">
        <FlightOd2OriginCodeSelect />
      </Form.Item>
      <Form.Item label="Departure date">
        <FlightOd1OriginDepartureDatePicker minLeadTime={this.props.minLeadTime} style={{width: '100%'}} />
      </Form.Item>
      <Form.Item label="Return Date">
        <FlightOd2OriginDepartureDatePicker style={{width: '100%'}} />
      </Form.Item>
      <Form.Item label="Adults">
        <FlightPtcAdtSelect />
      </Form.Item>
      <Form.Item label="Children">
        <FlightPtcCnnSelect />
      </Form.Item>
      <Form.Item label="Infants">
        <FlightPtcInfSelect />
      </Form.Item>
    </Form>

    return (
      <FormValueContext.Provider value={this.props.value}><FormRcContext.Provider value={this}><FormState.Provider value={this.state}>
        {children}
      </FormState.Provider></FormRcContext.Provider></FormValueContext.Provider>
    )
  }
}

export class FlightsParamsAdvancedForm extends React.PureComponent<{
  onChange: (FlightParams) => void,
  onSearch: (FlightParams) => void,
  onTypeChange: (FlightParams) => void,
  value: FlightsParams,
  client: TravelCloudClient,
  defaultAirportCodes?: string[],
  defaultCityCodes?: string[],
  type: string,
  loading?: boolean,
  minLeadTime?: number
}> {

  render() {
    const rowStyle = {display: "flex", flexDirection: "row" as any, maxWidth: "1000px", margin: "20px 0"}
    return <FlightsParamsForm {...this.props}>
      <Radio.Group value={this.props.type} onChange={(e) => this.props.onTypeChange(e.target.value)}>
        <Radio.Button value="return">Return</Radio.Button>
        <Radio.Button value="one">One way</Radio.Button>
        <Radio.Button value="multi">Multi-city</Radio.Button>
      </Radio.Group>
      {this.props.type !== 'multi'
        ? <div style={rowStyle}>
            <div style={{flex: '2'}}>
              <div>From</div>
              <FlightsParamsForm.Od1OriginCodeSelect style={{width: '95%'}} />
            </div>
            <div style={{flex: '0'}}>
              <div>&nbsp;</div>
            </div>
            <div style={{flex: '2'}}>
              <div>To</div>
              <FlightsParamsForm.Od2OriginCodeSelect style={{width: '95%'}} />
            </div>
            <div style={{flex: '1'}}>
              <div>Depart</div>
              <FlightsParamsForm.Od1OriginDepartureDatePicker minLeadTime={this.props.minLeadTime} style={{width: '95%'}} />
            </div>
            {this.props.type === 'return' && <div style={{flex: '1'}}>
              <div>Return</div>
              <FlightsParamsForm.Od2OriginDepartureDatePicker style={{width: '95%'}} />
            </div>}
          </div>
        : <div>
          <div style={rowStyle}>
            <div style={{flex: '2'}}>
              <FlightsParamsForm.Od1OriginCodeSelect style={{width: '95%'}} />
            </div>
            <div style={{width: '30px'}}>→</div>
            <div style={{flex: '2'}}>
              <FlightsParamsForm.Od1DestinationCodeSelect style={{width: '95%'}} />
            </div>
            <div style={{flex: '1'}}>
              <FlightsParamsForm.Od1OriginDepartureDatePicker minLeadTime={this.props.minLeadTime} style={{width: '95%'}} />
            </div>
          </div>
          <div style={rowStyle}>
            <div style={{flex: '2'}}>
              <FlightsParamsForm.Od2OriginCodeSelect style={{width: '95%'}} />
            </div>
            <div style={{width: '30px'}}>→</div>
            <div style={{flex: '2'}}>
              <FlightsParamsForm.Od2DestinationCodeSelect style={{width: '95%'}} />
            </div>
            <div style={{flex: '1'}}>
              <FlightsParamsForm.Od2OriginDepartureDatePicker style={{width: '95%'}} />
            </div>
          </div>
        </div>
      }
      <div style={rowStyle}>
        <div style={{flex: '1'}}>
          <div>Adults</div>
          <FlightsParamsForm.PtcAdtSelect style={{width: '95%'}} />
        </div>
        <div style={{flex: '1'}}>
          <div>Children</div>
          <FlightsParamsForm.PtcCnnSelect style={{width: '95%'}} />
        </div>
        <div style={{flex: '1'}}>
          <div>Infants</div>
          <FlightsParamsForm.PtcInfSelect style={{width: '95%'}} />
        </div>
        <div style={{flex: '2'}}>
          <div>Cabin</div>
          <FlightCabinSelect style={{width: '95%'}} />
        </div>
        <div style={{flex: '2'}}>
          <div>&nbsp;</div>
          <Button type="primary" disabled={this.props.loading === true} onClick={() => this.props.onSearch(this.props.value)} style={{width: '95%'}}>Search</Button>
        </div>
      </div>
  </FlightsParamsForm>

  }
}