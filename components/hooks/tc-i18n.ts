import { useTranslation } from 'react-i18next'
import { useEffect } from 'react'
import { setCookie } from '../../travelcloud'

export default function useTCi18n () {

  const {t, i18n} = useTranslation()

  const changeLocale = ( _locale ) => {
    setCookie('__locale', _locale)
    return i18n.changeLanguage( _locale )
  }

  return { t, i18n, changeLocale }

}