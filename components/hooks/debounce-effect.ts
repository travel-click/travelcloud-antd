import React, {useEffect, useState, useRef} from "react";

export const useDebounceEffect = (callback, watchListArray, timeout) => {

  const timer = useRef(null)
  useEffect(() => {

    let defer = () => {
      timer.current = null 
      callback()
    }

    clearTimeout(timer.current)
    timer.current = setTimeout(defer, timeout)
  }, watchListArray)
}
