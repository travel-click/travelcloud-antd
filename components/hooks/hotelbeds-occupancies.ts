
const range = length => Array.from({ length }, (x, i) => i);

export const useHotelBedsOccupancy = () => {
  const updateOccupancies = (params, index, type, value) => {
    if (typeof params.occupancies[index] == "undefined") {
      params.occupancies[index] = {
        rooms: 1, // hardcoded for temporary
        [type]: value
      };
    } else {
      params.occupancies[index]["rooms"] = 1;
      params.occupancies[index][type] = value;
    }

    if (type == "children") {
      if (typeof params.occupancies[index]["paxes"] == "undefined") {
        params.occupancies[index]["paxes"] = [];
      }

      params.occupancies[index]["paxes"] = range(value).map(() => ({
        type: "CH",
        age: 12
      }));
    } else {
      if (params.occupancies[index]["children"] == null)
        params.occupancies[index]["children"] = 0;
    }

    return params.occupancies;
  };

  const updateOccupancyElementCount = (params, count) => {
    var diff = count - params.occupancies.length;

    if (diff > 0) {
      return params.occupancies.concat(range(diff).map(() => ({})));
    } else {
      return params.occupancies.slice(0, params.occupancies.length + diff);
    }
  };

  const updateChildrenPax = (params, occupancyIndex, paxIndex, age) => {
    if (
      typeof params.occupancies[occupancyIndex] != "undefined" &&
      typeof params.occupancies[occupancyIndex]["paxes"][paxIndex] !=
      "undefined"
    ) {
      params.occupancies[occupancyIndex]["paxes"][paxIndex].age = age;
    }

    return params.occupancies;
  };
  return { updateOccupancies, updateOccupancyElementCount, updateChildrenPax }
}