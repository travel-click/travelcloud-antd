import { useEffect, useMemo, useState } from 'react'
import { Cart, dateToIsoDate, computeProductBreakdown, range, ProductBreakdown } from '../../travelcloud'
import { v2_GroupTour, v2_PrivateTour } from '../../types'
import { computeAnyTour, newDateLocal, isAdjustmentEffective, TourType, computeBookableTimeLeft, computePriceRules, tcParseInt, AnyTourFormValue } from '../../order-product-compute'
import Big from 'big.js'

type AnyTourBookingController = {
  nextDeparture: string,
  lastDeparture: string,
  currentViewMonth: Date,
  nextCurrentViewMonth,
  prevCurrentViewMonth,
  dateInfoMap: { [date: string]: { capacity: number, booking: number, totalAdjustment, productBreakdown: ProductBreakdown, product } },
  departures: { isoDate, capacity, booking, totalAdjustment, productBreakdown: ProductBreakdown, product }[],
  yearMonthsWithDepartures: string[],

  departure_date: string,
  set_departure_date,

  show_num_of_rooms: boolean,
  num_of_rooms: number,
  set_num_of_rooms,

  adult: number,
  set_adult,

  child: number,
  set_child,

  adult2: number,
  set_adult2,

  child2: number,
  set_child2,

  adult3: number,
  set_adult3,

  child3: number,
  set_child3,

  adult4: number,
  set_adult4,

  child4: number,
  set_child4,

  adult5: number,
  set_adult5,

  child5: number,
  set_child5,

  child_no_bed: number,
  set_child_no_bed,

  extension: number,
  set_extension,

  addon_values: { [key: string]: number },
  set_addon_values,

  product,
  productBreakdown: ProductBreakdown,
  error,

  totalPax,
  hasChildDiscount: boolean,
  adultOptions: { num, price: string }[],
  childOptions: { num }[],
  //childOptions : {num, price: string}[],
  childNoBedOptions: { num, price: string }[],

  generateBookingForm
}

const pad = (x: number) => x < 10 ? '0' + x : x

export const parseAnyTour = (privateTour, tourType, form, cart): {
  adultOptions,
  childOptions,
  show_num_of_rooms,
  dateInfoMap,
  departures,
  yearMonthsWithDepartures,
  nextDeparture,
  lastDeparture,
  hasChildDiscount,
  childNoBedOptions
} | null => {

  if (privateTour == null) return null

  const max_pax = privateTour?.max_pax ?? 1
  const min_pax = privateTour?.min_pax ?? 1

  var adultOptions
  var childOptions

  if (max_pax === 1) {
    adultOptions = range(9).map(x => {
      const num = 1 + x
      var price = Big(privateTour?.['price_1'] ?? 0)
      price = price.times(num).toFixed(2)
      return { num, price }
    })
    childOptions = range(10).map(x => {
      const num = x
      var price = Big(privateTour?.['price_1'] ?? 0).minus(privateTour.price_child_discount ?? 0)
      price = price.times(num).toFixed(2)
      return { num, price }
    })
  } else {
    adultOptions = range(max_pax - min_pax + 1).map(x => {
      const num = min_pax + x
      const price = privateTour?.['price_' + num]
      return { num, price }
    })
    childOptions = range(max_pax - min_pax + 1).map(x => {
      const num = x
      return { num }
    })
  }

  const hasChildDiscount = privateTour?.price_child_age > 0 && privateTour?.price_child_discount != null && parseFloat(privateTour?.price_child_discount) > 0

  const maxChildNoBed = privateTour?.price_child_no_bed_max_pax ?? 0
  const childNoBedOptions = range(maxChildNoBed).map((x) => {
    const num = x + 1
    const price = Big(privateTour['price_child_no_bed_price']).mul(num).toFixed(2)
    return { num, price }
  })

  //@ts-ignore
  const capacitiesIndexed = privateTour?.capacities.reduce((acc, capacity) => {
    acc[capacity.year_month] = capacity
    return acc
  }, {})

  //@ts-ignore
  const bookingsIndexed = privateTour?.bookings.reduce((acc, capacity) => {
    acc[capacity.year_month] = capacity
    return acc
  }, {})

  // FIXME: Assume no "empty" capacities
  // FIXME: Ignores min_lead_time
  var nextDepartureData = null
  privateTour.capacities.find((capacity) => {
    return range(32).find((x) => {
      const isoDate = capacity.year_month.substr(0, 4) + '-' + capacity.year_month.substr(4, 2) + '-' + pad(x)
      const bookableTimeLeft = computeBookableTimeLeft(privateTour, isoDate)
      if (bookableTimeLeft < 10 * 60 * 1000) return false
      const result = parseInt(capacity['day_' + x]) > 0
      if (result) {
        nextDepartureData = isoDate
      }
      return result
    })
  })
  //privateTour?.capacities[0] == null ? null : range(32).find((x) => parseInt(privateTour.capacities[0]['day_' + x]) > 0)
  //var nextDepartureData = nextDepartureDataDay == null ? null : privateTour.capacities[0].year_month.substr(0, 4) + '-' + privateTour.capacities[0].year_month.substr(4, 2) + '-' + pad(nextDepartureDataDay)

  const lastDepartureDataDay = privateTour?.capacities[0] == null ? null : range(31).find((x) => parseInt(privateTour.capacities[privateTour.capacities.length - 1]['day_' + (31 - x)]) > 0)
  const lastDepartureData = lastDepartureDataDay == null ? null : privateTour.capacities[privateTour.capacities.length - 1].year_month.substr(0, 4) + '-' + privateTour.capacities[privateTour.capacities.length - 1].year_month.substr(4, 2) + '-' + pad(31 - lastDepartureDataDay)

  const dateInfoMap = {}
  const departures = []
  const departureYearMonthMap = {}

  var nextDeparture = null
  var lastDeparture = null    

  if (nextDepartureData != null && lastDepartureData != null) {
    const runningDate = newDateLocal(nextDepartureData);
    const endDate = newDateLocal(lastDepartureData);

    while (endDate >= runningDate) {
      const isoDate = dateToIsoDate(runningDate);

      const bookableTimeLeft = computeBookableTimeLeft(privateTour, isoDate)
      if (bookableTimeLeft < 10 * 60 * 1000) { //ensure there's at least 10 minutes left for booking to complete
        runningDate.setDate(runningDate.getDate() + 1);
        continue
      }

      const runningDateYearMonth = runningDate.getFullYear() + "" + pad(runningDate.getMonth() + 1)

      departureYearMonthMap[runningDateYearMonth] = true

      if (nextDeparture == null) {
        nextDeparture = isoDate
      }
      lastDeparture = isoDate

      const capacitydm = capacitiesIndexed[runningDateYearMonth]
      const capacity = capacitydm?.['day_' + runningDate.getDate()] ?? 0

      const bookingdm = bookingsIndexed[runningDateYearMonth]
      const booking = bookingdm?.['day_' + runningDate.getDate()] ?? 0

      var totalAdjustment = Big(0)

      form.departure_date = isoDate
      const product = cart?.addAnyTour(privateTour, form, tourType, true)
      const productBreakdown = computeProductBreakdown(product)
      const error = cart?.lastComputed?.error

      dateInfoMap[isoDate] = { capacity, booking, productBreakdown, totalAdjustment: totalAdjustment.toFixed(2), product, error }
      if (capacity > 0) departures.push({ isoDate, capacity, booking, productBreakdown, totalAdjustment: totalAdjustment.toFixed(2), product, error })
      runningDate.setDate(runningDate.getDate() + 1);
    }
  }

  const yearMonthsWithDepartures = Object.entries(departureYearMonthMap).map(x => x[0])

  const show_num_of_rooms = privateTour.grouping_unit != null && privateTour.grouping_unit !== '' && privateTour.max_pax !== 1

  return {
    adultOptions,
    childOptions,
    show_num_of_rooms,
    dateInfoMap,
    departures,
    yearMonthsWithDepartures,
    nextDeparture,
    lastDeparture,
    hasChildDiscount,
    childNoBedOptions
  }

}

// order is used to trigger react change detection
export const useAnyTourBooking = (privateTour: v2_PrivateTour | v2_GroupTour, tourType: TourType, cart: Cart, order): AnyTourBookingController => {
  //const [show_num_of_rooms, set_show_num_of_rooms] = useState(false)
  const [num_of_rooms, set_num_of_rooms] = useState(1)
  //const [adultOptions, set_adultOptions] = useState([])
  //const [childOptions, set_childOptions] = useState([])
  const [adult, set_adult_real] = useState(1)
  const [child, set_child_real] = useState(0)
  const [adult2, set_adult2_real] = useState(1)
  const [child2, set_child2_real] = useState(0)
  const [adult3, set_adult3_real] = useState(1)
  const [child3, set_child3_real] = useState(0)
  const [adult4, set_adult4_real] = useState(1)
  const [child4, set_child4_real] = useState(0)
  const [adult5, set_adult5_real] = useState(1)
  const [child5, set_child5_real] = useState(0)
  const [child_no_bed, set_child_no_bed] = useState(0)
  const [addon_values, set_addon_values] = useState({})
  const [extension, set_extension] = useState(0)
  const [departure_date, set_departure_date] = useState(null)
  const [currentViewMonth, setCurrentViewMonth] = useState(null)

  const set_adult = (val) => {
    if (val + child > privateTour.max_pax && privateTour.max_pax !== 1) {
      set_child_real(privateTour.max_pax - val)
    }
    if (val + child < privateTour.min_pax) {
      set_child_real(privateTour.min_pax - val)
    }
    set_adult_real(val)
  }

  const set_adult2 = (val) => {
    if (val + child2 > privateTour.max_pax && privateTour.max_pax !== 1) {
      set_child2_real(privateTour.max_pax - val)
    }
    if (val + child2 < privateTour.min_pax) {
      set_child2_real(privateTour.min_pax - val)
    }
    set_adult2_real(val)
  }

  const set_adult3 = (val) => {
    if (val + child3 > privateTour.max_pax && privateTour.max_pax !== 1) {
      set_child3_real(privateTour.max_pax - val)
    }
    if (val + child3 < privateTour.min_pax) {
      set_child3_real(privateTour.min_pax - val)
    }
    set_adult3_real(val)
  }

  const set_adult4 = (val) => {
    if (val + child4 > privateTour.max_pax && privateTour.max_pax !== 1) {
      set_child4_real(privateTour.max_pax - val)
    }
    if (val + child4 < privateTour.min_pax) {
      set_child4_real(privateTour.min_pax - val)
    }
    set_adult4_real(val)
  }

  const set_adult5 = (val) => {
    if (val + child5 > privateTour.max_pax && privateTour.max_pax !== 1) {
      set_child5_real(privateTour.max_pax - val)
    }
    if (val + child5 < privateTour.min_pax) {
      set_child5_real(privateTour.min_pax - val)
    }
    set_adult5_real(val)
  }

  const set_child = (val) => {
    if (val + adult > privateTour.max_pax && privateTour.max_pax !== 1) {
      set_adult_real(privateTour.max_pax - val)
    }
    if (val + adult < privateTour.min_pax) {
      set_adult_real(privateTour.min_pax - val)
    }
    set_child_real(val)
  }

  const set_child2 = (val) => {
    if (val + adult2 > privateTour.max_pax && privateTour.max_pax !== 1) {
      set_adult2_real(privateTour.max_pax - val)
    }
    if (val + adult2 < privateTour.min_pax) {
      set_adult2_real(privateTour.min_pax - val)
    }
    set_child2_real(val)
  }

  const set_child3 = (val) => {
    if (val + adult3 > privateTour.max_pax && privateTour.max_pax !== 1) {
      set_adult3_real(privateTour.max_pax - val)
    }
    if (val + adult3 < privateTour.min_pax) {
      set_adult3_real(privateTour.min_pax - val)
    }
    set_child3_real(val)
  }

  const set_child4 = (val) => {
    if (val + adult4 > privateTour.max_pax && privateTour.max_pax !== 1) {
      set_adult4_real(privateTour.max_pax - val)
    }
    if (val + adult4 < privateTour.min_pax) {
      set_adult4_real(privateTour.min_pax - val)
    }
    set_child4_real(val)
  }

  const set_child5 = (val) => {
    if (val + adult5 > privateTour.max_pax && privateTour.max_pax !== 1) {
      set_adult5_real(privateTour.max_pax - val)
    }
    if (val + adult5 < privateTour.min_pax) {
      set_adult5_real(privateTour.min_pax - val)
    }
    set_child5_real(val)
  }

  const generateBookingForm = (isoDate) => {
    var form: AnyTourFormValue = {
      adult, child, child_no_bed, tour_id: privateTour?.id, departure_date: isoDate, extension, addon_values
    }

    if (num_of_rooms >= 2) form = { ...form, adult2, child2 }
    if (num_of_rooms >= 3) form = { ...form, adult3, child3 }
    if (num_of_rooms >= 4) form = { ...form, adult4, child4 }
    if (num_of_rooms >= 5) form = { ...form, adult5, child5 }

    return form
  }

  useEffect(() => {
    if (privateTour == null) return
    
    const default_addon_values = {}
    if (privateTour?.addons != null) {
      for (var addon of privateTour.addons) {
        default_addon_values[addon.addon_code] = 0
      }
    }

    var defaultAdults = 1

    if (privateTour?.min_pax <= 2 && privateTour?.max_pax >= 2) {
      defaultAdults = 2
    }

    set_adult_real(defaultAdults)
    set_adult2_real(defaultAdults)
    set_adult3_real(defaultAdults)
    set_adult4_real(defaultAdults)
    set_adult5_real(defaultAdults)
    set_child_real(0)
    set_child2_real(0)
    set_child3_real(0)
    set_child4_real(0)
    set_child5_real(0)
    set_addon_values(default_addon_values)
  }, [privateTour])

    /*
    - if max_pax is 1
      - adult options 1 to 9, child options 0 to 9
      - show_num_of_rooms = false
      - price = quantity * price_1
    - if max_pax not 1, no unit
      - adult option min_pax to max_pax, child option 0 to (max_pax - min_pax)
      - show_num_of_rooms = false
      - price = price_x
    - if max_pax not 1, with unit
      - adult option min_pax to max_pax, child option 0 to (max_pax - min_pax)
      - show_num_of_rooms = true
      - price = price_x

  */

  //const calculation = useMemo(, [privateTour])

  const nextCurrentViewMonth = () => {
    //console.log(dateToIsoDate(currentViewMonth), dateToIsoDate(newDate))
    setCurrentViewMonth(newDateLocal(
      currentViewMonth.getFullYear(),
      currentViewMonth.getMonth() + 1,
      1
    ))
  }

  const prevCurrentViewMonth = () => {
    //console.log(dateToIsoDate(currentViewMonth), currentViewMonth.getMonth() - 1)
    setCurrentViewMonth(newDateLocal(
      currentViewMonth.getFullYear(),
      currentViewMonth.getMonth() - 1,
      1
    ))
  }

  const product = cart?.addAnyTour(privateTour, generateBookingForm(departure_date), tourType, true)
  const productBreakdown = computeProductBreakdown(product)
  const error = cart?.lastComputed?.error

  // for info only
  var totalDays = privateTour?.number_of_days
  if (totalDays == null) totalDays = 0
  totalDays += extension

  var totalPax = tcParseInt(adult) + tcParseInt(child)
  if (num_of_rooms >= 2) totalPax += tcParseInt(adult2) + tcParseInt(child2)
  if (num_of_rooms >= 3) totalPax += tcParseInt(adult3) + tcParseInt(child3)
  if (num_of_rooms >= 4) totalPax += tcParseInt(adult4) + tcParseInt(child4)
  if (num_of_rooms >= 5) totalPax += tcParseInt(adult5) + tcParseInt(child5)
  totalPax += tcParseInt(child_no_bed)

  const parsed = parseAnyTour(privateTour, tourType, generateBookingForm(departure_date), cart)

  if (currentViewMonth == null && parsed?.nextDeparture != null) setCurrentViewMonth(newDateLocal(parsed.nextDeparture))

  return {
    adultOptions: parsed?.adultOptions,
    childOptions: parsed?.childOptions,
    show_num_of_rooms: parsed?.show_num_of_rooms,
    dateInfoMap: parsed?.dateInfoMap,
    departures: parsed?.departures,
    yearMonthsWithDepartures: parsed?.yearMonthsWithDepartures,
    nextDeparture: parsed?.nextDeparture,
    lastDeparture: parsed?.lastDeparture,
    hasChildDiscount: parsed?.hasChildDiscount,
    childNoBedOptions: parsed?.childNoBedOptions,

    currentViewMonth, nextCurrentViewMonth, prevCurrentViewMonth, departure_date, set_departure_date,
    num_of_rooms, set_num_of_rooms,
    adult, set_adult, child, set_child,
    adult2, set_adult2, child2, set_child2,
    adult3, set_adult3, child3, set_child3,
    adult4, set_adult4, child4, set_child4,
    adult5, set_adult5, child5, set_child5,
    child_no_bed, set_child_no_bed, extension, set_extension, addon_values, set_addon_values,
    product, productBreakdown, generateBookingForm, error, totalPax    
  }
}