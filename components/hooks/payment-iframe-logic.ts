import { useEffect, useState } from "react";
import { TravelCloudClient } from "../../travelcloud";

export function usePayload(
  ref: string,
  target: string,
  client: TravelCloudClient,
  paymentProcessor: string,
  amount: string, 
  successPage: string, 
  failurePage: string
): string[] {

  const [errorResp, setErrorResp] = useState(null)

  useEffect(() => {
    if (paymentProcessor == null || amount == null || successPage == null || failurePage == null ) {
      // todo: show some error or fail ?
    } else {
      const prefix = location.protocol + "//" + location.host;
      const successUrl = successPage.replace(/^\/+/g, "");
      const failureUrl = failurePage.replace(/^\/+/g, "");

      const payload = {
        order_ref: ref,
        cancel: prefix + location.pathname + location.search,
        success: prefix + "/" + successUrl,
        failure: prefix + "/" + failureUrl,
        payment_processor: paymentProcessor,
        amount: amount
      };

      client.post("rpc/payment_redirect", null, payload).then(post => {
        if (post.error != null) {
          // fixme: to create another order on timeout
          setErrorResp(post)
        } else {
          const f = document.createElement("form");
          f.action = post.result.action;
          f.target = target;
          if (post.result.form != null) {
            f.method = "POST";
            for (let input of post.result.form) {
              const i = document.createElement("input");
              i.type = "hidden";
              i.name = input.name;
              i.value = input.value;
              f.appendChild(i);
            }
          }
          document.body.appendChild(f);
          f.submit();
        }
      });
    }
  }, [ref]);

  return [ errorResp ];
}
