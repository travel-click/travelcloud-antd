import { useState, useEffect } from "react";
import {
  Cart,
  TcResponse,
  TravelCloudClient,
  mergeFormState
} from "../../travelcloud";
import { Order } from "../../types";
import Big from 'big.js';

export const computeAttractionView = (item, cart) => {
  item.ticketTypes = item.ticketTypes.map((ticketType)=> {
    if (cart == null) {
      ticketType._view = {
        beforeDiscounts: Big(ticketType.travelcloud_price),
        afterDiscounts: Big(ticketType.travelcloud_price)
      }
      return ticketType}

    const product = cart.addGlobaltix(item, {
        id: ticketType.id,
        fromResellerId: ticketType.fromResellerId,
        quantity: 1
    }, true);

    if (product == null) {
      ticketType._view = {
        beforeDiscounts: Big(ticketType.travelcloud_price),
        afterDiscounts: Big(ticketType.travelcloud_price)
      }
      return ticketType}

    const beforeDiscounts = product.items
      .filter((item) => parseInt(item['price']) > 0)
      .reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))

    const afterDiscounts = product.items
      .reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))

    ticketType._view = {beforeDiscounts, afterDiscounts}

    return ticketType
  })

  return item
}

export function useGlobaltixBookingForm(
  client: TravelCloudClient
): { searchParams, attractionList, ticketTypeGet, fetchTicketTypeGet, clearTicketTypeGet, updateSearchParamsAndSearch } {

  const [ticketTypeGet, setTicketTypeGet] = useState<any>()
  const [attractionList, setAttractionList] = useState<any>()
  const [searchParams, setSearchParams] = useState<any>({
    keyword: "",
    countryId: "",
    tab: "ALL",
    cityId: "",
    product_type: "",
    field: 'description',
    page: 1
  })

  const fetchTicketTypeGet = async (id) => {
    setTicketTypeGet({loading: true})
    const ticketTypeGet = await client.globaltix('ticketType/get', { id })
    setTicketTypeGet(ticketTypeGet)
    //setFormState(null)
  }

  const clearTicketTypeGet = async (id) => {
    setTicketTypeGet(null)
    //setFormState(null)
  }

  const updateSearchParamsAndSearch = async (val = null, key = null) => {
    if (val != null && key != null) {
      searchParams[key] = val;
      if (key == 'countryId') {
        searchParams['cityId'] = "";
      }

      if (key != 'page') {
        searchParams.page = 1;
      }
    }

    if (searchParams.countryId == '') {
      searchParams.countryId = '1';
    }

    setSearchParams(Object.assign({}, searchParams))

    setAttractionList({loading: true})
    const attractionList = await client.globaltix('attraction/list', searchParams)
    setAttractionList(attractionList)
  }

  return {searchParams, attractionList, ticketTypeGet, fetchTicketTypeGet, clearTicketTypeGet, updateSearchParamsAndSearch}
}