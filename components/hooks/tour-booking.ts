import React, { useState } from "react";
import {
  dateToIsoDate,
  mapTourOptionDepartures,
  TravelCloudClient,
  Cart,
  TcResponse
} from "../../travelcloud";
import { SelectedOptionDayInfo, TourBookingFormValue, Room } from "../tour-booking";
import Big from "big.js";
import { computeTour, newDateLocal } from "../../order-product-compute";
import { computePrice } from "../flights-result";
import { Tour, FlightDetail } from "../../types";


interface Kvp {
  [key: string]: any;
} 

export type TourBookingFormController = {
  tour: Tour,
  client: TravelCloudClient,
  cart: Cart,
  status: { allowRooms: boolean, allowChildBed: boolean, allowChildNoBed: boolean, allowInfant: boolean },
  updateBookingForm: (Kvp) => void,
  updateOptionId: (string) => void,
  updateDepartureDateAndFlightResult: (string) => Promise<void>,
  getBookingForm: () => TourBookingFormValue,
  adjustNumberOfRooms: (x: number) => void,
  adjustRoom: (key: number, type: string, change: number) => void,
  nextCurrentViewMonth: () => void,
  prevCurrentViewMonth: () => void
}

export type TourBookingFormState = {
  tour_id: string,
  option_id: string,
  departure_date: string,
  selectedOption: Tour['options'][0],
  selectedOptionDateInfoMap: {[key: string]: SelectedOptionDayInfo},
  tourComputed: TourComputed,
  flightComputed: {beforeDiscount: Big, afterDiscount: Big},
  rooms: Room[],
  currentViewMonth: Date,
  flightResult: TcResponse<FlightDetail>,
  loading: boolean,
  tourPlusFlightPrice: Big
}

export interface TourComputed {
  tourFormHasAddons: boolean,
  allowRooms: boolean,
  selectedOption: any,
  selectedOptionDateInfoMap: {[key: string]: SelectedOptionDayInfo},
  tourOptionsWithCheapestComputed,
  totalPrice,
  totalDeposit,
  invoiceEntries,
  tourAddonsComputed
}

export function useTourBookingForm(
  tour,
  cart,
  client,
  options: {source: string}
): { tourBookingFormController: TourBookingFormController; tourBookingFormState:TourBookingFormState } {
  const {source} = options;
  const maxChildrenNoBed = parseInt(tour["max_children_no_bed"]);
  const maxInfants = parseInt(tour["max_infants"]);
  const allowRooms =
    tour.price_type === "ALL" || tour.price_type.indexOf("SGL") !== -1;
  const allowChildBed =
    tour.price_type === "ALL" || tour.price_type.indexOf("CWB") !== -1;
  const allowChildNoBed =
    tour.price_type === "ALL" ||
    (allowRooms &&
      allowChildBed &&
      tour.price_type.indexOf("CNB") !== -1 &&
      maxChildrenNoBed > 0);
  const allowInfant =
    allowRooms &&
    maxInfants > 0 &&
    (tour.price_type === "ALL" || tour.price_type.indexOf("INF") !== -1);

  // we need bookingFormState to be mutable so that multiple successive set form state can work
  var [bookingFormState, setBookingFormState] = useState({
    tour_id: tour.id,
    option_id: null,
    departure_date: null,
    selectedOption: null,
    selectedOptionDateInfoMap: null,
    tourComputed: computeContext(tour, cart, {}),
    flightComputed: null,
    rooms: [{ adult: 2, child_with_bed: 0, child_no_bed: 0, infant: 0 }],
    currentViewMonth: null,
    flightResult: null,
    loading: false,
    tourPlusFlightPrice: null
  });

  const updateBookingForm = (updates) => {
    if (updates.loading !== false && bookingFormState.loading) return

    bookingFormState = Object.assign({}, bookingFormState, updates)

    bookingFormState.tourComputed = computeContext(tour, cart, bookingFormState);
    setBookingFormState(bookingFormState);
  };

  const bookingFormController = {
    tour,
    client,
    cart,
    status: { allowRooms, allowChildBed, allowChildNoBed, allowInfant },
    updateBookingForm,
    updateOptionId: option_id => {
      const selectedOption = tour.options.find(option => option.id == option_id);

      const selectedOptionDateInfoMap: {
        [key: string]: SelectedOptionDayInfo;
      } = {};

      const updates = {
        option_id: option_id,
        departure_date : null,
        selectedOption : selectedOption,
        selectedOptionDateInfoMap : selectedOptionDateInfoMap,
        currentViewMonth : newDateLocal(
          selectedOption["_next_departure"]
        )
      }

      const priceComputationRooms = [
        {
          adult: 2
        }
      ];

      const priceComputationDivisor = priceComputationRooms.reduce(
        (acc, room) =>
          Object.entries(room).reduce(
            (acc2, kvp) => acc2 + parseInt("" + kvp[1]),
            acc
          ),
        0
      );

      if (selectedOption != null && cart != null && tour != null) {
        const runningDate = newDateLocal(selectedOption["_next_departure"]);
        const endDate = newDateLocal(selectedOption["_last_departure"]);
        const departure_datesIndexed = selectedOption.departures.reduce(
          (acc, departure) => {
            acc[departure.date] = departure;
            return acc;
          },
          {}
        );

        while (endDate >= runningDate) {
          const isoDate = dateToIsoDate(runningDate);
          const departure = departure_datesIndexed[isoDate];

          // no departure or departure full
          const disabled =
            selectedOption.on_demand_advance_booking === "0"
              ? departure == null ||
                parseInt(departure.slots_taken) >=
                  parseInt(departure.slots_total)
              : isoDate < selectedOption["_next_departure"] ||
                isoDate > selectedOption["_last_departure"];

          const noDeparture =
            (selectedOption.on_demand_advance_booking === "0" &&
              departure == null) ||
            (selectedOption.on_demand_advance_booking !== "0" && disabled);

          const product = cart.addTour(
            tour,
            {
              tour_id: tour.id,
              option_id: selectedOption.id,
              departure_date: isoDate,
              rooms: priceComputationRooms
            },
            true
          );

          const beforeDiscounts = product.items
            .filter(item => parseInt(item["price"]) > 0)
            .reduce(
              (acc, item) => acc.add(Big(item.price).times(item.quantity)),
              Big(0)
            )
            .div(priceComputationDivisor);

          const afterDiscounts = product.items
            .reduce(
              (acc, item) => acc.add(Big(item.price).times(item.quantity)),
              Big(0)
            )
            .div(priceComputationDivisor);

          const slotsRemaining =
            selectedOption.on_demand_advance_booking === "0" &&
            departure != null
              ? departure.slots_total - departure.slots_taken
              : null;

          selectedOptionDateInfoMap[isoDate] = {
            noDeparture,
            disabled,
            date: newDateLocal(runningDate.getTime()),
            afterDiscounts,
            beforeDiscounts,
            slotsRemaining,
            product
          };

          runningDate.setDate(runningDate.getDate() + 1);
        }
      };

      updateBookingForm(updates)
    },
    updateDepartureDateAndFlightResult: async departure_date => {
      if (bookingFormState.loading) return
      const client: TravelCloudClient = bookingFormController.client;
    
      var weekday = new Array(7);
      weekday[0] = "sun";
      weekday[1] = "mon";
      weekday[2] = "tue";
      weekday[3] = "wed";
      weekday[4] = "thu";
      weekday[5] = "fri";
      weekday[6] = "sat";
      
      var weekdayStr = weekday[(newDateLocal(departure_date)).getDay()];
      var od1 = bookingFormController.tour.attributes.od1
      var od2 = bookingFormController.tour.attributes.od2
      if (bookingFormController.tour.attributes['od1-' + weekdayStr] != null) od1 = bookingFormController.tour.attributes['od1-' + weekdayStr]
      if (bookingFormController.tour.attributes['od2-' + weekdayStr] != null) od2 = bookingFormController.tour.attributes['od2-' + weekdayStr]

      if (od1 == null && od2 == null) {
        bookingFormController.updateBookingForm({"departure_date":departure_date, "flightResult":null});
        return}
      else bookingFormController.updateBookingForm({"departure_date":departure_date, "flightResult":null, "loading":true});

      const flightParam: any = offsetFlightItinerary(od1, od2, departure_date)
      flightParam['source'] = source
      flightParam['ptc_adt'] = bookingFormState.rooms.reduce((acc, room) => room.adult + acc, 0)
      flightParam['ptc_cnn'] = bookingFormState.rooms.reduce((acc, room) => room.child_no_bed + room.child_no_bed + acc, 0)
      flightParam['ptc_inf'] = bookingFormState.rooms.reduce((acc, room) => room.infant + acc, 0)

      const flightResult = await client.flight(flightParam);
      var tourPlusFlightPrice = null
      var flightComputed = null
      if (flightResult.result != null) {
        flightComputed = computePrice( cart, flightResult.result, flightResult.result.pricings[0].id, (x) => x )
        tourPlusFlightPrice = Big(bookingFormState.tourComputed.totalPrice).plus(flightComputed.afterDiscount)
      }
      
      bookingFormController.updateBookingForm({
        "loading":false,
        flightResult,
        flightComputed,
        tourPlusFlightPrice});
    },
    getBookingForm: () => {
      return {
        tour_id: bookingFormState.tour_id,
        rooms: bookingFormState.rooms,
        option_id: bookingFormState.option_id,
        departure_date: bookingFormState.departure_date
      };
    },
    adjustNumberOfRooms: (x: number) => {
      if (x === -1 && bookingFormState.rooms.length > 1) {
        updateBookingForm({
          "rooms":
          bookingFormState.rooms.slice(0, bookingFormState.rooms.length - 1)
        });
      } else if (x === 1 && bookingFormState.rooms.length < 3) {
        bookingFormState.rooms.push({
          adult: 2,
          child_with_bed: 0,
          child_no_bed: 0,
          infant: 0
        });
        updateBookingForm({"rooms": bookingFormState.rooms});
      }
    },
    adjustRoom: (key, type, change) => {
      var room = Object.assign({}, bookingFormState.rooms[key]);
      //FIXME
      const isFull =
        bookingFormState.rooms.reduce(
          (acc, room) =>
            acc +
            room.adult +
            (room.child_no_bed || 0) +
            (room.child_with_bed || 0),
          0
        ) >= 9;
      const maxBeds = parseInt(tour["max_beds"]);
      const maxAdults = parseInt(tour["max_adults"]);
      const maxChildrenNoBed = parseInt(tour["max_children_no_bed"]);
      const maxInfants = parseInt(tour["max_infants"]);

      const adult = room.adult;
      const childBed = room.child_with_bed;
      const childNoBed = room.child_no_bed;
      const infant = room.infant;

      const canAddAdult =
        isFull === false &&
        (allowRooms === false ||
          (adult < maxAdults && adult + childBed < maxBeds));
      const canAddChildBed =
        isFull === false &&
        (allowRooms === false || adult + childBed < maxBeds);
      const canAddChildNoBed =
        isFull === false &&
        childNoBed < maxChildrenNoBed &&
        adult + childBed >= 2;
      const canAddInfant = infant < maxInfants;
      const canSubtractChildBed =
        childBed > 0 && (childBed > 1 || childNoBed === 0);

      if (type === "adult" && change === 1 && canAddAdult) {
        room.adult++;
        bookingFormState.rooms[key] = room;
        updateBookingForm({"departure_date": null, "flightResult": null});
      }

      if (type === "adult" && change === -1 && room.adult > 1) {
        room.adult--;
        bookingFormState.rooms[key] = room;
        updateBookingForm({"departure_date": null, "flightResult": null});
      }

      if (type === "child_with_bed" && change === 1 && canAddChildBed) {
        room.child_with_bed++;
        bookingFormState.rooms[key] = room;
        updateBookingForm({"departure_date": null, "flightResult": null});
      }

      if (type === "child_with_bed" && change === -1 && canSubtractChildBed) {
        room.child_with_bed--;
        bookingFormState.rooms[key] = room;
        updateBookingForm({"departure_date": null, "flightResult": null});
      }

      if (type === "child_no_bed" && change === 1 && canAddChildNoBed) {
        room.child_no_bed++;
        bookingFormState.rooms[key] = room;
        updateBookingForm({"departure_date": null, "flightResult": null});
      }

      if (type === "child_no_bed" && change === -1 && room.child_no_bed > 0) {
        room.child_no_bed--;
        bookingFormState.rooms[key] = room;
        updateBookingForm({"departure_date": null, "flightResult": null});
      }

      if (type === "infant" && change === 1 && canAddInfant) {
        room.infant++;
        bookingFormState.rooms[key] = room;
        updateBookingForm({"departure_date": null, "flightResult": null});
      }

      if (type === "infant" && change === -1 && room.infant > 0) {
        room.infant--;
        bookingFormState.rooms[key] = room;
        updateBookingForm({"departure_date": null, "flightResult": null});
      }
    },
    nextCurrentViewMonth: () => updateBookingForm({
      currentViewMonth: newDateLocal(
        bookingFormState.currentViewMonth.getFullYear(),
        bookingFormState.currentViewMonth.getMonth() + 1,
        1
      )
    }),
    prevCurrentViewMonth: () => updateBookingForm({
      currentViewMonth: newDateLocal(
        bookingFormState.currentViewMonth.getFullYear(),
        bookingFormState.currentViewMonth.getMonth() - 1,
        1
      )
    })
  };

  if (tour.options.length === 1 && bookingFormState.option_id == null) bookingFormController.updateOptionId(tour.options[0].id)

  return { tourBookingFormController: bookingFormController, tourBookingFormState: bookingFormState };
}

function computeContext(tour, cart, value): TourComputed {
  const priceComputationRooms = [
    {
      adult: 2
    }
  ];

  if (tour != null && value.tour_id !== tour.id) value.tour_id = tour.id;

  var selectedOption = null;
  var selectedDeparture = null;

  const option_id = value.option_id;
  const departure_date = value.departure_date;

  if (option_id !== "") {
    for (var i in tour.options) {
      if (tour.options[i].id === option_id) selectedOption = tour.options[i];
    }
  }

  if (selectedOption != null && departure_date !== "") {
    for (var i in selectedOption.departures) {
      if (selectedOption.departures[i].id === departure_date)
        selectedDeparture = selectedOption.departures[i];
    }
  }

  const priceComputationDivisor = priceComputationRooms.reduce(
    (acc, room) =>
      Object.entries(room).reduce(
        (acc2, kvp) => acc2 + parseInt("" + kvp[1]),
        acc
      ),
    0
  );

  var product = null;
  try {
    product =
      cart == null
        ? computeTour(tour, value).product
        : cart.addTour(tour, value, true);
  } catch (e) {
    return null;
  }

  const totalPrice =
    product == null
      ? null
      : product.items.reduce(
          (acc, item) => acc.add(Big(item.price).times(item.quantity)),
          Big(0)
        );

  const totalDeposit =
    product == null
      ? null
      : product.items.reduce(
          (acc, item) => acc.add(Big(item.deposit).times(item.quantity)),
          Big(0)
        );

  const invoiceItemsGrouped =
    product == null
      ? {}
      : product.items.reduce((acc, line) => {
          if (acc[line.sub_header] == null) acc[line.sub_header] = [];
          acc[line.sub_header].push(line);
          return acc;
        }, {});

  // not sure how to enable Object.entries in babel
  const invoiceEntries = [];
  for (var i in invoiceItemsGrouped) {
    invoiceEntries.push([i, invoiceItemsGrouped[i]]);
  }

  const allowRooms =
    tour.price_type === "ALL" || tour.price_type.indexOf("SGL") !== -1;

  var tourOptionsWithCheapestComputed;
  var tourAddonsComputed = [];

  if (cart != null && tour != null) {
    tourOptionsWithCheapestComputed = tour.options.map(option => {
      option = Object.assign({}, option);
      option["_cheapest_computed"] = mapTourOptionDepartures(
        option,
        (departure, departureDate) => {
          //compute price for each departure

          if (
            departure != null &&
            departure.slots_taken >= departure.slots_total
          )
            return null;

          const product = cart.addTour(
            tour,
            {
              tour_id: tour.id,
              option_id: option.id,
              departure_date: departureDate,
              rooms: priceComputationRooms
            },
            true
          );

          if (product == null) return null;
          const beforeDiscounts = product.items
            .filter(item => parseInt(item["price"]) > 0)
            .reduce(
              (acc, item) => acc.add(Big(item.price).times(item.quantity)),
              Big(0)
            )
            .div(priceComputationDivisor);
          const afterDiscounts = product.items
            .reduce(
              (acc, item) => acc.add(Big(item.price).times(item.quantity)),
              Big(0)
            )
            .div(priceComputationDivisor);
          const afterDiscountsDeposit = product.items
            .reduce(
              (acc, item) => acc.add(Big(item.price).times(item.quantity)),
              Big(0)
            )
            .div(priceComputationDivisor);
          return {
            beforeDiscounts,
            afterDiscounts,
            afterDiscountsDeposit,
            product,
            departureDate
          };
        }
      )
        // filter out invalid departures
        .filter(val => val != null)

        // find the cheapest
        .reduce((acc, computed) => {
          if (acc == null || computed.afterDiscounts.lt(acc.afterDiscounts)) {
            return computed;
          }
          return acc;
        }, null);

      return option;
    });

    if (value.tour_addons != null) {
      tourAddonsComputed = value.tour_addons.map(tour_addon_value => {
        const tour_addon = tour.tour_addons.find(
          x => x.id === tour_addon_value.id
        );
        const original_option_id = tour_addon_value.option_id;
        const options_computed = [];

        tour_addon_value.option_id = "0";
        const product = cart.addTour(tour, value, true);

        if (product == null) return {};

        const price = product.items.reduce(
          (acc, item) => acc.add(Big(item.price).times(item.quantity)),
          Big(0)
        );

        const price_diff = price.minus(totalPrice);

        for (const option of tour_addon.options) {
          tour_addon_value.option_id = option.id;

          const product = cart.addTour(tour, value, true);

          if (product == null) continue;

          const price = product.items.reduce(
            (acc, item) => acc.add(Big(item.price).times(item.quantity)),
            Big(0)
          );

          const price_diff = price.minus(totalPrice);

          options_computed.push({
            option,
            price: price.toFixed(2),
            price_diff: price_diff.toFixed(2),
            original_option_id
          });
        }

        tour_addon_value.option_id = original_option_id;

        return {
          tour_addon,
          tour_addon_value,
          price: price.toFixed(2),
          price_diff: price_diff.toFixed(2),
          options_computed
        };
      });
    }
  }

  const selectedOptionDateInfoMap: {
    [key: string]: SelectedOptionDayInfo;
  } = {};

  if (selectedOption != null && cart != null && tour != null) {
    const runningDate = newDateLocal(selectedOption["_next_departure"]);
    const endDate = newDateLocal(selectedOption["_last_departure"]);
    const departure_datesIndexed = selectedOption.departures.reduce(
      (acc, departure) => {
        acc[departure.date] = departure;
        return acc;
      },
      {}
    );

    while (endDate >= runningDate) {
      const isoDate = dateToIsoDate(runningDate);
      const departure = departure_datesIndexed[isoDate];

      // no departure or departure full
      const disabled =
        selectedOption.on_demand_advance_booking === "0"
          ? departure == null ||
            parseInt(departure.slots_taken) >= parseInt(departure.slots_total)
          : isoDate < selectedOption["_next_departure"] ||
            isoDate > selectedOption["_last_departure"];

      const noDeparture =
        (selectedOption.on_demand_advance_booking === "0" &&
          departure == null) ||
        (selectedOption.on_demand_advance_booking !== "0" && disabled);

      const product = cart.addTour(
        tour,
        {
          tour_id: tour.id,
          option_id: selectedOption.id,
          departure_date: isoDate,
          rooms: priceComputationRooms
        },
        true
      );

      const beforeDiscounts = product.items
        .filter(item => parseInt(item["price"]) > 0)
        .reduce(
          (acc, item) => acc.add(Big(item.price).times(item.quantity)),
          Big(0)
        )
        .div(priceComputationDivisor);

      const afterDiscounts = product.items
        .reduce(
          (acc, item) => acc.add(Big(item.price).times(item.quantity)),
          Big(0)
        )
        .div(priceComputationDivisor);

      const slotsRemaining =
        selectedOption.on_demand_advance_booking === "0" && departure != null
          ? departure.slots_total - departure.slots_taken
          : null;

      selectedOptionDateInfoMap[isoDate] = {
        noDeparture,
        disabled,
        date: newDateLocal(runningDate.getTime()),
        afterDiscounts,
        beforeDiscounts,
        slotsRemaining,
        product
      };

      runningDate.setDate(runningDate.getDate() + 1);
    }
  }

  var tourFormHasAddons =
    (value.generic_addons != null && value.generic_addons.length > 0) ||
    (value.tour_addons != null && value.tour_addons.length > 0);

  //tour.tour_addons.reduce((acc, tour_addon) => acc || tour_addon.options.length > 0, false) || tour.generic_addons.length > 0

  return {
    tourOptionsWithCheapestComputed,
    selectedOptionDateInfoMap,
    allowRooms,
    selectedOption,
    totalPrice,
    totalDeposit,
    invoiceEntries,
    tourFormHasAddons,
    tourAddonsComputed
  };
}

export function dateDiffInDays(stringA: string, stringB: string) {
  // Discard the time and time-zone information.
  const a = newDateLocal(stringA);
  const b = newDateLocal(stringB);
  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / (1000 * 60 * 60 * 24));
}

export function offsetFlightItinerary(
  od1: string,
  od2: string,
  departure_date: string
): { "od1.id": string; "od2.id": string } {
  const odsplit = od1.split("/");
  const flightArrivalDate = odsplit[odsplit.length - 1].substr(25, 10);
  const dateDiff = dateDiffInDays(flightArrivalDate, departure_date);

  const doOffset = (str1: string) => {
    const newDate1 = newDateLocal(str1.substr(0, 10));
    const newDate2 = newDateLocal(str1.substr(25, 10));
    newDate1.setDate(newDate1.getDate() + dateDiff);
    newDate2.setDate(newDate2.getDate() + dateDiff);
    const formatDate1 = newDate1.toISOString().substr(0, 10);
    const formatDate2 = newDate2.toISOString().substr(0, 10);
    return (
      formatDate1 + str1.substring(10, 25) + formatDate2 + str1.substring(35)
    );
  };

  const newOd1 = odsplit.map(doOffset).join("/");
  const newOd2 = od2
    .split("/")
    .map(doOffset)
    .join("/");

  return { "od1.id": newOd1, "od2.id": newOd2 };
}
