import { useState } from "react";
import { Cart, TcResponse, TravelCloudClient } from "../../travelcloud";

export type OutboundBookingController = {
  roomList, setRoomList,
  priceCheckRes, setPriceCheckRes,
  tourMaster, setTourMaster,
  addRoom, minusRoom,
  adultOnChange, childWithBedOnChange, childWithoutBedOnChange, infantOnChange,
  addToCart
}

export const useOutboundBooking = (client: TravelCloudClient, cart: Cart, defaultRoom, obPriceRules, obDepositRules): OutboundBookingController => {
  // landOnly not supported yet
  // const [landOnly, setLandOnly] = useState('N');
  const landOnly = 'N'
  const [roomList, setRoomList] = useState([{ ...Object.assign({}, defaultRoom, {roomNo: 1}) }]);
  const [priceCheckRes, setPriceCheckRes] = useState<TcResponse<any>>({})
  const [tourMaster, setTourMaster] = useState(null)

  const addToCart = async () => {
    setPriceCheckRes({loading: true})

    const priceCheckRes = await client.outboundPost('tourbookings/pricecheck', {}, {
      tourCode: tourMaster.tourCode,
      landOnly,
      roomList
    });
    setPriceCheckRes(priceCheckRes)
    cart.reset().addOutboundTour({tourMaster, priceCheck: priceCheckRes.result, price_rules: obPriceRules.result, depositRules: obDepositRules.result}, {
      tourCode: tourMaster.tourCode,
      landOnly,
      roomList
    })
  }

  const addRoom = () => {
    if (roomList.length >= 3) return;
    const newRoom = { ...defaultRoom };
    newRoom.roomNo = roomList.length + 1;
    roomList.push(newRoom);
    setRoomList([...roomList]);
  }

  const minusRoom = () => {
    if (roomList.length <= 1) return;
    roomList.pop();
    setRoomList([...roomList]);
  }

  const adultOnChange = (index) => (value) => {
    const room = roomList[index]
    room.adults = value;
    roomList[index] = { ...room };
    setRoomList([...roomList]);
  }

  const childWithBedOnChange = (index) => (value) => {
    const room = roomList[index]
    room.children = value;
    roomList[index] = { ...room };
    setRoomList([...roomList]);
  }

  const childWithoutBedOnChange = (index) => (value) => {
    const room = roomList[index]
    room.childrenNoBed = value;
    roomList[index] = { ...room };
    setRoomList([...roomList]);
  }
  const infantOnChange = (index) => (value) => {
    const room = roomList[index]
    room.infants = value;
    roomList[index] = { ...room };
    setRoomList([...roomList]);
  }

  return {
    roomList, setRoomList,
    priceCheckRes, setPriceCheckRes,
    tourMaster, setTourMaster,
    addRoom, minusRoom,
    adultOnChange, childWithBedOnChange, childWithoutBedOnChange, infantOnChange,
    addToCart
  }

}