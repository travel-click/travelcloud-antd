import {useState} from 'react';
import {encodeFilters} from '../flights/component-logic';
import Big from 'big.js'
import { sortBy } from '../../travelcloud';

const useHotelbedsFilter = () => {
  const init : {
    name: string;
    boardCodes: Array<any>;
    categories: Array<any>;
    minPrice: Number;
    maxPrice: Number;
  } = {
    name: '',
    boardCodes: [],
    categories: [],
    minPrice: 100,
    maxPrice: 50000
  };

  const [filters, setFilters] : [any, any] = useState(init);
  const [minPrice, setMinPrice]: [ any, any ] = useState(0)
  const [maxPrice, setMaxPrice]: [ any, any ] = useState(0)

  const setName = (name: string) : any => {
    setFilters({...filters, name });
  }

  const setBoardCodeParams = (value) : any => {
    const existingIndex = filters.boardCodes.indexOf(value);

    if (existingIndex == -1) {
      setFilters({...filters, boardCodes: [...filters.boardCodes, value]});
    } else {
      setFilters({...filters, boardCodes: filters.boardCodes.filter(b_c => b_c !== value)});
    }
  }

  const setCategoryParams = (value) : any => {
    const existingIndex = filters.categories.indexOf(value);

    if (existingIndex == -1) {
      setFilters({...filters, categories: [...filters.categories, value]});
    } else {
      setFilters({...filters, categories: filters.categories.filter(c => c !== value)});
    }
  }

  const setPriceRange = ([min, max]) : any => {
    setFilters({...filters, minPrice: min, maxPrice: max});
  }

  const clearFilters = () => {
    setFilters(init)
  }

  const filterResults = ({
    hotels,
    filters, // fixme: no need to pass filters here
    setCategories,
    setBoardCodes,
    Router,
  }) => {
    const encodedParams = encodeFilters(filters);

    let tempCategories = [];
    let tempBoardCodes = [];

    if (hotels != null && hotels) {

      let rates = []

      hotels.map(hotel => {
        if (hotel.categoryName != null && tempCategories.indexOf(hotel.categoryName) == -1) {
          tempCategories.push(hotel.categoryName);
        }

        hotel.rooms != null && hotel.rooms.map(room => {
          room.rates.map(rate => {
            
            rates.push(parseInt(rate._view.afterDiscount.toFixed()))

            if (tempBoardCodes.indexOf(rate.boardName) == -1) {
              tempBoardCodes.push(rate.boardName);
            }
          })
        })

      });

      if (rates.length > 0) {

        rates = sortBy( rates, x => x)

        setMinPrice(rates[ rates.length - 1 ]) 
        setMaxPrice(rates[ 0 ])
      }

    }

    setCategories(tempCategories.sort());
    setBoardCodes(tempBoardCodes.sort());

    let filteredData = hotels != null ? hotels.slice() : [];

    if (encodedParams.length > 0) {
      Router.push({
        pathname: Router.pathname,
        query: {...Router.query, filters: encodeFilters(filters)}
      })
    }

    filteredData = filteredData.map(hotel => {
  
      if (hotel._skipFiltersAndSort === true) 
        return hotel

      const rooms = hotel.rooms.map(room => {

        const rates = room.rates.filter(rate => rate.travelcloud_price >= filters.minPrice && 
          rate.travelcloud_price <= filters.maxPrice)

        if (rates.length > 0) {
          return Object.assign({}, room, { rates })
        }  

      }).filter(room => room != null)

      return rooms.length > 0 ? Object.assign({}, hotel, {
        rooms 
      }) : null

    }).filter(hotel => hotel != null)
    
    if (filters.categories.length > 0) {
      filteredData = filteredData.filter(hotel => hotel._skipFiltersAndSort ? true : filters.categories.includes(hotel.categoryName));
    }

    if (filters.boardCodes.length > 0) {
      filteredData = filteredData.map(hotel => {

        if (hotel._skipFiltersAndSort === true)
          return hotel

        const rooms = hotel.rooms.map(room => {
          const rates = room.rates.filter(rate => filters.boardCodes.indexOf(rate.boardName) !== -1)

          if (rates.length > 0) {
            return Object.assign({}, room, {rates});
          }
        }).filter(room => room != null);

        return rooms.length > 0 ? Object.assign({}, hotel, {rooms}) : null
      }).filter(hotel => hotel != null);
    }

    if (filters.name.length > 0) {
      filteredData = filteredData.filter(hotel => {

        if (hotel._skipFiltersAndSort === true)
          return true

        if (hotel.name.toLowerCase().search(filters.name.toLowerCase()) >= 0) {
          return hotel;
        }
      })
    }

    return filteredData;
  };

  return {
    setName,
    setBoardCodeParams,
    setCategoryParams,
    setPriceRange,
    filters,
    setFilters,
    filterResults,
    clearFilters,
    minPrice, 
    maxPrice
  }
}

export default useHotelbedsFilter;