import { useState } from "react";
import { isEmptyObject } from "../../travelcloud";
import moment from "moment";

export const useHotelbedsSearchForm = ({ formValue, onChange, minLeadTime }: 
  { formValue?: any, onChange?: any, minLeadTime?: any }) => {

  const [params, setParams]: [ any, any ] = useState(
    !isEmptyObject(formValue)
      ? formValue
      : {
        // sourceMarket: null,
        stay: {
          checkIn: null,
          checkOut: null,
          // shiftDays: 1
        },
        hotels: {
          hotel: []
        },
        occupancies: [{ rooms: 1, adults: 1, children: 0 }],
        destinationCode: null,
        destinationOrHotelName: "" // default value for autocomplete, fixme: any cooler way
      }
  );

  const onStayChange = pairs => {

    params["stay"] = Object.assign({}, 
      params["stay"], 
      pairs, 
      Date.parse(pairs['checkIn']) >= Date.parse(params["stay"]['checkOut']) ? 
        {  checkOut: moment(pairs['checkIn']).add( 1, 'days' ).format("YYYY-MM-DD")  } : {}
      );

    setParams(Object.assign({}, params));

    onChange ? onChange(params) : null;
  };

  const onHotelsOrDestinationsChange = pairs => {
    params[ "destinationOrHotelName" ] = pairs.name;
    params[ "destinationCode" ] = pairs.destinationCode;

    if (pairs.hotelCode != null)
      params["hotels"] = { hotel: [pairs.hotelCode] };
  

    setParams(Object.assign({}, params));
    onChange ? onChange(params) : null;
  };

  const onOccupanciesChange = occupancies => {
    setParams({ ...params, occupancies });
    onChange ? onChange(params) : null;
  };

  const onSourceMarketChange = (sourceMarket) => {
    console.log('source market', sourceMarket)
    setParams({ ...params, sourceMarket })
  }

  return { params, controller: { 
    onStayChange, 
    onHotelsOrDestinationsChange, 
    onOccupanciesChange, 
    onSourceMarketChange 
  }}
}