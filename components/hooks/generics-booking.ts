import React, { useState, useEffect } from "react";
import {
  Cart,
  TcResponse
} from "../../travelcloud";
import Big from "big.js";
import { Generic, Order } from "../../types";
import moment from "moment";

export type GenericViewOptions = Generic['options'][0] & {
  _view?: {
    beforeDiscount: Big,
    afterDiscount: Big,
    maxQuantity: number,
    quantity: number,
    updateQuantity: (number) => void,
    dateTime: any //moment
    updateDateTime: (number) => void
  }
}

export type GenericView = {
  [P in keyof Pick<Generic, Exclude<keyof Generic, 'options'>>]? : Generic[P]
} & {
  _view?: {
    dateTime: any, //moment
    updateDateTime: (number) => void,
    quantityDifferentFromCart: boolean,
    updateCart: () => void,
    totalPrice: Big,
    totalQuantity: number
  }
  options: GenericViewOptions[]
}

export function computeGenericView(
  generic: Generic, cart: Cart,
  orderQuantityMap: {[key:string]: number} = {},  quantityMap: {[key:string]: number} = {}, setQuantityMap: (any) => void = (x) => {},
  orderDateMap: {[key:string]: any} = {},  dateMap: {[key:string]: any} = {}, setDateMap: (any) => void = (x) => {},
  orderGenericDateMap: {[key:string]: any} = {},  genericDateMap: {[key:string]: any} = {}, setGenericDateMap: (any) => void = (x) => {},
  //orderTimeMap: {[key:string]: number} = {},  TimeMap: {[key:string]: number} = {}, setTimeMap: (any) => void = (x) => {}
  ): GenericView {
  const genericView: GenericView = generic
  
  genericView.options = generic.options.map((option: GenericViewOptions) => {

    const key = generic.id + '/' + option.id

    var beforeDiscount
    var afterDiscount

    const maxQuantity = option.stock_type === 'Counter' ? parseInt(option.stock) : 99

    if (cart == null) {
      beforeDiscount = Big(option.price)
      afterDiscount = Big(option.price)
    }

    else {
      const product = cart.addGeneric(generic, {
        generic_id: generic.id,
        option_id: option.id,
        quantity: 1
      }, true)

      if (product == null) {
        beforeDiscount = Big(option.price)
        afterDiscount = Big(option.price)
      }

      else {
        beforeDiscount = product.items
                .filter((item) => parseInt(item['price']) > 0)
                .reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))

        afterDiscount = product.items
          .reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))
      }
    }

    option._view = {
      beforeDiscount,
      afterDiscount,
      maxQuantity,
      quantity: quantityMap[key] || 0,
      updateQuantity: (val) => {
        if (val > maxQuantity) val = maxQuantity
        if (val < 0) val = 0
        quantityMap[key] = val
        setQuantityMap(Object.assign({}, quantityMap))
      },
      dateTime: dateMap[key] || null, //needs to be null, cannot be undefined
      updateDateTime: (val) => {
        dateMap[key] = val
        setDateMap(Object.assign({}, dateMap))
      },

    }

    return option})
  const qmEntries = Object.entries(quantityMap)
  genericView._view = {
    dateTime: genericDateMap[genericView.id],
    updateDateTime: (val) => {
      genericDateMap[genericView.id] = val
      setDateMap(Object.assign({}, dateMap))
      for (var option of genericView.options) {
        const key = generic.id + '/' + option.id
        dateMap[key] = val
      }
      setDateMap(Object.assign({}, dateMap))
    },
    quantityDifferentFromCart: qmEntries.reduce(kvp => {
      if (kvp[1] !== orderQuantityMap[kvp[0]]) return true
      return kvp
    }, false),

    totalPrice: genericView.options.reduce((acc, option) => {
      const key = generic.id + '/' + option.id
      return acc.add(option._view.afterDiscount.times(quantityMap[key] || 0))
    }, Big(0)),

    totalQuantity: genericView.options.reduce((acc, option) => {
      const key = generic.id + '/' + option.id
      return acc + (quantityMap[key] || 0)
    }, 0),

    updateCart: () => {
      for (var option of generic.options) {
        const key = generic.id + '/' + option.id
        const form: any = {
          generic_id: generic.id,
          option_id: option.id,
          quantity: quantityMap[key] || 0
        }

        if (dateMap[key] != null) {
          form.from_date = dateMap[key].format('YYYY-MM-DD')
          form.from_time = dateMap[key].format('HH:mm')
        }

        cart.addGeneric(generic, form)
      }
    }
  }

  return genericView
}

const initialMap = {}
const initialDateMap = {} // this is per option
const initialProductDateMap = {} // this is per product

export function useGenericsBookingForm(
  generics: TcResponse<Generic[]>,
  cart: Cart,
  order: TcResponse<Order>
): { minDateTime, genericsView: GenericView[] } {

  const [quantityMap, setQuantityMap] = useState(initialMap)
  const [orderQuantityMap, setOrderQuantityMap] = useState({})

  const [dateMap, setDateMap] = useState(initialDateMap)
  const [orderDateMap, setOrderDateMap] = useState({})

  const [genericDateMap, setGenericDateMap] = useState(initialProductDateMap)
  const [orderGenericDateMap, setOrderGenericDateMap] = useState({})

  const [minDateTime, setMinDateTime] = useState(null)

  useEffect(() => {
    if (order == null || order.result == null || order.result.products == null) return

    const quantityMapFromOrder = order.result.products.reduce((acc, product) => {
      if (product.product_type === 'nontour_product') acc[product.product_id] = parseInt(product.quantity)
      return acc
    }, {})

    setOrderQuantityMap(quantityMapFromOrder)

    const dateMapFromOrder = order.result.products.reduce((acc, product) => {
      if (product.product_type === 'nontour_product' && product.from_date != null && product.from_time != null) {
        const newMoment = moment(product.from_date + 'T' + product.from_time)
        acc[product.product_id] = moment(product.from_date + 'T' + product.from_time)
        if (minDateTime == null || minDateTime > newMoment) setMinDateTime(newMoment)
      }
      return acc
    }, {})

    setOrderDateMap(dateMapFromOrder)

    const genericDateMapFromOrder = order.result.products.reduce((acc, product) => {
      const split = product.product_id.split('/')
      if (product.product_type === 'nontour_product' && product.from_date != null && product.from_time != null) acc[split[0]] = moment(product.from_date + 'T' + product.from_time)
      return acc
    }, {})

    setOrderGenericDateMap(genericDateMapFromOrder)
    
    // we only load quantity from order once
    if (quantityMap === initialMap) setQuantityMap(quantityMapFromOrder)
    if (dateMap === initialDateMap) setDateMap(dateMapFromOrder)
    if (genericDateMap === initialProductDateMap) {
      setGenericDateMap(genericDateMapFromOrder)}
  }, [order])

  const genericsBookingFormController = {
  }

  var genericsView: GenericView[] = generics == null || generics.result == null
    ? null
    : generics.result.map(generic => 
        computeGenericView(
          generic, cart,
          orderQuantityMap, quantityMap, setQuantityMap,
          orderDateMap, dateMap, setDateMap,
          orderGenericDateMap, genericDateMap, setGenericDateMap,
          //orderTimeMap, timeMap, setTimeMap
          ))

  return {
    minDateTime,
    genericsView
  };
}