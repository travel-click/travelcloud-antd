import {useState, useEffect} from 'react'
import { range, TcResponse } from '../../travelcloud'
import { FlightDetail } from '../../types'

//probably don't need hooks for this
var serviceMap = {}
//const segmentIdXbagServiceMap = {}

export type SegmentProps = {
  segment,
  key,
  selectedPax,
  setSelectedPax,
  segmentSeatSelection,
  setSegmentSeatSelection,
  forEachPax
}

export type PaxProps = {
  key, selected,
  alcXbag, alcXbagValue, setAlcXbagValue,
  alcBulk, alcBulkValue, setAlcBulkValue,
  alcPiec, alcPiecValue, setAlcPiecValue,
}

export type FlightSeatsState = {
  getAllSelections, forEachFareSegment,
  flightAncillaries, setFlightAncillaries,
  seatSelection, setSeatSelection,
  selectionServiceQty, setSelectionServiceQty,
  selectedPax, setSelectedPax
}

function paxIds(ptc_adt, ptc_cnn): string[] {
  return range(ptc_adt).map((index) => 'ADT' + (index + 1)).concat(range(ptc_cnn || 0).map((index) => 'CNN' + (index + 1)))
}

export function useFlightAncillaries({client}): FlightSeatsState {
  const [flightAncillaries, setFlightAncillariesInternal] = useState<TcResponse<FlightDetail>>({loading: true})
  const [seatSelection, setSeatSelection] = useState<{
    [seg: string] : {
      [pax: string] : any // this is for selected seat
    }
  }>({})

  const [selectionServiceQty, setSelectionServiceQty] = useState<{
    [pax_offerItemId: string] : any // this is qty customer selected
  }>({})

  const [selectedPax, setSelectedPax] = useState<string>('ADT1')

  const getAllSelections = () => {
    const seats = []
    const seatsPricingsIndexed:{[key: string]: FlightDetail['pricings'][0]['fares'][0]['segments'][0]['seats_pricings'][0]} = {}
    for (var fare of flightAncillaries.result.pricings[0].fares) {
      for (var segment of fare.segments) {
        for (var seats_pricing of segment.seats_pricings) {
          seatsPricingsIndexed[seats_pricing.offer_item_id] = seats_pricing
        }
      }
    }
    const entries = Object.entries(seatSelection)
    for (const segmentSelection of entries) {
      const entries2 = Object.entries(segmentSelection[1])
      for (const seat of entries2) {
        if (seat[1] != null) {
          
          var pricing = null
          for (var oid of seat[1].offer_item_ids) {
            if (seatsPricingsIndexed[oid].paxes.find(x => x === seat[0])) {
              pricing = seatsPricingsIndexed[oid]
            }
          }

          seats.push({
            // segment_id: segmentSelection[0],
            traveler_ref: seat[0],
            row_number: seat[1].row_number,
            column_id: seat[1].column_id,
            offer_item_id: pricing.offer_item_id
          })
        }
      }
    }

    const services = []

    const entriesb2 = Object.entries(selectionServiceQty)
    for (const serviceQty of entriesb2) {
      if (serviceQty[1] != null && serviceQty[1] > 0) {
        //const service = serviceMap[serviceQty[0]]
        const [traveler_ref, offer_item_id] = serviceQty[0].split('/')
        services.push({
          traveler_ref,
          // orig_offer_item_id: service.orig_offer_item_id,
          offer_item_id,
          quantity: serviceQty[1]
        })
      }
    }

    return {seats, services}
  }
  
  const setFlightAncillaries = (json) => {

    serviceMap = {}

    if (json.result == null) {
      setFlightAncillariesInternal(json)
      return
    }
    const initialSelectionServiceQty = {}
    const initialSelection = {}
    var initialPax = null
    for (const fareIndex in json.result.pricings[0].fares) {
      const fare = json.result.pricings[0].fares[fareIndex]
      for (const segmentIndex in fare.segments) {
        const segment = fare.segments[segmentIndex]
        const segmentId = segment.id
        initialSelection[segmentId] = {}
        for (const service of (segment.services || [])) {
          for (const pax of service.paxes) {
            if (initialPax == null) {
              initialPax = pax
              setSelectedPax(pax)
            }
            serviceMap[pax + '/' + service.service_code] = service
            initialSelectionServiceQty[pax + '/' + service.offer_item_id] = 0
            initialSelection[segmentId][pax] = null
          }
        }
        for (const seats_pricing of (segment.seats_pricings || [])) {
          for (const pax of seats_pricing.paxes) {
            if (initialPax == null) {
              initialPax = pax
              setSelectedPax(pax)
            }
          }
        }
        // for (var adtIndex = 1; adtIndex <= pricing.ptc_breakdown_map.adt.quantity; adtIndex++) {
        //   initialSelection[segmentId]['ADT' + adtIndex] = null
        //   initialSelectionServiceQty[segmentId]['ADT' + adtIndex + '/XBAG'] = 0
        //   initialSelectionServiceQty[segmentId]['ADT' + adtIndex + '/BULK'] = 0
        //   initialSelectionServiceQty[segmentId]['ADT' + adtIndex + '/PIEC'] = 0
        // }
        // for (var cnnIndex = 1; cnnIndex <= pricing.ptc_breakdown_map?.cnn?.quantity || 0; cnnIndex++) {
        //   initialSelection[segmentId]['CNN' + cnnIndex] = null
        //   initialSelectionServiceQty[segmentId]['CNN' + cnnIndex + '/XBAG'] = 0
        //   initialSelectionServiceQty[segmentId]['CNN' + cnnIndex + '/BULK'] = 0
        //   initialSelectionServiceQty[segmentId]['CNN' + cnnIndex + '/PIEC'] = 0
        // }
      }
    }
    setSeatSelection(initialSelection)
    setSelectionServiceQty(initialSelectionServiceQty)
    setFlightAncillariesInternal(json)
  }

  const forEachFareSegment = (component) => {
    const result = []
    if (flightAncillaries.result == null) return []

    for (var fareIndex in flightAncillaries.result.pricings[0].fares) {
      for (var segmentIndex in flightAncillaries.result.pricings[0].fares[fareIndex].segments) {
        const segment = flightAncillaries.result.pricings[0].fares[fareIndex].segments[segmentIndex]
        const segmentId = segment.id
        const segmentSeatSelection = seatSelection[segmentId]
        const pricing = flightAncillaries.result.pricings[0]
     
        const setSegmentSeatSelection = (segmentId) => (segmentSelection) => {
          seatSelection[segmentId] = Object.assign({}, segmentSelection)
          setSeatSelection(Object.assign({}, seatSelection))
        }
        const setSegmentPaxServiceSelection = (offer_item_id) => (value) => {
          selectionServiceQty[offer_item_id] = value
          setSelectionServiceQty(Object.assign({}, selectionServiceQty))
        }
        const forEachPax = (component2) => {
          var segmentServicesPax = [];
          for (const service of (segment.services || [])) {
            for (const pax of service.paxes) {
              segmentServicesPax.push(pax);
            }
          }
          const uniqueSegmentServicesPax = Array.from(new Set(segmentServicesPax));
          
          return uniqueSegmentServicesPax.map((key) => {
            const alcXbag = serviceMap[key + '/XBAG']
            const alcXbagValue = alcXbag == null ? null : selectionServiceQty[key + '/' + alcXbag.offer_item_id]
            const setAlcXbagValue = alcXbag == null ? null : setSegmentPaxServiceSelection(key + '/' + alcXbag.offer_item_id)

            const alcBulk = serviceMap[key + '/BULK']
            const alcBulkValue = alcBulk == null ? null : selectionServiceQty[key + '/' + alcBulk.offer_item_id]
            const setAlcBulkValue = alcBulk == null ? null : setSegmentPaxServiceSelection(key + '/' + alcBulk.offer_item_id)

            const alcPiec = serviceMap[key + '/PIEC']
            const alcPiecValue = alcPiec == null ? null : selectionServiceQty[key + '/' + alcPiec.offer_item_id]
            const setAlcPiecValue = alcPiec == null ? null : setSegmentPaxServiceSelection(key + '/' + alcPiec.offer_item_id)

            return component2({
              key,
              selected: selectedPax === key,
              alcXbag,
              alcXbagValue,
              setAlcXbagValue,
              alcBulk,
              alcBulkValue,
              setAlcBulkValue,
              alcPiec,
              alcPiecValue,
              setAlcPiecValue,
            })
          })
          // return paxIds(pricing.ptc_breakdown_map.adt.quantity, pricing.ptc_breakdown_map?.cnn?.quantity).map((key) => {
          //   return component2({
          //     key,
          //     selected: selectedPax === key,
          //     alcXbagValue: selectionServiceQty[key + '/XBAG'],
          //     alcXbag: serviceMap[key + '/XBAG'],
          //     setAlcXbagValue: setSegmentPaxServiceSelection(segmentId, key + '/XBAG'),

          //     alcBulkValue: selectionServiceQty[key + '/BULK'],
          //     alcBulk: serviceMap[key + '/BULK'],
          //     setAlcBulkValue: setSegmentPaxServiceSelection(segmentId, key + '/BULK'),

          //     alcPiecValue: selectionServiceQty[key + '/PIEC'],
          //     alcPiec: serviceMap[key + '/PIEC'],
          //     setAlcPiecValue: setSegmentPaxServiceSelection(segmentId, key + '/PIEC')
          //   })
          // })
        }

        const segmentProps: SegmentProps = {
          segment,
          key: fareIndex + '/' + segmentIndex,
          selectedPax,
          setSelectedPax,
          segmentSeatSelection,
          setSegmentSeatSelection: setSegmentSeatSelection(segmentId),
          forEachPax
        }

        result.push(component(segmentProps))
      }
    }

    return result
  }

  return {
    getAllSelections, forEachFareSegment,
    flightAncillaries, setFlightAncillaries, 
    seatSelection, setSeatSelection,
    selectionServiceQty, setSelectionServiceQty,
    selectedPax, setSelectedPax
  }
}

export function findInMap(map, val): [string, any] {
  return Object.entries(map).find((kvp) => kvp[1] === val)
}

export function seatMapToTable(
  seatMap: FlightDetail['pricings'][0]['fares'][0]['segments'][0]['seats_maps'][0],
  seatPricings: FlightDetail['pricings'][0]['fares'][0]['segments'][0]['seats_pricings']
  ): any {
  const table = []

  const translateColumnId = seatMap.column_ids.reduce((acc, id, index) => {
    acc[id] = index
    return acc
  }, {})

  const numberOfRows = parseInt(seatMap.last_row) - parseInt(seatMap.first_row) + 1
  const numberOfCols = seatMap.column_ids.length

  const first_row = parseInt(seatMap.first_row)

  for (var rowIndex = 0; rowIndex < numberOfRows; rowIndex++) {
    const row = []
    for (var colIndex = 0; colIndex < numberOfCols; colIndex++) {
      row.push(null)
    }
    table.push(row)
  }

  // index pricings
  const seatPricingsIndexed = seatPricings.reduce((acc, item) => {
    acc[item.offer_item_id] = item
    return acc
  },{})

  for (var seat of seatMap.seats) {
    //@ts-ignore
    seat.pricing = seat.offer_item_ids.reduce((acc, oid) => acc || seatPricingsIndexed[oid], null)
    table[parseInt(seat.row_number) - first_row][translateColumnId[seat.column_id]] = seat
  }

  return table //seatMap.seats.reduce
}