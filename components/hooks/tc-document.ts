import { useRouter } from "next/router"
import { useEffect, useLayoutEffect, useState, useRef } from "react"

export const useTcDocument = (client, page=null) => {

  const router = useRouter()
  page = page ?? router?.asPath

  const [ loading, setLoading ]: [ boolean, CallableFunction ] = useState(null)
  const [ content, setContent ]: [ string, CallableFunction ] = useState(null)

  useEffect(() => {
    setLoading( true )
    client.getDocument( page )
      .then(data => {
        setLoading( false )
        setContent( data )
      })
  }, [])

  return { content, loading }
}