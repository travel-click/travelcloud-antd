import { HotelbedsFormValue, newDateLocal } from "../../order-product-compute"
import { TravelCloudClient, validateHotelbedsParams, groupBy, sortBy, Cart, chain, overlap, isDateRangeOverlap, getHeadAndTail } from "../../travelcloud"
import { useState, useEffect, useCallback } from "react"
import Big from "big.js";
import moment from "moment";

export enum SearchBehavior {
  ManualTrigger = 0,
  AutoSearchOnValueChange = 1,
  AutoSearchOnFirstRender = 2,
}

export type HotelbedsResponse = {
  checkIn: string,
  checkOut: string,
  hotels: any[],
  total: number 
}

export function useHotelbeds({formValue, client, cart, searchType = SearchBehavior.AutoSearchOnFirstRender }
  :{ formValue:HotelbedsFormValue, client: TravelCloudClient, cart: Cart, searchType?: SearchBehavior }):any {

  const [ result, setResult ]: [ HotelbedsResponse, any ] = useState({ checkIn: '', checkOut: null, hotels: [], total: null }) // fixme: provide proper type
  const [ loading, setLoading ]: [ any, any ] = useState( false ) // fixme: provide proper type

  const [ types, setTypes ] = useState({})

  useEffect(() => {

    (async () => {
        let facilityTypes = await client.hotelbedsFacilityTypes()
        let terminalTypes = await client.hotelbedsTerminalTypes()
        let imageTypes = await client.hotelbedsImageTypes()

        setTypes({
          facilityTypes, terminalTypes, imageTypes
        })
    })()

  }, [])

  const search = useCallback(( formValue ) => {
    if (validateHotelbedsParams( formValue )) {
      setLoading( true )
      client.hotelbedsSearch( formValue )
        .then(resp => {

          console.log('cart', cart)

          setResult({
            ...resp, 
            hotels: resp.hotels != null ? resp.hotels.map(hotel => computeHotelbedsView(hotel, cart)) : []
          })

          setLoading( false )

        })
    }
  }, [ cart ])

  useEffect(() => {

    console.log('111')
    console.log('cart', cart)

    if (cart != null) {
      searchType === SearchBehavior.AutoSearchOnFirstRender && search( formValue  )
    }

  }, [ cart ])

  useEffect(() => {

    console.log('222')
    console.log('cart', cart)

    if (cart != null) {
      searchType === SearchBehavior.AutoSearchOnValueChange && search( formValue )
    }

  }, [ formValue, searchType ])


  return [ 
    result,
    loading,
    search, 
    types 
  ]
}


export const useRoomCharacteristicMap = (roomTypes, client) => {

  const [map, setMap] = useState({})

  useEffect(()=>{

    (async() => {
      let roomCharacteristicType = await client.getRoomCharacteristicMap(roomTypes)
      roomCharacteristicType != null && setMap(getRoomCharacteristicMap(roomCharacteristicType,roomTypes))
    })()

  },[])

  return map
}

/**
 * Hotelbeds Utilities 
 * todo: things below are probably not hooks, 
 *       please move to somewhere proper
 */

 // fixme: check about performance concern for a long list of hotels
export const computeHotelbedsView = ( hotel, cart ) => {
  
  hotel.rooms = hotel.rooms.map(room => {

    return Object.assign({}, room, {
      rates: room.rates.map(rate => {

        let checkrates = {
          rooms: [
            { ...room, rates: [ rate ]}
          ]
        }

        let product = cart.addHotelbedsHotel({ static: hotel.details, checkrates }, {}, true)

        let beforeDiscount = product.items
          .filter(item => parseInt(item[ 'price' ]) > 0)
          .reduce((acc, item) => acc.add(Big( item.price ).times( item.quantity )), Big( 0 ))

        let afterDiscount = product.items
          .reduce((acc, item) => acc.add(Big( item.price ).times( item.quantity )), Big( 0 ))

        let hasDiscount = ! beforeDiscount.eq( afterDiscount )

        return {
          ...rate, _view: { beforeDiscount, afterDiscount, hasDiscount }
        }

      })
    })

  })

  return hotel
}

export const mergeImageTypes = (images, imageTypes) => {
  return groupBy( images, ({ imageTypeCode }) => imageTypeCode )
      .map( group => sortBy(group, ({ visualOrder }) => visualOrder ))
      .map( images => images.map((x: any) => 
        Object.assign(x, { 
          description: imageTypes[x.imageTypeCode].description
        })))
}

export const mergeFacilityTypes = (facilities, facilityTypes ) => {
  let groupedFacilities = groupBy(facilities, (x: any) => x.facilityGroupCode)
  let groups = groupedFacilities.map((arr: any[]) => arr[0].facilityGroupCode)
    .map((groupCode: any) => facilityTypes.groups[ groupCode ])
    .map((group: any) => {
      return { 
        ...group, 
        facilities: facilities
          .filter(({facilityGroupCode}) => facilityGroupCode === group.code) 
          .map(facility => {

            let desc = facilityTypes.facilities[`${facility.facilityCode}.${facility.facilityGroupCode}`]

            return desc != null ? {
              ...facility, ...desc
            }  : null
            
          })
          .filter(facility => facility != null)
      }
    })
    .filter(({ facilities }: any) => facilities.length > 0)

    return groups
}

export const getRoomCharacteristicMap = (rooms, char)=>{
  let charMap = {};
  for (let i = 0; i < char.length; i++) {
    var result = rooms.filter(room => {
      if (room.characteristic === char[i]) {
        return room.characteristicDescription.content;
      }
  
      return null;
    });
    charMap[result[0].characteristic] =
      result[0].characteristicDescription.content;
  }
  return charMap
}

export const prepareCancellationPolicy = (rate, currency) => {

  const isPrevDay = (date) => {
    // todo: also check timezone
    let d = newDateLocal()
    return d.getTime() > newDateLocal(date).getTime()
  }

  const output = rate.cancellationPolicies != null ? chain(rate.cancellationPolicies, [
    arr => arr.length === 1 && isPrevDay(arr[0].from) && Big(rate.travelcloud_price).eq(arr[0].travelcloud_price) ? [] : arr,
    arr => sortBy(arr, policy => newDateLocal(policy.from).getTime(), 'asc'),
    overlap
  ])
    .reduce((acc, [policy, nextPolicy], index) => {

      let _policies = []

      if (index === 0 && !isPrevDay(policy.from)) {
        _policies.push(`Free cancellation before ${formatDate(policy.from)}`)
      }

      if (nextPolicy === null) {
        if (isPrevDay(policy.from))
          _policies.push(`${currency} ${policy.travelcloud_price}`)
        else
          _policies.push(`${currency} ${policy.travelcloud_price} after ${formatDate(policy.from)}`)

      } else {
        if (isPrevDay(policy.from))
          _policies.push(`${currency} ${policy.travelcloud_price} before ${formatDate(nextPolicy.from)}`)
        else
          _policies.push(`${currency} ${policy.travelcloud_price} after ${formatDate(policy.from)} before ${formatDate(nextPolicy.from)}`)
      }

      return acc.concat(_policies)

    }, []) : []
    return output.length > 0 ? output : null;
}

export const isFacilityAvailable = (f: any, checkIn, checkOut) => {

    // fixme: made things into separate conditions for easier reading
    //        feel free to make changes

    let flag = true

    // indLogic: Boolean 
    // Indicator if the facility exists at the hotel or not
    if (f.indLogic != null && f.indLogic != true) {
      flag = false
      console.log(1)
    }

    // indYesOrNo: Boolean
    // Indicator if the mandatory facility exists at the hotel or not
    if (flag === true && f.indYesOrNo != null && f.indYesOrNo != true) {
      flag = false
      console.log(2)
      console.log(f.indYesOrNo)
    }

    // dateFrom: YYYY-MM-DD
    // dateTo: YYYY-MM-DD
    // timeTo: HH:MM:SS
    // timeTo: HH:MM:SS

    if (f.dateFrom != null && f.dateTo != null && !isDateRangeOverlap(
      [newDateLocal(f.dateFrom), newDateLocal(f.dateTo)],
      [newDateLocal(checkIn), newDateLocal(checkOut)])
    ) {
      flag = false
    }


    return flag

  }


export const RateClassInfo = {
  NOR: 'Normal Rate',
  NRF: 'Rate has restrictive cancellation fees, 100% stay from the moment of booking.',
  PAQ: 'Packaged rate, extra service sold together with the accommodation (i.e. ski, excursions, tickets,…)',
  DIS: 'Packaged rate for Disney hotels, extra service (ticket entry) sold together with the accommodation. ',
  CAN: 'Special Offers for Local Canarian Islands residents. Proof of residency is requested at the check-in.',
  SEN: 'Special Offers for Senior travellers. Proof of age is required at the check-in.',
  HNO: 'Special Offers for Honeymooners. Certificate of marriage is requested at the check-in. ',
  JUN: 'Special Offers for Junior travellers. Proof of age is required at the check-in.',
  BAL: 'Special Offers for local Balearic Islands residents. Proof of residency is requested at the check-in.',
  ROU: 'Special contracts where the client can be allocated to any of the resorts of the category and area chosen. Property name is confirmed few days before arrival.'
}

const formatDate = (dateStr: string) => moment(dateStr).format('DD/MM/YYYY h:m a')

export const getLowestPrice = (rooms: any): string => {

  if (rooms.length == 0)
    return ''

  let rates = rooms.map(({ rates }) => rates)
    .flat()

  let [lowestRate,] = getHeadAndTail(rates, (rate: any) => parseInt(rate._view.afterDiscount))

  return lowestRate._view.afterDiscount.toString() ?? ''
}


export const getHotelResultUtilities = {
  findStars: (text) => {
    let stars;
    if (!!text.match(/\d.+/)) {
      stars = text.replace(/\D/g, "");
      return parseInt(stars);
    } else return null;
  },
  countNights: (checkIn, checkOut) => {
    const dayInMilliseconds = 1000 * 60 * 60 * 24;
    const mCheckIn = moment(checkIn)
    const mCheckOut = moment(checkOut)
    const diff = (mCheckOut.diff(mCheckIn))
    const nights = !!diff && (diff / dayInMilliseconds)
    return nights
  }
}
