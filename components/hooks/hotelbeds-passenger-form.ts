import {useState} from 'react';
import {HotelbedsSelectedRateInfo} from '../hotelbeds-results'
import {range, groupBy} from '../../travelcloud';

export const useHotelbedsPassengerHook = ({item} : {item: any}) :any => {

  const generateRooms = ({hotel, room, rates} : HotelbedsSelectedRateInfo) => {

    return rates.map((rate:any) => ({
      rateKey: rate.rateKey,
      paxes: range(rate.rooms).reduce((acc, cur, index) => {
          return acc.concat(
            range(rate.adults).map(_ => ({ roomId: index + 1, type: 'AD', name: ['', ''] })),
            range(rate.children).map(_ => ({ roomId: index + 1, type: 'CH', name: ['', ''] })),
          )
      }, []),
      _display: {
        hotelName: hotel.name,
        roomName: room.name,
        adults: rate.adults,
        children: rate.children,
        rooms: rate.rooms
      },
    }))

  }

  const [ isLeadPassengerOnly, setPassengerFlag ] = useState(true)
  const [ rooms, setRooms ] = useState(generateRooms( item ))
  const [ leadPassengerRooms, setLeadPassengerRooms ] = useState(generateRooms( item ))
  
  const onChange = (params, onUpdate, isFormValidated) => {

    let { name, roomIndex, paxIndex } = params
    const flag = params.isLeadPassengerOnly != null ? params.isLeadPassengerOnly : isLeadPassengerOnly

    if (params.isLeadPassengerOnly != null) 
      setPassengerFlag(params.isLeadPassengerOnly)

    if (name != null && roomIndex != null && paxIndex != null) {

      let updateFormState = flag ? setLeadPassengerRooms : setRooms

      updateFormState((prev) => {
          if (prev[ roomIndex ] != null 
              && prev[ roomIndex ].paxes[ paxIndex ] != null) {
            prev[ roomIndex ].paxes[ paxIndex ].name = name
            return [].concat(prev)
          }

          else {
            return []
          }
        })
    }

    const data = flag ? leadPassengerRooms : rooms
    
    if (data.length > 0 && data[0] != null && 
        data[0].paxes.length > 0 && data[0].paxes[0] != null) {

      onUpdate({
        holder: { name: data[0].paxes[0].name[0], surname: data[0].paxes[0].name[1] },

        rooms: data.map(room => {
          return {
            rateKey: room.rateKey,
            paxes: room.paxes.map(({roomId, type, name}) => ({roomId, type, name: name.join(' ')}))
          }
        })

      })

      if ( flag ) {
        let flag = data.filter(room => {

          let groupedRooms = groupBy(room.paxes, x => x.roomId)

          return groupedRooms.map((group, gIdx) => group[0])
            .filter(({name}) => name[0] != '' && name[1] != '').length  === groupedRooms.length

        }).length === data.length

        isFormValidated( flag )

      } else {
        let flag = data.filter(room => {
          return room.paxes.filter(({name}) => name[0] != '' && name[1] != '').length == room.paxes.length
        }).length === data.length

        isFormValidated( flag )

      }

    }

  }

  return {
    isLeadPassengerOnly,
    setPassengerFlag,
    rooms,
    setRooms,
    leadPassengerRooms,
    setLeadPassengerRooms,
    onChange,
  }
}