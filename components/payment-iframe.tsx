import React, { useEffect, useState } from "react";
import { TravelCloudClient } from "../travelcloud";
import { usePayload } from "../components/hooks/payment-iframe-logic";

interface PaymentIframeProps{
  orderRef: string,
  tcUser: string, 
  paymentProcessor: string,
  amount: string, 
  successPage?: string, 
  failurePage?: string,
  name?: string, 
  className?: string, 
  style?: any, 
  scrolling?: boolean, 
  onSuccess?: (params: URLSearchParams /* redirected values from payment processer */) => any, 
  onFailure?: (params: URLSearchParams, error?: any) => any
  children: any
}

const PaymentIframe: React.FC<PaymentIframeProps> = (
  { 
    orderRef, 
    tcUser,
    paymentProcessor,
    amount,
    successPage,
    failurePage,
    name, 
    className, 
    style, 
    scrolling, 
    onSuccess, 
    onFailure,
    children
  }
) => {

  const [ errorResp ] = usePayload(
    orderRef, 
    name, 
    new TravelCloudClient({tcUser}), 
    paymentProcessor, 
    amount, 
    successPage, 
    failurePage
  );

  useEffect(() => {
    if (errorResp != null) {
      onFailure != null ? onFailure(null, errorResp) : redirectFailurePage();
    }
  }, [ errorResp ])

  const redirectSuccessPage = () => {
    window.location.pathname = successPage;
  }

  const redirectFailurePage = () => {
    window.location.pathname = failurePage;
  }

  // for 2C2P - https://developer.2c2p.com/docs/using-iframe
  // sometimes, the payment complete page doesn't auto redirect
  // sometimes, there is a 5 second countdown then redirect
  const handlePaymentPostMessages = ({ data }) => {
    const { paymentResult } = data;
    if (paymentResult) {
      const { respCode, respDesc, respData } = paymentResult;
  
      //alert(respCode+': '+respDesc+': '+respData);
      if(respCode == '2000'){
        //both success and failure returns the exact same respCode, respDesc, respData
        redirectSuccessPage()
      }
      if(respCode == '1001'){
        // break out of iframe when redirected to 3ds page
        window.location.replace(respData); 
      }
    }
  }
  // Subscribe on post messages
  window.addEventListener('message', handlePaymentPostMessages)

  const onLoad = (e) => {
    const loading: any = document.getElementById("loading");
    loading.style.display = "none";
    e.target.style.display = "";
  }

  // fixme: should move to component logic ?
  useEffect(() => {

    const paymentIFrameEl:any = document.getElementById('payment-iframe')
    const loading: any = document.getElementById("loading");
    loading.style.display = "";
    paymentIFrameEl.style.display = "none";

    const uri = (frame) => `${frame.location.protocol}//${frame.location.host}/${frame.location.pathname.replace(/^\/+/g, "")}`
    const pathname = (frame) => frame.location.pathname.replace(/^\/+/g, "")  
    const query = frame => new URLSearchParams(frame.location.search)

    const houseKeeping = (timerId, iFrame) => {
      clearInterval(timerId)
      iFrame != null && iFrame.remove()
      window.removeEventListener('message', handlePaymentPostMessages)
    }

    const timerId = setInterval(() => {
      try {
        if (paymentIFrameEl != null && paymentIFrameEl.contentWindow != null && paymentIFrameEl.contentWindow.document) {
          const topFrameURI = uri(window);
          const currentFrameURI = uri(paymentIFrameEl.contentWindow)

          if (pathname(paymentIFrameEl.contentWindow) == successPage) {
            // pass query params redirected from payment processer
            onSuccess != null ? onSuccess(query( paymentIFrameEl.contentWindow )) : redirectSuccessPage();
            houseKeeping(timerId, paymentIFrameEl)
          }

          else if (pathname(paymentIFrameEl.contentWindow) == failurePage) {
            // pass query params redirected from payment processer
            onFailure != null ? onFailure(query( paymentIFrameEl.contentWindow )) : redirectFailurePage();
            houseKeeping(timerId, paymentIFrameEl)
          }

          else if (topFrameURI == currentFrameURI) {
            // order cancel
            // fixme: any better idea or improvement
            window.location.reload()
          }

        }
      } 

      catch(error) {
        if (error instanceof DOMException) {
          // do nothing
          // console.log('do nothing')
        }

        else {
          throw error
        }
      }
    }, 500)

    return () => {
      console.log("component dismount")
      houseKeeping(timerId, paymentIFrameEl)

      // clear interval
      // remove iframe if it still exists 
    }
  }, [])  

  return (
    <>
        <div id="loading" style={{margin: 'auto'}}>
          {children}
        </div>
        <iframe
          id={"payment-iframe"}
          className={className}
          name={name}
          style={style}
          frameBorder={0}
          onLoad={onLoad}
          scrolling={scrolling ? 'yes' : 'no'}
        />
    </>

  );
};

PaymentIframe.defaultProps = {
  name: 'payment-iframe',
  style: {},
  className: '',
  scrolling: false,
  successPage: 'payment-successful',
  failurePage: 'payment-failed',
}

export default PaymentIframe;
