import React from 'react'
import {Cart} from '../travelcloud'
import {Order as OrderType} from '../types'
import Big from 'big.js'
import { newDateLocal } from '../order-product-compute'

function notEmpty(x) {
  return x != null && x !== "" && x !== '0000-00-00' && x !== '0000-00-00 00:00:00'
}

export function formatCurrency(x, decimalPlace=2) {
  if (x == null) return ''
  const neg = parseFloat(x) < 0 ? '-' : ''
  return neg + '$' + Big(x).abs().toFixed(decimalPlace).toString()
}

function formatProductDateTime(product) {
  if (!notEmpty(product.from_date)) return null;
  var date
  if (product.from_time == null || product.from_time == "") {
    date = (newDateLocal(product.from_date)).toLocaleDateString("en-US", { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric' })
  } else {
    date = (newDateLocal(product.from_date + " " + product.from_time)).toLocaleDateString("en-US", { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', hour12: true })
  }
  
  // remove weekday
  return date.substring(5)
}

export const Order = (
  {order, config, cart,
    hidePaymentErrors = false,
    showSection = {status: true, contact: true, products: true, remove: true, message: true, travelers: true, payments: true}}
  : {order: OrderType, config?, cart?: Cart,
    hidePaymentErrors?: boolean,
    showSection?: {status?: boolean, contact?: boolean, products?: boolean, remove?: boolean, message?: boolean, travelers?: boolean, payments?: boolean}}) => {
  const products: OrderType['products'] = order.products || []
  const fee = order.payments == null ? Big(0) : order.payments.reduce((acc, p) => acc.add(p.fee || 0), Big(0))
  const subtotal = Big(order.payment_required).minus(fee)

  const hasResidenceCountry = order.travelers != null && order.travelers.length > 0 && notEmpty(order.travelers[0].residence_country)
  const hasPassportIssueDate = order.travelers != null && order.travelers.length > 0 && notEmpty(order.travelers[0].passport_issue_date)
  const hasPassportIssueCountry = order.travelers != null && order.travelers.length > 0 && notEmpty(order.travelers[0].passport_issue_country)

  var travelerColSpan = 6
  if (hasResidenceCountry) travelerColSpan++
  if (hasPassportIssueDate || hasPassportIssueCountry) travelerColSpan++

  var paymentsFiltered = order.payments
  if (hidePaymentErrors === true && paymentsFiltered != null) {
    paymentsFiltered = paymentsFiltered.filter(payment => parseFloat(payment.amount) != 0)
  }

  // group by name
  // same products may have different names
  const productsGrouped = Object.entries(products.reduce((acc, product) => {
    const key = product.product_name + product.product_source_ref
    if (acc[product.product_name] == null) acc[product.product_name] = []
    acc[product.product_name].push(product)
    return acc
  }, {}))

  const email = order.email ?? order.customer?.email

  // properties in the outermost dive may not be rendered when
  // nextjs replaces client rendered dom with server rendered dom
  // add additional div outside to workaround this
  return <div><div className="tc-invoice">
    {showSection.status && <table style={{width: '100%'}}><tbody><tr>
      <td style={{textAlign: 'left', verticalAlign: 'top'}}>
        <h1>{order.order_status}</h1>
        {order.ref != null &&
          <div>
            <div>Reference: REF-{order.ref}</div>
            <div>Order Date: {order.order_date}</div>
          </div>}
      </td>
      {config != null && <td style={{textAlign: 'right', verticalAlign: 'top'}}>
        {/* h1 to align the margins and paddings */}
        {config["COMPANY_LOGO"].trim() !== '' && <h1><img src={config["COMPANY_LOGO"]} /></h1>}
      </td>}
    </tr></tbody></table>}

    {showSection.contact && order.first_name != null && order.first_name != "" && <table style={{width: '100%'}}><tbody>
      <tr>
      <td style={{textAlign: 'left', verticalAlign: 'top'}}>
        <div className="block-header">Bill To</div>
        <div>{order.first_name} {order.last_name}</div>
        {order.customer != null && <div>{email}</div>}
        <div>{order.phone_country != null && order.phone_country && "+" + order.phone_country} {order.phone}</div>
      </td>

      {config != null && <td style={{textAlign: 'right', verticalAlign: 'top'}}>
          <div className="block-header">From</div>
          <div>
              {config["COMPANY_NAME"]}
          </div>
          {config["COMPANY_ADDRESS"].split("\n").map((line, index) => <div key={index}>{line}</div>)}
          <div>
              {config["SYSTEM_EMAIL"]}
          </div>
          <div>
              {config["COMPANY_PHONE"]}
          </div>
      </td>}
      </tr></tbody></table>}


    {showSection.products && productsGrouped.map((kvp, index) => {
      const firstProduct = kvp[1][0]
      const products: OrderType['products'] = kvp[1] as any

      const dateTimeGrouped = products.reduce((acc, product) => {
        const key = product.from_date + product.from_time
        if (acc[key] == null) {
          acc[key] = []
        }
        acc[key].push(product)
        return acc
      }, {})
      return <table key={index} className="invoice_table_style"><tbody>
        <tr className="item_name">
          <td colSpan={3} className="col1">
            {firstProduct.product_name}
          </td>
          <td colSpan={1}>
          {showSection.remove && cart && <a className="remove" onClick={() => cart.removeProductByName(firstProduct.product_name)}>remove</a>}
          </td>
        </tr>
        {notEmpty(firstProduct.product_source_ref) && <tr>
          <td colSpan={4} className="col1">
            <div className="subtitle">Reservation Code: {firstProduct.product_source_ref}</div>
          </td>
        </tr>}
        {notEmpty(firstProduct.voucher) && notEmpty(firstProduct.voucher.flight_tickets) && 
        firstProduct.voucher.flight_tickets.map((flightTicket, index) => 
          <tr key={index}>
            <td colSpan={4} className="col1">
              <div className="subtitle">Ticket Number {index+1}: {flightTicket.ticket_number}</div>
            </td>
          </tr>
        )}
        <tr className="header">
          <td style={{width:'60%'}} className="col1">Items</td>
          <td style={{width:'10%'}}>Units</td>
          <td style={{width:'15%'}}>Unit Price</td>
          <td style={{width:'15%'}}>Sub Total</td>
        </tr></tbody>
        {Object.entries(dateTimeGrouped).map((kvp2, index2) => {
          const firstProduct2 = kvp2[1][0]
          const products2: OrderType['products'] = kvp2[1] as any
          const subHeaderGrouped = products2.reduce((acc, product) => {
            return product.items.reduce((acc2, line) => {
              if (acc2[line.sub_header] == null) acc2[line.sub_header] = []
              acc2[line.sub_header].push(line)
              return acc2
            }, acc)
          }, {})

          return <tbody key={index2}>
            {
              index2 === 0 && notEmpty(firstProduct2.from_date) && firstProduct2.product_type !== 'flight'
                && <tr className="dateheader">
                  <td className="col1">
                    <div>
                      [{formatProductDateTime(firstProduct2)}]
                    </div>
                  </td>
                </tr>
            }
            {
              Object.entries(subHeaderGrouped).reduce((acc, kvp3, index3) => {
                const sub_header = kvp3[0]
                const items: OrderType['products'][0]['items'] = kvp3[1] as any

                if (sub_header != null && sub_header != "") acc.push(
                  <tr className="subheader" key={"sh" + index3}>
                    <td className="col1">
                      <div>
                        {sub_header}
                      </div></td>
                  </tr>
                )

                for (var index4 in items) {
                  const item = items[index4]
                  acc.push(
                    <tr key={index3 + '-' + index4}>
                      <td className="col1">
                        <div>
                          {item.name}
                        </div></td>
                      <td>{item.quantity}</td>
                      <td>{formatCurrency(item.price)}</td>
                      <td>{formatCurrency(parseFloat(item.quantity) * parseFloat(item.price))}</td>
                    </tr>
                  )
                }
                
                return acc
              }, [])
            }
          </tbody>})}
      </table>
    })}
    {showSection.products && [<div className="total" key={1}>
      {showSection.products && fee.gt(0) && [
        <div key={1}>
          <span>Subtotal</span>
          <span className="amount">{formatCurrency(subtotal)}</span>
        </div>,
        <div key={2}>
          <span>Processing Fee</span>
          <span className="amount">{formatCurrency(fee)}</span>
        </div>
    ]}
      <div style={{fontWeight: 'bold'}} key={3}>
        <span>Total</span>
        <span className="amount">{formatCurrency(order.payment_required)}</span>
      </div>
    </div>,
    <div className="tc-clearfix" key={2} />]}

    {showSection.message && order.user_message != null && order.user_message.trim() !== "" && <table className="invoice_table_style"><tbody>
      <tr className="item_name">
        <td className="col1">Additional Requests</td>
      </tr>
      <tr>
        <td className="col1">{order.user_message}</td>
      </tr>
      </tbody></table>}

    {showSection.travelers && order.travelers != null && order.travelers.length > 0 && <table className="invoice_table_style"><tbody>
      <tr className="item_name">
        <td className="col1" colSpan={travelerColSpan}>Travelers</td>
      </tr>
      <tr className="header">
        <td className="col1">Name</td>
        <td>Birth</td>
        <td>Gender</td>
        <td>Nation</td>
        {hasResidenceCountry && <td>Resident</td>}
        <td>Passport</td>
        <td>Expiry</td>
        {(hasPassportIssueCountry || hasPassportIssueDate) && <td>Issued</td>}
      </tr>
      {order.travelers != null && order.travelers.length > 0 && order.travelers.map((traveler, index) => <tr key={index}>
        <td className="col1">{traveler.first_name} {traveler.last_name}</td>
        <td>{traveler.birth_date}</td>
        <td>{traveler.title === 'Ms' ? 'Female' : 'Male'}</td>
        <td>{traveler.country}</td>
        {hasResidenceCountry && <td>{traveler.residence_country}</td>}
        <td>{traveler.passport}</td>
        <td>{traveler.expiry}</td>
        {(hasPassportIssueCountry || hasPassportIssueDate) && <td>{traveler.passport_issue_country} {traveler.passport_issue_date}</td>}
      </tr>)}
      </tbody></table>}

    {showSection.payments && <table className="invoice_table_style"><tbody>
      <tr className="item_name">
        <td className="col1" colSpan={4}>Payments</td>
      </tr>
      <tr className="header">
        <td className="col1">Description</td>
        <td>Date</td>
        <td>Status</td>
        <td>Amount</td>
      </tr>
      {paymentsFiltered != null && paymentsFiltered.length > 0
        ?  paymentsFiltered.map((payment, index) => <tr key={index}>
            <td className="col1">{payment.description.length === 0 ? "Added by " + payment.source : payment.description}</td>
            <td>{payment.date_added}</td>
            <td>{payment.status}</td>
            <td>{formatCurrency(payment.amount)}</td>
          </tr>)
        : <tr>
            <td className="col1" colSpan={4}>No payments received</td>
          </tr>}
    </tbody></table>}
  </div></div>}