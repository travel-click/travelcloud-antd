import React, { useEffect, useState } from 'react'
import { newDateLocal } from '../order-product-compute'
import { TravelCloudClient } from '../travelcloud'
import { prepareCancellationPolicy } from './hooks/hotelbeds'

export const loadRateComments = (hotelCode: number, rates: any[], client: TravelCloudClient): Promise<any> => {

    let rateCodes: number[] = rates
      .filter((rate:any) => rate.rateCommentsId != null)
      .map(({ rateCommentsId }) => rateCommentsId.split('|'))
      .map(([ incoming, code, rate ]: [ number, number, string ]) => code)

    return rateCodes.length ? client.hotelbedsRateComments(hotelCode, rateCodes)  : null
}

export const parseAndFilterRateComments = (rateCommentsId: string, rateComments: any[], checkInDate: string) => {
    const [ incoming, code, rate ] = rateCommentsId.split('|')

    const mesg:any[] = rateComments.map((comment:any):any => {
    //   console.log(1)
      if (comment.incoming == incoming && comment.code == code) {
        // console.log(2)
        return comment.commentsByRates.map(({comments, rateCodes}) => {
        //   console.log(3)
        //   console.log('rateCodes', rateCodes)
        //   console.log('rate', rate)
        //   console.log('value', rateCodes.indexOf( rate ))

          if (rateCodes.indexOf(parseInt( rate )) !== -1) {
            // console.log(4)
            return comments.map(({dateEnd, dateStart, description}: any) => {
              let checkIn = Date.parse( checkInDate )
              return checkIn >= Date.parse(dateStart) && checkIn <= Date.parse(dateEnd) ? description : null
            })
          }
        })
      }
    })

    return (mesg as any).flat().filter(x => x != null)
}

export const HotelBeds = (
  {order, client}
  : {order, client: TravelCloudClient}) => {
  const products = order.products || []

  const productsFiltered = products.filter(product => product.product_source === 'hotelbeds')

  if (productsFiltered.length === 0) return null
  
  console.log('order products', products)

  let rates = productsFiltered.map(product => product.product_detail.checkrates.rooms)
    .flat()
    .map(room => room.rates)
    .flat()
  
  const [ rateComments, setRateComments ] = useState([])
  
  useEffect(() => {

    (async () => {
      // assume to have only one hotel per checkout
      let hotelCode = productsFiltered[0].product_detail.static.code

      let data = await loadRateComments( hotelCode, rates, client )
      if (data != null ) setRateComments( data )
    })()

  }, [])
    
  // const { states, controller } = useHotelbedsRateComments({ hotelCode: hotel.code, client })

  // properties in the outermost dive may not be rendered when
  // nextjs replaces client rendered dom with server rendered dom
  // add additional div outside to workaround this
  return <div><div className="tc-invoice">
    {productsFiltered.map((product, index) => {

      // const { states, controller } = useHotelbedsRateComments({ rates, client })
      // useEffect(() => controller.fetchRateComments(), [])

      console.log('product', product)

      const staticData = product.product_detail.static;
      const checkrates = product.product_detail.checkrates;

      // find the days between epoch time after timezone
      function daysBetween(startDate: string, endDate: string) : any {
        const millisecondsPerDay = 24 * 60 * 60 * 1000;
        return (newDateLocal(endDate).getTime() - newDateLocal(startDate).getTime()) / millisecondsPerDay;
      }
      

      return <div key={index}>
        <table className="invoice_table_style"><tbody>
          <tr className="item_name">
            <td className="col1">Accommodation Info</td>
          </tr>
        </tbody></table>

        <div className="block-header">Contact info</div>
        <div>{staticData.name.content} {checkrates.categoryName != null && "(" + checkrates.categoryName + ")"}</div>
        {staticData != null && <div>{staticData.address.content.trim() + ', ' + staticData.countryCode + ' ' + staticData.postalCode}</div>}
        {staticData != null && staticData.email && <div>{staticData.email}</div>}

        {product.voucher == null && <>
          <table className="invoice_table_style"><tbody>
            <tr className="header">
              <td className="col1" style={{width: "40%"}}>Room type</td>
              <td>Units</td>
              <td>Adults</td>
              <td>Children (Ages)</td>
              <td>Board</td>
            </tr>
            {
              product.product_detail.checkrates.rooms.map((room, roomIndex) => 
                room.rates.map((rate, rateIndex) => 
                  <tr key={roomIndex + '-' + rateIndex}>
                    <td className="col1">{room.name}</td>              
                    <td>{rate.rooms}</td>
                    <td>{rate.adults}</td>
                    <td>{rate.children} {rate.children > 0 && "(" + rate.childrenAges + ")"}</td>
                    <td>{rate.boardName}</td>
                  </tr>
              ))
            }
          </tbody></table>

          <div className="block-header">Cancellation Policies</div>
          {
            checkrates.rooms.map((room, roomIndex) => (
              <ul key={roomIndex}>
                <h4>{room.name}</h4>
                {
                  room.rates.map((rate, rateIndex) => (
                    <div key={rateIndex} style={{ marginTop: 10 }}>
                      <h4>{rate.adults} Adults, {rate.children} Children</h4>
                      {prepareCancellationPolicy(rate, checkrates.currency).map((policy, n) => (
                        <li key={n}>{policy}</li>
                      ))}
                    </div>
                  ))
                }
              </ul>
            ))
          }

          <div className="block-header">Remarks</div>
          {rateComments.length > 0 ? product.product_detail.checkrates.rooms.map((room) => room.rates.map((rate, index) => (
            <div key={index} style={{ marginTop: 10 }}>
              <h4>{rate.adults} Adults, {rate.children} Children</h4>
              {parseAndFilterRateComments(rate.rateCommentsId, rateComments, product.from_date).map((mesg: string, index: number) => (
                <div key={index}>{mesg}</div>
              ))}
            </div>

          ))) : 'No rate comments for this hotel.' }
          
        </>}
        {product.voucher != null && <>
        
        <table className="invoice_table_style"><tbody>
          <tr className="item_name">
            <td className="col1">Accommodation voucher</td>
            <td>{product.voucher.reference}</td>
          </tr>
        </tbody></table>

        <table className="invoice_table_style"><tbody>
          <tr className="header">
          <td className="col1">Client</td>
            <td>Nights</td>
            <td>Total Pax</td>
            <td>From</td>
            <td>To</td>
          </tr>
          <tr>
            <td className="col1">{product.voucher.holder.name} {product.voucher.holder.surname}</td>
            <td>{
                daysBetween(product.voucher.hotel.checkIn, product.voucher.hotel.checkOut)
              }</td>
            <td>{
              product.voucher.hotel.rooms.reduce((acc, room) => 
                room.rates.reduce((acc2, rate) => acc2 + rate.adults + rate.children, acc), 0)
            }</td>
            <td>{product.voucher.hotel.checkIn}</td>
            <td>{product.voucher.hotel.checkOut}</td>
          </tr>
        </tbody></table>

        <table className="invoice_table_style"><tbody>
          <tr className="header">
            <td className="col1">Room type</td>
            <td>Units</td>
            <td>Adults</td>
            <td>Children</td>
            <td>Board</td>
          </tr>
          {
            product.voucher.hotel.rooms.map((room, roomIndex) => 
              room.rates.map((rate, rateIndex) => 
              <tr key={roomIndex + '-' + rateIndex}>
                <td className="col1">{room.name}</td>              
                <td>{rate.rooms}</td>
                <td>{rate.adults}</td>
                <td>{rate.children}</td>
                <td>{rate.boardName}</td>
              </tr>
            ))
          }
        </tbody></table>
        <table className="invoice_table_style"><tbody>
          <tr className="header"><td className="col1">Room ID</td><td>Type</td><td>Age</td><td>Name</td>
          </tr>
          {
            product.voucher.hotel.rooms.map((room, roomIndex) => room.paxes.map((pax, paxIndex) => 
            <tr key={roomIndex + '-' + paxIndex}>
              <td className="col1">{pax.roomId}</td>
              <td>{pax.type}</td>
              <td>{pax.age != null ? pax.age : '-'}</td>
              <td>{pax.name != null && pax.name.trim() !== '' ? pax.name : 'TBA'}</td>
            </tr>))
          }
        </tbody></table>
        <div className="block-header">Remarks</div>
        { product.voucher.hotel.rooms.map((room) => room.rates.map((rate, index) => <div key={index}>{rate.rateComments}</div>)) }
        <div className="block-header">Payment information</div>
        <div>Payable through {product.voucher.hotel.supplier.name}, acting as agent for the service operating company, details of which can be provided upon request. VAT: {product.voucher.hotel.supplier.vatNumber} Reference: {product.voucher.reference}.</div>
        </>}
      </div>
    })}

  </div></div>}