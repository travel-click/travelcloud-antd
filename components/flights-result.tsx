import React from 'react'
import { formatCurrency, TcResponse, Cart } from '../travelcloud'
import { Button, Icon } from 'antd'
import moment from 'moment'
import { NoResult } from './no-result'
import {FlightSearch, FlightDetail} from '../types'
import Big from 'big.js';

export function ignoreTimezoneExtractTime( datetime, format = 'h:mm:ss a' ) {
  return moment( datetime.replace(/[+-][0-9][0-9]:[0-9][0-9]$/,'') ).format( format );
}

export function ignoreTimezoneExtractDate( datetime, format = 'Do MMMM[,] YYYY' ) {
  return moment( datetime.replace(/[+-][0-9][0-9]:[0-9][0-9]$/,'') ).format( format );
}

export function computePrice(cart: Cart, flight: FlightDetail, fareId: string, formater?: CallableFunction): {
  beforeDiscount: string,
  afterDiscount: string
} {
  if (typeof formater == 'undefined') formater = formatCurrency
  const fare = flight.pricings.find((fare) => fare.id == fareId)

  if (fare == null) {
    return {
      afterDiscount: "Invalid fare id",
      beforeDiscount: "Invalid fare id"}}

  if (cart == null) {
    return {
      afterDiscount: formater(fare.price),
      beforeDiscount: formater(fare.price)}}

  const product = cart.addFlight(flight, fareId, true)

  if (product == null) {
    return {
      afterDiscount: formater(fare.price),
      beforeDiscount: formater(fare.price)}
  }

  // more like "before discounts" and "after discounts"
  const beforeDiscount = product.items.filter((item) => parseInt(item['price']) > 0).reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))
  const afterDiscount = product.items.reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))

  return {
    afterDiscount: formater(afterDiscount),
    beforeDiscount: formater(beforeDiscount)}
}

export function renderFares(flightIndex: number, cart: Cart, flight: FlightDetail, onFareClick?: (index: number, flight: FlightDetail, fareId: string) => any) {
  const indexServices = (acc2, fare, fareDetailIndex) => {
    if (acc2[fareDetailIndex] == null) acc2[fareDetailIndex] = {
      origin: fare.segments[0].origin,
      destination: fare.segments[fare.segments.length - 1].destination,
      servicesSet: {}
    }
    const brand = fare.brand
    if (brand == null) return acc2
    const services = brand.services
    if (services == null) return acc2
    acc2[fareDetailIndex].servicesSet = services.reduce((acc3, service) => {
      acc3[service.name] = true
      return acc3
    }, acc2[fareDetailIndex].servicesSet)
    return acc2
  }

  const fares = flight.pricings
  var fareServicesIndexed = fares.reduce((acc, fare) => {
    return fare.fares.reduce(indexServices, acc)
  }, new Array(fares[0].fares.length))

  fareServicesIndexed = fareServicesIndexed.map(x => {
    x.servicesSet = Object.keys(x.servicesSet)
    return x
  })

  const cellStyle = {height: 50, borderBottom: "1px solid #aaa", padding: "0 10px", overflow: 'hidden', minWidth: '160px', textAlign: 'center' as any}
  const headerStyle = {backgroundColor: '#fed', fontWeight: 'bold' as any}

  return <div style={{margin: "30px", overflowX: 'auto'}}>
    <div style={{display: 'flex', flexDirection: 'row'}}>
      <div style={{display: 'block', width: 200, margin: '0 10px'}}>
        <div style={{height: 122, lineHeight: "122px", fontSize: 20, color: '#666', textAlign: 'center'}}>
        </div>
        {fareServicesIndexed.map((info, index) => {
          return <div key={index}>
            <div style={{...cellStyle, ...headerStyle, display: 'table'}}>
              <div style={{verticalAlign: 'middle', display: 'table-cell'}}>
                {info.origin.code} → {info.destination.code}
              </div>
            </div>
            {info.servicesSet.map(serviceName => <div key={serviceName} style={cellStyle}>{serviceName}</div>)}
          </div>})}
      </div>
      {fares.map((detail, i1) => {
        const computed = computePrice(cart, flight, detail.id)
        return <div key={i1} style={{width: 200, border: "2px solid #eee", borderRadius: 5, margin: '0 10px'}}>
          <div style={{height: 120, textAlign: 'center', padding: 15}}>
            <div style={{fontSize: 20, color: '#666'}}>{computed.afterDiscount}</div>
            <div style={{margin: 10}}><Button type="primary" onClick={() => onFareClick(flightIndex, flight, detail.id)}>Select</Button></div>
          </div>
        {detail.fares.map((fare, i2) => {
          const servicesIndexed = fare.brand == null || fare.brand.services == null ? {} : fare.brand.services.reduce((acc, service) => {
            acc[service.name] = service
            return acc
          }, {})

          const name = fare.brand != null && fare.brand.name != null ? fare.brand.name : getBestCabinFromSegments(fare.segments)
          var bookingCodes = fare.segments.map(x => x.booking_code).filter(x => x != null).join('/')
          if (bookingCodes.length !== 0) bookingCodes = "(" + bookingCodes + ")"
          return <div key={i2}>
            <div style={{...cellStyle, ...headerStyle, display: 'table'}}>
              <div style={{verticalAlign: 'middle', display: 'table-cell'}}>
                {name} {bookingCodes}
              </div>
            </div>
            {fareServicesIndexed[i2].servicesSet.map((serviceName, index) => {
              return <div key={index} style={{...cellStyle, textAlign: 'center', fontSize: 28}}>
                {servicesIndexed[serviceName] != null && servicesIndexed[serviceName].included === true ? <Icon style={{color: '#7C7'}} type="check" /> : <Icon style={{color: '#C77'}} type="close" />}
            </div>})}
          </div>})}
      </div>})}
    </div>
  </div>
}

export function getBestCabinFromSegments(segments: any[]): string {
  const decode = {
  'PremiumFirst' : 6,
  'First' : 5,
  'PremiumBusiness' : 4,
  'Business' : 3,
  'PremiumEconomy' : 2,
  'Economy' : 1,
  }

  return segments.reduce((acc, segment) => {
    if (acc == null) return segment.cabin
    if (decode[segment.cabin] > decode[acc]) return segment.cabin
    return acc
  }, null)
}

export function getOriginDestinationFromFareSegments(segments: any[]): {
  origin: {code: string, name: string},
  destination: {code: string, name: string},
} {
  const lastSegment = segments[segments.length - 1]
  return {
    origin: segments[0].origin,
    destination: segments[segments.length - 1].destination
  }
}

export function computeBaggage(fare): string | null {
  if (fare == null) return ""

  const baggage_display = []
  if (fare.baggage_weight_amount) {
    baggage_display.push(fare.baggage_weight_amount + " " + fare.baggage_weight_unit.toLocaleLowerCase())
  }
  if (fare.baggage_pieces && fare.baggage_pieces != 0) {
    if (fare.baggage_pieces > 1) {
      baggage_display.push(`${fare.baggage_pieces} Pieces`)
    } else {
      baggage_display.push(`${fare.baggage_pieces} Piece`)
    }
  }

  if (baggage_display.length == 0) return null
  return baggage_display.join(' x ')
}

function formatTime(minutes, hours, days) {
  let time_display = ""
  if (minutes === 0) {}
  else if (minutes === 1) time_display = "1 minute"
  else time_display = minutes + " minutes"

  if (hours === 0) {}
  else if (hours === 1) time_display = "1 hour " + time_display
  else time_display = hours + " hours " + time_display

  if (days === 0) {}
  else if (days === 1) time_display = "1 day " + time_display
  else time_display = days + " days " + time_display

  return time_display;
}

export function computeOd(od: FlightDetail['od1']): {
  departure_marketing_carrier: {code: string, name: string},
  departure_operating_carrier: {code: string, name: string},
  departure_flight_number: string,
  departure_airport: {code: string, name: string},
  departure_terminal: string | null,
  departure_datetime: string,
  departure_local_time: string,
  departure_local_date: string,
  arrival_airport: {code: string, name: string},
  arrival_datetime: string,
  arrival_local_time: string,
  arrival_local_date: string,
  arrival_terminal: string | null,
  total_time:{
    days: number,
    hours: number,
    minutes: number,
    display: string
  },
  /*
  stops_time:{
    days: number,
    hours: number,
    minutes: number,
    display: string
  },
  */
  stops: {
    count: number,
    display: string,
    display_with_airport_codes: string
  },
  stops_airport_codes: string[]
} {
  if (od.segments[0].flights != null) {
    const firstFlight = od.segments[0].flights[0]
    const lastSegment = od.segments[od.segments.length - 1]
    const lastFlight = lastSegment.flights[lastSegment.flights.length - 1]

    const departure_airport = firstFlight.departure_airport
    const departure_datetime = firstFlight.departure_datetime
    const departure_terminal = firstFlight.departure_terminal
    const departure_local_time = ignoreTimezoneExtractTime(departure_datetime)
    const departure_local_date = ignoreTimezoneExtractDate(departure_datetime)

    const arrival_airport = lastFlight.arrival_airport
    const arrival_datetime = lastFlight.arrival_datetime
    const arrival_terminal = lastFlight.arrival_terminal
    const arrival_local_time = ignoreTimezoneExtractTime(arrival_datetime)
    const arrival_local_date = ignoreTimezoneExtractDate(arrival_datetime)

    const duration = moment.duration(moment(arrival_datetime).diff(departure_datetime))
    const days = duration.days()
    const hours = duration.hours()
    const minutes = duration.minutes()
    const time_display = formatTime(minutes, hours, days)

    //const flightCount = od.segments.reduce((acc, segment) => acc + segment.flights.length, 0)
    const stops_airport_codes = od.segments.reduce((acc, segment) => acc.concat(segment.flights.map((flight => flight.departure_airport.code))), [])
    stops_airport_codes.shift()
    const stops_count = stops_airport_codes.length
    let stops_display = ''
    if (stops_count === 0) stops_display = "Direct"
    else if (stops_count === 1) stops_display = "1 stop"
    else stops_display = stops_count + " stops"

    var stops_display_with_airport_codes = stops_display
    if (stops_count > 0) stops_display_with_airport_codes += " (" + stops_airport_codes.join(', ') + ")"
    
    return {
      departure_operating_carrier: od.segments[0].operating_carrier,
      departure_marketing_carrier: od.segments[0].marketing_carrier,
      departure_flight_number: od.segments[0].flight_number,
      departure_airport,
      departure_terminal,
      departure_datetime,
      departure_local_time,
      departure_local_date,
      arrival_airport,
      arrival_terminal,
      arrival_datetime,
      arrival_local_time,
      arrival_local_date,
      total_time:{
        days,
        hours,
        minutes,
        display: time_display
      },
      stops: {
        count: stops_count,
        display: stops_display,
        display_with_airport_codes: stops_display_with_airport_codes
      },
      stops_airport_codes
    }
  }
  else if (od.segments[0].flights_info != null) {

    const lastSegment = od.segments[od.segments.length - 1]

    // the time and dates in flights_info is sometimes without timezone or inaccurate timezone (pkfare)
    // we assume that the arrival datetime of a segment has the same timezone as the departure datetime of the next segment
    const totalMinutes = od.segments.reduce((acc, seg, index) => {
      acc += seg.flights_info.total_minutes
      const nextSegment = od.segments[index + 1]
      if (nextSegment != null) {
        const arrivalMoment = moment(seg.flights_info.destination_datetime.replace(/[+-][0-9][0-9]:[0-9][0-9]$/,''))
        const departureMoment = moment(nextSegment.flights_info.origin_datetime.replace(/[+-][0-9][0-9]:[0-9][0-9]$/,''))
        acc += moment.duration(departureMoment.diff(arrivalMoment)).minutes()
      }
      return acc;
    }, 0);

    const days = Math.floor(totalMinutes/60/24)
    const hours = Math.floor((totalMinutes/60) % 24)
    const minutes = Math.floor(totalMinutes % 60)
    const time_display = formatTime(minutes, hours, days)

    const stops_airport_codes = od.segments.reduce((acc, segment) => {
      acc.push(segment.flights_info.origin.code)
      if (segment.flights_info.stops != null) acc.concat(segment.flights_info.stops.map(stop => stop.airport.code))
      return acc
    }, [])

    stops_airport_codes.shift()
    const stops_count = stops_airport_codes.length
    let stops_display = ''
    if (stops_count === 0) stops_display = "Direct"
    else if (stops_count === 1) stops_display = "1 stop"
    else stops_display = stops_count + " stops"

    var stops_display_with_airport_codes = stops_display
    if (stops_count > 0) stops_display_with_airport_codes += " (" + stops_airport_codes.join(', ') + ")"    

    return {
      departure_operating_carrier: od.segments[0].operating_carrier,
      departure_marketing_carrier: od.segments[0].marketing_carrier,
      departure_flight_number: od.segments[0].flight_number,
      departure_airport: od.segments[0].flights_info.origin,
      departure_terminal: od.segments[0].flights_info.origin_terminal,
      departure_datetime: od.segments[0].flights_info.origin_datetime,
      departure_local_time: ignoreTimezoneExtractTime(od.segments[0].flights_info.origin_datetime),
      departure_local_date: ignoreTimezoneExtractDate(od.segments[0].flights_info.origin_datetime),
      arrival_airport: lastSegment.flights_info.destination,
      arrival_terminal: lastSegment.flights_info.destination_terminal,
      arrival_datetime: lastSegment.flights_info.destination_datetime,
      arrival_local_time: ignoreTimezoneExtractTime(lastSegment.flights_info.destination_datetime),
      arrival_local_date: ignoreTimezoneExtractDate(lastSegment.flights_info.destination_datetime),
      total_time:{
        days,
        hours,
        minutes,
        display: time_display
      },
      stops: {
        count: stops_count,
        display: stops_display,
        display_with_airport_codes: stops_display_with_airport_codes
      },
      stops_airport_codes
    }
  }

  else {
    throw new Error("Either flights or flights_info must be defined")
  }
}


export const FlightsResult: React.StatelessComponent<{
  flights: FlightSearch,
  flightMap: {[key: string]: TcResponse<FlightDetail>},
  onFlightClick?: (index: number, flight: any) => any,
  onFareClick?: (index: number, flight: FlightDetail, fareId: string) => any,
  cart: Cart}>

  = ({flights, flightMap, onFlightClick, onFareClick, cart}) => {

    return <>{flights.map((flight, index) => {

      const od1Info = computeOd(flight.od1)
      const od2Info = flight.od2 != null ? computeOd(flight.od2) : null

      const computed = computePrice(cart, flight, flight.pricings[0].id)

      return <div className="ant-card" style={{padding: 16, border: '1px solid #e8e8e8'}} key={index}>
        <table style={{width: '100%'}}><tbody>
          <tr>
            <td style={{width: '70%'}}>
              <table style={{width: '100%'}}><tbody>
                <tr><th colSpan={3}>Departing flight</th></tr>
                <tr>
                  <td style={{width: '22%'}}>
                    <img src={`//cdn.net.in/static/airlines/${od1Info.departure_marketing_carrier.code}.gif`} />
                  </td>
                  <td style={{width: '22%'}}>
                    <div>
                      <span>{od1Info.departure_airport.code}</span>
                      <span> →</span>
                    </div>
                    <div>{od1Info.departure_local_time}</div>
                    <div>{od1Info.departure_local_date}</div>
                  </td>
                  <td style={{width: '22%'}}>
                    <div>
                      <span>{od1Info.arrival_airport.code}</span>
                    </div>
                    <div>{od1Info.arrival_local_time}</div>
                    <div>{od1Info.arrival_local_date}</div>
                  </td>
                  <td style={{width: '22%'}}>
                    <div>{od1Info.departure_marketing_carrier.code}{od1Info.departure_flight_number}</div>
                    <div>{od1Info.stops.display}</div>
                    <div>{od1Info.total_time.display}</div>
                  </td>
                </tr>
                {od2Info != null
                  && [<tr key="1"><th colSpan={3}>Return flight</th></tr>,
                    <tr key="2">
                      <td style={{width: '22%'}}>
                      <img src={`//cdn.net.in/static/airlines/${od2Info.departure_marketing_carrier.code}.gif`} />
                      </td>
                      <td style={{width: '22%'}}>
                        <div>
                        <span>{od2Info.departure_airport.code}</span>
                          <span> →</span>
                        </div>
                        <div>{od2Info.departure_local_time}</div>
                        <div>{od2Info.departure_local_date}</div>
                      </td>
                      <td style={{width: '22%'}}>
                        <div>
                          <span>{od2Info.arrival_airport.code}</span>
                        </div>
                        <div>{od2Info.arrival_local_time}</div>
                        <div>{od2Info.arrival_local_date}</div>
                      </td>
                      <td style={{width: '22%'}}>
                        <div>{od2Info.departure_marketing_carrier.code}{od2Info.departure_flight_number}</div>
                        <div>{od2Info.stops.display}</div>
                        <div>{od2Info.total_time.display}</div>
                      </td>
                    </tr>]}
              </tbody></table>
            </td>
            <td style={{width: '30%'}}>
              <div style={{margin: "20px 10px", fontSize: 16}}>{
                computed.beforeDiscount === computed.afterDiscount
                ? computed.beforeDiscount
                : <>
                    <span style={{textDecoration: 'line-through'}}>{computed.beforeDiscount}</span>
                    &nbsp;
                    <b>{computed.afterDiscount}</b>
                  </>}</div>
              <Button type="primary" disabled={flightMap[index] != null} onClick={() => onFlightClick(index, flight)}>Select flight</Button>
            </td>
          </tr>
          {flightMap[index] != null
            &&
              <tr>
                <td colSpan={4} style={{height:400}}>
                  {flightMap[index].result == null
                    ? <NoResult
                        response={flightMap[index]}
                        type="fares"
                        loadingMessage="Verifying prices and availability..." />
                    : renderFares(index, cart, flightMap[index].result, onFareClick)}
                </td>
              </tr>}
          </tbody></table>
      </div>
    })}</>}