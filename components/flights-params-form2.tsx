import React, {useRef, useEffect, useState} from 'react'
import moment from "moment";
import {
  Button,
  Radio,
  Icon,
  Collapse,
  Col,
  Row,
  Dropdown,
  Checkbox,
  Slider,
  Tooltip,
  Drawer,
  Divider,
  Popover
} from "antd";
import { TravelCloudClient, FlightsParams, identity, Cart } from '../travelcloud'
import {
  FlightCabinSelect,
  FlightsParamsForm,
  filterFlights,
  createFlightStopsFilter,
  createAirlinesFilter,
  useFlightFilters
} from "./flights/component-logic";

interface FlightsParamsAdvancedFormProps{
  onChange: (FlightParams) => void,
  onSearch: (FlightParams) => void,
  value: FlightsParams,
  client: TravelCloudClient,
  defaultAirportCodes?: string[],
  defaultCityCodes?: string[],
  airportsOnly?: boolean,
  loading?: boolean,
  flightParamStore: any, // fix for temporary
  children?: any,
  minLeadTime?: number,
  showOd4?: boolean
}

interface FlightSearchTriggerProps{
  departureCode: any, 
  returnCode: any, 
  flightsParam: any,
  windowSize: any,
  cabinType: any,
  numberOfTravellers: any,
  setSearchForm: (any) => any, 
  setFilterDrawer: (any) => any
}

interface SearchComponentDropDownProps{
  client: TravelCloudClient,
  flightsParam: FlightsParams,
  handleParamsChange: (one) => any,
  search: (one, two) => any,
  validateSearch: (one, two) => any,
  flights: any
  searchForm: boolean,
  setSearchForm: (one) => any,
  type: any,
  setType: (one) => any,
  setFlightType: (one) => any,
  didmountFlag: any,
  setDidmountFlag: (one) => any,
  flightTriggerProps: FlightSearchTriggerProps,
  flightParamStore: any // fix for
}

// fixme: onTypeChange() shouldn't trigger here
// fixme: why there is 2 types
export function FlightsParamsTypeSelect(props) {

  const onTypeChange = (value) => {
    props.flightParamStore.setType(value);
    //props.flightParamStore.setFlightType(value);
  }

  return (
    <Radio.Group
      value={props.flightParamStore.type}
      onChange={e => onTypeChange(e.target.value)}
    >
      <Radio.Button value="return">Return</Radio.Button>
      <Radio.Button value="one">One way</Radio.Button>
      <Radio.Button value="multi">Multi-city</Radio.Button>
    </Radio.Group>
  )
}

export function FlightsParamsAdvancedForm(props: FlightsParamsAdvancedFormProps){

  const rowStyle = {display: "flex", flexDirection: "row" as any, maxWidth: "1000px", margin: "20px 0"}

  const children = props.children ? props.children : (
    <React.Fragment>
      <FlightsParamsTypeSelect {...props} />
      {props.flightParamStore.type !== "multi" ? (
        <div style={rowStyle}>
          <div style={{ flex: "2" }}>
            <div>From</div>
            <FlightsParamsForm.Od1OriginCodeSelect style={{ width: "95%" }} />
          </div>
          <div style={{ flex: "0" }}>
            <div>&nbsp;</div>
          </div>
          <div style={{ flex: "2" }}>
            <div>To</div>
            <FlightsParamsForm.Od2OriginCodeSelect style={{ width: "95%" }} />
          </div>
          <div style={{ flex: "1" }}>
            <div>Depart</div>
            <FlightsParamsForm.Od1OriginDepartureDatePicker
              minLeadTime={props.minLeadTime}
              style={{ width: "95%" }}
            />
          </div>
          {props.flightParamStore.type === "return" && (
            <div style={{ flex: "1" }}>
              <div>Return</div>
              <FlightsParamsForm.Od2OriginDepartureDatePicker
                style={{ width: "95%" }}
              />
            </div>
          )}
        </div>
      ) : (
        <div>
          <div style={rowStyle}>
            <div style={{ flex: "2" }}>
              <FlightsParamsForm.Od1OriginCodeSelect style={{ width: "95%" }} />
            </div>
            <div style={{ width: "30px" }}>→</div>
            <div style={{ flex: "2" }}>
              <FlightsParamsForm.Od1DestinationCodeSelect
                style={{ width: "95%" }}
              />
            </div>
            <div style={{ flex: "1" }}>
              <FlightsParamsForm.Od1OriginDepartureDatePicker
                minLeadTime={props.minLeadTime}
                style={{ width: "95%" }}
              />
            </div>
          </div>
          <div style={rowStyle}>
            <div style={{ flex: "2" }}>
              <FlightsParamsForm.Od2OriginCodeSelect style={{ width: "95%" }} />
            </div>
            <div style={{ width: "30px" }}>→</div>
            <div style={{ flex: "2" }}>
              <FlightsParamsForm.Od2DestinationCodeSelect
                style={{ width: "95%" }}
              />
            </div>
            <div style={{ flex: "1" }}>
              <FlightsParamsForm.Od2OriginDepartureDatePicker
                style={{ width: "95%" }}
              />
            </div>
          </div>
          {
            props.showOd4 &&
            <>
              <div style={rowStyle}>
                <div style={{ flex: "2" }}>
                  <FlightsParamsForm.Od3OriginCodeSelect style={{ width: "95%" }} />
                </div>
                <div style={{ width: "30px" }}>→</div>
                <div style={{ flex: "2" }}>
                  <FlightsParamsForm.Od3DestinationCodeSelect
                    style={{ width: "95%" }}
                  />
                </div>
                <div style={{ flex: "1" }}>
                  <FlightsParamsForm.Od3OriginDepartureDatePicker
                    style={{ width: "95%" }}
                  />
                </div>
              </div>
              <div style={rowStyle}>
                <div style={{ flex: "2" }}>
                  <FlightsParamsForm.Od4OriginCodeSelect style={{ width: "95%" }} />
                </div>
                <div style={{ width: "30px" }}>→</div>
                <div style={{ flex: "2" }}>
                  <FlightsParamsForm.Od4DestinationCodeSelect
                    style={{ width: "95%" }}
                  />
                </div>
                <div style={{ flex: "1" }}>
                  <FlightsParamsForm.Od4OriginDepartureDatePicker
                    style={{ width: "95%" }}
                  />
                </div>
              </div>
            </>
          }
        </div>
      )}
      <div style={rowStyle}>
        <div style={{ flex: "1" }}>
          <div>Adults</div>
          <FlightsParamsForm.PtcAdtSelect style={{ width: "95%" }} />
        </div>
        <div style={{ flex: "1" }}>
          <div>Children</div>
          <FlightsParamsForm.PtcCnnSelect style={{ width: "95%" }} />
        </div>
        <div style={{ flex: "1" }}>
          <div>Infants</div>
          <FlightsParamsForm.PtcInfSelect style={{ width: "95%" }} />
        </div>
        <div style={{ flex: "2" }}>
          <div>Cabin</div>
          <FlightCabinSelect style={{ width: "95%" }} />
        </div>
        <div style={{ flex: "2" }}>
          <div>&nbsp;</div>
          <Button
            type="primary"
            disabled={props.loading === true}
            onClick={() => props.onSearch(props.value)}
            style={{ width: "95%" }}
          >
            Search
          </Button>
        </div>
      </div>
    </React.Fragment>
  )

  return (
    <FlightsParamsForm {...props}>
       {children} 
    </FlightsParamsForm>
  );
}

export function FlightsChangeParamsAdvancedForm(props: FlightsParamsAdvancedFormProps){

  const rowStyle = {display: "flex", flexDirection: "row" as any, maxWidth: "1000px", margin: "20px 0"}

  const children = props.children ? props.children : (
    <React.Fragment>
      <FlightsParamsTypeSelect {...props} />
      {props.flightParamStore.type !== "multi" ? (
        <div style={rowStyle}>
          <div style={{ flex: "2" }}>
            <div>From</div>
            <FlightsParamsForm.Od1OriginCodeSelect style={{ width: "95%" }} />
          </div>
          <div style={{ flex: "0" }}>
            <div>&nbsp;</div>
          </div>
          <div style={{ flex: "2" }}>
            <div>To</div>
            <FlightsParamsForm.Od2OriginCodeSelect style={{ width: "95%" }} />
          </div>
          <div style={{ flex: "1" }}>
            <div>Depart</div>
            <FlightsParamsForm.Od1OriginDepartureDatePicker
              minLeadTime={props.minLeadTime}
              style={{ width: "95%" }}
            />
          </div>
          {props.flightParamStore.type === "return" && (
            <div style={{ flex: "1" }}>
              <div>Return</div>
              <FlightsParamsForm.Od2OriginDepartureDatePicker
                style={{ width: "95%" }}
              />
            </div>
          )}
        </div>
      ) : (
        <div>
          <div style={rowStyle}>
            <div style={{ flex: "2" }}>
              <FlightsParamsForm.Od1OriginCodeSelect style={{ width: "95%" }} />
            </div>
            <div style={{ width: "30px" }}>→</div>
            <div style={{ flex: "2" }}>
              <FlightsParamsForm.Od1DestinationCodeSelect
                style={{ width: "95%" }}
              />
            </div>
            <div style={{ flex: "1" }}>
              <FlightsParamsForm.Od1OriginDepartureDatePicker
                minLeadTime={props.minLeadTime}
                style={{ width: "95%" }}
              />
            </div>
          </div>
          <div style={rowStyle}>
            <div style={{ flex: "2" }}>
              <FlightsParamsForm.Od2OriginCodeSelect style={{ width: "95%" }} />
            </div>
            <div style={{ width: "30px" }}>→</div>
            <div style={{ flex: "2" }}>
              <FlightsParamsForm.Od2DestinationCodeSelect
                style={{ width: "95%" }}
              />
            </div>
            <div style={{ flex: "1" }}>
              <FlightsParamsForm.Od2OriginDepartureDatePicker
                style={{ width: "95%" }}
              />
            </div>
          </div>
          {
            props.showOd4 &&
            <>
              <div style={rowStyle}>
                <div style={{ flex: "2" }}>
                  <FlightsParamsForm.Od3OriginCodeSelect style={{ width: "95%" }} />
                </div>
                <div style={{ width: "30px" }}>→</div>
                <div style={{ flex: "2" }}>
                  <FlightsParamsForm.Od3DestinationCodeSelect
                    style={{ width: "95%" }}
                  />
                </div>
                <div style={{ flex: "1" }}>
                  <FlightsParamsForm.Od3OriginDepartureDatePicker
                    style={{ width: "95%" }}
                  />
                </div>
              </div>
              <div style={rowStyle}>
                <div style={{ flex: "2" }}>
                  <FlightsParamsForm.Od4OriginCodeSelect style={{ width: "95%" }} />
                </div>
                <div style={{ width: "30px" }}>→</div>
                <div style={{ flex: "2" }}>
                  <FlightsParamsForm.Od4DestinationCodeSelect
                    style={{ width: "95%" }}
                  />
                </div>
                <div style={{ flex: "1" }}>
                  <FlightsParamsForm.Od4OriginDepartureDatePicker
                    style={{ width: "95%" }}
                  />
                </div>
              </div>
            </>
          }
        </div>
      )}
      <div style={rowStyle}>
        <div style={{ flex: "2" }}>
          <div>Cabin</div>
          <FlightCabinSelect style={{ width: "95%" }} />
        </div>
        <div style={{ flex: "2" }}>
          <div>&nbsp;</div>
          <Button
            type="primary"
            disabled={props.loading === true}
            onClick={() => props.onSearch(props.value)}
            style={{ width: "95%" }}
          >
            Search
          </Button>
        </div>
      </div>
      
    </React.Fragment>
  )

  return (
    <FlightsParamsForm {...props}>
       {children} 
    </FlightsParamsForm>
  );
}

//fixme: stateless component into stateful component
export function FlightSearchTrigger({
  departureCode, returnCode, flightsParam, windowSize, cabinType, numberOfTravellers, setSearchForm, setFilterDrawer
}: FlightSearchTriggerProps){
  return (
    <div className="flight-search-mobile-header-container">
      <Row type="flex" gutter={16}>
        <Col xs={24} lg={8}>
          <div className="flight-search-mobile-header">
            <h2 className="color-primary">
              <strong>{departureCode}</strong>
            </h2>
            {/* <SvgIcon
              name="plane"
              width="40"
              height="40"
              fill="#d02c2f"
              style={{ margin: "0 30px" }}
            /> */}
            <h2
              style={{
                display: "inline-block",
                color: returnCode ? "#D02C2F" : "#ccc"
              }}
            >
              <strong>{returnCode || "---"}</strong>
            </h2>
          </div>
        </Col>
        <Col
          xs={24}
          lg={10}
          style={{
            display: "flex",
            alignItems: "center",
            marginBottom: windowSize.width < 992 ? 10 : 0
          }}
        >
          {windowSize.width < 1200 ? (
            <div style={{ textAlign: "center", width: "100%" }}>
              <strong className="color-primary">
                {moment(flightsParam["od1.origin_datetime"]).format(
                  "DD/MM/YYYY"
                )}
              </strong>
              &nbsp;-&nbsp;
              <strong className="color-primary">
                {moment(flightsParam["od2.origin_datetime"]).format(
                  "DD/MM/YYYY"
                )}
              </strong>
              ,&nbsp;
              <div>
                <strong>
                  {cabinType[flightsParam.cabin]}, &nbsp;
                  {numberOfTravellers} &nbsp;
                  {numberOfTravellers > 1 ? "Travellers" : "Traveller"}
                </strong>
              </div>
            </div>
          ) : (
            <div>
              <div>
                <div style={{ display: "inline-block", margin: "0 10px" }}>
                  Depart:{" "}
                  <strong className="color-primary">
                    {moment(flightsParam["od1.origin_datetime"]).format(
                      "DD/MM/YYYY"
                    )}
                  </strong>
                </div>
                <div style={{ display: "inline-block", margin: "0 10px" }}>
                  Return:{" "}
                  <strong className="color-primary">
                    {moment(flightsParam["od2.origin_datetime"]).format(
                      "DD/MM/YYYY"
                    )}
                  </strong>
                </div>
              </div>
              <div className="color-primary" style={{ margin: "0 10px" }}>
                <strong>
                  {cabinType[flightsParam.cabin]}, &nbsp;
                  {numberOfTravellers} &nbsp;
                  {numberOfTravellers > 1 ? "Travellers" : "Traveller"}
                </strong>
              </div>
            </div>
          )}
        </Col>
        <Col
          xs={24}
          lg={6}
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: windowSize.width < 992 ? "center" : "flex-end"
          }}
        >
          <Button
            size="large"
            onClick={() => 
              setSearchForm(currentSearchForm => !currentSearchForm)
            }
          >
            MODIFY SEARCH
          </Button>
          {windowSize.width < 992 && (
            <Button
              onClick={() => setFilterDrawer(true)}
              size="large"
              style={{ marginLeft: 20 }}
            >
              FILTER
            </Button>
          )}
        </Col>
      </Row>
    </div>
  );
}

// fixme: provide type
// todo: do we need separate state store for sort
export function SortButtons({ sort, setSort }):any {
  return (
    <Col xs={24} lg={16} style={{ margin: "0 -5px" }}>
      <Button
        size="small"
        className={
          sort === "default"
            ? "flights-sort-tab active"
            : "flights-sort-tab"
        }
        onClick={() => setSort("default")}
      >
        Recommended
      </Button>
      <Button
        size="small"
        className={
          sort === "cheapest"
            ? "flights-sort-tab active"
            : "flights-sort-tab"
        }
        onClick={() => setSort("cheapest")}
      >
        Cheapest Flight
      </Button>
      <Button
        size="small"
        className={
          sort === "quickest"
            ? "flights-sort-tab active"
            : "flights-sort-tab"
        }
        onClick={() => setSort("quickest")}
      >
        Quickest Flight
      </Button>
    </Col>
  )
}

// todo: provide type for flights
// todo: provide type for query
type FlightResultWrapperType = {
  flights: any, 
  customPriceDisplay: CallableFunction, 
  query: any,
  handleFilters: (v1,v2) => void, 
  resultElement?: (props: { filters: any, handleFilters: (v1,v2) => void, airlines: any[], flightStops: any[] }) => any, 
  resultElementProps?: { 
    formatToHours: (input) => string, 
    flightType: string 
  },
  cart: Cart,
  airlineCharLimit?: any 
}

export function FilterResultWrapper({
  flights, customPriceDisplay, query, 
  handleFilters, resultElement, resultElementProps, cart,airlineCharLimit=14
} : FlightResultWrapperType) {
  
  // if(airlineCharLimit == null){

  //   const airlineCharLimit = 14
  //   console.log("hello")
  // }
  // console.log(airlineCharLimit)
  const flightStops = createFlightStopsFilter( flights, cart, customPriceDisplay ),
    airlines = createAirlinesFilter( flights, cart, customPriceDisplay, airlineCharLimit );

  const [ localFilters, handleLocalFilters ] = useFlightFilters( query )

  const updateLocalFilters = (v1, v2) => { 
    handleLocalFilters(v1,v2);
    setTimeout(() => handleFilters(v1,v2) , 300)
  }

  if (resultElementProps == null) {
    resultElementProps = {
      formatToHours: identity,
      flightType: 'one'
    }
  }

  return resultElement === null ? (
    <FilterResults
      flightStops={flightStops}
      handleFilters={updateLocalFilters}
      filters={localFilters}
      formatToHours={resultElementProps.formatToHours}
      flightType={resultElementProps.flightType}
      airlines={airlines}
    />
  ) : resultElement({ filters: localFilters, handleFilters: updateLocalFilters, flightStops, airlines })
}

// fixme: provide type
export function ResultNumber({ filteredFlights, limit }) {
  return filteredFlights ? (
    <Col xs={24} lg={8}>
      <p style={{ textAlign: "right", marginBottom: 4 }}>
        {filteredFlights.length < 21 ? (
          <small>{filteredFlights.length} results</small>
        ) : (
          <small>
            showing{" "}
            <a
              onClick={() => console.log('load more scrolling...')
                //need dependencies scroll-to-element
                //Ref: https://www.npmjs.com/package/scroll-to-element
                // scrollToElement("#load-more", {
                //   offset: 200,
                //   ease: "in-out-circ",
                //   duration: 1000
                // })
              }
            >
              {limit}
            </a>{" "}
            of {filteredFlights.length} results
          </small>
        )}
      </p>
    </Col>
  ) : null;
}

export function FilterResults({ flightStops, handleFilters, filters, formatToHours, flightType, airlines }
  : { flightStops: any, 
    handleFilters:(v1:any, v2:any) => void, 
    filters: any, 
    formatToHours: (val:any) => any,
    flightType: string, airlines: any[]  }) {

  //elements
  const $clearAirlines = (
    <div style={{ marginBottom: 5, textAlign: "right" }}>
      <small
        style={{ cursor: "pointer" }}
        onClick={() => handleFilters('airlines', [])}
      >
        clear
      </small>
    </div>
  );

  return (
    <Collapse
      className="collapsible-filters round-checkboxes vertical-checkboxes"
      bordered={false}
      defaultActiveKey={["stops", "price", "outbound", "return", "airlines"]}
      // expandIcon={({ isActive }) => (
      //   <SvgIcon
      //     name={isActive ? "up" : "down"}
      //     width="10"
      //     height="10"
      //     fill="rgba(0,0,0,.6)"
      //   />
      // )}
    >
      <Collapse.Panel key="stops" header="Stops">
        <Checkbox.Group
          className="vertical-checkboxes"
          options={flightStops}
          onChange={value => handleFilters("stops", value)}
          value={filters.stops}
        />
      </Collapse.Panel>
      <Collapse.Panel key="outbound" header="Outbound Timing">
        <div>
          Departure &nbsp;
          {filters.outbound_departure.length > 0 ? (
            <span>
              {`0${filters.outbound_departure[0]}:00`.slice(-5)} -{" "}
              {`0${filters.outbound_departure[1]}:00`
                .slice(-5)
                .replace("24:00", "23:59")}
            </span>
          ) : (
            <span>00:00 - 23:59</span>
          )}
        </div>
        <Slider
          range
          min={0}
          max={24}
          defaultValue={filters.outbound_departure.length > 0 ? [filters.outbound_departure[0], filters.outbound_departure[1]] : [0,24]}
          onChange={range => handleFilters("outbound_departure", range)}
          tipFormatter={formatToHours}
        />
        <div>
          Arrival &nbsp;
          {filters.outbound_arrival.length > 0 ? (
            <span>
              {`0${filters.outbound_arrival[0]}:00`.slice(-5)} -{" "}
              {`0${filters.outbound_arrival[1]}:00`
                .slice(-5)
                .replace("24:00", "23:59")}
            </span>
          ) : (
            <span>00:00 - 23:59</span>
          )}
        </div>
        <Slider
          range
          min={0}
          max={24}
          defaultValue={filters.outbound_arrival.length > 0 ? [filters.outbound_arrival[0], filters.outbound_arrival[1]] : [0,24]}
          onChange={range => handleFilters("outbound_arrival", range)}
          tipFormatter={formatToHours}
        />
      </Collapse.Panel>
      {flightType !== "one" && (
        <Collapse.Panel key="return" header="Return Timing">
          <div>
            Departure &nbsp;
            {filters.return_departure.length > 0 ? (
              <span>
                {`0${filters.return_departure[0]}:00`.slice(-5)} -{" "}
                {`0${filters.return_departure[1]}:00`
                  .slice(-5)
                  .replace("24:00", "23:59")}
              </span>
            ) : (
              <span>00:00 - 23:59</span>
            )}
          </div>
          <Slider
            range
            min={0}
            max={24}
            defaultValue={filters.return_departure.length > 0 ? [filters.return_departure[0], filters.return_departure[1]] : [0,24]}
            onChange={range => handleFilters("return_departure", range)}
            tipFormatter={formatToHours}
          />
          <div>
            Arrival &nbsp;
            {filters.return_arrival.length > 0 ? (
              <span>
                {`0${filters.return_arrival[0]}:00`.slice(-5)} -{" "}
                {`0${filters.return_arrival[1]}:00`
                  .slice(-5)
                  .replace("24:00", "23:59")}
              </span>
            ) : (
              <span>00:00 - 23:59</span>
            )}
          </div>
          <Slider
            range
            min={0}
            max={24}
            defaultValue={filters.return_arrival.length > 0 ? [filters.return_arrival[0], filters.return_arrival[1]] : [0,24]}
            onChange={range => handleFilters("return_arrival", range)}
            tipFormatter={formatToHours}
          />
        </Collapse.Panel>
      )}
      <Collapse.Panel key="price" header="Price">
        <div>
          From &nbsp;
          {filters.price_range.length > 0 ? (
            <span>
              S${filters.price_range[0].toLocaleString()} - $
              {filters.price_range[1].toLocaleString()}
            </span>
          ) : (
            <span>S$0 - S$5,000</span>
          )}
        </div>
        <Slider
          range
          min={0}
          max={5000}
          defaultValue={filters.price_range.length > 0 ? [filters.price_range[0], filters.price_range[1]] : [0,5000]}
          step={100}
          onChange={value => handleFilters("price_range", value)}
          tipFormatter={value => {
            return `$${value.toLocaleString()}`;
          }}
        />
      </Collapse.Panel>
      <Collapse.Panel key="airlines" header="Airlines">
        {$clearAirlines}
        <Checkbox.Group
          className="vertical-checkboxes"
          options={airlines}
          onChange={value => handleFilters("airlines", value)}
          value={filters.airlines}
        />
      </Collapse.Panel>
    </Collapse>
  );
}

// fixme: what's the purpose of this function, just wrapper ?
export function SearchComponentDrawer(props: FlightSearchTriggerProps) {
  return (
    <Col xs={24} xl={15}>
      <FlightSearchTrigger {...props} />
    </Col>
  );
}


export function Steps({ windowSize }: { windowSize: any }){
  return (
    <Col
      xs={24}
      xl={9}
      style={{
        display: "flex",
        justifyContent: windowSize.width < 1200 ? "center" : "flex-end",
        alignItems: "center",
        padding: windowSize.width < 1200 ? "0 0 20px" : "20px 0"
      }}
    >
      <small>
        <div className="steps">
          <div className="step">
            <div className="step-num step-active">1</div>
            <h5>Flights</h5>
          </div>
          {!(windowSize.width < 992) && (
            <Icon
              type="double-right"
              style={{ fontSize: 14, margin: "0 10px" }}
            />
          )}
          <div className="step">
            <div className="step-num">2</div>
            <h5>Travellers</h5>
          </div>
          {!(windowSize.width < 992) && (
            <Icon
              type="double-right"
              style={{ fontSize: 14, margin: "0 10px" }}
            />
          )}
          <div className="step">
            <div className="step-num">3</div>
            <h5>Checkout</h5>
          </div>
          {!(windowSize.width < 992) && (
            <Icon
              type="double-right"
              style={{ fontSize: 14, margin: "0 10px" }}
            />
          )}
          <div className="step">
            <div className="step-num">4</div>
            <h5>Confirmation</h5>
          </div>
        </div>
      </small>
    </Col>
  );
}

export function SearchComponentDropdown({
  client,
  flightsParam,
  handleParamsChange,
  search,
  validateSearch,
  flights,
  searchForm,
  setSearchForm,
  type,
  setType,
  setFlightType,
  didmountFlag,
  setDidmountFlag,
  flightTriggerProps,
  flightParamStore
}: SearchComponentDropDownProps) {
  return (
    <Col xs={24} xl={15}>
      <Popover
        title={null}
        trigger="click"
        placement="bottomLeft"
        overlayStyle={{
          background: "#fff",
          boxShadow: "0 0 8px rgba(0,0,0,.1)",
          zIndex: 0
        }}
        visible={searchForm}
        onVisibleChange={visible => setSearchForm(visible)}
        content={
          <div className="flight-form-container" style={{ width: "100%" }}>
            <FlightsParamsAdvancedForm
              client={client}
              flightParamStore={flightParamStore}
              // searchMode={searchMode}
              // travellers={openTravellers}
              // toggleTravellers={toggleTravellers}
              // onSwapClick={() => handleSwap()}
              onChange={value => handleParamsChange(value)}
              onSearch={value => search(client, flightsParam)}
              value={flightsParam}
              defaultAirportCodes={[
                "SIN",
                "BKK",
                "KUL",
                "HKG",
                "LHR",
                "HND",
                "PVG"
              ]}
              defaultCityCodes={["NYC"]}
              loading={flights.loading === true}
              // windowWidth={windowSize.width}
            />
          </div>
        }
      >
        <FlightSearchTrigger {...flightTriggerProps} />
      </Popover>
    </Col>
  );
}