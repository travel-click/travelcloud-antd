import React, { useState, useEffect } from "react"
import { Modal, Spin, Row, Col, Tabs } from "antd"
import { Icon } from "antd";
import GoogleMapReact from 'google-map-react';
import { mergeImageTypes, useRoomCharacteristicMap } from "../hooks/hotelbeds";
import { TravelCloudClient, groupBy, firstCapitalLastLower } from "../../travelcloud";

export const Lightbox = ({ visible, onCancel, uri }) => {

  const [ imageLoading, setImageLoading ] = useState(true)

  useEffect(()=> {

    if( visible ) {
      let image = new Image()
      setImageLoading(true)
      image.onload = () => setImageLoading(false)
      image.src = uri
    }

  }, [ visible ])

  return (
    <Modal 
      closable={true}
      footer= {false}
      visible={visible}
      destroyOnClose={true}
      centered
      bodyStyle={{width: 'auto',
              height: 'auto'}}
      onCancel={onCancel}
    >
      {imageLoading ?
        <div style={{ textAlign: 'center',}}> <Spin  /> </div>: 
        <img src={uri} width="85%" height="auto" />}
    </Modal>
  )
}

export const Map = (center: { latitude: string, longitude: string, googleAPIKey: string }) => {

  const lat:number = parseFloat( center.latitude );
  const lng:number = parseFloat( center.longitude );
  const AnyReactComponent = ({ lat,lng}) =>      
     <Icon type="environment" className="map-icon" style={{ fontSize: '3em' }} theme="twoTone" twoToneColor="#eb2f96" />
  
  return (<div style={{ height: '50vh', width: '100%' }}>
    <GoogleMapReact
      bootstrapURLKeys={{ key: center.googleAPIKey }}
      defaultCenter={{ lat, lng }}
      defaultZoom={16} >
        <AnyReactComponent
            lat={lat}
            lng={lng}
          />
    </GoogleMapReact>
    
  </div>);
}

export const ImagesTab = ({ images, imageTypes }: { images: Array<any>, imageTypes: any }): any => {

  const thumbnailUrlPrefix = 'http://photos.hotelbeds.com/giata/medium/';
  const urlPrefix = 'http://photos.hotelbeds.com/giata/bigger/';
  let groupedImages = mergeImageTypes( images, imageTypes )

  const [ path, setPath ] = useState('')

  return (groupedImages.length > 0 ? <Row>
    <Lightbox visible={path != ''} onCancel={() => setPath('')} uri={path} />
    <Col span={8}>
      <img width="100%" src={groupedImages[0][0] ? urlPrefix + groupedImages[0][0].path : ''} />
    </Col>
    <Col span={15} offset={1}>
      {groupedImages.map((images, index: number) => (
        <div key={index}>
          <h4>{images[0].description.content}</h4>
          <div>
            {images.map((x, index) => (
              <div key={index} style={{
                  backgroundImage: `url(${thumbnailUrlPrefix}${x.path})`,
                  height: '65px',
                  width: '100px',
                  backgroundSize: 'cover',
                  display: 'inline-block',
                  margin: '0 2px 0 2px',
                  backgroundPosition: 'center'
                }} 
                onClick={() => { setPath(urlPrefix + x.path) }}
              />
            ))}
          </div>

        </div>
      ))}
    </Col>
  </Row> : null)
}

export const ImagesTab2 = ({ images, imageTypes, client }: 
  { images: Array<any>, imageTypes, client: TravelCloudClient }):any => {

  const thumbnailUrlPrefix = 'http://photos.hotelbeds.com/giata/medium/';
  const urlPrefix = 'http://photos.hotelbeds.com/giata/bigger/';
  
  const [path, setPath] = useState('')

  const {TabPane} = Tabs

  let groupedImages = mergeImageTypes( images.filter(x => x.imageTypeCode !== 'HAB'), imageTypes )
  let roomImages = groupBy(images.filter(x => x.imageTypeCode === 'HAB'), x => x.characteristicCode )
    
  let roomTypes = roomImages.map(room => room[0].characteristicCode )
    .filter(x=> x !== undefined)

  const map = useRoomCharacteristicMap( roomTypes, client )

  return (groupedImages.length > 0 ? <Row>
    <Lightbox visible={path != ''} onCancel={() => setPath('')} uri={path} />
    <Col span={24} offset={1}>
     <Tabs defaultActiveKey="1" tabPosition="left">

        {roomImages.map((images, index: number)=> (
          (Object.keys(map).map((x)=> (
               x === images[0].characteristicCode && map[x] != null ? 
               <TabPane tab={`Room (${firstCapitalLastLower(map[x])})`} key={`room-${index + 1}`} >
                    {images.map((x, index) => (
                        <div key={index} style={{
                          backgroundImage: `url(${thumbnailUrlPrefix}${x.path})`,
                          height: '65px',
                          width: '100px',
                          backgroundSize: 'cover',
                          display: 'inline-block',
                          margin: '0 2px 0 2px',
                          backgroundPosition: 'center'
                        }}
                        onClick={() => { setPath(urlPrefix + x.path) }}
                        />
                    ))}
                </TabPane>      : null
            )))
        ))}

        {groupedImages.map((images, index: number) => (
          
          <TabPane tab={images[0].description.content} key={`other-${index + 1}`}>
            {console.log("images in groupedImage",images)}
            {/* <h4>{images[ 0 ].description.content}</h4> */}
            <div>
              {images.map((x, index) => (
                  <div key={index} style={{
                    backgroundImage: `url(${x.thumbnail})`,
                    height: '65px',
                    width: '100px',
                    backgroundSize: 'cover',
                    display: 'inline-block',
                    margin: '0 2px 0 2px',
                    backgroundPosition: 'center'
                  }}
                  onClick={() => setPath(x.photo) }
                  />
                ))}
            </div>
          </TabPane>
        ))}
      </Tabs>

    </Col>
  </Row> : null)
}
