import React, {useState} from 'react';
import { Icon, Button } from 'antd'

import HotelbedsPassenger from '../components/hotelbeds-passenger-form';

interface PassengerContentInterface {
  hotel: any;
  room: any;
  rates: any;
  onAddToCart: any;
}

const PassengerContent = (props: PassengerContentInterface):any => {
  const { hotel, room, rates, onAddToCart } = props;
  const [ bookingForm, setBookingForm ] = useState({})
  const [ isFormValidated, setIsFormValidated ] = useState(false)

  return (
    <React.Fragment>
      <section>
        <HotelbedsPassenger
          key={rates.map(({rateKey}:any)=> rateKey).join(',')}
          item={{ hotel, room, rates }}
          isFormValidated={(flag) => setIsFormValidated(flag)}
          onUpdate={(bookingForm) => setBookingForm( bookingForm )}
        />
      </section>

      <section style={{marginTop: "20px"}}>
        <Button
          onClick={() => onAddToCart(bookingForm)}
          disabled={!isFormValidated}
        >
          <Icon type="shopping-cart" /> Checkout &gt;
        </Button>
      </section>
    </React.Fragment>
  )
}

export default PassengerContent;