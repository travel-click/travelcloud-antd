import React from 'react'
import {formatCurrency, Cart, objectEntries} from '../travelcloud'
import { Order as OrderType } from '../types'

function notEmpty(x) {
  return x != null && x !== "" && x !== '0000-00-00' && x !== '0000-00-00 00:00:00'
}

type Schedule = {
  // multiple schedules may have the same time
  time: string, // for flight, this is derieved from product_detail
  product_type: string, // used to determine how we parse product_detail
  product_source: string, // used to determine how we parse product_detail
  products: OrderType['products'] // used for display in component
}

export type ItineraryDay = {
  // all schedules with same dates are grouped
  date: string,
  schedules: Schedule[]
}

// this function arranges scheduled activities chronologically and filters out things without schedules
// rendering everything is done separately
export const orderToItineraryDays = (order: OrderType): ItineraryDay[] => {
  // transient type
  type ScheduleSingle = {
    date: string,
    time: string,
    product_name: string,
    product_type: string, 
    product_source: string,
    product: OrderType['products'][0]
  }
  
  const scheduleSingles: ScheduleSingle[] = order.products.reduce((acc, product) => {
    if (product.product_type === "nontour_product") {
      if (product.from_date == null) return acc
      acc.push({
        date: product.from_date,
        time: product.from_time,
        product_name: product.product_name,
        product_type: product.product_type,
        product_source: product.product_source,
        product: product
      })
    }
    return acc
  }, [])

  const groupedByDate: {[date: string]: ScheduleSingle[]} = scheduleSingles.reduce((acc, ss) => {
    if (acc[ss.date] == null) acc[ss.date] = []
    acc[ss.date].push(ss)
    return acc
  }, {})

  const groupedByDateTimeProduct: ItineraryDay[]
    = Object.entries(groupedByDate)
      .sort((kvp1, kvp2) => kvp1[0] > kvp2[0] ? 1 : 0)
      .map((kvp) => {
        const groupedByTimeProduct: {[key: string]: ScheduleSingle[]} = kvp[1].reduce((acc, ss) => {
          const timekey = ss.time || 'NO TIME'
          const key = timekey + '%%' + ss.product_name + '%%' + ss.product_source + '%%' + ss.product_type
          if (acc[key] == null) acc[key] = []
          acc[key].push(ss)
          return acc
        }, {})

        const schedules
          = Object.entries(groupedByTimeProduct)
            .sort((kvp1, kvp2) => kvp1[0] > kvp2[0] ? 1 : 0)
            .map(kvp => {
              return {
                time: kvp[1][0].time,
                product_type: kvp[1][0].product_type,
                product_source: kvp[1][0].product_source,
                products: kvp[1].reduce((acc, ss) => {acc.push(ss.product); return acc;}, [])
              }
            })

        return {
          date: kvp[0],
          schedules
        }
      })

  return groupedByDateTimeProduct
}

export const Itinerary = (
  {itineraryDays} : {itineraryDays: ItineraryDay[]}) => {

  // properties in the outermost dive may not be rendered when
  // nextjs replaces client rendered dom with server rendered dom
  // add additional div outside to workaround this
  return <div><div className="tc-invoice">

    {itineraryDays.map((itineraryDay, index) => {
      return <table key={index} className="invoice_table_style"><tbody>
        <tr className="item_name">
          <td colSpan={2} className="col1">
            {itineraryDay.date}
          </td>
        </tr>
        <tr className="header">
          <td className="col1">Time</td>
          <td className="col1">Description</td>
        </tr></tbody>

        {itineraryDay.schedules.map((schedule, index) => {
          if (schedule.product_type === 'nontour_product') {
            return <tr>
              <td className="col1">
                {schedule.time}
              </td>
              <td className="col1">
                {schedule.products[0].product_name} &gt; {schedule.products[0].items[0].name}
              </td>
            </tr>
          }
        })}
      </table>
    })}
    
  </div></div>}