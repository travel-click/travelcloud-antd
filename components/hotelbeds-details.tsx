import React, { useMemo, useState, useEffect } from 'react';

import CarouselArrow from './carousel-arrow';
import { Row, Col, Typography, Card, Carousel, Statistic, Icon } from 'antd';
import moment from 'moment';
import { TravelCloudClient } from '../travelcloud';
import { useHotelbedsRateComments } from './hooks/hotelbeds-rate-comments'
import { loadRateComments, parseAndFilterRateComments } from './voucher'
import { prepareCancellationPolicy, RateClassInfo, mergeFacilityTypes } from './hooks/hotelbeds';
import { Facility } from './hotelbeds-results'

const HotelbedsDetail = (
  {
    hotel,
    room,
    rates,
    client,
    checkIn,
    checkOut,
    types,
    children = null
  }: {
    hotel: any,
    room: any,
    rates: any,
    client: TravelCloudClient,
    checkIn: any,
    checkOut: any,
    types: any,
    children?: (params: {
      hotel: any,
      room: any,
      rates: any,
      facilities: any,
      images: any,
      imageUrlPrefix: any,
      checkIn: any,
      checkOut: any,
      controller: any
    }) => any
  }) => {

  const { states, controller } = useHotelbedsRateComments({ hotel, rates, client })
  useEffect(() => controller.fetchRateComments(), [hotel])

  const imageUrlPrefix = 'http://photos.hotelbeds.com/giata/';

  const photoSlides = {
    arrows: true,
    prevArrow: <CarouselArrow classname='slick-arrow slick-prev' theme='filled' iconType='left-circle' />,
    nextArrow: <CarouselArrow classname='slick-arrow slick-next' theme='filled' iconType='right-circle' />,
    dots: false,
    autoplay: false,
    centerMode: true,
    variableWidth: true
  }

  const formatDate = (dateStr: string) => moment(dateStr).format('DD/MM/YYYY')

  // fixme: need to refactor, move all logics to somewhere else
  let roomData = hotel.details.rooms.find(({ roomCode }) => roomCode === room.code);
  let facilities = roomData.roomFacilities != null ? mergeFacilityTypes(roomData.roomFacilities, types.facilityTypes) : []

  const images = hotel != null && hotel.details != null && hotel.details.images != null ?
    hotel.details.images
      .filter(photo => photo.characteristicCode != null)
      .filter(photo => photo.characteristicCode.toLowerCase() === roomData.characteristicCode.toLowerCase())
      .map(photo => ({ url: imageUrlPrefix + photo.path, description: photo.description }))
    : [];

  // add in library
  const findStars = (text) => {
    let stars;
    if (!!text.match(/\d.+/)) {
      stars = text.replace(/\D/g, "");
      return parseInt(stars);
    } else return null;
  };

  const params = {
    hotel,
    room,
    rates,
    facilities,
    images,
    imageUrlPrefix,
    checkIn,
    checkOut,
    controller
  }

  if (children != null) {
    return children(params)
  } else {
    return (
      <div id="hotelbeds-details">
        <h1>
          {hotel.name}{" "}
          {[...Array(findStars(hotel.categoryName))].map((i, index) => (
            <img src="/static/star.png" />
          ))}
          {hotel.categoryName.includes("HALF") && <img src="/static/half-star.png" />}
        </h1>
        <h5>{hotel.categoryName}</h5>
        <span>{hotel.details.address.content}</span>
        <span>{hotel.details.city.content}</span>
        <p>Zip: {hotel.details.postalCode}</p>
        <Typography>{hotel.destinationName}</Typography>
        <Typography>From {formatDate(hotel.checkIn)} To {formatDate(hotel.checkOut)}</Typography>
        <Carousel className='hotel-photos' {...photoSlides}>
          {images.map((photo, key) =>
            <div key={key}>
              <div style={{ position: 'relative' }}>
                <img src={photo.url} height={300} />
                {photo.description != null && <div style={{ position: 'absolute', bottom: 0, left: 0, right: 0, padding: 10, textAlign: 'center' }}>{photo.description.content}</div>}
              </div>
            </div>
          )}
        </Carousel>

        <Card
          style={{ marginTop: "5px" }}
          title={`${room.name} (${rates[0].boardName})`}>

          <small>
            {rates[0]['rateClass'] !== 'NOR' && <Icon type="exclamation-circle" style={{ paddingRight: 5 }} />}
            {RateClassInfo[rates[0]['rateClass']]}
          </small>

          {facilities != null && (
            <Row>
              <Col>
                {facilities.map((group, index) => (
                  <>
                    <Typography.Text strong>{group.description.content}</Typography.Text>
                    <ul key={index} style={{ listStyleType: "none" }}>
                      {group.facilities.map((facility, index) => (
                        <Facility key={index} facility={facility} checkIn={checkIn} checkout={checkOut} />
                      ))}
                    </ul>
                  </>
                ))}
              </Col>
            </Row>
          )}

          {rates.map((rate: any, index: number) => (
            <Row key={index}>
              <Col span={8}>
                <b>Cancellation Charges</b>
                {prepareCancellationPolicy(rate, hotel.currency).map((policy, index) => (
                  <React.Fragment key={index}>
                    <Typography>{policy}</Typography>
                  </React.Fragment>
                ))}

                {rate.promotions != null && rate.promotions.length > 0 ? (
                  <React.Fragment>
                    <Typography.Text strong>Promotions</Typography.Text>
                    <ul>
                      {rate.promotions.map((p, index) => (
                        <li key={index}>{p.name}</li>
                      ))}
                    </ul>
                  </React.Fragment>
                ) : null}

                {rate.offers != null && rate.offers.length > 0 ? (
                  <React.Fragment>
                    <Typography.Text strong>Offers</Typography.Text>
                    <ul>
                      {rate.offers.map((p, idx) => (
                        <li key={idx}>{p.name} {hotel.currency} {p.amount}</li>
                      ))}
                    </ul>
                  </React.Fragment>
                ) : null}
              </Col>
              <Col span={14}>
                <Row>
                  <Col span={6}>
                    <Statistic title="" value={rate.rooms} suffix=" rooms" />
                  </Col>
                  <Col span={6}>
                    <Statistic title="" value={rate.adults} suffix=" adults" />
                  </Col>
                  <Col span={6}>
                    <Statistic title="" value={rate.children} suffix=" children" />
                  </Col>
                  <Col span={6}>
                    <div style={{ textAlign: 'right', paddingTop: 5 }}>
                      <div>
                        {rate._view.hasDiscount && (
                          <Typography.Text delete>
                            {hotel.currency}
                            {rate._view.beforeDiscount.toString()}
                          </Typography.Text>
                        )}
                      </div>
                      <div>
                        <Typography.Text strong>
                          {hotel.currency}
                          {rate._view.afterDiscount.toString()}
                        </Typography.Text>
                      </div>
                    </div>
                  </Col>




                </Row>

                {parseInt(rate.children) > 0 && <p>Children Ages: {rate.childrenAges}</p>}
              </Col>
              <Col span={6}>
                {rate.rateCommentsId != null && (
                  <React.Fragment>
                    <b>Rate Comments</b>
                    {controller.getRateCommentsById(rate.rateCommentsId, checkIn).map((mesg: string, index: number) => (
                      <div key={index}>{mesg}</div>
                    ))}
                  </React.Fragment>
                )}
              </Col>
            </Row>
          ))}
        </Card>
      </div>
    )
  }
}

export default HotelbedsDetail;