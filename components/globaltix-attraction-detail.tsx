import React from 'react'
import { Modal, Button } from 'antd';

/* Use ANTD Modal */
const AttractionModal = ({ data, handleClose, show }) => {

  return (
    <Modal
      title="Attraction Detail"
      visible={show}
      onCancel={handleClose}
      width={800}
      footer={null}
    >
      <p><img alt="No Image" width="100%" src={"https://uat-api.globaltix.com/api/image?name=" + data.imagePath} /></p>
      <h4>{data.title}</h4>
      <p>{data.description}</p>
      <p><hr /></p>
      <p>Operating Hours</p>
      <p>{data.hoursOfOperation}</p>
    </Modal>

  );
};
export default AttractionModal;