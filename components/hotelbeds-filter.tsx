import React, {useEffect, useState, useRef} from "react";
import { Form, Input, Col, Checkbox, Slider } from "antd";
import {useDebounceEffect} from "./hooks/debounce-effect";

const HotelBedsFilter = ({
  filters,
  boardCodes,
  categories,
  setName,
  setBoardCodeParams,
  setCategoryParams,
  setPriceRange,
  minPrice,
  maxPrice
}) => {

  const [ name, updateName ] =  useState('')
  useDebounceEffect(() => setName( name ), [ name ], 500)

  let step = 100
  let _min = minPrice - minPrice % step
  let _max = maxPrice - maxPrice % step + step

  return (
    <div>
      <Form.Item label="Hotel Name">
        <Input value={name} placeholder="Search" onChange={(e) => updateName(e.target.value)} />
      </Form.Item>
      
      {boardCodes.length > 0 &&
        <Form.Item label="Board">
          {boardCodes.map(b_c => (
            <Col key={b_c} span={24}>
              <Checkbox checked={filters.boardCodes.includes(b_c)} onChange={(e) => setBoardCodeParams(b_c)}>{b_c}</Checkbox>
            </Col>
          ))}
        </Form.Item>
      }

      {categories.length > 0 && 
        <Form.Item label="Category">
          {categories.map(c => (
            <Col key={c} span={24}>
              <Checkbox checked={filters.categories.includes(c)} onChange={(e) => setCategoryParams(c)}>{c}</Checkbox>
            </Col>
          ))}
        </Form.Item>
      }

      <Form.Item label="Price (S$)">
        <strong style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center'
        }}>
          {`${_min} <=> ${_max}`}
        </strong>

        <Slider
          min={_min}
          max={_max}
          range={true}
          step={step}
          defaultValue={[_min, _max]}
          marks={{ [_min]: _min, [_max]: _max }}
          onAfterChange={setPriceRange}
        />
      </Form.Item>
    </div>
  );
};

export default HotelBedsFilter;
