# travelcloud-antd

TravelCloud workflows implemented using ant design react components.

## Getting started
npx create-next-app --ts  
npm i travelcloud-antd antd@3

## Breaking Changes
### 0.2.0
1. New way of Initialization TravelCloudClient
  - TravelCloudClient Constructor accepts config object instead of tcUser
  ```typescript
  let client = new TravelCloudClient(config) 
  ```

### 0.1.6
1. Tour booking component
  - Inlined some CSS styles
  - Remove e.preventDefault() from onSubmit callback
2. First parameter of initialTourBookingForm() change from tour.id to tour
  - Before: initialTourBookingForm(props.query.id)
  - After: tourBookingForm: initialTourBookingForm(props.tour.result)

### 0.1.12
1. New way of passing price_rules to Cart in _app
  ```
  import { loadGetInitialProps } from 'next-server/dist/lib/utils'

  // in the App component
  static async getInitialProps({ Component, ctx }) {
    const client = new TravelCloudClient(config.tcUser)
    const pageProps = await loadGetInitialProps(Component, ctx)
    const priceRules = await client.priceRules({is_public: '1'})
    return { pageProps, priceRules }
  }

  componentDidMount() {
    this.setState({
      cart: new Cart({
        client: this.client,
        priceRules: this.props.priceRules,
        onOrderChange: (order) => this.setState({ order }),
        onCustomerChange: (customer) => this.setState({ customer }),
        onPriceRulesChange: (priceRules) => this.setState({ priceRules })
      })
    })
  }
  ```

  ### 0.1.24
  Updated CSS for GlobalTix. Replaced all p and span tags with div tags.

  ### 0.1.37
  1. Refactor flight search components to use hooks
  2. Refactor country selection in checkout form

  ### 0.1.41
  CheckoutForm component now requires order and tcUser properties. This is to support airline customer loyalty inputs for travelers.