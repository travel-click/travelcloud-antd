# Migration Guide from v0.1 to v0.2

## New way of initializing TravelCloudClient
Need to change all the TravelCloudClient Initialization project wide
since its constructor accepts config object instead of tc user

v0.1.x
```typescript
import config from '../customize/config'
let client = new TravelCloudClient(config.tcUser)
```

v0.2.x
```typescript
import config from '../customize/config'
import TravelCloudClientConfig from 'travelcloud-antd/travelcloud'
let client = new TravelCloudClient(config)
```

Config object must follow the type `TravelCloudClientConfig` defined 
in `travelcloud-antd/travelcloud`


## i18n Translation and Integration with Translation API (optional)
