import React, { useEffect } from "react";
import { Tabs, Button, Col, Row, Select } from "antd";
import { useRouter } from "next/router";
import { NoResult } from "../components/no-result";
import { formatCurrency, range } from "../travelcloud";
import Big from 'big.js';
import { PaxProps, SegmentProps, useFlightAncillaries } from "../components/hooks/flight-ancillaries";
import { SeatMap } from "./flight-seats";

const SegmentTab = ({
  segment,
  key,
  selectedPax,
  setSelectedPax,
  segmentSeatSelection,
  setSegmentSeatSelection,
  forEachPax
}: SegmentProps) => {
  
  return <Tabs.TabPane tab={segment.origin.code + ' -> ' + segment.destination.code} key={key}>
    <Row>
      <Col span={24} className="pax-list">
        <table>
          <tbody>
            <tr>
              <th style={{width: '15%'}}></th>
              <th style={{width: '25%'}}>Weight</th>
              <th style={{width: '20%'}}>Oversized</th>
              <th style={{width: '20%'}}>Pieces</th>
              <th style={{width: '20%'}}>Seats</th>
            </tr>
            {forEachPax(({
              key, selected,
              alcXbag, alcXbagValue, setAlcXbagValue,
              alcBulk, alcBulkValue, setAlcBulkValue,
              alcPiec, alcPiecValue, setAlcPiecValue,
            }: PaxProps) => {
              return [
              //<tr><td colSpan={5}><pre>{JSON.stringify({key, selected, alcXbag, alcXbagValue, alcBulk, alcBulkValue, alcPiec, alcPiecValue}, null, 2)}</pre></td></tr>,
              <tr key={key} className={selected ? "selected" : ""}>
                <th>{key}</th>
                <td>
                  {alcXbag != null &&
                    <Select style={{width: '90%'}} value={alcXbagValue} onChange={setAlcXbagValue}>
                      <Select.Option value={0}>No extra weight</Select.Option>
                      {
                        range(10).map((x) => {
                        var amt = (x + 1) * 5
                        var price = Big(amt).times(alcXbag.unit_price).toFixed(2)
                        return <Select.Option key={x} value={amt}>+{amt} {alcXbag.name.split(" ")[1]} ({formatCurrency(price)})</Select.Option>
                      })
                      }
                    </Select>
                  }
                  {alcXbag == null &&
                    <span>Not available</span>
                  }
                </td>
                <td>
                  {alcBulk != null &&
                    <Select style={{width: '90%'}} value={alcBulkValue} onChange={setAlcBulkValue}>
                      <Select.Option value={0}>No oversized</Select.Option>
                      <Select.Option value={1}>Oversized (+{formatCurrency(alcBulk.unit_price)})</Select.Option>
                    </Select>
                  }
                  {alcBulk == null &&
                    <span>Not available</span>
                  }
                </td>
                <td>
                  {alcPiec != null &&
                    <Select style={{width: '90%'}} value={alcPiecValue} onChange={setAlcPiecValue}>
                      <Select.Option value={0}>No extra piece</Select.Option>
                      <Select.Option value={1}>Extra piece (+{formatCurrency(alcPiec.unit_price)})</Select.Option>
                    </Select>
                  }
                  {alcPiec == null &&
                    <span>Not available</span>
                  }
                </td>
                <td>{segmentSeatSelection[key] == null ? 'None' : (segmentSeatSelection[key].row_number + segmentSeatSelection[key].column_id)} (<span className="seat-selection" onClick={() => setSelectedPax(key)}>{selected ? "Select on seat map" : "Change"}</span>)</td>
              </tr>]})}
          </tbody>
        </table>
      </Col>
    </Row>
    <Row>
      <Col span={24} className="seat-map">
        {segment.seats_maps?.map((sm, index) => <SeatMap seatPricings={segment.seats_pricings} selectedPax={selectedPax} seatMap={sm} key={index} segmentSeatSelection={segmentSeatSelection} setSegmentSeatSelection={setSegmentSeatSelection} />)}
      </Col>
    </Row>
  </Tabs.TabPane>
}

const FlightSeatsPage = ({client, cart}) => {
  const router = useRouter()
  const query = router.query
  const flightSeatsState = useFlightAncillaries({client})
  const {flightAncillaries, setFlightAncillaries, getAllSelections} = flightSeatsState

  var loaded = false
  
  useEffect(() => {
    ;(async function() {
      try {
        if (router.query.source == null || router.query.product_source_ref == null || loaded) return

        setFlightAncillaries({loading: true})
        loaded = true

        //flight_change_ancillaries probably doesn't work
        const endpoint = router.query['od1.id'] == null ? 'flight_change_services' : 'flight_change_ancillaries'
        
        /*
        const response = await fetch(
            `http://localhost:3030/static/flight_change_ancillaries.json`
        );
        const json = await response.json();        
        */
        console.log({query, rq: router.query})
        const json = await client.get('rpc/' + endpoint, router.query)

        if (json.result == null) {
          setFlightAncillaries({error: true})
        } else {
          setFlightAncillaries(json)
        }

      } catch (e) {
        setFlightAncillaries({error: true})
        console.error(e);
      }

      })();
    }, [query]) 

  return <div className="main-container">
    <Button style={{float:'right'}} onClick={() => {
      var {seats, services} = getAllSelections()
      var addResult = null

      if (router.query['od1.id'] == null) {
        addResult = cart
          .reset()
          .addFlightChangeServices(flightAncillaries.result, router.query.source, router.query.product_source_ref, {services, seats})

      } else {
        addResult = cart
          .reset()
          .addFlightChangeWithAncillaries(flightAncillaries.result, flightAncillaries.result.pricings[0].id, router.query.product_source_ref, {seats})
      }
      
      if (addResult != null) router.push("/checkout")
    }}>Next Step</Button>
    <div style={{clear: 'both'}}></div>
    {flightAncillaries.result == null && (
      <NoResult
        response={flightAncillaries}
        type="flights"
        loadingMessage="Loading flight ancillaries..."
      />)}
    {flightAncillaries.result != null && <Tabs>
      {flightSeatsState.forEachFareSegment(SegmentTab)}
      <Tabs.TabPane tab="Fare rules" key="rules">
        {flightAncillaries?.result?.pricings[0].fare_rules.map((rule) => {
          return <>
            <h3>{rule.origin_code} -&gt; {rule.destination_code} ({rule.fare_basis} - {rule.name})</h3>
            {rule.rules.map((txt) => {
              return <>
                <h4>{txt.category_name}</h4>
                <div>{txt.text}</div>
              </>
            })}
          </>
        })}
      </Tabs.TabPane>
    </Tabs>}
  </div>
}

export default FlightSeatsPage
