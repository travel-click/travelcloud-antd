import React from 'react'
import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import { TravelCloudClient } from "../travelcloud"
import { TcDocument } from '../components/tc-document'
import config from '../customize/config'

const cmsdemo2 = ({ contents }) => {
  return <>
    <Head>
      <TcDocument contents={contents} blockId="__head" />
    </Head>
    <h1>CMS Demo2</h1>
    <TcDocument contents={contents} blockId="body.html" />
    <Link href='/cmsdemo'>
      <a>Go to Demo 1</a>
    </Link>
  </>
}

cmsdemo2.getInitialProps = async ({ asPath }) => {
  const client = new TravelCloudClient(config)
  const contents = await client.getDocument(asPath)
  return {contents}
}

export default cmsdemo2