import config from "../customize/config";
import { TravelCloudClient, redirectToPayment } from "../travelcloud";
import { Layout, Row, Col, Card, Button, Icon, Avatar, Select } from "antd";
import { Order } from "../components/order";
import React from "react";
import Big from "big.js";
import PaymentIframe from "../components/payment-iframe";
import { Spin } from "antd";

export default class extends React.PureComponent<any, any> {
  client = new TravelCloudClient(config);
  state: any = {
    loading: true,
    noRef: false,
    order: null
  };

  async componentDidMount() {
    const ref = new URL(location.href).searchParams.get("ref");
    if (ref == null) {
      this.setState({
        loading: false,
        noRef: true
      });
    } else {
      const order = await this.client.order({ ref });
      this.setState({
        loading: false,
        order
      });
    }
  }

  async payWithAccountCredit() {
    const ref = new URL(location.href).searchParams.get("ref");
    if (ref == null) {
      this.setState({
        loading: false,
        noRef: true
      });
      return;
    }
    this.setState({
      loading: true
    });
    await this.props.cart.payOrderWithCustomerCredit(ref);
    const order = await this.client.order({ ref });
    this.setState({
      loading: false,
      order
    });
  }
  render() {
    const cart = this.props.cart;
    const order = this.props.order.result;

    if (this.state.loading) {
      return (
        <div
          style={{
            minHeight: 1000,
            backgroundColor: "#fff",
            margin: "64px auto",
            padding: 64,
            maxWidth: 1600
          }}
        >
          <Card loading={true} style={{ border: 0 }}></Card>
        </div>
      );
    }

    if (
      this.state.noRef ||
      (this.state.order != null && this.state.order.result == null)
    ) {
      return (
        <div
          style={{
            minHeight: 1000,
            backgroundColor: "#fff",
            margin: "64px auto",
            padding: 64,
            maxWidth: 1600
          }}
        >
          <h1>Unable to load order</h1>
        </div>
      );
    }

    const customer = this.props.customer && this.props.customer.result;
    const bigAccountBalance =
      customer == null
        ? Big(0)
        : this.props.customer.result.payments
            .reduce((acc, payment) => acc.plus(payment.amount), Big(0))
            .times(-1);

    return (
      <Layout
        style={{
          backgroundColor: "#fff",
          margin: "64px auto",
          padding: 64,
          maxWidth: 1600
        }}
      >
        <Row>
          {this.state.order.result.order_status === "Canceled" && (
            <Col span={10}>
              <h1>Your order has expired</h1>
            </Col>
          )}
          {this.state.order.result.order_status === "Invoice" && (
            <Col span={10}>
              <h1>Your payment has been received</h1>
              <p>
                Thank you for your order! Our team is processing your order.
              </p>
              <p>Please check your email for a copy of the invoice.</p>
            </Col>
          )}
          {this.state.order.result.order_status === "Quotation" && (
            <Col span={10}>
              <PaymentIframe
                orderRef={this.state.order.result.ref}
                tcUser={config.tcUser}
                paymentProcessor="pay_2c2p_v4"
                amount={this.state.order.result.payment_required}
                successPage="payment-successful"
                failurePage="payment-failed"
                name="payment-processor"
                className="iframe-container"
                style={{
                  display: "block",
                  boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)",
                  transition: "0.3s",
                  width: "100%",
                  marginLeft: "auto",
                  marginRight: "auto",
                  height: "1000px"
                }}
                scrolling={false}
                // onSuccess={(query) => console.log('successful ...', query)}
                // onFailure={(query, error) => console.log('failed ...', query, error)}
              >
                <div style={{ width: "100%", marginTop: 32, textAlign: "center" }}>
                  <Spin size="large" style={{ margin: "auto" }} />
                  &nbsp; &nbsp; Loading payment UI...
                </div>
              </PaymentIframe>
            </Col>
          )}
          <Col span={12} offset={2}>
            <Order order={this.state.order.result} />
          </Col>
        </Row>
      </Layout>
    );
  }
}
