import React, { Fragment, useState, useEffect } from 'react'
import { Row, Col, Icon, Card, Button, Tabs, Popover, Carousel, Drawer } from 'antd'
import { TravelCloudClient, formatCurrency, Cart, validateLegacyHotelParams } from '../travelcloud'
import qs from 'qs'
import config from '../customize/config'
import Head from 'next/head'
import Link from 'next/link'
import { NoResult } from '../components/no-result'
import CarouselArrow from '../components/carousel-arrow'
import imageExists from 'image-exists'
import moment from 'moment'
import Typography from 'antd/lib/typography/Typography';
import { HotelbedsQuery, HotelbedsFormValue } from '../order-product-compute';
import HotelbedsPassenger from '../components/hotelbeds-passenger-form'
import { useHotelbeds } from '../components/hooks/hotelbeds'

const HotelDetailPage = ({ query, client, cart, openCart }) => {

  const [ availability, loading ] = useHotelbeds({ formValue: query, client, cart })
  const hotels:any = availability.hotels == null ? [] : availability.hotels
  const details:any = hotels.length == 0 ? {} : hotels[0].details

  const [ bookingForm, setBookingForm ] = useState({})
  const [ isFormValidated, setIsFormValidated ] = useState(false)
  const [ passengerDrawerVisible, setPassengerDrawerVisible ] = useState( false )

  const imageUrlPrefix = 'http://photos.hotelbeds.com/giata/';
  const images = details != null && details.images != null ? 
    details.images.map(photo => ({ url: imageUrlPrefix + photo.path, description: photo.description }))
    : [];

  const searchURI:string = "hotelbeds-hotels?" + qs.stringify( query )

  // todo: check whether the response for rate check should possibly contain only one room and rate 
  const addToCart = () => {
    setPassengerDrawerVisible( false )

    hotels.forEach((hotel:any) => {
      cart.addHotelbedsHotel( hotel, bookingForm )
    })

    openCart()
  }

  const photoSlides = {
    arrows: true,
    prevArrow: <CarouselArrow classname='slick-arrow slick-prev' theme='filled'  iconType='left-circle' />,
    nextArrow: <CarouselArrow classname='slick-arrow slick-next' theme='filled' iconType='right-circle' />,
    dots: false,
    autoplay: false,
    centerMode: true,
    variableWidth: true
  }

  const formatDate = (dateStr:string) => moment(dateStr).format('DD/MM/YYYY')

  return (
    <div className="main-container">
      <Head><title>{config.defaultTitle} | Hotel</title></Head>
      {loading || hotels.error != null || hotels.length == 0
        ? <div className='wrap pad-y'>
            <NoResult response={hotels} loading={loading} type='hotel'>
              Cannot load selected hotel. Please&nbsp;
              <Link href={searchURI}>
                <a>select a different hotel</a>
              </Link>
              &nbsp;or different dates.
            </NoResult>
          </div> : <div>
            <h1>{hotels[0].name}</h1>
            <Typography>{hotels[0].destinationName}</Typography>
            <Typography>From {formatDate(hotels[0].checkIn)} To {formatDate(hotels[0].checkOut)}</Typography>
            <Carousel className='hotel-photos' {...photoSlides}> 
              {images.map((photo,key) =>
                <div key={key}>
                  <div style={{position:'relative'}}>
                    <img src={photo.url} height={300} />
                    {photo.description.length > 0 && <div style={{position:'absolute',bottom:0,left:0,right:0,color:'#fff',background:'rgba(0,0,0,.5)',padding:10,textAlign:'center'}}>{photo.description}</div>}
                  </div>
                </div>
              )}
            </Carousel>
            <section>
            {hotels.map((hotel, index) => (
              <Card key={index} 
                style={{marginTop: "5px"}}
                title={hotel.rooms[0].name} 
                extra={hotel.currency + ' ' + hotel.rooms[0].rates[0].net}>
                <Row>
                  <Col span={12}>
                    <b>Cancellation Charges</b>
                    {hotel.rooms[0].rates[0].cancellationPolicies.map((policy, index) => (
                      <React.Fragment key={index}>
                        <Typography>Before {formatDate( policy.from )}: Free</Typography>
                        <Typography>After {formatDate( policy.from )}: {policy.amount}</Typography>
                      </React.Fragment>
                    ))}
                  </Col>
                  <Col span={12}>
                    <Typography>
                      {hotel.rooms[0].rates[0].rateComments}
                    </Typography>
                  </Col>
                </Row>
                <Row>
                  <Col span={4}>

                  </Col>
                </Row>
              </Card>
            ))}
            </section>
            <section>
              <Card style={{marginTop: "1em"}}>
                <Button onClick={() => setPassengerDrawerVisible(true)}>Fill up passengers' information</Button>
              </Card>
            </section>
            <Drawer placement="right" visible={passengerDrawerVisible} onClose={() => setPassengerDrawerVisible( false )} width={1024}>
              <section>
                {/* temporary */}
                {/* <HotelbedsPassenger  isFormValidated={setIsFormValidated} onUpdate={(bookingForm) => setBookingForm( bookingForm ) } /> */}
                <Card style={{marginTop: "5px"}}>
                <Button onClick={addToCart} disabled={!isFormValidated}><Icon type="shopping-cart" /> Add to Cart</Button>
                </Card>
              </section>
            </Drawer>

          </div>}
    </div>
  )
}

HotelDetailPage.getInitialProps = async (context)
  : Promise<{ query:HotelbedsQuery }> => {
  const query: HotelbedsQuery = qs.parse(context.asPath.split(/\?/)[1]);
  return { query }
}

export default HotelDetailPage