import React from 'react'
import { Row, Col, Carousel } from 'antd'
import { TravelCloudClient, TcResponse, Cart, extractValueFromFormState } from '../travelcloud'
import config from '../customize/config'
import { v2_PrivateTours } from '../types'
import { NextPage, NextPageContext } from 'next'
import Link from 'next/link'

interface Context extends NextPageContext {
    cart: any
  }

const PrivateToursPage: NextPage<{privateTours: TcResponse<v2_PrivateTours>, cart: Cart}> = (props) => {
    const client = new TravelCloudClient(config)

    return <div className="main-container">
      <h2>Private tours</h2>
      {props.privateTours.result != null && props.privateTours.result.length > 0 && props.privateTours.result.map((privateTour) => {
        return <div><Link href={"/private-tour?id=" + privateTour.id}>{privateTour.name}</Link></div>
      })}
      {(props.privateTours.result == null || props.privateTours.result.length == 0) && <div>No tours found</div>}
    </div>

}


PrivateToursPage.getInitialProps = async (context: Context) => {
    const client = new TravelCloudClient(config)
    const privateTours = await client.privateTours()
    const query = context.query
    const cart = context.cart
    return { privateTours, query, cart }
  }
  
  export default PrivateToursPage