import React from 'react'
import Head from 'next/head'
import useTCi18n from '../components/hooks/tc-i18n'
import { Button } from 'antd'
import { Trans } from 'react-i18next'

export function Index(props: any): any {
  
  const {changeLocale, t} = useTCi18n()

  let count = 2
  let resultCount = 100

  let user = "gmx"

  return (
    <>
      {/* use of t() function */}
      <Head>
        <title>{t('title.Hotel', { count })}</title>
      </Head>
      <div>
        {/* how to change language */}
        <Button onClick={() => changeLocale('en-US')}>English</Button>
        <Button onClick={() => changeLocale('zh-CN')}>Chinese</Button>
        <Button onClick={() => changeLocale('my')}>Burmese</Button>
        <Button onClick={() => changeLocale('fr')}>French</Button>

        {/* interpolation  */}
        <h3>
          <Trans i18nKey="result.Hotel" count={count}>
            Hey {{user}} Found <b>{{count}}</b> Hotels
          </Trans>
        </h3>

        {/* context support */}
        {/* "A boyfriend" */}
        <p>{t('friend', {context: 'male', count: 1})}</p> 

        {/* "A girlfriend" */}
        <p>{t('friend', {context: 'female', count: 1})}</p> 

        {/* "100 boyfriends" */}
        <p>{t('friend', {context: 'male', count: 100})}</p> 

        {/* "100 girlfriends" */}
        <p>{t('friend', {context: 'female', count: 100})}</p> 


        <h1>{t('title.Second')}</h1>
        <h1>{t('Hey how are you?')}</h1>
        <h1>{t('How are you getting on?')}</h1>

        <h1>{t('Dancing with your ghosts')}</h1>
        <h1>{t('Againsts all odds')}</h1>
        <h1>{t('Heal the World')}</h1>

         
        <h1>{t('Log in')}</h1>
        <h1>{t('Log out')}</h1>
        <h1>{t('Register')}</h1>
        <h1>{t('Checkout')}</h1>
      </div>
    </>
  )
}


export default Index