import { Button, Icon, Input, Select } from "antd";
import React, { useContext } from "react";
import { TcResponse, TravelCloudClient } from "../../travelcloud";
import { SiteContext } from "../_app";
import config from '../../customize/config'
import { useOutboundBooking } from "../../components/hooks/outbound";

const { Option } = Select;

const OutBoundPage = ({ tourTypeData, obPriceRules, obDepositRules } : { tourTypeData: TcResponse<any>, obPriceRules, obDepositRules}) => {
  const defaultRoom = {
    adults: 1,
    children: 0,
    childrenNoBed: 0,
    infants: 0,
  };

  const site = useContext(SiteContext)
  const client = new TravelCloudClient(config)

  const {
    roomList,
    priceCheckRes,
    tourMaster, setTourMaster,
    addRoom, minusRoom,
    adultOnChange, childWithBedOnChange, childWithoutBedOnChange, infantOnChange,
    addToCart
  } = useOutboundBooking(client, site.cart, defaultRoom, obPriceRules, obDepositRules)

  if (tourTypeData == null)
    return (
      <div>Loading...</div>
    )

  const firstResult = tourTypeData.result?.[0]

  if (firstResult == null)
    return (
      <div>
        <h1>Tour not found</h1>
      </div>
    );

  return (
    <div style={{ margin: "32px" }}>
      <h1>{firstResult.briefDescription}</h1>
      <h2>Select departure</h2>
      <table><tbody>
        <tr>
          <th>Code</th>
          <th>Date</th>
          <th>Airline</th>
          <th>TWN</th>
          <th>Seats</th>
          <th></th>
        </tr>
      {
      firstResult.tourMasters.map((tm, key) => {
        const price_rule = obPriceRules?.result?.find(pr => pr.condition_product_code === tm.tourCode.trim() && pr.adjustment_per_unit !== "")
        return (
          <tr key={key} style={{backgroundColor: tourMaster === tm ? "#FFFF99" : "#FFF"}}>
            <td>{tm.tourCode}</td>
            <td>{tm.departureDate}</td>
            <td>{tm.mainAirline}</td>
            {price_rule
              ? <td><span style={{textDecoration: "line-through", color: "#666"}}>{tm.twinFare}</span> {parseInt(tm.twinFare) + parseInt(price_rule.adjustment_per_unit)}</td>
              : <td>{tm.twinFare}</td>}
            <td>{tm.balanceSeat}</td>
            <td><Button onClick={() => setTourMaster(tm)}>Select</Button></td>
          </tr>
        );
      })
      }
      </tbody></table>
      {tourMaster != null && <>
      <h2>Pricing</h2>
      <table>
        <tbody>
        <tr>
          <th></th>
          <th>Single</th>
          <th>Twin</th>
          <th>Triple</th>
          <th>Quad</th>
          <th>Child Without Bed</th>
          <th>Child Half Twin</th>
          <th>Child With Bed</th>
          <th>Infant</th>
        </tr>
        <tr>
          <th>Package</th>
          <td>{tourMaster.singleFare}</td>
          <td>{tourMaster.twinFare}</td>
          <td>{tourMaster.tripleFare}</td>
          <td>{tourMaster.quadFare}</td>
          <td>{tourMaster.childWithoutBedFare}</td>
          <td>{tourMaster.childHalfTwinFare}</td>
          <td>{tourMaster.childWithBedFare}</td>
          <td>{tourMaster.infantFare}</td>
        </tr>
        </tbody>
      </table>
      <h2>Booking</h2>
      
      <div>
        <div style={{float: 'left'}}>Number of rooms &nbsp;</div>
        <Input
          style={{float: 'left', width: '200px', marginRight: ''}}
          className="tc-inc-dec-input"
          type="number"
          addonAfter={
            <Icon
              type="plus"
              onClick={addRoom}
            />
          }
          addonBefore={
            <Icon
              type="minus"
              onClick={minusRoom}
            />
          }
          min={1}
          max={3}
          value={roomList.length}
          // prevent readonly warning
          onChange={() => {}}
        />

      </div>
      <hr style={{ clear: "both" }} />
      {roomList.map((room, index) => {
        return (
          <div key={index} className='tc-outbound-rooms'  style={{ clear: "both" }}>
            <div style={{lineHeight: '32px' }}>Room {room.roomNo}</div>
            <div>
              Adult &nbsp;
              <Select
                defaultValue={1}
                value={room.adults}
                onChange={adultOnChange(index)}
              >
                <Option value={1}>1</Option>
                <Option value={2}>2</Option>
                <Option value={3}>3</Option>
              </Select>
            </div>
            <div>
              Child with bed &nbsp;
              <Select
                defaultValue={1}
                value={room.children}
                onChange={childWithBedOnChange(index)}
              >
                <Option value={0}>0</Option>
                <Option value={1}>1</Option>
                <Option value={2}>2</Option>
              </Select>
            </div>
            <div>
              Child without bed &nbsp;
              <Select
                defaultValue={1}
                value={room.childrenNoBed}
                onChange={childWithoutBedOnChange(index)}
              >
                <Option value={0}>0</Option>
                <Option value={1}>1</Option>
              </Select>
            </div>
            <div>
              Infant &nbsp;
              <Select
                defaultValue={1}
                value={room.infants}
                onChange={infantOnChange(index)}
              >
                <Option value={0}>0</Option>
                <Option value={1}>1</Option>
                <Option value={2}>2</Option>
              </Select>
            </div>
          </div>
        );
      })}

      <div style={{ clear: "both" }} />
      <Button onClick={async () => {
        await addToCart()
        site.openCart()        
      }} loading={priceCheckRes.loading === true}>Add to Cart</Button>
      </>}
    </div>
  );
  
};

export default OutBoundPage;

export async function getStaticProps(context) {
  var tourType = context.params?.tourType
  const client = new TravelCloudClient(config)
  var tourTypeData
  var obPriceRules
  var obDepositRules

  if (tourType != null && tourType.match(/^[A-Z]+$/) != null && tourType.length > 3 && tourType.length < 10) {
    tourTypeData = await client.outboundGet('tourmasters/searchtourtype', {q: tourType})
    obPriceRules = await client.outboundGet('price_rules')
    obDepositRules = await client.outboundGet('deposit_rules')
  } else {
    tourTypeData = {error: "Invalid tourType"}
  }

  return {
    props: { tourTypeData, obPriceRules, obDepositRules },
    revalidate: 10, // In seconds
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}
