import React, { useEffect, useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import * as NProgress from 'nprogress/nprogress'
import { Layout, Menu, Icon, Button, Col, Drawer } from 'antd'
import '../customize/styles.less'
import Router from 'next/router'
import config from '../customize/config'
import { Cart, TravelCloudClient, setupFacebookPixel, setupGtag, parseCookie, loadi18n, useCart } from '../travelcloud'
import { Order } from '../components/order'
import { Account } from '../components/account'
import { PrintDiv } from '../components/print-div';

import { UserManager } from "oidc-client-ts"
import settings from "../customize/config-oid"

import en_US from "../locales/en-US/translation.json"
import fr from "../locales/fr/translation.json"
import th from "../locales/th/translation.json"
import vi from "../locales/vi/translation.json"
import ne from "../locales/ne/translation.json"
import zh_CN from "../locales/zh-CN/translation.json"
import my from "../locales/my/translation.json"

const { Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;
Router.events.on('routeChangeStart', (url) => NProgress.start())
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

export const SiteContext = React.createContext<{
  client: any,
  customer: any,
  order: any,
  cart: Cart,
  openCart: any,
  openCustom: any
}>(null);

const resources = {
  'en_US': { translation: en_US },
  fr: { translation: fr },
  th: { translation: th },
  vi: { translation: vi },
  ne: { translation: ne },
  'zh_CN': { translation: zh_CN },
  my: { translation: my },
}

const Page = ({ Component, pageProps }) => {
  const { customer, order, cart, client, initCart } = useCart(config.tcUser);
  const [collapsed, setCollapsed] = useState(true)
  const [siderAction, setSiderAction] = useState('')
  const [siderCustomContent, setSiderCustomContent] = useState(<div></div>)
  const [visible, setVisible] = useState(false)

  const cartIsEmpty = order.result == null || order.result.products == null || order.result.products.length === 0

  useEffect(() => {
    /**
     * TODO: improve to a better approach 
     *       to load i18n resources instead of importing
     *       maybe, parent class or dynamic loading
     */
    // componentWillMount
    loadi18n(resources, config.translation, true)

    // componentDidMount
    const cart = initCart()
    cart.refreshPriceRules()
    setupGtag(Router, config)
    setupFacebookPixel(Router, config)

    //@ts-ignore-error
    window.openIdLogin = () => {
      const mgr = new UserManager(settings);
      mgr.signinRedirect({ state: { some: "data" } }).then(function () {
        console.log("signinRedirect done");
      }).catch(function (err) {
        console.error(err);
        console.log(err);
      });
    }

    //@ts-ignore-error
    window.openIdLoginCallback = () => {
      const mgr = new UserManager(settings);
      mgr.signinRedirectCallback().then(function (user) {
        console.log("signed in", user);
      }).catch(function (err) {
        console.error(err);
        console.log(err);
      });
    }

    //@ts-ignore-error
    window.openIdLogout = () => {
      const mgr = new UserManager(settings);
      mgr.removeUser().then(function () {
        console.log("user removed");
      }).catch(function (err) {
        console.error(err);
        console.log(err);
      });
    }

    //@ts-ignore-error
    window.openIdGetUser = () => {
      const mgr = new UserManager(settings);
      mgr.getUser().then(function (user) {
        console.log("get user", user)
        if (user != null) cart.openIdLogin(user.id_token)
      }).catch(function (err) {
        console.error(err);
      });
    }
  }, [])

  const openCustom = async (content) => {
    setSiderAction('custom')
    setCollapsed(false)
    setSiderCustomContent(content)
  }

  const openCart = async () => {
    setSiderAction('cart')
    setCollapsed(false)
  }
  const openAccount = async () => {
    setSiderAction('account')
    setCollapsed(false)
    await cart.refreshCustomer()
  }
  const closeCart = () => {
    if (collapsed === false) {
      setSiderAction('')
      setCollapsed(true)
    }
  }

  /**
 * Set drawer open
 */
  const showDrawer = () => {
    setVisible(true)
  };

  /**
   * Set drawer close
   */
  const drawrOnClose = () => {
    setVisible(false)
  };

  const navigateToGitLab = () => {
    var filename = window.location.pathname
    if (filename === '/') filename = '/index'
    else if (window.location.pathname.indexOf("/generics/") === 0) filename = '/generics/[categories.name]'
    window.open('https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages' + filename + '.tsx')
  }

  return (
    <div>
      <Head>
        <title>TravelCloud Antd Demo</title>
      </Head>
      <Layout hasSider={true}>
        <PrintDiv printDivId="printHeader" hideOriginalDiv={true}>
          <h1>Trip My Feet</h1>
        </PrintDiv>
        <Layout className={collapsed === false && 'tc-dim-no-scroll-bars'} onClick={closeCart} style={{ overflow: 'hidden' }}>
          <Menu
            theme="dark"
            mode="horizontal"
            style={{ lineHeight: '64px', borderBottom: 0 }}>
            <Menu.Item key="cart" style={{ float: 'right' }} onClick={() => openCart()}>
              <Icon type="shopping-cart" style={{ fontSize: 16 }} /> Cart
            </Menu.Item>
            <Menu.Item key="customer" style={{ float: 'right' }} onClick={() => openAccount()}>
              <Icon type="user" style={{ fontSize: 16 }} /> My Account
            </Menu.Item>
            <Menu.Item key="gitlab" style={{ float: 'right' }} onClick={() => navigateToGitLab()} >
              <Icon type="github" />View in GitLab
            </Menu.Item>
            <Menu.Item key="drawer" style={{ float: 'left' }} onClick={showDrawer} >
              <svg xmlns="http://www.w3.org/2000/svg" style={{ opacity: 0.85, marginBottom: 6, verticalAlign: 'middle' }} width="40" height="40" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none" /><path fill="white" d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z" /></svg>
            </Menu.Item>
          </Menu>
          <div style={{ backgroundColor: '#fff', margin: '48px auto', maxWidth: 1800, width: '100%' }}>
            <Content>
              <SiteContext.Provider value={{
                client: client,
                customer: customer,
                order: order,
                cart: cart,
                openCart: () => openCart(),
                openCustom: (jsx) => openCustom(jsx)
              }}>
                <Component client={client} customer={customer} order={order} cart={cart} openCart={() => openCart()} {...pageProps} />
              </SiteContext.Provider>
            </Content>
          </div>
          <Footer style={{ textAlign: 'center' }}>
            {config.companyName} ©2018
          </Footer>
        </Layout>
        <Sider
          className="cart-sider"
          collapsedWidth={0}
          collapsible
          collapsed={collapsed}
          theme="light"
          width={1000}
          trigger={null}
          style={{ width: 1000, overflow: 'auto', height: '100vh', position: 'fixed', right: 0, zIndex: 2, boxShadow: '-1px 0 5px 0px rgba(0, 0, 0, 0.2), -4px 0 10px 0px rgba(0, 0, 0, 0.1)' }}>
          <div style={{ padding: "32px", position: "relative", width: 1000 }}>
            <Icon type="close" style={{ fontSize: 24, position: 'absolute', top: 32, right: 32, cursor: 'pointer' }} onClick={closeCart} />
            {siderAction === 'cart'
              && <div>
                <div style={{ paddingBottom: 16, fontSize: 24, paddingRight: 16, lineHeight: "32px" }}>Shopping Cart</div>
                {cartIsEmpty
                  ? <div>Your cart is empty</div>
                  : <>
                    <Order order={order.result} showSection={{ products: true, remove: true }} cart={cart} key={1} />
                    <Button
                      key={2}
                      onClick={() => {
                        setCollapsed(true)
                        Router.push('/checkout')
                      }}
                      style={{ marginTop: 16, float: 'right' }}
                      type="primary">Proceed to checkout <Icon type="right" /></Button>
                  </>}
              </div>}
            {siderAction === 'account'
              && <Account customer={customer} cart={cart} />}
            {siderAction === 'custom'
              && siderCustomContent}
          </div>
        </Sider>
      </Layout>
      <Layout>
        <Drawer
          title="Pages"
          placement='left'
          closable={false}
          onClose={drawrOnClose}
          visible={visible}
          bodyStyle={{ padding: "16px 0" }}
          width={400}
        >
          <Menu
            style={{ width: '100%' }}
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            mode="inline"
          >
            <SubMenu title="Tours">
              <Menu.Item><Link href="/tours-search"><a>Search</a></Link></Menu.Item>
              <Menu.Item><Link href="/tours-filter-by-option"><a>Filter by option type</a></Link></Menu.Item>
              <Menu.Item><Link href="/tour?id=9"><a>Details / Cart</a></Link></Menu.Item>
            </SubMenu>
            <SubMenu title="Generics">
              <Menu.Item><Link href="/generics/Attractions"><a>Search by category / Cart</a></Link></Menu.Item>
            </SubMenu>
            <SubMenu title="Flights">
              <Menu.Item><Link href="/index"><a>Custom search form</a></Link></Menu.Item>
              <Menu.Item><Link href="/flights"><a>Search</a></Link></Menu.Item>
              <Menu.Item><Link href="/flights-tabbed"><a>Search tabbed / One-way component</a></Link></Menu.Item>
            </SubMenu>
            <SubMenu title="HotelBeds">
              <Menu.Item><Link href="/hotelbeds-hotel"><a>Search</a></Link></Menu.Item>
            </SubMenu>
            <SubMenu title="Globaltix">
              <Menu.Item><Link href="/globaltix-search-cart"><a>Search + Cart</a></Link></Menu.Item>
              <Menu.Item><Link href="/globaltix-details?attractionId=44"><a> Details</a></Link></Menu.Item>
            </SubMenu>
            <SubMenu title="Others">
              <Menu.Item><Link href="/payment"><a>Payment</a></Link></Menu.Item>
              <Menu.Item><Link href="/checkout"><a>Checkout</a></Link></Menu.Item>
              <Menu.Item><Link href="/price-rules"><a>Price rules</a></Link></Menu.Item>
              <Menu.Item><Link href="/coupon"><a>Coupon</a></Link></Menu.Item>
              <Menu.Item><Link href="/contact"><a>Contact form</a></Link></Menu.Item>
            </SubMenu>
          </Menu>
        </Drawer>
      </Layout>
    </div>
  )
}

export default Page;