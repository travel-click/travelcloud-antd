import React, { useContext, useState } from 'react'
import { Row, Col, Menu, List, Tabs, Button, Checkbox, DatePicker } from 'antd'
import { TravelCloudClient, TcResponse, Cart, formatCurrency } from '../../travelcloud'
import { NoResult } from '../../components/no-result'
import config from '../../customize/config'
import Head from 'next/head'
import Router, { useRouter } from 'next/router'
import { NextPage, NextPageContext } from 'next'
import { Generic, Order } from '../../types'
import { useGenericsBookingForm, GenericView, GenericViewOptions } from '../../components/hooks/generics-booking'
import moment from "moment";
import { IncDecInput } from '../../components/inc-dec-input'
import { SiteContext } from '../_app'

interface Context extends NextPageContext {
  cart: Cart,
  order: TcResponse<Order>,
  openCart
}

const GenericsPage: NextPage<{categories, genericsServer: TcResponse<Generic[]>, cart: Cart, order: TcResponse<Order>, openCart}> = (props) => {
  const router = useRouter()
  const {cart, order, customer, openCart, client} = useContext(SiteContext)
  const {genericsServer} = props
  const [genericsClient, setGenericsClient] = useState(null)
  //const [loading, setLoading] = useState(false)
  const generics = genericsClient || genericsServer

  const {
    genericsView
  } = useGenericsBookingForm(generics, cart, order)


  return <div style={{minHeight: 1000, backgroundColor: '#fff', padding: 42}}>
    <Head>
      <title>{config.defaultTitle} | Generics</title>
    </Head>
    <Row gutter={32}>
      <Col span={6}>
      <h1>Tickets</h1>
      <Menu
        selectedKeys={[router.query['categories.name'] as string]}
        onClick={async (x) => {
          if (generics.loading) return
          setGenericsClient({loading: true})
          const genericsLoading = await client.generics({'categories.name': x.key})
          setGenericsClient(genericsLoading)
          Router.push('/generics/[categories.name]', '/generics/' + x.key, {shallow: true})
        }}
        mode="vertical">
        {props.categories.result != null
          &&props.categories.result.map(category =>
            <Menu.Item key={category.name}>
              <span>{category.name}</span>
            </Menu.Item>)}
      </Menu>
      </Col>
      <Col span={18}>
        {generics.loading || generics.result == null || generics.result.length === 0
          ? <NoResult
              style={{paddingTop: 128}}
              response={generics}
              loading={generics.loading}
              type="generics" />
          : <GenericsResult genericsView={genericsView} openCart={openCart} />}
      </Col>
    </Row>
  </div>
}

GenericsPage.getInitialProps = async (context: Context) => {
  const client = new TravelCloudClient(config)
  const genericsServer = await client.generics(context.query)
  const categories = await client.categories({category_type: 'nontour_product'})
  const query = Object.assign({}, context.query)

  return { categories, genericsServer, query,
    order: context.order,
    cart: context.cart,
    openCart: context.openCart
  }
}
  
export default GenericsPage

const GenericsResult: React.ComponentType<{genericsView: GenericView[], openCart}> = ({genericsView, openCart}) => {
  return <List
    dataSource={genericsView}
    renderItem={(generic: GenericView) => {
      const isLimousineService = generic.categories.find(category => category.name === "Limousine Service")
      return <List.Item>
        <div style={{width: '100%'}}>
          <div style={{
            backgroundImage: `url(${generic.photo_url})`,
            height: 400,
            width: "40%",
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            float: 'left'}} />
          <div style={{padding: '16px 32px', float: 'left', width: "60%"}}>
            <h2 className="tc-full-width-ellipsis">{generic.name}</h2>
            <Tabs defaultActiveKey="1">
              <Tabs.TabPane tab="Prices" key="1">
                <table>
                  <tbody>
                    {
                      isLimousineService
                      ? generic.options.map((option: GenericViewOptions) => {
                        return <tr key={option.id}>
                          <td style={{paddingRight: 32, minWidth: 150}}>
                            <Checkbox
                              checked={option._view.quantity > 0}
                              onChange={(e)=> {
                                if (e.target.checked) {
                                  option._view.updateQuantity(1)
                                  option._view.updateDateTime(moment())
                                }
                                else {
                                  option._view.updateQuantity(0)
                                  option._view.updateDateTime(null)
                                }
                              }}>
                              {option.name}
                            </Checkbox>
                          </td>
                          <td style={{paddingRight: 32, fontWeight: 'bold'}}>{
                            option._view.beforeDiscount.eq(option._view.afterDiscount) ?
                            <span>{formatCurrency(option.price)}</span> :
                          <><span style={{textDecoration: "line-through"}}>{formatCurrency(option._view.beforeDiscount)}</span> <span>{formatCurrency(option._view.afterDiscount)}
                          </span></>
                          }</td>
                          <td style={{width: 150}}>
                            <DatePicker showTime placeholder="Pickup date and time" value={option._view.dateTime} onChange={(val) => {option._view.updateQuantity(1); option._view.updateDateTime(val)}} />
                          </td>
                        </tr>})
                      : generic.options.map((option: GenericViewOptions) => {
                          return <tr key={option.id}>
                            <td style={{paddingRight: 32, minWidth: 150}}><div className="tc-full-width-ellipsis">{option.name}</div></td>
                            <td style={{paddingRight: 32, fontWeight: 'bold'}}>{
                              option._view.beforeDiscount.eq(option._view.afterDiscount) ?
                              <span>{formatCurrency(option.price)}</span> :
                              <><span style={{textDecoration: "line-through"}}>{formatCurrency(option._view.beforeDiscount)}</span> <span>{formatCurrency(option._view.afterDiscount)}</span></>
                            }</td>
                            <td style={{width: 150}}><IncDecInput min={0} max={option._view.maxQuantity} value={option._view.quantity} onChange={option._view.updateQuantity} /></td>
                          </tr>})}
                    <tr style={{height: 16}}>
                      <td colSpan={3}></td>
                    </tr>
                    <tr style={{borderTop: '2px solid #999'}}>
                      <td>Total ({generic._view.totalQuantity} items)</td>
                      <td style={{fontWeight: 'bold'}}>{formatCurrency(generic._view.totalPrice)}</td>
                      <td>
                        <Button style={{width: '100%', margin: "8px 0"}}
                          type="primary"
                          onClick={() => {
                            generic._view.updateCart()
                            openCart()
                          }}>
                          Update cart
                        </Button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </Tabs.TabPane>
              <Tabs.TabPane tab="Description" key="2"><div style={{height: 260, overflowY: 'auto'}} dangerouslySetInnerHTML={{__html: generic.description}} /></Tabs.TabPane>
            </Tabs>
          </div>
        </div>
      </List.Item>}}
    />
}