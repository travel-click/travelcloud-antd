
import { Order } from '../components/order'
import { Layout, Row, Col, Spin, Card, Button, Icon, Input } from 'antd'
import React, { useContext, useEffect, useState } from 'react'
import { SiteContext } from './_app'

function nnne(x: string): boolean {
  return x != null && x.length > 0
}

function explainEval(order, pr) {
  const checkoutCode = order.checkout_code
  var explaination = []
  if (checkoutCode != null) {
    if (pr._checkout_code_passed) explaination.push(<li key={explaination.length}><b style={{ color: 'green' }}>Passed</b> condition_checkout_code_hash</li>)
    else if (!nnne(pr.condition_checkout_code_hash)) explaination.push(<li key={explaination.length}><b style={{ color: 'yellow' }}>Not required</b> condition_checkout_code_hash</li>)
  }

  for (var x of pr._fail_reason) {
    explaination.push(<li key={explaination.length}><b style={{ color: 'red' }}>Failed</b> {x}</li>)
  }

  if (explaination.length === 0) explaination.push(<li key={explaination.length}><b style={{ color: 'green' }}>Passed</b> all conditions</li>)

  return explaination
}

const PriceRulePage = () => {
  const { cart, order, customer } = useContext(SiteContext)
  const [checkoutCode, setCheckoutCode] = useState("")
  const priceRules = cart?.priceRules || {}

  const customerPriceRules
    = customer?.result == null
      ? null
      : customer.result.categories.reduce((acc, category) => acc.concat(category.customer_in_any_price_rules), [])

  const checkoutCodeStatus = nnne(order?.result?.checkout_code) ? "Using code " + order?.result?.checkout_code : "No checkout code"

  if (order.loading) {
    return <div style={{ minHeight: 1000, backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600 }}>
      <Card loading={true} style={{ border: 0 }}></Card>
    </div>
  }

  return (
    <Layout style={{ backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600 }}>
      <Row>
        <Col span={12}>
          <h1>Public price rules</h1>
          {priceRules.loading
            ? <div>Loading...</div>
            : (priceRules.result == null || priceRules.result.length === 0
              ? <div>No public price rules</div>
              : <ul>{priceRules.result.map((priceRule) =>
                <li key={String(priceRule.id)}>
                  {priceRule.name}
                </li>)}</ul>)
          }
          <h1>Customer price rules</h1>
          {customer.result == null
            ? <div>Customer not logged in</div>
            : (customerPriceRules.length === 0
              ? <div>No customer price rules</div>
              : <ul>{customerPriceRules.map((priceRule) =>
                <li key={String(priceRule.id)}>
                  {priceRule.name}
                </li>)}</ul>)
          }

          {/* This is for testing purpose. You do not need to manually refresh price rules in normal usage. */}
          <h1>Refresh price rules</h1>
          <Button onClick={() => {
            window['travelCloudClientCache'] = null
            cart.refreshPriceRules(true)
            cart.refreshCustomer()
          }}>
            Refresh price rules
          </Button>

          <h1>Checkout code</h1>
          <Input placeholder="Code" value={checkoutCode} onChange={(event) => setCheckoutCode(event.target.value.toUpperCase())} />
          <Button onClick={() => { cart.setCheckoutCode(checkoutCode) }}>Apply code</Button>

        </Col>
        <Col span={12}>
          {(order?.result?.products == null || order.result.products.length === 0)
            ? <h1>Your cart is empty</h1>
            : <>
              <h1>Order Details</h1>
              <Order order={order.result} showSection={{ products: true }} cart={cart} />

              <h1>Price rules evaluation - {checkoutCodeStatus}</h1>
              {order.result.products.map((product, i) => <div key={i}>
                <h3>{product.product_name}</h3>

                {product._price_rules_combined_eval.map((pr, ii) => <div key={ii}>
                  <div><b>{pr.name}</b></div>
                  <ul>{explainEval(order.result, pr)}</ul>
                </div>)}

              </div>)}
            </>
          }

        </Col>
      </Row>
    </Layout>
  )
}

export default PriceRulePage