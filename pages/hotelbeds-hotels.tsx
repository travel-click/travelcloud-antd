import React, {useEffect, useState, useCallback, useMemo} from 'react'
import { Row, Col, Drawer, Button, Result, } from 'antd'
import { validateHotelbedsParams } from '../travelcloud'

import config from '../customize/config'
import Head from 'next/head'
import Router from 'next/router'
import qs from 'qs'

import { NoResult } from '../components/no-result'
import { HotelbedsSearchForm, isEmptyObject } from '../components/hotelbeds-search-form';
import { HotelbedsResult, } from '../components/hotelbeds-results';
import { HotelbedsQuery, HotelbedsFormValue } from '../order-product-compute';
import HotelbedsDetail from '../components/hotelbeds-details';
import PassengerContent from '../components/hotelbeds-passeger-content';
import HotelbedsFilter from '../components/hotelbeds-filter';
import {parseFilters} from '../components/flights/component-logic';

import useHotelbedsFilter from '../components/hooks/hotelbeds-filter';
import usePaginateResults from '../components/hooks/paginate-results';
import { useHotelbeds } from '../components/hooks/hotelbeds'

import { Trans } from 'react-i18next'
import useTCi18n from '../components/hooks/tc-i18n'
export function Index(props: any): any {
  // initial states
  const [formValue, setFormValue]: [ HotelbedsFormValue, any ] = useState( props.query );

  // fetch hotels
  const [ result, loading, search, types ] = useHotelbeds({ formValue, client: props.client, cart: props.cart })
  const [categories, setCategories] = useState([]);
  const [boardCodes, setBoardCodes] = useState([]);
  // const [loading, setLoading]: [ boolean, any ] = useState(false);

  const {
    filterResults,
    setName,
    setBoardCodeParams,
    setCategoryParams,
    setPriceRange,
    filters,
    setFilters,
    minPrice,
    maxPrice
  } = useHotelbedsFilter();

  const [ { hotel, room, rates }, setSelectedItem ]: [ any, any ] = useState({ hotel: null, room: null, rates: null })
  const updateHotel = useCallback((patch:any) => setSelectedItem(prev => Object.assign(prev, patch)), [])

  useEffect(() => {
    if (props.query.filters) {
      const parsedParams = parseFilters(props.query.filters) as any;

      setFilters({
        ...filters,
        name: parsedParams.name ? parsedParams.name[0] : '',
        boardCodes: parsedParams.boardCodes ? parsedParams.boardCodes : [],
        categories: parsedParams.categories ? parsedParams.categories : [],
      });
    }
  }, []);

  useEffect(() => {
    if ( ! isEmptyObject( formValue ) && validateHotelbedsParams( formValue )) {
      Router.push('/hotelbeds-hotels?' + qs.stringify( formValue ))
    }
  }, [ formValue ])

  const [ flag, setFlag ] = useState(false)

  const filteredResults = useMemo(() => {

    return filterResults({
      hotels: result.hotels,
      filters, // fixme: no need to pass filters here
      setCategories,
      setBoardCodes,
      Router,
    });
  }, [result, filters]);

  // const resultsToRender = result.hotels
  const {resultsToRender, loadMore, isLastPage} = usePaginateResults( filteredResults, 10 );
  const {changeLocale, t} = useTCi18n()

  return (
    <div id="hotelbeds" style={{ minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32 }}>
      <Head>
        {/* <title>{config.defaultTitle} | Hotels</title> */}
        <title>{t('title.Hotel')}</title>
      </Head>
      <h1>{t('title.Hotel')}</h1>
      <Button onClick={() => changeLocale('zh-CN')}>Change lang</Button>
      <HotelbedsSearchForm 
        client={props.client} 
        formValue={formValue}
        minLeadTime={5} // default minLeadTime is 1 day
        onSearch={(params) => {
          setFormValue(Object.assign( {}, params ))

          if ( ! isEmptyObject( params )) {
            search( params )
            Router.push('/hotelbeds-hotels?' + qs.stringify( formValue ))
          }
        }}
        defaultCityCodes={['SIN', 'BKK', 'KUL', 'HKG']}
      />

      {loading || result.hotels == null || result.hotels.length === 0
          ? <NoResult
          style={{ paddingTop: 128 }}
          response={result}
          loading={loading}
          type="hotels">
              {validateHotelbedsParams(formValue) || "Please enter your destination and dates on the left"}
          </NoResult>
            :
          <Row gutter={32}>
              <Col span={6}>
                <HotelbedsFilter
                  filters={filters}
                  setName={setName}
                  setBoardCodeParams={setBoardCodeParams}
                  setCategoryParams={setCategoryParams}
                  setPriceRange={setPriceRange}
                  boardCodes={boardCodes}
                  categories={categories}
                  minPrice={minPrice}
                  maxPrice={maxPrice}
                />
            </Col>
            <Col span={18}>
              <HotelbedsResult 
                hotels={resultsToRender}
                types={types}
                checkIn={result.checkIn}
                checkOut={result.checkOut}
                client={props.client}
                // hotels={filteredResults}
                // hotels={result.hotels}
                onHotelRoomRateClick={(item) => { updateHotel(item); setFlag(true) }}
                googleAPIKey={config.googleAPIKey}
              />
              <div style={{textAlign: 'center'}}>
              <Button disabled={isLastPage()} onClick={() => loadMore()}>Load More</Button>
              </div>
            </Col>
        </Row>}
        
        <Drawer visible={flag} onClose={() => setFlag(false)} placement={"right"} width={1040}>
          <HotelbedsDetail hotel={hotel} room={room} 
            rates={rates} checkIn={result.checkIn} 
            checkOut={result.checkOut} client={props.client} types={types} />
          {/* <DrawerContent>
            {() => {
              return <h1>Hello</h1>
            }}
          </DrawerContent> */}
          <PassengerContent hotel={hotel} room={room} rates={rates} onAddToCart={(bookingForm) => {
              setFlag(false)

              const data = {
                ...hotel,
                rooms: [
                  {
                    ...room,
                    rates
                  }
                ],
                checkIn: result.checkIn,
                checkOut: result.checkOut
              }

              let product = props.cart.reset().addHotelbedsHotel({ static: hotel.details, checkrates: data }, bookingForm);
              Router.push('/checkout');
            }
          }
          />
        </Drawer>
    </div>

  )
}

Index.getInitialProps = async (context): Promise<{ query:HotelbedsQuery }> => {
  const query: HotelbedsQuery = qs.parse(context.asPath.split(/\?/)[1]);
  return { query }
}

export default Index