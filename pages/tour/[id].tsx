import React, { useContext, useState } from 'react'
import { Row, Col, Carousel } from 'antd'
import { TravelCloudClient, TcResponse, Cart, extractValueFromFormState } from '../../travelcloud'
import config from '../../customize/config'
import {TourBookingForm, initialTourBookingForm} from '../../components/tour-booking'
import Router from 'next/router'
import { PrintDiv } from '../../components/print-div';
import { SiteContext } from '../_app'

const TourPage = ({tour}) => {
  const {cart, order, customer} = useContext(SiteContext)
  const [tourBookingForm, setTourBookingForm] = useState(initialTourBookingForm(tour))
  if (tour == null) return <div>Tour not found</div>
  return (
    <div>
      <Row style={{margin: '32px 64px'}}>
        <h1 style={{fontSize: 40, fontWeight: 'normal', marginBottom: 0}}>{tour.name}</h1>
      </Row>
      <Row style={{backgroundColor: '#fff', margin: '32px 64px', height: '650px'}}>
        <Col span={12}>
          <Carousel autoplay>
            <div key='cover'><div style={{
              backgroundImage: `url(${tour.photo_url})`,
              height: '650px',
              width: '100%',
              backgroundSize: 'cover',
              backgroundPosition: 'center'}} /></div>
            {tour.photos.map((photo, index) =>
              <div key={index}><div style={{
                backgroundImage: `url(${photo.url})`,
                height: '650px',
                width: '100%',
                backgroundSize: 'cover',
                backgroundPosition: 'center'}} /></div>)}
          </Carousel>
        </Col>
        <Col span={12} style={{height: '100%'}}>
          <TourBookingForm
            cart={cart}
            value={tourBookingForm}
            tour={tour}
            onChange={(tourBookingForm) => setTourBookingForm(tourBookingForm)}
            onSubmit={() => {
              cart.reset().addTour(tour, tourBookingForm)
              Router.push('/checkout')
            }} />
        </Col>
      </Row>
      <PrintDiv printDivId="printPhoto" hideOriginalDiv={true}>
        <h2 style={{fontSize: 40, fontWeight: 'normal', marginBottom: 0}}>{tour.name}</h2>
        <img src={tour.photo_url} style={{display: 'block', margin: "20px auto"}} />
      </PrintDiv>
      <PrintDiv printDivId="printItinerary">
        <Row gutter={64} style={{backgroundColor: '#fff', margin: '32px 64px', padding: 32}}>
          <Col span={12}>
            <h2>Highlights</h2>
            <div dangerouslySetInnerHTML={{__html: tour.short_desc}} />
            <h2>Extras</h2>
            <div dangerouslySetInnerHTML={{__html: tour.extras}} />
            <h2>Remarks</h2>
            <div dangerouslySetInnerHTML={{__html: tour.remarks}} />
          </Col>
          <Col span={12}>
            <h2>Itinerary</h2>
            <div dangerouslySetInnerHTML={{__html: tour.itinerary}} />
          </Col>
        </Row>
      </PrintDiv>
    </div>
  )
}

export async function getStaticPaths() {
  return { paths: [], fallback: true };
}

export async function getStaticProps(context) {
  const client = new TravelCloudClient(config)
  const tour = await client.tour(context.params)
  return {props: { tour: tour.result }, revalidate: 120};
};

export default TourPage
