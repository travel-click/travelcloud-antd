import { CheckoutForm, initialCheckoutForm } from '../components/checkout'
import { Cart, updateFormState, TravelCloudClient } from '../travelcloud'
import { formatCurrency, Order } from '../components/order'
import { Layout, Row, Col, Card, Button, Icon, Checkbox, Input } from 'antd'
import React from 'react'
import { withRouter, SingletonRouter } from 'next/router'
import config from '../customize/config'
import { HotelBeds } from '../components/voucher'
import { FareRules } from '../components/fare-rules2'
import { SiteContext } from './_app'

export function getCheckoutCodePriceRule(order) {

  // customer didn't enter anything
  if (order?.checkout_code == null || order?.checkout_code == "") return null;

  const allCorrectPriceRules = order?.products?.map((product) => product._price_rules_combined_eval).flat()

  // there's probably no order
  if (allCorrectPriceRules == null) return null;

  // we return success if any
  const success = allCorrectPriceRules.find((pr) => pr._checkout_code_passed && pr._fail_reason.length === 0)
  if (success != null) return success
  
  // we return correct code but fail next
  const withError = allCorrectPriceRules.find((pr) => pr._checkout_code_passed)
  if (withError != null) return {error: true, price_rule: withError}

  // return null
  return null
}

class Page extends React.PureComponent<{router: SingletonRouter, order:any, cart: Cart, client: TravelCloudClient}, any> {
  static contextType = SiteContext

  formRef

  state = {
    submittingOrder: false,
    orderLoading: true,
    error: null,
    errorRef: null,
    checkoutForm: null,
    subscribeEdm: false,
    checkoutCode: ''
  }

  componentDidMount() {
    // prefetch payment page
    this.props.router.prefetch('/payment')
  }

  onSubmit (e) {
    e.preventDefault();
    this.formRef.props.form.validateFieldsAndScroll(async (error, values) => {
      if (!error) {
        if (this.state.subscribeEdm){
          // we ignore the return status because we don't want to affect checkout
          await this.props.client.subscribeEdm({
            'email': values.email,
            'mailchimp_list_id': 'd22fa571f3',
            'attributes': {
              // FNAME and LNAME are based on mailchimp default merge_fields
              'FNAME' : values.first_name,
              'LNAME' : values.last_name
            }
          })
        }

        this.setState({
          submittingOrder: true
        })

        const result = await this.props.cart.checkout(values)

        if (result.result == null) {
          this.setState({
            error: true
          })
        } else {
          if (result.result.order_status === 'Quotation' || result.result.order_status === 'Invoice') {
            this.props.router.push('/payment?ref=' + result.result.ref)
          } else {
            this.setState({
              error: true,
              errorRef: result.result.ref
          })}
        }
      } else {
        console.log('error', error, values);
      }
    });
  };

  static getDerivedStateFromProps(props, current_state) {
    if (current_state.orderLoading === true && props.order.result != null) {
      // scroll up after order loaded
      setTimeout(() => window.scrollTo(0, 0), 100)
      return {
        checkoutCode: props.order.result.checkout_code,
        orderLoading: false,
        checkoutForm: initialCheckoutForm(props.order.result, true)
      }
    }
    return null
  }

  render() {
    const cart = this.props.cart
    const order = this.props.order.result

    const checkoutCodePriceRule = getCheckoutCodePriceRule(order)

    var checkoutCodeError
    var checkoutCodeMessage

    if (order?.checkout_code == null || order?.checkout_code === "") {
      checkoutCodeError = false
      checkoutCodeMessage = ""
    }
    else if (checkoutCodePriceRule == null) {
      // can't find any matching price rules
      checkoutCodeError = true
      checkoutCodeMessage = "Code is invalid"
    }
    else if (checkoutCodePriceRule._fail_reason == null || checkoutCodePriceRule._fail_reason.length > 1) {
      // possible to examine _fail_reason to give better errors
      checkoutCodeError = true
      checkoutCodeMessage = "Code is invalid"
    }
    else {
      checkoutCodeError = false
      checkoutCodeMessage = checkoutCodePriceRule.name + " " + formatCurrency(checkoutCodePriceRule._adjustment)
    }

    if (this.state.error === true) {
      return  <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600}}>
        <h1>We were not able to process your booking online</h1>
          <p>Please contact our friendly travel consultants to perform your booking.</p>
          {this.state.errorRef != null && <p>Order reference: {this.state.errorRef}</p>}
        </div>
    }

    if (this.props.order.loading) {
      return <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600}}>
        <Card loading={true} style={{border: 0}}></Card>
      </div>
    }

    if (order == null || order.products == null || order.products.length === 0) {
      return <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600}}>
        <h1>Your cart is empty</h1>
      </div>
    }

    return (
      <Layout style={{backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600}}>
        <Row>
          <Col span={12}>
            <h1>Contact Details</h1>
            <CheckoutForm
              order={order}
              tcUser={config.tcUser}
              formState={this.state.checkoutForm}
              onChange={updateFormState(this, 'checkoutForm')}
              wrappedComponentRef={(inst) => this.formRef = inst}
              defaultCarrierCodes="SQ,MH" />
            <div style={{marginBottom: '1em'}}>
              <Checkbox
                checked={this.state.subscribeEdm}
                onChange={(e) => {
                  this.setState({
                    subscribeEdm: e.target.checked,
                  })
                }}>
                Subscribe to our newsletter for the latest promotion
              </Checkbox>
            </div>
            <Button size="large" type="primary" disabled={this.state.submittingOrder} onClick={(e) => this.onSubmit(e)}>Proceed to payment <Icon type={this.state.submittingOrder ? 'loading' : 'right'} /></Button>
          </Col>
          <Col span={12}>
            <h1>Order Details</h1>
            <Order order={order} showSection={{products: true}} cart={cart} />
            <HotelBeds order={order} client={this.props.client} />
            {order.products.map((product, index) => {
              const flight = product.product_detail
              const pricing_id = product.form.pricing_id
              if (flight == null || pricing_id == null) return null;
              return <FareRules flight={flight} pricing_id={pricing_id} client={this.context.client} key={index} />
            })}

            <div style={{marginTop: '32px'}}>
              <Input
                placeholder='Promo code'
                value={this.state.checkoutCode}
                onChange={(event) => this.setState({checkoutCode: event.target.value})}
                style={{width:'80%'}}
              />
              <Button onClick={() => this.props.cart.setCheckoutCode(this.state.checkoutCode)} style={{width:'20%'}}>Apply</Button>
            </div>
            <div
              style={{color: checkoutCodeError ? '#F66' : '#000', padding: '8px 16px' }}>
              {checkoutCodeMessage}
            </div>
            
          </Col>
        </Row>
      </Layout>
  )}
}

export default withRouter(Page)