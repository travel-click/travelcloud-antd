import React, { useEffect } from 'react'
import Router from 'next/router'
import moment from 'moment'
import config from '../customize/config'

import { TravelCloudClient, Cart, TcResponse } from '../travelcloud'
import { Icon, Row, Col, Button, Input, Modal, Spin, Form, DatePicker, Select } from "antd";
import { Document, Generic, Order } from '../types'
import { NextPage, NextPageContext } from 'next';
import { useGlobaltixBookingForm, computeAttractionView } from '../components/hooks/globaltix-booking';
import { IncDecInput } from '../components/inc-dec-input';
import { FormComponentProps } from 'antd/lib/form';
import { WrappedFormUtils } from 'antd/lib/form/Form';
import { GlobaltixBookingForm } from '../components/globaltix-booking-form'

const { Option } = Select;

interface Context extends NextPageContext {
  cart: Cart,
  openCart
}

const GlobaltixDetailsPage: NextPage<{}> = (props: any) => {
  const {ticketTypeGet, fetchTicketTypeGet, clearTicketTypeGet} = useGlobaltixBookingForm(props.client)
  const computed = computeAttractionView(props.attraction, props.cart)

  const ticketTypeGetData = ticketTypeGet != null && ticketTypeGet.result != null && ticketTypeGet.result.data
  return (
      <div className="main-container">
          <>
            <p><img alt="No Image" width="100%" src={"https://uat-api.globaltix.com/api/image?name=" + computed.imagePath} /></p>
            <h1>{computed.title}</h1>
            <p>{computed.description}</p>
            <p></p>
            <h2>Operating Hours</h2>
            <p>{computed.hoursOfOperation}</p>
          </>
          {computed.ticketTypes.map(ticket => {
              return <div className="tickets-details" key={"ticket-details" + ticket.id}>
                  <Row type="flex" justify="space-between" align="middle" gutter={10} key={"ticket-row" + ticket.id}>
                      <Col lg={1} md={24} sm={24} xs={24} key={"ticket-col" + ticket.id}>
                          <Icon type="star" className="list-star1" key={"ticket-star" + ticket.id} />
                      </Col>
                      <Col lg={10} md={24} sm={24} xs={24} className="ticket-name" key={"ticket-name" + ticket.id}>
                          <div key={"catgory-name" + ticket.id}><div className="catgory-name" key={"catgory-name-div" + ticket.id}>{ticket.variation.name}</div><strong>{ticket.name}</strong><div className="ticketname-des" key={"ticketname-des" + ticket.id}>{'Usual Price ' + ticket.merchantCurrency + ' ' + parseFloat(ticket.originalPrice).toFixed(2)}</div><div className="ticket-merchant" key={"ticket-merchant" + ticket.id}>{'Merchant ' + ticket.sourceName}</div></div>
                      </Col>
                      <Col lg={8} md={24} sm={24} xs={24} className="ticket-price" key={"ticket-price" + ticket.id}>
                          <div className="rghtAlign-marginRght" key={"rghtAlign-marginRght" + ticket.id}>

                          {ticket._view.beforeDiscounts.eq(ticket._view.afterDiscounts)
                              && <div key={"ticket-currency" + ticket.id}><strong>{ticket.currency + ' '}</strong>{ ticket._view.afterDiscounts.toFixed(2)}</div>}

                          {!ticket._view.beforeDiscounts.eq(ticket._view.afterDiscounts)
                              && <>
                                  <div key={"ticket-currency" + ticket.id}>
                                  <strong>{ticket.currency + ' '}</strong>
                                  <span style={{ textDecoration: 'line-through' }}>{ticket._view.beforeDiscounts.toFixed(2)} </span>
                                  { ticket._view.afterDiscounts.toFixed(2)}</div>
                                  </>}
                          </div>
                      </Col>
                      <Col lg={2} md={24} sm={24} xs={24} className="addto-cart" key={"addto-cart" + ticket.id}>
                          <Button type="primary" onClick={() => fetchTicketTypeGet(ticket.id)}>Booking</Button>
                      </Col>
                  </Row>
              </div>
          })}
          <Modal
            title={ticketTypeGetData.name}
            visible={ticketTypeGet != null}
            onCancel={() => clearTicketTypeGet()}
            width={800}
            footer={null}
          >
            <Spin spinning={ticketTypeGet != null && ticketTypeGet.loading === true}>
              <div style={{minHeight: '400px', maxHeight: '600px', 'overflowY': 'auto'}}>
                {ticketTypeGetData && <>
                  <h3>Description</h3>
                  <p>{ticketTypeGetData.description}</p>
                  {ticketTypeGetData.variation ? <><h3>Type: </h3> <p>{ticketTypeGetData.variation.name}</p></> : ''}
                  <><h3>Terms & Condition: </h3><p>{ticketTypeGetData.termsAndConditions}</p></>
                  <hr style={{color: '#DDD', margin: '12px 0'}} />
                  <GlobaltixBookingForm
                    ticketTypeGet={ticketTypeGet}
                    onAddToCart={(formValue)=> {
                      props.cart.addGlobaltix(props.attraction, globaltixFormValueToCartValue(props.attraction, ticketTypeGetData.id, formValue))
                      props.openCart()
                      clearTicketTypeGet()
                    }} />
                </>}
              </div>
            </Spin>
          </Modal>
      </div>
  );
}

export const globaltixFormValueToCartValue = (attraction, ticketTypeGetId, formValue) => {
  
  const payload: any = {
    id: ticketTypeGetId,
    quantity: formValue.quantity
  }

  delete formValue.quantity;

  if (formValue.visitDate != null) {
    payload.visitDate = formValue.visitDate.format('YYYY-MM-DD')
    delete formValue.visitDate
  }

  if (attraction.from != null) {
    payload.fromResellerId = attraction.from
  }

  const entries = Object.entries(formValue)

  if (entries.length > 0) {
    payload.questionList = entries.map((kvp: any) => {
      if (kvp[1].format != null)
      return {
        id: kvp[0],
        answer: kvp[1].format('YYYY-MM-DD')
      }
      else return {
        id: kvp[0],
        answer: kvp[1]
      }
    })
  }

  return payload
}

GlobaltixDetailsPage.getInitialProps = async (context: Context) => {
  const client = new TravelCloudClient(config)
  const attractionId = context.query.attractionId
  const attractionGet = await client.globaltix('attraction/get', { id: attractionId })
  const ticketTypeList = await client.globaltix('ticketType/list', { attraction_id: attractionId })
  const additionalInfo = await client.documents({code: 'globaltix/' + attractionId })
  let attraction = attractionGet.result.data;
  attraction.ticketTypes = ticketTypeList.result.data;

  // there is a document with the globaltix code
  let document = null
  if (additionalInfo.result != null && additionalInfo.result.length > 0) {
      document = additionalInfo.result[0]
  }

  return {document, attraction}
}

export default GlobaltixDetailsPage;