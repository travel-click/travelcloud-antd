import React from 'react'
import { Row, Col, Carousel } from 'antd'
import { TravelCloudClient, TcResponse, Cart, extractValueFromFormState } from '../travelcloud'
import config from '../customize/config'
import {TourBookingForm2} from '../components/tour-booking2'
import { Tour } from '../types'
import { NextPage, NextPageContext } from 'next'
import { useTourBookingForm } from '../components/hooks/tour-booking'

interface Context extends NextPageContext {
  cart: any
}

const TourPage: NextPage<{tour: TcResponse<Tour>, cart: Cart}> = (props) => {
  const tour = props.tour.result
  if (tour == null) return <div>Tour not found</div>

  const client = new TravelCloudClient(config)

  const {tourBookingFormController, tourBookingFormState} = useTourBookingForm(props.tour.result, props.cart, client, {source: 'webconnect'})


  return (
    <div>
      <Row style={{margin: '32px 64px'}}>
        <h1 style={{fontSize: 40, fontWeight: 'normal', marginBottom: 0}}>{tour.name}</h1>
      </Row>
      <Row style={{backgroundColor: '#fff', margin: '32px 64px', height: '650px'}}>
        <Col span={16}>
          <Carousel autoplay>
            <div key='cover'><div style={{
              backgroundImage: `url(${tour.photo_url})`,
              height: '650px',
              width: '100%',
              backgroundSize: 'cover',
              backgroundPosition: 'center'}} /></div>
            {tour.photos.map((photo, index) =>
              <div key={index}><div style={{
                backgroundImage: `url(${photo.url})`,
                height: '650px',
                width: '100%',
                backgroundSize: 'cover',
                backgroundPosition: 'center'}} /></div>)}
          </Carousel>
          <h2>Highlights</h2>
          <div dangerouslySetInnerHTML={{__html: tour.short_desc}} />
          <h2>Extras</h2>
          <div dangerouslySetInnerHTML={{__html: tour.extras}} />
          <h2>Remarks</h2>
          <div dangerouslySetInnerHTML={{__html: tour.remarks}} />
          <h2>Itinerary</h2>
          <div dangerouslySetInnerHTML={{__html: tour.itinerary}} />
        </Col>
        <Col span={8} style={{height: '100%'}}>
          <TourBookingForm2 bookingFormController={tourBookingFormController} bookingFormState={tourBookingFormState} />
        </Col>
      </Row>
    </div>
  )
}

TourPage.getInitialProps = async (context: Context) => {
  const client = new TravelCloudClient(config)
  const tour = await client.tour(context.query)
  const query = context.query
  const cart = context.cart
  return { tour, query, cart }
}

export default TourPage