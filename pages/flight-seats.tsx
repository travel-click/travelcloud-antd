import React, { useEffect } from "react";
import { Tabs, Button, Col, Row, Select } from "antd";
import { useRouter } from "next/router";
import { NoResult } from "../components/no-result";
import { formatCurrency, range } from "../travelcloud";
import Big from 'big.js';
import { findInMap, PaxProps, seatMapToTable, SegmentProps, useFlightAncillaries } from "../components/hooks/flight-ancillaries";
import { FlightDetail } from "../types";

export const SeatMap = ({seatMap, seatPricings, segmentSeatSelection, setSegmentSeatSelection, selectedPax}) => {
  console.log({seatPricings, selectedPax})
  const seatPricingsTyped = seatPricings as FlightDetail['pricings'][0]['fares'][0]['segments'][0]['seats_pricings']
  const seatTable = seatMapToTable(seatMap, seatPricings.filter(pricing => pricing.paxes.find(pax => pax === selectedPax) != null))
  return seatTable.map((row, rowIndex) => {
    if (row[0] == null) return null
    return <div key={rowIndex} style={{display: 'flex', justifyContent: 'center'}}>
      {row.map((cell, cellIndex) => {
        const seatPricingsTypedFiltered = seatPricingsTyped.filter(sp => sp.paxes.find(p => p === selectedPax))

        const pricing = cell == null ? null : seatPricingsTypedFiltered.find((sp) => {
          return cell.offer_item_ids.find(id => id === sp.offer_item_id)
        })

        const disabled = cell == null || cell.bookable !== true || pricing == null
        var classNames = ['seat']
        const kvp = findInMap(segmentSeatSelection, cell)
        if (kvp != null) classNames.push("selected")
        if (disabled) classNames.push("disabled")
        var thisCellIsA = false
        if (cell != null) {
          cell.seat_char_codes.forEach((cc) => {
            classNames.push("padis-"+cc.code)
            if (cc.code === "A") thisCellIsA = true
          })
        }

        if (thisCellIsA) {
          const nextCellIsA = row[cellIndex + 1]?.seat_char_codes.find((cc) => cc.code === 'A')
          if (nextCellIsA != null) classNames.push('left-of-aisle')
        }

        return <div key={cellIndex} className={classNames.join(" ")}  onClick={()=>{
          if (disabled) return          
          segmentSeatSelection[selectedPax] = cell
          setSegmentSeatSelection(segmentSeatSelection)
          }}>
            {cell == null && <div>
              <div></div>
              <div></div>
            </div>}
            {cell != null && <div>
              {kvp == null && <div>{cell.row_number}{cell.column_id}</div>}
              {kvp != null && <div>{kvp[0]}</div>}
              <div>{pricing != null && formatCurrency(pricing.unit_price)}</div>
            </div>}
          </div>
      })}
    </div>
  })
}

const SegmentTab = ({
  segment,
  key,
  selectedPax,
  setSelectedPax,
  segmentSeatSelection,
  setSegmentSeatSelection,
  forEachPax
}: SegmentProps) => {
  
  return <Tabs.TabPane tab={segment.origin.code + ' -> ' + segment.destination.code} key={key}>
    <Row>
      <Col span={24} className="pax-list">
        <table>
          <tbody>
            <tr>
              <th style={{width: '15%'}}></th>
              <th style={{width: '25%'}}>Weight</th>
              <th style={{width: '20%'}}>Oversized</th>
              <th style={{width: '20%'}}>Pieces</th>
              <th style={{width: '20%'}}>Seats</th>
            </tr>
            {forEachPax(({
              key, selected,
              alcXbag, alcXbagValue, setAlcXbagValue,
              alcBulk, alcBulkValue, setAlcBulkValue,
              alcPiec, alcPiecValue, setAlcPiecValue,
            }: PaxProps) => {
              return <tr key={key} className={selected ? "selected" : ""}>
                <th>{key}</th>
                <td>
                  {alcXbag != null &&
                    <Select style={{width: '90%'}} value={alcXbagValue} onChange={setAlcXbagValue}>
                      <Select.Option value={0}>No extra weight</Select.Option>
                      {
                        range(10).map((x) => {
                        var amt = (x + 1) * 5
                        var price = Big(amt).times(alcXbag.unit_price).toFixed(2)
                        return <Select.Option key={x} value={amt}>+{amt} {alcXbag.name.split(" ")[1]} ({formatCurrency(price)})</Select.Option>
                      })
                      }
                    </Select>
                  }
                  {alcXbag == null &&
                    <span>Not available</span>
                  }
                </td>
                <td>
                  {alcBulk != null &&
                    <Select style={{width: '90%'}} value={alcBulkValue} onChange={setAlcBulkValue}>
                      <Select.Option value={0}>No oversized</Select.Option>
                      <Select.Option value={1}>Oversized (+{formatCurrency(alcBulk.unit_price)})</Select.Option>
                    </Select>
                  }
                  {alcBulk == null &&
                    <span>Not available</span>
                  }
                </td>
                <td>
                  {alcPiec != null &&
                    <Select style={{width: '90%'}} value={alcPiecValue} onChange={setAlcPiecValue}>
                      <Select.Option value={0}>No extra piece</Select.Option>
                      <Select.Option value={1}>Extra piece (+{formatCurrency(alcPiec.unit_price)})</Select.Option>
                    </Select>
                  }
                  {alcPiec == null &&
                    <span>Not available</span>
                  }
                </td>
                <td>{segmentSeatSelection[key] == null ? 'None' : (segmentSeatSelection[key].row_number + segmentSeatSelection[key].column_id)} (<span className="seat-selection" onClick={() => setSelectedPax(key)}>{selected ? "Select on seat map" : "Change"}</span>)</td>
              </tr>})}
          </tbody>
        </table>
      </Col>
    </Row>
    <Row>
      <Col span={24} className="seat-map">
        {segment.seats_maps?.map((sm, index) => <SeatMap seatPricings={segment.seats_pricings} selectedPax={selectedPax} seatMap={sm} key={index} segmentSeatSelection={segmentSeatSelection} setSegmentSeatSelection={setSegmentSeatSelection} />)}
      </Col>
    </Row>
  </Tabs.TabPane>
}

const FlightSeatsPage = ({client, cart}) => {
  const router = useRouter()
  const query = router.query
  const flightSeatsState = useFlightAncillaries({client})
  const {flightAncillaries, setFlightAncillaries, getAllSelections} = flightSeatsState

  var loaded = false
  
  useEffect(() => {
    ;(async function() {
      try {
        if (query.ptc_adt == null || loaded) return
        const ptc_adt = parseInt(query.ptc_adt + "")
        const ptc_cnn = parseInt(query.ptc_cnn + "")

        if (isNaN(ptc_adt) || isNaN(ptc_cnn)) throw new Error("Invalid ptc_adt or ptc_cnn")
        if (ptc_adt + ptc_cnn > 9) throw new Error("Too many ptc_adt or ptc_cnn")

        setFlightAncillaries({loading: true})
        loaded = true
        
        /*
        const response = await fetch(
            `http://localhost:3030/static/flight_ancillaries.json`
        );
        const json = await response.json();
        */
        

        const json = await client.get('rpc/flight_ancillaries', query)

        if (json.result == null) {
          setFlightAncillaries({error: true})
        } else {
          setFlightAncillaries(json)
        }

      } catch (e) {
        setFlightAncillaries({error: true})
        console.error(e);
      }

      })();
    }, [query]) 

  return <div className="main-container">
    <Button style={{float:'right'}} onClick={() => {
      const {seats, services} = getAllSelections()
      cart
        .reset()
        .addFlightWithAncillaries(flightAncillaries.result, {pricingId: flightAncillaries.result.pricings[0].id, seats, services, prime_workflow: true})
      router.push("/checkout")
    }}>Next Step</Button>
    <div style={{clear: 'both'}}></div>
    {flightAncillaries.result == null && (
      <NoResult
        response={flightAncillaries}
        type="flights"
        loadingMessage="Loading flight ancillaries..."
      />)}
    {flightAncillaries.result != null &&
      <Tabs>
        {flightSeatsState.forEachFareSegment(SegmentTab)}
        <Tabs.TabPane tab="Fare rules" key="rules">
          {flightAncillaries?.result?.pricings[0].fare_rules.map((rule, index) => {
            return <div key={index}>
              <h3>{rule.origin_code} -&gt; {rule.destination_code} ({rule.fare_basis} - {rule.name})</h3>
              {rule.rules.map((txt) => {
                return <>
                  <h4>{txt.category_name}</h4>
                  <div>{txt.text}</div>
                </>
              })}
            </div>
          })}
        </Tabs.TabPane>
      </Tabs>}
  </div>
}

export default FlightSeatsPage
