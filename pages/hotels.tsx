import React from 'react'
import { Row, Col, Spin } from 'antd'
import { TravelCloudClient, formatCurrency } from '../travelcloud'
import { LegacyHotelsParamsForm } from '../components/legacy-hotels-params-form'
import { LegacyHotelsResult } from '../components/legacy-hotels-result'
import { NoResult } from '../components/no-result'
import config from '../customize/config'
import Head from 'next/head'
import Router from 'next/router'

export default class extends React.PureComponent<any> {
  private client = new TravelCloudClient(config);

  static async getInitialProps (context) {
    const query = context.query
    return { query }
  }

  state = {
    hotels: {} as any,
    loading: false,
    cityCode: this.props.query.cityCode,
    checkInDate: this.props.query.checkInDate,
    checkOutDate: this.props.query.checkOutDate,
    adults: this.props.query.adults || 2,
    children: 0
  }

  async componentDidMount() {
    // update state with loading true
    this.setState({loading: true})

    // search hotels with default state
    const hotels = await this.client.legacyHotels(this.state)

    // update state with hotels
    this.setState({loading: false, hotels})
  }

  async paramsChange(value) {
    // update url
    Router.push({
      pathname: Router.pathname,
      query: value
    })

    // update state with filter values and loading true
    this.setState({loading: true, ...value})

    // search hotels with filter values
    const hotels = await this.client.legacyHotels(value)

    // update state with hotels and loading false
    this.setState({loading: false, hotels})
  }

  onHotelClick(hotel) {
    Router.push({
      pathname: '/hotel',
      query: {
        hotelId: hotel.hotelId,
        cityCode: this.props.query.cityCode,
        checkInDate: this.props.query.checkInDate,
        checkOutDate: this.props.query.checkOutDate,
        adults: this.props.query.adults || 2,
        children: 0}
    })
    this.setState({
      loading: true
    })
  }

  render() {
    return (
      <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32}}>
        <Head>
          <title>{config.defaultTitle} | Hotels</title>
        </Head>
        <Row gutter={32}>
          <Col span={6}>
            <h1>Hotels</h1>
            <LegacyHotelsParamsForm
              client={this.client}
              onChange={(value) => this.paramsChange(value)}
              value={this.state}
              defaultCityCodes={['SIN', 'BKK', 'KUL', 'HKG']}
              />
          </Col>
          <Col span={18}>
            {this.state.loading || this.state.hotels.result == null || this.state.hotels.result.length === 0
              ? <NoResult
                  style={{paddingTop: 128}}
                  response={this.state.hotels}
                  loading={this.state.loading}
                  type="hotels">
                  Please enter your destination and dates on the left
                </NoResult>
              : <LegacyHotelsResult
                  hotels={this.state.hotels.result}
                  onHotelClick={(hotel)=>this.onHotelClick(hotel)}
                  />}
          </Col>
        </Row>
      </div>
    )
  }
}
