import React, { createContext, useContext, Fragment, useRef, useCallback, useState, useEffect} from 'react'
import { TravelCloudClient, Cart, validateFlightsParams, TcResponse, FlightsParams } from '../travelcloud'
import {
  Button,
  Icon,
  Collapse,
  Col,
  Row,
  Dropdown,
  Checkbox,
  Slider,
  Tooltip,
  Drawer,
  Divider,
  Popover,
  Modal
} from "antd";
import {
  FlightsParamsAdvancedForm,
  SearchComponentDrawer,
  SearchComponentDropdown,
  FlightSearchTrigger,
  Steps,
  ResultNumber,
  SortButtons,
  FilterResults,
  FilterResultWrapper
} from "../components/flights-params-form2";
import moment from "moment";
import FareBreakdownModal from "../components/fare-breakdown";
import FareRulesModal from "../components/fare-rules";
import { FlightsResult } from '../components/flights-result3'
// import { FlightsResult2 } from '../components/flights-result2'
import { NoResult } from '../components/no-result'
import config from '../customize/config'
import Head from 'next/head'
import Router from 'next/router'
import { FlightSearch, FlightDetail } from '../types'
import { useFlights, useFlightParams, useFlightFilters, 
  filterFlights, useAirFares, createFlightStopsFilter, createAirlinesFilter, useFlightSorter, useUrlDispatch, parseFilters, useFlightFilterResults, FlightOd1OriginDepartureDatePicker } from '../components/flights/component-logic'
import { SiteContext } from './_app';
import { FareRules } from '../components/fare-rules2';

const cabinType = {
  Y: "Economy",
  S: "Premium Economy",
  C: "Business",
  F: "First Class"
};

export default function Flights ({cart, query, client} : {cart: Cart, query:any, client: TravelCloudClient}):any {
  const site = useContext(SiteContext)
  const onPricingSelect = (flight, pricing_id, search_id) => {
    cart
      .reset()
      .addFlightWithAncillaries(flight, {pricing_id, search_id});
    Router.push("/checkout");}
  const onSeatSelect = (query) => {
    Router.push({pathname:"/flight-seats", query})
  }
  const onLoadStart = (flightsParam) => Router.push({ pathname: Router.pathname, query:  flightsParam })
  const data = useFlights({ defaultType: 'return', query, client, sources: [ query.source == null ? 'travelport' : query.source ], onLoadStart, autoSearch: false, onPricingSelect, onSeatSelect })
  const { validateAndSearch, handleParamsChange,
    flights, flightsParam, flightType, flightMap,
    onFlightClick, onFareClick,
    isPrimeWorflowModelVisible, choosePrimeWorkflow, chooseNotPrimeWorkflow } = data

    /*
    const flightStopsFilter = createFlightStopsFilter( flights )
    const airlinesFilter = createAirlineFilter( flights )

    const filteredFlights = useFlightFilters( flights, parseFilters( filters ))
    const sortedFlights = useFlightSorters( filteredFlights, sort )
    */
  const { fareBreakdownModalData, fareRulesModalData, showFareBreakdownModal, showFareRulesModal } = useAirFares()

  const [ limit , setLimit ] = useState(50);

  useEffect(() => validateAndSearch(), []);

  function customPriceDisplay(defaultPriceDisplay) {
    return (
      "S$" +
      Math.ceil(Number(defaultPriceDisplay.replace("$", ""))).toLocaleString()
    );
  }

  function formatToHours(value){
    return `0${value}:00`.slice(-5).replace("24:00", "23:59");
  };

  // handleFilters(filterProperty, value)
  // filterProperty { stops, price_range, outbound_departure, outbound_arrival, return_departure, return_arrival, airlines }
  // todo: refactor to place the same logic together
  // const [ filteredFlights, filters, handleFilters ] = useFlightFilters( flights, cart, query )
  const [ filters, handleFilters ] = useFlightFilters( query )
  useUrlDispatch( filters )

  const filteredFlights = useFlightFilterResults(flights, filters, cart)
  const [ sortedFilter, sort, setSort] = useFlightSorter( filteredFlights, "default" )

  // todo: refactor to place the logic together
  // const [ sort, setSort ] = useState("default");
  // const sortedFilter = useFlightSorter( fliteredFlights, sort )

  const numberOfTravellers =
    parseInt(flightsParam.ptc_adt) +
    parseInt(flightsParam.ptc_cnn || 0) +
    parseInt(flightsParam.ptc_inf || 0);

  return (
    <div>
      <div style={{backgroundColor: '#fff', margin: '48px', padding: 32}}>
        <Head>
          <title>{config.defaultTitle} | Flights</title>
        </Head>

        <h1>Flights</h1>
        <FlightsParamsAdvancedForm
          client={client}
          flightParamStore={data}
          // searchMode={searchMode}
          onChange={value => handleParamsChange(value)}
          onSearch={value => validateAndSearch()}
          value={flightsParam}
          defaultAirportCodes={["SIN", "BKK", "KUL", "HKG"]}
          defaultCityCodes={["NYC"]}
          airportsOnly={query.source === 'pkfare'}
          showOd4={query.source === 'sqndc'}
          loading={flights.loading === true}
        />
      </div>
      <div className="search-flight-filter">
        <Row gutter={50}>
          <Col xs={24} lg={6} offset={1}>
            {(flights.result && flights.result.length > 0) ? (
              <FilterResultWrapper
                flights={flights}
                customPriceDisplay={customPriceDisplay} 
                query={query} 
                handleFilters={handleFilters}
                cart={cart}
                // airlineCharLimit={21}
                resultElement={({ filters, handleFilters, airlines, flightStops }) => (
                  <FilterResults
                    flightStops={flightStops}
                    handleFilters={handleFilters}
                    filters={filters}
                    formatToHours={formatToHours}
                    flightType={flightType}
                    airlines={airlines}
                  />)}
                />
            ) : null}
          </Col>
          {flights.result == null || flights.result.length === 0 ? (
            <Col span={24} className="pad-y">
              <NoResult
                response={flights}
                type="flights"
                loadingMessage="Searching for the lowest fares..."
              />
            </Col>
          ) : (
            <Col xs={24} lg={16}>
              <Fragment>
                <Row type="flex" gutter={24}>
                  <SortButtons sort={sort} setSort={setSort} />
                </Row>

                <FlightsResult
                  cart={cart}
                  flights={sortedFilter.slice(0, limit)}
                  flightMap={flightMap}
                  onFlightClick={onFlightClick}
                  onFareClick={onFareClick}
                  onFareRulesClick={(flight, pricing_id) => {
                    site.openCustom(<FareRules autoOpen={true} flight={flight} pricing_id={pricing_id} client={client} />)
                  }}
                />
              </Fragment>

              { /** Flight Result Component - 2 **/
                /* 
                <FlightsResult2
                cart={cart}
                flights={sortedFilter.slice(0, limit)}
                flightMap={flightMap}
                onFlightClick={(index, flight) => {
                  handleFlightMap(client, index, flight);
                  // for webconnect
                  // cart
                  //   .reset()
                  //   .addFlight(flight, flight["pricings"][0]["id"]);
                  // Router.push("/checkout");
                }}
                onFareClick={(index, flight, fareId) => {
                  cart.reset().addFlight(flight, fareId);
                  Router.push("/checkout");
                }}

                onFareBreakdownClick={details =>
                  showFareBreakdownModal(details)
                }
                onFareRulesClick={details => showFareRulesModal(details)}
                showFlightModal={flightModal}
                showFlightMapModal={detail => showFlightMapModal(detail)}
                type={flightType}
                windowWidth={windowSize.width}
                cabin={cabinType[flightsParam.cabin]}
              /> */}
            </Col>
          )}
        </Row>
        <FareBreakdownModal {...fareBreakdownModalData} />
        <FareRulesModal {...fareRulesModalData} />
      </div>

      <Modal title="Select booking type" visible={isPrimeWorflowModelVisible} footer={null}>
        <Button onClick={chooseNotPrimeWorkflow}>Reserve my seat and make payment later.</Button>
        <Button onClick={choosePrimeWorkflow}>Instant confirmation. Make payment online.</Button>
      </Modal>
    </div>
  );

}

Flights.getInitialProps = (context: any) => {
    const query = context.query
    return { query }
}
