import React, { useState, useEffect, CSSProperties } from 'react'
import { Row, Col, Menu, List, Tabs, Button, Checkbox, DatePicker, Select, Switch } from 'antd'
import { TravelCloudClient, TcResponse, Cart, formatCurrency } from '../travelcloud'
import { NoResult } from '../components/no-result'
import config from '../customize/config'
import Head from 'next/head'
import Router, { useRouter } from 'next/router'
import { NextPage, NextPageContext } from 'next'
import { Generic, Order } from '../types'
import { useGenericsBookingForm, GenericView, GenericViewOptions } from '../components/hooks/generics-booking'
import moment from "moment";
import { IncDecInput } from '../components/inc-dec-input'
import { orderToItineraryDays, ItineraryDay } from '../components/itinerary'

const { Option } = Select;
const { TabPane } = Tabs;

const Itinerary = (
  {itineraryDays, showDescriptions} : {itineraryDays: ItineraryDay[], showDescriptions}) => {

  return <div>
    <ul className="timeline">
    {itineraryDays.map((itineraryDay, index) => {
      return [
        <li key={index} className="date-header" data-date={moment(itineraryDay.date).format('MMMM Do YYYY')}>
          &nbsp;
        </li>,
        itineraryDay.schedules.map((schedule, index2) => {
          if (schedule.product_type === 'nontour_product') {
            const productDetail: any = schedule.products[0].product_detail
            return <li key={index + '/' + index2} className="event" data-date={schedule.time}>
              <h3>{schedule.products[0].product_name}</h3>
              {showDescriptions && <>
                <div style={{
                  backgroundImage: `url(${productDetail.photo_url})`,
                  height: 250,
                  width: 350,
                  border: '1px solid #999',
                  margin: '0 10px 10px 0',
                  backgroundSize: 'cover',
                  backgroundPosition: 'center',
                  float: 'left'}} />
                <div dangerouslySetInnerHTML={{__html: productDetail.description}} />
                <div style={{clear: 'both'}} />
              </>}
            </li>
          }
        })
      
      ]
    })}
    </ul>
  </div>
  }

interface Context extends NextPageContext {
  cart: Cart,
  order: TcResponse<Order>,
  openCart
}

const categories = [
  {
    label: "Airport Transfer",
    type: "generics",
    params: {
      'categories.name':'Limousine Service'
    }
  },
  {
    label: "Meals",
    type: "generics",
    params: {
      'categories.name':'Meals'
    }
  },
  {
    label: "Attractions",
    type: "generics",
    params: {
      'categories.name':'Attractions'
    }
  }
]

const ItineraryBuilderPage: NextPage<{categories, genericsServer: TcResponse<Generic[]>, cart: Cart, order: TcResponse<Order>, openCart}> = (props) => {
  const router = useRouter()
  const client = new TravelCloudClient(config)
  const {cart, order, openCart} = props
  const [generics, setGenerics] = useState<TcResponse<any>>({})
  const [navIndex, setNavIndex] = useState(null)
  const [tabKey, setTabKey] = useState('1')
  const [showDescriptions, setShowDescriptions] = useState(true)
 
  const navClick = async (index) => {
    const clickItem = categories[parseInt(index)]
    setNavIndex(index)
    if (clickItem.type === 'generics') {
      if (generics.loading) return
      setGenerics({loading: true})
      const genericsLoading = await client.generics(clickItem.params)
      setGenerics(genericsLoading)

    }
  }

  useEffect(() => {
    navClick("0")
  }, [])

  const itineraryDays = (props.order == null || props.order.result == null || props.order.result.products == null || props.order.result.products.length === 0)
    ? []
    : orderToItineraryDays(props.order.result)

  const {
    minDateTime,
    genericsView
  } = useGenericsBookingForm(generics, cart, order)

  return <div style={{minHeight: 1000, backgroundColor: '#fff', padding: 42}}>
    <Head>
      <title>{config.defaultTitle} | Itinerary builder</title>
    </Head>
    <Tabs activeKey={tabKey} onTabClick={(x) => setTabKey(x)}>
      <TabPane tab="Builder" key="1">
        <Row gutter={32}>
          <Col span={6}>
          <Menu
            selectedKeys={[navIndex + ""]}
            onClick={(x) => navClick(x.key)}
            mode="vertical">
            {categories.map((category, index) =>
              <Menu.Item key={index + ""}>
                <span>{category.label}</span>
              </Menu.Item>)}
          </Menu>
          </Col>
          <Col span={18}>
            {generics == null || generics.loading || generics.result == null || generics.result.length === 0
              ? <NoResult
                  style={{paddingTop: 128}}
                  response={generics}
                  loading={generics != null && generics.loading}
                  type="generics" />
              : <GenericsResult genericsView={genericsView} onUpdateCart={() => setTabKey("2")} minDateTime={minDateTime} />}
          </Col>
        </Row>
      </TabPane>
      <TabPane tab="Itinerary" key="2">
        <div>
          <Switch onChange={(checked) => setShowDescriptions(checked)} checked={showDescriptions} /> Show descriptions
        </div>
        {
          itineraryDays.length === 0
            ? <div>No Itinerary</div>
            : <Itinerary itineraryDays={itineraryDays} showDescriptions={showDescriptions} />
        }
      </TabPane>
    </Tabs>
  </div>
}

export default ItineraryBuilderPage

const GenericsResult: React.ComponentType<{minDateTime, genericsView: GenericView[], onUpdateCart}> = ({minDateTime, genericsView, onUpdateCart}) => {
  return <List
    dataSource={genericsView}
    renderItem={(generic: GenericView) => {
      const isLimousineService = generic.categories.find(category => category.name === "Limousine Service")
      return <List.Item>
        <div style={{width: '100%'}}>
          <div style={{
            backgroundImage: `url(${generic.photo_url})`,
            height: 400,
            width: "40%",
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            float: 'left'}} />
          <div style={{padding: '16px 32px', float: 'left', width: "60%"}}>
            <h2 className="tc-full-width-ellipsis">{generic.name}</h2>
            <Tabs defaultActiveKey="1">
              <Tabs.TabPane tab="Prices" key="1">
                <table>
                  {
                    isLimousineService
                    ? <tbody>
                        {generic.options.map((option: GenericViewOptions) => {
                        return <tr key={option.id}>
                          <td style={{paddingRight: 32, minWidth: 150}}>
                            <Checkbox
                              checked={option._view.quantity > 0}
                              onChange={(e)=> {
                                if (e.target.checked) {
                                  option._view.updateQuantity(1)
                                  option._view.updateDateTime(moment())
                                }
                                else {
                                  option._view.updateQuantity(0)
                                  option._view.updateDateTime(null)
                                }
                              }}>
                              {option.name}
                            </Checkbox>
                          </td>
                          <td style={{paddingRight: 32, fontWeight: 'bold'}}>{
                            option._view.beforeDiscount.eq(option._view.afterDiscount) ?
                            <span>{formatCurrency(option.price)}</span> :
                          <><span style={{textDecoration: "line-through"}}>{formatCurrency(option._view.beforeDiscount)}</span> <span>{formatCurrency(option._view.afterDiscount)}
                          </span></>
                          }</td>
                          <td style={{width: 300}}>
                            <DateTimePicker defaultPickerValue={minDateTime} value={option._view.dateTime} onChange={(val) => {option._view.updateDateTime(val)}} />
                          </td>
                        </tr>})}
                        <tr style={{height: 16}}>
                          <td colSpan={3}></td>
                        </tr>
                        <tr style={{borderTop: '2px solid #999'}}>
                          <td>Total ({generic._view.totalQuantity} items)</td>
                          <td style={{fontWeight: 'bold'}}>{formatCurrency(generic._view.totalPrice)}</td>
                          <td>
                            <Button style={{width: '100%', margin: "8px 0"}}
                              type="primary"
                              onClick={() => {
                                generic._view.updateCart()
                                onUpdateCart()
                              }}>
                              Update itinerary
                            </Button>
                          </td>
                        </tr>
                      </tbody>
                    : <tbody>
                        {generic.options.map((option: GenericViewOptions) => {
                          return <tr key={option.id}>
                            <td style={{paddingRight: 32, minWidth: 150}}><div className="tc-full-width-ellipsis">{option.name}</div></td>
                            <td style={{paddingRight: 32, fontWeight: 'bold'}}>{
                              option._view.beforeDiscount.eq(option._view.afterDiscount) ?
                              <span>{formatCurrency(option.price)}</span> :
                              <><span style={{textDecoration: "line-through"}}>{formatCurrency(option._view.beforeDiscount)}</span> <span>{formatCurrency(option._view.afterDiscount)}</span></>
                            }</td>
                            <td style={{width: 150}}><IncDecInput min={0} max={option._view.maxQuantity} value={option._view.quantity} onChange={option._view.updateQuantity} /></td>
                          </tr>})}
                          <tr style={{height: 16}}>
                            <td colSpan={3}></td>
                          </tr>
                          <tr style={{borderTop: '2px solid #999'}}>
                            <td colSpan={2}><DateTimePicker defaultPickerValue={minDateTime} value={generic._view.dateTime} onChange={(val) => {generic._view.updateDateTime(val)}} /></td>
                            <td>
                              <Button style={{width: '100%', margin: "8px 0"}}
                                type="primary"
                                onClick={() => {
                                  generic._view.updateCart()
                                  onUpdateCart()
                                }}>
                                Update itinerary
                              </Button>
                            </td>
                          </tr>
                      </tbody>
                    }
                </table>
              </Tabs.TabPane>
              <Tabs.TabPane tab="Description" key="2"><div style={{height: 260, overflowY: 'auto'}} dangerouslySetInnerHTML={{__html: generic.description}} /></Tabs.TabPane>
            </Tabs>
          </div>
        </div>
      </List.Item>}}
    />
}

type DateTimePickerProps = {
  defaultPickerValue, value, onChange
}
const DateTimePicker = (props) => {
  const hours = props.value == null ? null : props.value.hours()
  return <>
    <DatePicker style={{width: '150px', marginRight: '10px'}} defaultPickerValue={props.defaultPickerValue} value={props.value} onChange={props.onChange} />
    <Select style={{width: '100px'}} disabled={props.value == null} onChange={x => props.onChange(props.value.hours(x).minutes(0))}  value={hours}>
      <Option value={1}>1 AM</Option>
      <Option value={2}>2 AM</Option>
      <Option value={3}>3 AM</Option>
      <Option value={4}>4 AM</Option>
      <Option value={5}>5 AM</Option>
      <Option value={6}>6 AM</Option>
      <Option value={7}>7 AM</Option>
      <Option value={8}>8 AM</Option>
      <Option value={9}>9 AM</Option>
      <Option value={10}>10 AM</Option>
      <Option value={11}>11 AM</Option>
      <Option value={12}>12 Noon</Option>
      <Option value={13}>1 PM</Option>
      <Option value={14}>2 PM</Option>
      <Option value={15}>3 PM</Option>
      <Option value={16}>4 PM</Option>
      <Option value={17}>5 PM</Option>
      <Option value={18}>6 PM</Option>
      <Option value={19}>7 PM</Option>
      <Option value={20}>8 PM</Option>
      <Option value={21}>9 PM</Option>
      <Option value={22}>10 PM</Option>
      <Option value={23}>11 PM</Option>
    </Select>
  </>
}