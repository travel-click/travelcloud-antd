import React from 'react'
import { Row, Col, Button } from 'antd';
import { TravelCloudClient, validateLegacyHotelsParams, validateFlightsParams, validateHotelbedsParams } from '../travelcloud'
import { LegacyHotelsParamsForm } from '../components/legacy-hotels-params-form'
import { FlightsParamsForm } from '../components/flights-params-form'
import { HotelbedsSearchForm } from '../components/hotelbeds-search-form'
import config from '../customize/config'
import Router from 'next/router'
import qs from 'qs'

export default class extends React.PureComponent<any> {
  private client = new TravelCloudClient(config)

  static async getInitialProps(context) {
    const client = new TravelCloudClient(config)
    const query = context.query
    return { query }
  }

  state = {
    hotelsParams: {
      cityCode: undefined,
      checkInDate: undefined,
      checkOutDate: undefined,
      adults: 2,
      children: 0
    },
    hotelsParamsInvalid: true,

    flightsParams: {
      source: 'travelport',
      'od1.origin_airport.code': undefined,
      'od1.origin_datetime': undefined,
      'od2.origin_airport.code': undefined,
      'od2.origin_datetime': undefined,
      cabin: 'Y',
      ptc_adt: 1,
      ptc_cnn: 0,
      ptc_inf: 0,
    },
    flightsParamsInvalid: true,
    hotelbeds: {
        code: undefined,
        checkIn: undefined,
        checkOut: undefined,
        shiftDays: 1,
        occupancies: []
    },
    hotelbedsParamsInvalid: true,
  }
  render() {
    return (
      <div className="main-container">
        <h2>Hotel Search</h2>
        <HotelbedsSearchForm
          client={this.client}
          onChange={(value) => this.setState({ hotelbeds: value, hotelbedsParamsInvalid: validateHotelbedsParams(value) == null })}
        />
        <div>
          <Button
              disabled={this.state.hotelbedsParamsInvalid}
              type="primary"
              onClick={() => Router.push('/hotelbeds-hotels?' + qs.stringify( this.state.hotelbeds ))}>
              Search Hotel
          </Button>
        </div>
        <h2 style={{marginTop: 32}}>Flight Search</h2>
        <FlightsParamsForm
          client={this.client}
          onChange={(value) => this.setState({ flightsParams: value, flightsParamsInvalid: validateFlightsParams(value) == null })}
          value={this.state.flightsParams}
          defaultAirportCodes={['SIN', 'BKK', 'KUL', 'HKG']}
          defaultCityCodes={['NYC']}>
          <Row>
            <Col span={12}>
              <FlightsParamsForm.Od1OriginCodeSelect style={{ width: '98%' }} placeholder="From" />
            </Col>
            <Col span={12}>
              <FlightsParamsForm.Od2OriginCodeSelect style={{ width: '98%' }} placeholder="To" />
            </Col>
          </Row>
          <div>
            <Row>
              <Col span={12}>
                <FlightsParamsForm.Od1OriginDepartureDatePicker style={{ width: '98%' }} placeholder="Depart" />
              </Col>
              <Col span={12}>
                <FlightsParamsForm.Od2OriginDepartureDatePicker style={{ width: '98%' }} placeholder="Return" />
              </Col>
            </Row>
          </div>
          <div>
            <Row>
              <Row>
                <Col span={8}>
                  <FlightsParamsForm.PtcAdtSelect style={{ width: '98%' }} />
                </Col>
                <Col span={8}>
                  <FlightsParamsForm.PtcCnnSelect style={{ width: '98%' }} />
                </Col>
                <Col span={8}>
                  <FlightsParamsForm.PtcInfSelect style={{ width: '97%' }} />
                </Col>
              </Row>
            </Row>
          </div>
        </FlightsParamsForm>
        <div>
          <Button
            type="primary"
            disabled={this.state.flightsParamsInvalid}
            onClick={() => Router.push({
              pathname: '/flights',
              query: this.state.flightsParams
            })}>
            Search Flight
        </Button>
        </div>
      </div>
    )
  }
}
