import React, { Fragment, useEffect, useState } from 'react'

import config from '../customize/config'

import { TravelCloudClient, Cart } from '../travelcloud'

import { Spin, Pagination, Collapse, Icon, Row, Col, Button, Modal } from 'antd';
import { GlobalTixSearchForm } from '../components/globaltix-search-form';
import Big from 'big.js';
import { NextPage, NextPageContext } from 'next';
import { useGlobaltixBookingForm, computeAttractionView } from '../components/hooks/globaltix-booking';
import { useRouter } from 'next/router'
import { GlobaltixBookingForm } from '../components/globaltix-booking-form';
import { globaltixFormValueToCartValue } from './globaltix-details';

const Panel = Collapse.Panel;

interface Context extends NextPageContext {
  cart: Cart,
  openCart
}

// DEPRECATED - please use computeAttractionView instead
export function computeGlobalTixAttraction(orgItem: any, cart?: Cart): any {
  if (cart == null) return orgItem

  const item = JSON.parse(JSON.stringify(orgItem))

  var priceRulesIndexed = {}

  const priceRulesCombined = cart.getCombinedPriceRules()
  priceRulesIndexed = priceRulesCombined.reduce((acc, pr) => {
  acc[pr.id] = pr
  return acc
  }, {})

  item.ticketTypes = item.ticketTypes.map((ticketType)=> {
      const product = cart.addGlobaltix(item, {
          id: ticketType.id,
          fromResellerId: ticketType.fromResellerId,
          quantity: 1
      }, true);

      if (product == null) return null

      // a better name for these would be beforeDiscounts/afterDiscounts
      const beforeDiscounts = product.items
      .filter((item) => parseInt(item['price']) > 0)
      .reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))

      const afterDiscounts = product.items
      .reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))

      const priceRuleIds = product.items.reduce((acc, item) => {
          if (item.price_rule_id != null) acc.push(item.price_rule_id)
          return acc
        }, [])

      const appliedPriceRules = priceRuleIds.reduce((acc, id) => {
          acc.push(priceRulesIndexed[id])
          return acc
      }, [])

      ticketType._computed = {beforeDiscounts, afterDiscounts, priceRuleIds, appliedPriceRules}

      return ticketType
  })

  return item
}

const GlobaltixSearchCartPage: NextPage<{}> = (props: any) => {
  const router = useRouter()
  const {searchParams, attractionList, ticketTypeGet, fetchTicketTypeGet, clearTicketTypeGet, updateSearchParamsAndSearch} = useGlobaltixBookingForm(props.client)
  const [selectedAttraction, setSelectedAttraction] = useState()

  const loading = attractionList != null &&  attractionList.loading
  const attractionViewList = attractionList != null &&  attractionList.result != null && attractionList.result.data.map(attraction => computeAttractionView(attraction, props.cart))
  const totalNumberOfItems = attractionList != null &&  attractionList.result != null && attractionList.result.size
  const ticketTypeGetData = ticketTypeGet != null && ticketTypeGet.result != null && ticketTypeGet.result.data

  useEffect(() => {updateSearchParamsAndSearch()}, [])

  return (
    <div style={{ minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32 }}>
      <Fragment key="2">
        <section id="content" key="3">
          <article id="search" key="a1">
            <GlobalTixSearchForm countryList={props.countryList} cityList={props.cityList} searchFormKeys={searchParams} onSearch={(value, detail) => updateSearchParamsAndSearch(value, detail)} />
          </article>
          {totalNumberOfItems &&
            <div style={{marginBottom: '2em'}}>
              <Pagination current={searchParams.page} pageSize={10} total={totalNumberOfItems} onChange={(pageNumber) => updateSearchParamsAndSearch(pageNumber, 'page')} /> </div>
          }
          <article key="a2">
            {loading &&
              <div style={{textAlign: 'center'}}><Spin tip="Loading..." spinning={loading} /></div>
            }
            {!loading &&
              <div key={"main-container"}  className="globaltix-result">
              {attractionViewList == false || attractionViewList.length === 0 ? (
                <h4>No matching products found!</h4>
              ) : (
                  attractionViewList.map((attraction, index) => {
        
                      return (
                        <Collapse
                          className="main-collaspe-relative"
                          key={"collaspepanel" + index}
                          accordion
                        >
                          <Panel
                            header={
                              <div className="header-list1" key={"header" + index}>
                                <Icon key={"starticon" + index} type="star" className="list-star" />
                                <img
                                  width="10%"
                                  src={
                                    "https://uat-api.globaltix.com/api/image?name=" +
                                    attraction.imagePath
                                  }
                                  alt="No Image"
                                  key={"image" + index}
                                />
                                <span key={"title" + index}>{attraction.title}</span>
                                <span key={"iconspan" + index}>
                                  <Icon
                                    type="info-circle"
                                    className="attractionInfo"
                                    onClick={(event) => {
                                      event.preventDefault()
                                      router.push({
                                        pathname: '/globaltix-details',
                                        query: {'attractionId': attraction.id }})
                                      }}
                                  />
                                </span>
                              </div>
                            }
                            key={"upperpanel" + index}
                          >
                            {attraction.ticketTypes.map(ticket => (
                              <div className="tickets-details" key={"ticket-details" + ticket.id}>
                                <Row type="flex" justify="space-between" align="middle" gutter={10} key={"ticket-row" + ticket.id}>
                                  <Col lg={1} md={24} sm={24} xs={24} key={"ticket-col" + ticket.id}>
                                    <Icon type="star" className="list-star1" key={"ticket-star" + ticket.id} />
                                  </Col>
                                  <Col lg={10} md={24} sm={24} xs={24} className="ticket-name" key={"ticket-name" + ticket.id}>
                                    <div key={"catgory-name" + ticket.id}><div className="catgory-name" key={"catgory-name-div" + ticket.id}>{ticket.variation.name}</div><strong>{ticket.name}</strong><div className="ticketname-des" key={"ticketname-des" + ticket.id}>{'Usual Price ' + ticket.merchantCurrency + ' ' + parseFloat(ticket.originalPrice).toFixed(2)}</div><div className="ticket-merchant" key={"ticket-merchant" + ticket.id}>{'Merchant ' + ticket.sourceName}</div></div>
                                  </Col>
                                  <Col lg={8} md={24} sm={24} xs={24} className="ticket-price" key={"ticket-price" + ticket.id}>
                                    <div className="rghtAlign-marginRght" key={"rghtAlign-marginRght" + ticket.id}>
                                      {ticket._view == null
                                       && <div key={"ticket-currency" + ticket.id}><strong>{ticket.currency + ' '}</strong>{parseFloat(ticket.travelcloud_price).toFixed(2)}</div>}
        
                                      {ticket._view != null && ticket._view.beforeDiscounts.eq(ticket._view.afterDiscounts)
                                       && <div key={"ticket-currency" + ticket.id}><strong>{ticket.currency + ' '}</strong>{ ticket._view.afterDiscounts.toFixed(2)}</div>}
        
                                      {ticket._view != null && !ticket._view.beforeDiscounts.eq(ticket._view.afterDiscounts)
                                       && <>
                                        <div key={"ticket-currency" + ticket.id}>
                                          <strong>{ticket.currency + ' '}</strong>
                                          <span style={{ textDecoration: 'line-through' }}>{ticket._view.beforeDiscounts.toFixed(2)} </span>
                                          { ticket._view.afterDiscounts.toFixed(2)}</div>
                                        </>}
                                    </div>
                                  </Col>
                                  <Col lg={2} md={24} sm={24} xs={24} className="addto-cart" key={"addto-cart" + ticket.id}>
                                    <Button type="primary" onClick={() => {setSelectedAttraction(attraction); fetchTicketTypeGet(ticket.id)}}>Booking</Button>
                                  </Col>
                                </Row>
                              </div>
                            ))}
                          </Panel>
                        </Collapse>
                      );
                    }
                  )
                )}
            </div>
            }
          </article>
        </section>
      </Fragment>
      <Modal
        title={ticketTypeGetData.name}
        visible={ticketTypeGet != null}
        onCancel={() => clearTicketTypeGet()}
        width={800}
        footer={null}
      >
        <Spin spinning={ticketTypeGet != null && ticketTypeGet.loading === true}>
          <div style={{minHeight: '400px', maxHeight: '600px', 'overflowY': 'auto'}}>
            {ticketTypeGetData && <>
              <h3>Description</h3>
              <p>{ticketTypeGetData.description}</p>
              {ticketTypeGetData.variation ? <><h3>Type: </h3> <p>{ticketTypeGetData.variation.name}</p></> : ''}
              <><h3>Terms & Condition: </h3><p>{ticketTypeGetData.termsAndConditions}</p></>
              <hr style={{color: '#DDD', margin: '12px 0'}} />
              <GlobaltixBookingForm
                ticketTypeGet={ticketTypeGet}
                onAddToCart={(formValue)=> {
                  props.cart.addGlobaltix(selectedAttraction, globaltixFormValueToCartValue(selectedAttraction, ticketTypeGetData.id, formValue))
                  props.openCart()
                  clearTicketTypeGet()
                }} />
            </>}
          </div>
        </Spin>
      </Modal>
    </div>
  )
}

GlobaltixSearchCartPage.getInitialProps = async (context: Context) => {
  const client = new TravelCloudClient(config)
  // step 1
  // https://documenter.getpostman.com/view/3866783/globaltix-partner-api/RVnSH2gE#6a8ab8a6-c06d-9ee8-dd58-ddb0ea34f339
  const countryList = await client.globaltix('country/getAllListingCountry');
  const result = countryList.result.data;

  // https://documenter.getpostman.com/view/3866783/globaltix-partner-api/RVnSH2gE#b26a2600-47c2-a452-98c4-c2895941788c
  const cityList = await client.globaltix('city/getAllCities');
  const cityresult = cityList.result.data;

  return { countryList: result, cityList: cityresult }
}

export default GlobaltixSearchCartPage;