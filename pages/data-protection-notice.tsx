import React from 'react'
import config from '../customize/config'
import { NextPage } from 'next'
import { DataProtectionNotice } from '../components/data-protection-notice'


export default () => {
  return <div style={{margin: '32px'}}>
    <DataProtectionNotice companyName={config.companyName} dpoContact={
      <p>
      <div>John Doe</div>
      <div>dpo@pytheas.travel</div>
    </p>} />
  </div>
}