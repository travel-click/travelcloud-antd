import React from 'react'
import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import { TravelCloudClient } from "../travelcloud"
import { TcDocument } from '../components/tc-document'
import config from '../customize/config'

const cmsdemo = ({ client, contents }) => {
  console.log('contents xxyyyzzz', contents)
  return <>
    <Head>
      <TcDocument contents={contents} blockId="__head" />
    </Head>
    <TcDocument contents={contents} blockId="body.html" />
    <Link href='/cmsdemo2'>
      <a>Go to Demo 2</a>
    </Link>
  </>
}

cmsdemo.getInitialProps = async ({ asPath }) => {
  const client = new TravelCloudClient(config)
  const contents = await client.getDocument(asPath)
  return {contents}
}

export default cmsdemo