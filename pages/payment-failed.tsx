export default ({client, cart}) => {
  return (
    <div className="main-container">
      <h2>We are unable to process your payment</h2>
      <p>Our friendly travel consultants will contact your within the next 24 hours to help you finalize your booking.</p>
    </div>
  )
}