export default ({client, cart}) => {
  return (
    <div className="main-container">
      <h2>We have received your payment</h2>
      <p>Our systems are in the process of finalizing your booking. You should receive your invoice within 10 minutes. If you have made flight or hotel bookings, you should receive travel vouchers within the next 60 minutes.</p>
    </div>
  )
}