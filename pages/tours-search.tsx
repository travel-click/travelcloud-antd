import React from 'react'
import { Row, Col } from 'antd'
import { ToursResult } from '../components/tours-result'
import { TravelCloudClient } from '../travelcloud'
import { ToursParamsForm } from '../components/tours-params-form'
import { NoResult } from '../components/no-result'
import config from '../customize/config'
import Head from 'next/head'
import Router from 'next/router'
import { SiteContext } from './_app'

export default class extends React.Component<any, any> {
  static contextType = SiteContext

  static async getInitialProps (context) {
    const client = new TravelCloudClient(config)
    const categories = await client.categories({category_type: 'tour_package'})
    return { categories }
  }

  state = {
    'categories.name': null,
    '~boolean': null,
    tours: {loading: true} as any,
    query: {} as any
  }
  
  async componentDidMount() {
    const tours = await this.props.client.tours({})
    this.setState({tours})
  }

  async paramsChange(query) {
    // update state with filter values and loading true
    this.setState({query, tours: {loading: true}})

    const tours = await this.props.client.tours(query)
    this.setState({tours})
  }

  onTourClick(tour) {
    Router.push({
      pathname: '/tour/' + tour.id
    })
    this.setState({
      loading: true
    })
  }

  render() {
    
    return (
      <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32}}>
        <Head>
          <title>{config.defaultTitle} | Tours</title>
        </Head>
        <Row gutter={32}>
          <Col span={6}>
            <h1>Tour filters</h1>
            <ToursParamsForm
              categories={this.props.categories}
              onChange={(value) => this.paramsChange(value)}
              value={this.state.query} />
          </Col>
          <Col span={18}>
            {this.state.tours.result == null || this.state.tours.result.length === 0
              ? <NoResult
                  style={{paddingTop: 128}}
                  response={this.state.tours}
                  loading={this.state.tours.loading === true}
                  type="tours" />
              : <ToursResult
                  cart={this.props.cart}
                  toursResult={this.state.tours.result}
                  onTourClick={(tour) => this.onTourClick(tour)} />}
          </Col>
        </Row>
      </div>
    )
  }
}