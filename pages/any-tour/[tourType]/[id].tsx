import React, { useContext, useEffect } from 'react'
import { Select, Button } from 'antd'
import { TravelCloudClient, TcResponse, Cart, dateToIsoDate, range, formatCurrency } from '../../../travelcloud'
import config from '../../../customize/config'
import { NextPage, NextPageContext } from 'next'
import { useRouter } from 'next/router'
import { CalendarHorizontal } from '../../../components/calendar'
import { newDateLocal, TourType } from '../../../order-product-compute'
import Big from 'big.js'
import { useAnyTourBooking } from '../../../components/hooks/any-tour-booking'
import { v2_GroupTour, v2_PrivateTour } from '../../../types'
import { SiteContext } from '../../_app'
const { Option } = Select;

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

const AnyTourPage: NextPage<{anyTour: TcResponse<v2_PrivateTour | v2_GroupTour>, anyTourOptions: TcResponse<v2_PrivateTour[] | v2_GroupTour[]>, params, limits}> = (props) => {
  const router = useRouter()
  const siteContext = useContext(SiteContext)
  const anyTour = props.anyTour?.result
  const anyTourOptions: Array<v2_PrivateTour | v2_GroupTour> = props.anyTourOptions?.result

  const tourTypeMap = {
    'group': TourType.GroupTour,
    'private': TourType.PrivateTour
  }

  const tourType = tourTypeMap[props?.params?.tourType ?? '']
  
  const bookingFormState = useAnyTourBooking(anyTour, tourType, siteContext.cart, siteContext.order)

  if (anyTour == null || tourType == null) return <div>Tour not found</div>

  /*
  useEffect(() => {
    siteContext?.cart?.refreshLimits(anyTour?.price_rules?.map((pr) => pr.limit_code).filter((pr) => pr != null && pr.trim() !== ""))
  }, [siteContext.cart])
  */

  const RoomInput = ({adult, set_adult, child, set_child, room_num}) => {
    const pax = adult + child
    const roomPrice = Big(anyTour['price_' + pax]).minus(Big(anyTour.price_child_discount).mul(child)).toFixed(2)

    if (!bookingFormState.hasChildDiscount) return <div>
      <h3>{bookingFormState.show_num_of_rooms ? capitalizeFirstLetter(anyTour.grouping_unit) + " " + room_num : "Bookings"}</h3>
      <div>
        <Select style={{ width: 180 }} value={adult} onChange={(x) => set_adult(x)}>
          {bookingFormState.adultOptions.map(x => <Option key={x.num} value={x.num}>{x.num} pax ({formatCurrency(x.price)})</Option>)}
        </Select>
      </div>
    </div>
  
    return <div>
        <h3>{bookingFormState.show_num_of_rooms ? capitalizeFirstLetter(anyTour.grouping_unit) + " " + room_num + " - " + formatCurrency(roomPrice): "Bookings"}</h3>
        <div>
          <Select style={{ width: 180 }} value={adult} onChange={(x) => set_adult(x)}>
            {bookingFormState.adultOptions.map(x => <Option key={x.num} value={x.num}>{x.num} adult</Option>)}
          </Select>
          <Select style={{ width: 180 }} value={child} onChange={(x) => set_child(x)}>
            {bookingFormState.childOptions.map(x => <Option key={x.num} value={x.num}>{x.num} child</Option>)}
          </Select>
        </div>
    </div>}

  return <div className="main-container">
    <h1>{anyTour.name}</h1>

    {anyTourOptions?.length > 1 && <div style={{marginBottom: '1em'}}>
      <h2>Other Options</h2>
      {anyTourOptions.map((tour, key) => 
        tour.id === anyTour.id
        ? <b key={key} style={{display: 'block'}}>{tour.name}</b>
        : <a key={key} style={{display: 'block'}} href={"/any-tour/group/" + encodeURIComponent(tour.id)}>{tour.name}</a>)}
    </div>}

    {bookingFormState.nextDeparture == null && <>
      <h2>There are no departures available</h2>
    </>}
    {bookingFormState.nextDeparture != null && <>
      {bookingFormState.show_num_of_rooms && <div>
        <h3>Number of rooms</h3>
        <Select style={{ width: 180 }} value={bookingFormState.num_of_rooms} onChange={(x) => bookingFormState.set_num_of_rooms(x)}>
          <Option key={1} value={1}>1 {anyTour.grouping_unit}</Option>
          <Option key={2} value={2}>2 {anyTour.grouping_unit}</Option>
          <Option key={3} value={3}>3 {anyTour.grouping_unit}</Option>
          <Option key={4} value={4}>4 {anyTour.grouping_unit}</Option>
          <Option key={5} value={5}>5 {anyTour.grouping_unit}</Option>
        </Select>
      </div>}

      <RoomInput adult={bookingFormState.adult} set_adult={bookingFormState.set_adult} child={bookingFormState.child} set_child={bookingFormState.set_child} room_num={1} />
      {bookingFormState.num_of_rooms >= 2 && <RoomInput adult={bookingFormState.adult2} set_adult={bookingFormState.set_adult2} child={bookingFormState.child2} set_child={bookingFormState.set_child2} room_num={2} />}
      {bookingFormState.num_of_rooms >= 3 && <RoomInput adult={bookingFormState.adult3} set_adult={bookingFormState.set_adult3} child={bookingFormState.child3} set_child={bookingFormState.set_child3} room_num={3} />}
      {bookingFormState.num_of_rooms >= 4 && <RoomInput adult={bookingFormState.adult4} set_adult={bookingFormState.set_adult4} child={bookingFormState.child4} set_child={bookingFormState.set_child4} room_num={4} />}
      {bookingFormState.num_of_rooms >= 5 && <RoomInput adult={bookingFormState.adult5} set_adult={bookingFormState.set_adult5} child={bookingFormState.child5} set_child={bookingFormState.set_child5} room_num={5} />}

      {bookingFormState.childNoBedOptions.length > 0 && <div>
        <h3>Children (no bed)</h3>
        <Select style={{ width: 180 }} value={bookingFormState.child_no_bed} onChange={(x) => bookingFormState.set_child_no_bed(x)}>
          <Option key={0} value={0}>0</Option>
          {bookingFormState.childNoBedOptions.map(x => <Option key={x.num} value={x.num}>{x.num} (+{formatCurrency(x.price)})</Option>)}
        </Select>
      </div>}

      {anyTour.max_extension > 0 && <div>
        <h3>Tour duration</h3>
        <Select style={{ width: 180 }} value={bookingFormState.extension} onChange={(x) => bookingFormState.set_extension(x)}>
          <Option key={0} value={0}>{anyTour.number_of_days} days</Option>
          {range(anyTour.max_extension).map(x => <Option key={x + 1} value={x + 1}>{anyTour.number_of_days + x + 1} days(+{formatCurrency(Big(anyTour['price_ext_' + bookingFormState.totalPax]).mul(x+1).toFixed(2))})</Option>)}
        </Select>
      </div>}
      {anyTour.addons?.map((addon, key) => <div key={key}>
        <h3>{addon.name}</h3>
        <Select style={{ width: 180 }} value={bookingFormState.addon_values[addon.addon_code]} onChange={(x) => {bookingFormState.addon_values[addon.addon_code] = x; bookingFormState.set_addon_values(Object.assign({}, bookingFormState.addon_values))}}>
          <Option key={0} value={0}>0 pax</Option>
          {range(bookingFormState.totalPax).map(x => <Option key={x + 1} value={x + 1}>{x + 1} pax(+{formatCurrency(Big(addon.price).mul(x+1).toFixed(2))})</Option>)}
        </Select>
      </div>)

      }
      {/*
      <div>
        {bookingFormState.departure_date != null && <h3>Price {formatCurrency(bookingFormState.total)}</h3>}
        {bookingFormState.departure_date == null && <h3>Please select a departure date</h3>}
        <Button 
          disabled={bookingFormState.product == null}
          onClick={()=> {
            siteContext.cart.reset().addAnyTour(anyTour, generateBookingForm(bookingFormState.departure_date), tourType)
            router.push("/checkout");
        }} >Book Now</Button>
      </div>
      */}
      <div style={{clear: "both", paddingTop: '2em'}}></div>
      <table>
        <tbody>
          <tr>
            <th>Departure date</th>
            <th>Availability</th>
            <th>Total price</th>
            <th></th>
            <th></th>
          </tr>
          {bookingFormState.departures.map((departure, key) =>
            <tr key={key}>
              <td>{departure.isoDate}</td>
              <td>{departure.booking} / {departure.capacity}</td>
              <td>{formatCurrency(departure.productBreakdown?.product_total)}</td>
              <td>
                <Button 
                  disabled={departure.product == null}
                  onClick={()=> {
                    const product = siteContext.cart.reset().addAnyTour(anyTour, bookingFormState.generateBookingForm(departure.isoDate), tourType)
                    router.push("/checkout");
                }}>Book Now</Button></td>
            </tr>)}
        </tbody>
      </table>

      <div style={{clear: "both", paddingTop: '2em'}}></div>
      <h2>Departure dates</h2>
      <CalendarHorizontal
      className="tour-calendar"
      validRange={[
        newDateLocal(bookingFormState.nextDeparture),
        newDateLocal(bookingFormState.lastDeparture)
      ]}
      currentViewMonth={bookingFormState.currentViewMonth}
      onNext={() =>
        bookingFormState.nextCurrentViewMonth()
      }
      onPrev={() =>
        bookingFormState.prevCurrentViewMonth()
      }
      CalendarDayGenerator={(date, isPadding, key) => {
        const isoDate = dateToIsoDate(date);

        // if a month does not end on a sat, additional days are drawn to pad out the week
        // if isPadding, we pretned there's no dayInfo and don't display any info
        const dateInfo = bookingFormState.dateInfoMap[isoDate]
        const capacity = dateInfo?.capacity ?? 0
        const product = dateInfo?.product
        const disabled = isPadding || product == null
        const noDeparture = capacity === 0 || isPadding

        return (
        <td
          role="gridcell"
          key={key}
          className={
          "ant-fullcalendar-cell" +
          (disabled ? " ant-fullcalendar-disabled-cell" : "") +
          (bookingFormState.departure_date === isoDate
            ? " ant-fullcalendar-selected-day"
            : "")
          }
          onClick={() =>{
            if (disabled) return;
            //bookingFormState.set_departure_date(isoDate)
            siteContext.cart.reset().addAnyTour(anyTour, bookingFormState.generateBookingForm(isoDate), tourType)
            router.push("/checkout");
          }}
        >
          <div className="ant-fullcalendar-date">
          <div className="ant-fullcalendar-value">
            {date.getDate()}
          </div>
          {noDeparture ? (
            <div className="ant-fullcalendar-content"></div>
          ) : (
            <div className="ant-fullcalendar-content" style={{textAlign: 'center'}}>
            
            {noDeparture === false && <>
              <div>{formatCurrency(dateInfo.productBreakdown?.product_total)}</div>
              <div>{dateInfo.booking} / {dateInfo.capacity}</div>
              {parseFloat(dateInfo.totalAdjustment) > 0 && <div>+{formatCurrency(dateInfo.totalAdjustment)}</div>
              }
            </>}
            
            </div>
          )}
          </div>
        </td>
        );
      }}
      />
    </>}
  </div>
}

export async function getStaticPaths() {
  return { paths: [], fallback: true };
}

export async function getStaticProps(context) {
  const client = new TravelCloudClient(config)
  var anyTour = null
  var anyTourOptions = null
  const id = context.params.id?.toString()
  if (id != null && context.params.tourType === 'private') {
    anyTour = await client.privateTour({id})
    if (id.indexOf('/') !== -1) {
      anyTourOptions = await client.privateTours({'id/starts_with': id.split('/')[0] + "/"})
    }
  }
  if (id != null && context.params.tourType === 'group') {
    anyTour = await client.groupTour({id})
    if (id.indexOf('/') !== -1) {
      anyTourOptions = await client.groupTours({'id/starts_with': id.split('/')[0] + "/"})
    }
  }

  const limitCodes = anyTour?.result?.price_rules?.map(pr => pr.limit_code).filter(lc => lc != null && lc !== "")
  var limits = null

  if (limitCodes != null && limitCodes.length > 0) {
    limits = await client.limits(limitCodes)
  }

  return {props: { anyTour, anyTourOptions, limits, params: context.params }, revalidate: 120};
};

export default AnyTourPage