import config from "../customize/config";
import { TravelCloudClient, redirectToPayment, formatCurrency, urlParam } from "../travelcloud";
import { Layout, Row, Col, Card, Button, Icon, Avatar, Select } from "antd";
import { Order } from "../components/order";
import React from "react";
import Big from "big.js";
import {Order as OrderType} from '../types'
import Link from "next/link";

function decodeOdId(id: string) {
  var empty = "";
  if (id == null) return {
    origin_datetime : empty,
    origin : empty,
    destination_datetime : empty,
    destination : empty,
    connected : false,
    marketing_carrier : empty,
    operating_carrier : empty,
    flight_number : empty
  }
  if (id.length> 55) return {
    origin_datetime : id.substr(0, 22),
    origin : id.substr(22, 3),
    destination_datetime : id.substr(25, 22),
    destination : id.substr(47, 3),
    connected : id.substr(50, 1) === '1' ? true : false,
    marketing_carrier : id.substr(51, 2),
    operating_carrier : id.substr(53, 2),
    flight_number : id.substr(55)
  }

  return {
    origin_datetime : id.substr(0, 16),
    origin : id.substr(16, 3),
    destination_datetime : id.substr(19, 16),
    destination : id.substr(35, 3),
    //cabin : id.substr(50, 1),
    connected : id.substr(38, 1) === '1' ? true : false,
    marketing_carrier : id.substr(39, 2),
    operating_carrier : id.substr(41, 2),
    flight_number : id.substr(43)
  }
}

function decodeAllOdId(id: string) {
  if (id == null) return null
  return id.split('/').map(decodeOdId)
}

export const OrderManagement = ({order} : {order: OrderType}) => {
  const controls = order.products
    .map((product) => {
      if (product.product_source === 'sqndc' && product.product_status === 'Confirmed') {
        
        return <div>
          <h2>{product.product_name}</h2>
          {product.product_type === 'flight' && (() => {
            const od1Decoded = decodeAllOdId((product.form as any).od1_id)
            const od2Decoded = (product.form as any).od2_id != null ? decodeAllOdId((product.form as any).od2_id) : null
            const od3Decoded = (product.form as any).od3_id != null ? decodeAllOdId((product.form as any).od3_id) : null
            const od4Decoded = (product.form as any).od4_id != null ? decodeAllOdId((product.form as any).od4_id) : null
            const params = {
              "od1.origin_airport.code": od1Decoded[0].origin,
              "od1.origin_datetime": od1Decoded[0].origin_datetime.split('T')[0],
              "product_source_ref": product.product_source_ref,
              "source": product.product_source
            }
    
            if (od2Decoded != null) {
              params["od1.destination_airport.code"] = od1Decoded[od1Decoded.length - 1].destination
              params["od2.origin_airport.code"] = od2Decoded[0].origin
              params["od2.origin_datetime"] = od2Decoded[0].origin_datetime.split('T')[0]
              params["od2.destination_airport.code"] = od2Decoded[od2Decoded.length - 1].destination
    
              if (od3Decoded != null) {
                params["od3.origin_airport.code"] = od3Decoded[0].origin
                params["od3.origin_datetime"] = od3Decoded[0].origin_datetime.split('T')[0]
                params["od3.destination_airport.code"] = od3Decoded[od3Decoded.length - 1].destination
              }
    
              if (od4Decoded != null) {
                params["od4.origin_airport.code"] = od4Decoded[0].origin
                params["od4.origin_datetime"] = od4Decoded[0].origin_datetime.split('T')[0]
                params["od4.destination_airport.code"] = od4Decoded[od4Decoded.length - 1].destination
              }
            } else {
              params["od2.origin_airport.code"] = od1Decoded[od1Decoded.length - 1].destination
            }
            return <div>
              <Link href={"flights-change?" + urlParam(params)}>Modify flight itinerary</Link>
            </div>

          })()}
          <div>
            <Link href={"flight-change-seats?" + urlParam({"source": product.product_source, "product_source_ref": product.product_source_ref})}>Modify flight services</Link>
          </div>
        </div>
      }
      return null
    })
    .filter(x => x != null)

  if (controls.length === 0) return null

  return <div>
    <h1>Order Management</h1>
    {controls}
  </div>
}

export default class extends React.PureComponent<any, any> {
  client = new TravelCloudClient(config);
  state: any = {
    loading: true,
    noRef: false,
    order: null,
    customerCreditAmount: "0"
  };

  async componentDidMount() {
    const ref = new URL(location.href).searchParams.get("ref");
    if (ref == null) {
      this.setState({
        loading: false,
        noRef: true
      });
    } else {
      const order = await this.client.order({ ref });
      this.setState({
        loading: false,
        order,
        customerCreditAmount: order?.result == null ? '0' : Big(order?.result?.payment_required).minus(order?.result?.payment_received).toFixed(2)
      });
    }
  }

  async payWithAccountCredit() {
    const ref = new URL(location.href).searchParams.get("ref");
    if (ref == null) {
      this.setState({
        loading: false,
        noRef: true
      });
      return;
    }
    this.setState({
      loading: true
    });
    await this.props.cart.payOrderWithCustomerCredit(ref, this.state.customerCreditAmount);
    const order = await this.client.order({ ref });
    this.setState({
      loading: false,
      order,
      customerCreditAmount: order?.result == null ? '0' : Big(order?.result?.payment_required).minus(order?.result?.payment_received).toFixed(2)
    });
  }

  render() {
    const cart = this.props.cart;
    const order = this.props.order.result;

    if (this.state.loading) {
      return (
        <div
          style={{
            minHeight: 1000,
            backgroundColor: "#fff",
            margin: "64px auto",
            padding: 64,
            maxWidth: 1600
          }}
        >
          <Card loading={true} style={{ border: 0 }}></Card>
        </div>
      );
    }

    if (
      this.state.noRef ||
      (this.state.order != null && this.state.order.result == null)
    ) {
      return (
        <div
          style={{
            minHeight: 1000,
            backgroundColor: "#fff",
            margin: "64px auto",
            padding: 64,
            maxWidth: 1600
          }}
        >
          <h1>Unable to load order</h1>
        </div>
      );
    }

    const customer = this.props.customer && this.props.customer.result;
    const bigAccountBalance =
      customer?.payments == null
        ? Big(0)
        : customer.payments
            .reduce((acc, payment) => acc.plus(payment.amount), Big(0))
            .times(-1);

    return (
      <Layout
        style={{
          backgroundColor: "#fff",
          margin: "64px auto",
          padding: 64,
          maxWidth: 1600
        }}
      >
        <Row>
          {this.state.order.result.order_status === "Canceled" && (
            <Col span={10}>
              <h1>Your order has expired</h1>
            </Col>
          )}
          {this.state.order.result.order_status === "Invoice" && (
            <Col span={10}>
              <h1>Your payment has been received</h1>
              <p>
                Thank you for your order! Our team is processing your order.
              </p>
              <p>Please check your email for a copy of the invoice.</p>

              <OrderManagement order={this.state.order.result} />
            </Col>
          )}
          {this.state.order.result.order_status === "Quotation" && (
            <Col span={10}>
              <h1>Select payment method</h1>

              {this.state.order.result._payment_groups != null && this.state.order.result._payment_groups.map((paymentGroup, index1) =>{

                return <div key={index1}>
                  {paymentGroup.name !== 'default' && <h2>Payment to {paymentGroup.name}</h2>}
                  {paymentGroup.name === 'default' && this.state.order.result._payment_groups.length > 1 && <h2>Payment to {paymentGroup.name}</h2>}
                  {paymentGroup.options.map((option, index2) => {
                    const fee = Big(option.payment_required).minus(paymentGroup.payment_required).plus(paymentGroup.payment_received)
                    return <div
                      key={index1 + '/' + index2}
                      style={{
                        height: 114,
                        padding: 32,
                        border: "1px #ddd solid",
                        borderRadius: 5,
                        position: "relative",
                        marginTop: 32
                      }}
                    >
                      <div style={{fontSize: '0.9em', color: '#999'}}>Pay with {option.source_name}</div>
                      <div style={{fontSize: '1.2em'}}>Amount: {formatCurrency(option.payment_required)}</div>
                      {Big(fee).gt("0") && <div style={{fontStyle: 'italic'}}>(Inclusive of {formatCurrency(fee)} processing fee)</div>}
                      {Big(fee).lt("0") && <div style={{fontStyle: 'italic'}}>(Inclusive of {formatCurrency(fee)} discoun)t</div>}
                      <Button
                        style={{
                          position: "absolute",
                          top: 36,
                          right: 24,
                          width: 160
                        }}
                        type="primary"
                        onClick={() =>
                          redirectToPayment({
                            orderRef: this.state.order.result.ref,
                            tcUser: config.tcUser,
                            paymentProcessor: option.source,
                            successPage: "payment?ref=" + this.state.order.result.ref,
                            failurePage: "payment?ref=" + this.state.order.result.ref,
                            amount: option.payment_required
                          })
                        }
                      >
                        Continue <Icon type="right" />
                      </Button>
                    </div>
                  })
                }

                {paymentGroup.name === 'default' && customer != null && (
                  <div
                    style={{
                      padding: 32,
                      border: "1px #ddd solid",
                      borderRadius: 5,
                      position: "relative",
                      marginTop: 32
                    }}
                  >
                    <Card.Meta
                      avatar={
                        <Avatar
                          size="large"
                          icon="user"
                          style={{ backgroundColor: "#af0201", marginLeft: 16 }}
                        />
                      }
                      title={customer.name ? customer.name : customer.email}
                      description={
                        <div>
                          <div>Account balance ${bigAccountBalance.toFixed(2)}</div>
                          <div>Payment amount <input value={this.state.customerCreditAmount} onChange={(e) => this.setState({customerCreditAmount: e.target.value})} /></div>
                        </div>
                      }
                    />
                    <div style={{
                      textAlign: 'center',
                      marginTop: 16
                    }}>
                      <Button
                        disabled={bigAccountBalance.lt(
                          this.state.order.result.payment_required
                        )}
                        type="primary"
                        onClick={() => this.payWithAccountCredit()}
                      >
                        Pay with account credit <Icon type="right" />
                      </Button>
                    </div>
                  </div>
                )}
                
                </div>
              })}

              
              
            </Col>
          )}
          <Col span={12} offset={2}>
            <Order order={this.state.order.result} />
          </Col>
        </Row>
      </Layout>
    );
  }
}
