import React from 'react'
import config from '../customize/config'
import { TermsOfService } from '../components/terms-of-service'

export default () => (
  <div style={{padding: '32px'}}>
    <TermsOfService companyName={config.companyName} dataProtectionNoticePath="/data-protection-notice" />
  </div>
)