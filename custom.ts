export type NDC_OfferPriceRes1 = {
'Commission': {
'Amount': {
'CurCode': string,
'_text': string
},
'Percentage': {
'_text': string
},
'CommissionCode': {
'_text': string
},
'RemarkText': Array<{
'_text': string
}>
},
'DataLists': {
'BaggageAllowanceList': {
'BaggageAllowance': Array<{
'BaggageAllowanceID': {
'_text': string
},
'TypeCode': {
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'ApplicablePartyText': {
'_text': string
},
'DescText': Array<{
'_text': string
}>,
'WeightAllowance': Array<{
'MaximumWeightMeasure': {
'UnitCode': string,
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'ApplicablePartyText': {
'_text': string
},
'DescText': Array<{
'_text': string
}>
}>,
'BDC': {
'CarrierDesigCode': {
'_text': string
},
'CarrierName': {
'_text': string
},
'BagRuleCode': {
'_text': string
},
'BDCAnalysisResultCode': {
'_text': string
},
'BDCReasonText': {
'_text': string
}
},
'DimensionAllowance': Array<{
'BaggageDimensionCategory': {
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'MinMeasure': {
'UnitCode': string,
'_text': string
},
'ApplicablePartyText': {
'_text': string
},
'MaxMeasure': {
'UnitCode': string,
'_text': string
},
'DescText': Array<{
'_text': string
}>,
'Qty': {
'_text': string
}
}>,
'PieceAllowance': Array<{
'ApplicablePartyText': {
'_text': string
},
'TotalQty': {
'_text': string
},
'TypeText': {
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'Desc': Array<{
'_text': string
}>,
'PieceDimensionAllowance': Array<{
'BaggageDimensionCategory': {
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'MinMeasure': {
'UnitCode': string,
'_text': string
},
'ApplicablePartyText': {
'_text': string
},
'MaxMeasure': {
'UnitCode': string,
'_text': string
},
'DescText': Array<{
'_text': string
}>,
'Qty': {
'_text': string
}
}>,
'PieceWeightAllowance': Array<{
'MaximumWeightMeasure': {
'UnitCode': string,
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'ApplicablePartyText': {
'_text': string
},
'DescText': Array<{
'_text': string
}>
}>
}>
}>
},
'BaggageDisclosureList': {
'BaggageDisclosure': Array<{
'BaggageDisclosureID': {
'_text': string
},
'RuleTypeCode': {
'_text': string
},
'CommercialAgreementID': {
'_text': string
},
'DeferralInd': {
'_text': string
},
'FixedPrePaidInd': {
'_text': string
},
'CheckInChargesInd': {
'_text': string
},
'BDC': {
'CarrierDesigCode': {
'_text': string
},
'CarrierName': {
'_text': string
},
'BagRuleCode': {
'_text': string
},
'BDCAnalysisResultCode': {
'_text': string
},
'BDCReasonText': {
'_text': string
}
},
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>
}>
},
'ContactInfoList': {
'ContactInfo': Array<{
'ContactInfoID': {
'_text': string
},
'ContactTypeText': {
'_text': string
},
'IndividualRef': {
'_text': string
},
'ContactRefusedInd': {
'_text': string
},
'Phone': Array<{
'LabelText': {
'_text': string
},
'CountryDialingCode': {
'_text': string
},
'AreaCodeNumber': {
'_text': string
},
'PhoneNumber': {
'_text': string
},
'ExtensionNumber': {
'_text': string
}
}>,
'OtherAddress': Array<{
'LabelText': {
'_text': string
},
'OtherAddressText': {
'_text': string
}
}>,
'EmailAddress': Array<{
'LabelText': {
'_text': string
},
'EmailAddressText': {
'_text': string
}
}>,
'Individual': {
'IndividualID': {
'_text': string
},
'GenderCode': {
'_text': string
},
'TitleName': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
},
'SuffixName': {
'_text': string
},
'Birthdate': {
'_text': string
},
'BirthplaceText': {
'_text': string
}
},
'PostalAddress': Array<{
'LabelText': {
'_text': string
},
'StreetText': Array<{
'_text': string
}>,
'BuildingRoomText': {
'_text': string
},
'PO_BoxCode': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CityName': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
},
'CountryName': {
'_text': string
},
'CountryCode': {
'_text': string
}
}>
}>
},
'ContentSource': {
'ContentSource': Array<{
'refs': string,
'ListKey': string,
'NodePath': {
'_text': string
},
'AirlineID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'OtherID': {
'name': string,
'_text': string
}
}>
},
'DisclosureList': {
'DisclosureID': {
'_text': string
},
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>
},
'FareList': {
'FareGroup': Array<{
'refs': string,
'ListKey': string,
'Fare': {
'FareCode': {
'_text': string
},
'FareDetail': {
'FareIndicatorCode': {
'FiledFareInd': string,
'_text': string
},
'PassengerRefs': Array<{
'_text': string
}>,
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareComponent': Array<{
'Parameters': {
'Quantity': string
},
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareBasis': {
'FareBasisCode': {
'refs': string,
'Code': {
'_text': string
},
'Application': {
'_text': string
}
},
'FareRulesRemarks': {
'FareRulesRemark': Array<{
'refs': string,
'Category': {
'_text': string
},
'Text': {
'_text': string
}
}>
},
'FareBasisCityPair': {
'_text': string
},
'RBD': {
'_text': string
},
'CabinType': {
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
}
},
'TicketDesig': {
'Application': string,
'_text': string
},
'FareRules': {
'refs': string,
'Penalty': {
'CancelFeeInd': string,
'ChangeFeeInd': string,
'RefundableInd': string,
'ReuseInd': string,
'UpgradeFeeInd': string,
'refs': string,
'ObjectKey': string,
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'Details': {
'Detail': Array<{
'refs': string,
'Type': {
'_text': string
},
'Application': {
'_text': string
},
'Amounts': {
'Amount': Array<{
'CurrencyAmountValue': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
},
'AmountApplication': {
'_text': string
},
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
}
}>
}
},
'CorporateFare': {
'refs': string,
'ObjectKey': string,
'PricedInd': string,
'CompanyIndex': {
'refs': string,
'_text': string
},
'Contract': {
'refs': string,
'_text': string
},
'Account': {
'refs': string,
'_text': string
}
},
'AdvanceStay': {
'AdvancePurchase': {
'Context': string,
'_text': string
},
'AdvanceTicketing': {
'AdvanceReservation': {
'Context': string,
'_text': string
},
'AdvanceDeparture': {
'Context': string,
'_text': string
}
},
'MinimumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
},
'MaximumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
}
},
'Ticketing': {
'TicketlessInd': {
'_text': string
},
'InstantPurchase': {
'_text': string
},
'Endorsements': {
'Endorsement': Array<{
'_text': string
}>
}
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'PriceClassRef': {
'_text': string
},
'SegmentRefs': Array<{
'ON_Point': string,
'OFF_Point': string,
'_text': string
}>
}>,
'FlightMileage': {
'Value': {
'_text': string
},
'Application': {
'_text': string
}
},
'TourCode': {
'_text': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}
},
'FareBasisCode': {
'Code': {
'_text': string
},
'Application': {
'_text': string
}
}
}>
},
'InstructionsList': {
'Instruction': Array<{
'refs': string,
'ListKey': string,
'ClassOfServiceUpgrade': {
'Classes': {
'ClassOfService': Array<{
'refs': string,
'Code': {
'SeatsLeft': string,
'_text': string
},
'MarketingName': {
'CabinDesignator': string,
'_text': string
},
'FareBasisCode': {
'_text': string
}
}>
}
},
'FreeFormTextInstruction': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'SpecialBookingInstruction': {
'_text': string
}
}>
},
'MediaList': {
'Media': Array<{
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}>
},
'OriginDestList': {
'OriginDest': Array<{
'OriginDestID': {
'_text': string
},
'OriginCode': {
'_text': string
},
'DestCode': {
'_text': string
},
'PaxJourneyRefID': Array<{
'_text': string
}>
}>
},
'PaxJourneyList': {
'PaxJourney': Array<{
'PaxJourneyID': {
'_text': string
},
'DistanceMeasure': {
'UnitCode': string,
'_text': string
},
'Duration': {
'_text': string
},
'PaxSegmentRefID': Array<{
'_text': string
}>,
'SettlementInfo': {
'MethodCode': {
'_text': string
},
'SettlementAmount': {
'CurCode': string,
'_text': string
}
}
}>
},
'PaxList': {
'Pax': Array<{
'PaxID': {
'_text': string
},
'PTC': {
'_text': string
},
'AgeMeasure': {
'UnitCode': string,
'_text': string
},
'Birthdate': {
'_text': string
},
'ResidenceCountryCode': {
'_text': string
},
'CitizenshipCountryCode': {
'_text': string
},
'ProfileID_Text': {
'_text': string
},
'ProfileConsentInd': {
'_text': string
},
'ContactInfoRefID': {
'_text': string
},
'PaxRefID': {
'_text': string
},
'IdentityDoc': Array<{
'IdentityDocNumber': {
'_text': string
},
'IdentityDocTypeCode': {
'_text': string
},
'IssuingCountryCode': {
'_text': string
},
'TitleName': {
'_text': string
},
'CitizenshipCountryCode': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'ResidenceCountryCode': {
'_text': string
},
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
},
'SuffixName': {
'_text': string
},
'Birthdate': {
'_text': string
},
'BirthplaceText': {
'_text': string
},
'GenderCode': {
'_text': string
},
'IssueDate': {
'_text': string
},
'ExpiryDate': {
'_text': string
},
'Individual': Array<{
'IndividualID': {
'_text': string
},
'GenderCode': {
'_text': string
},
'TitleName': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
},
'SuffixName': {
'_text': string
},
'Birthdate': {
'_text': string
},
'BirthplaceText': {
'_text': string
}
}>,
'Visa': Array<{
'VisaNumber': {
'_text': string
},
'VisaTypeCode': {
'_text': string
},
'DurationOfStay': {
'_text': string
},
'EnterBeforeDate': {
'_text': string
},
'EntryQty': {
'_text': string
},
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': Array<{
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
}>
}>
}>,
'Individual': {
'IndividualID': {
'_text': string
},
'GenderCode': {
'_text': string
},
'TitleName': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
},
'SuffixName': {
'_text': string
},
'Birthdate': {
'_text': string
},
'BirthplaceText': {
'_text': string
}
},
'LoyaltyProgramAccount': Array<{
'ProgramName': {
'_text': string
},
'ProgramCode': {
'_text': string
},
'AccountNumber': {
'_text': string
},
'URL': {
'_text': string
},
'SignInID': {
'_text': string
},
'TierCode': {
'_text': string
},
'TierName': {
'_text': string
},
'TierPriorityText': {
'_text': string
},
'ProviderName': {
'_text': string
},
'Alliance': {
'AllianceCode': {
'_text': string
},
'Name': {
'_text': string
},
'URL': {
'_text': string
}
},
'Carrier': {
'AirlineDesigCode': {
'_text': string
},
'DuplicateDesigInd': {
'_text': string
},
'Name': {
'_text': string
}
}
}>,
'Remark': Array<{
'RemarkText': {
'_text': string
},
'DisplayInd': {
'_text': string
},
'CreationDateTime': {
'TimeZoneCode': string,
'_text': string
}
}>
}>
},
'PaxSegmentList': {
'PaxSegment': Array<{
'PaxSegmentID': {
'_text': string
},
'ARNK_Ind': {
'_text': string
},
'TicketlessInd': {
'_text': string
},
'SecureFlightInd': {
'_text': string
},
'Duration': {
'_text': string
},
'SegmentTypeCode': {
'_text': string
},
'CabinType': {
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
},
'Dep': {
'IATA_LocationCode': {
'_text': string
},
'AircraftScheduledDateTime': {
'TimeZoneCode': string,
'_text': string
},
'TerminalName': {
'_text': string
},
'BoardingGateID': {
'_text': string
},
'StationName': {
'_text': string
}
},
'Arrival': {
'IATA_LocationCode': {
'_text': string
},
'AircraftScheduledDateTime': {
'TimeZoneCode': string,
'_text': string
},
'TerminalName': {
'_text': string
},
'BoardingGateID': {
'_text': string
},
'StationName': {
'_text': string
}
},
'MarketingCarrierInfo': {
'CarrierDesigCode': {
'_text': string
},
'CarrierName': {
'_text': string
},
'MarketingCarrierFlightNumberText': {
'_text': string
},
'OperationalSuffixText': {
'_text': string
},
'RBD_Code': {
'_text': string
}
},
'OperatingCarrierInfo': {
'CarrierDesigCode': {
'_text': string
},
'CarrierName': {
'_text': string
},
'OperatingCarrierFlightNumberText': {
'_text': string
},
'RBD_Code': {
'_text': string
},
'OperationalSuffixText': {
'_text': string
},
'Disclosure': {
'DisclosureID': {
'_text': string
},
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>
}
},
'DatedOperatingLeg': Array<{
'DatedOperatingLegID': {
'_text': string
},
'CarrierAircraftType': {
'CarrierAircraftTypeCode': {
'_text': string
},
'CarrierAircraftTypeName': {
'_text': string
}
},
'ChangeofGaugeInd': {
'_text': string
},
'DistanceMeasure': {
'UnitCode': string,
'_text': string
},
'Dep': {
'IATA_LocationCode': {
'_text': string
},
'AircraftScheduledDateTime': {
'TimeZoneCode': string,
'_text': string
},
'TerminalName': {
'_text': string
},
'BoardingGateID': {
'_text': string
},
'StationName': {
'_text': string
}
},
'Arrival': {
'IATA_LocationCode': {
'_text': string
},
'AircraftScheduledDateTime': {
'TimeZoneCode': string,
'_text': string
},
'TerminalName': {
'_text': string
},
'BoardingGateID': {
'_text': string
},
'StationName': {
'_text': string
}
},
'IATA_AircraftType': {
'IATA_AircraftTypeCode': {
'_text': string
}
},
'OnGroundTime': {
'_text': string
}
}>,
'SettlementInfo': {
'MethodCode': {
'_text': string
},
'SettlementAmount': {
'CurCode': string,
'_text': string
}
}
}>
},
'PenaltyList': {
'Penalty': Array<{
'PenaltyID': {
'_text': string
},
'UpgradeFeeInd': {
'_text': string
},
'TypeCode': {
'_text': string
},
'DescText': {
'_text': string
},
'AppCode': {
'_text': string
},
'CancelFeeInd': {
'_text': string
},
'ChangeFeeInd': {
'_text': string
},
'PenaltyAmount': {
'CurCode': string,
'_text': string
},
'PenaltyPercent': {
'_text': string
}
}>
},
'PriceClassList': {
'PriceClass': Array<{
'PriceClassID': {
'_text': string
},
'Code': {
'_text': string
},
'Name': {
'_text': string
},
'FareBasisCode': {
'_text': string
},
'FareBasisAppText': {
'_text': string
},
'DisplayOrderText': {
'_text': string
},
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>,
'CabinType': Array<{
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
}>
}>
},
'SeatProfileList': {
'SeatProfile': Array<{
'SeatProfileID': {
'_text': string
},
'SeatWidthMeasure': {
'UnitCode': string,
'_text': string
},
'CharacteristicCode': {
'_text': string
},
'SeatPitchMeasure': {
'UnitCode': string,
'_text': string
},
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>,
'SeatKeywords': Array<{
'KeywordText': {
'_text': string
},
'KeywordValueText': Array<{
'_text': string
}>
}>
}>
},
'ServiceDefinitionList': {
'ServiceDefinition': Array<{
'ServiceDefinitionID': {
'_text': string
},
'OwnerCode': {
'_text': string
},
'Name': {
'_text': string
},
'ServiceCode': {
'_text': string
},
'ReasonForIssuanceCode': {
'_text': string
},
'ReasonForIssuanceSubCode': {
'_text': string
},
'DepositTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'NamingTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PaymentTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PriceGuaranteeTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'TicketingTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'BilateralProcessTimeLimit': Array<{
'Name': {
'_text': string
},
'TimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'DescText': {
'_text': string
}
}>,
'ValidatingCarrierCode': {
'_text': string
},
'Description': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>,
'ServiceDefinitionAssociation': {
'BaggageAllowanceRefID': {
'_text': string
},
'SeatProfileRefID': {
'_text': string
},
'ServiceBundle': {
'MaxQty': {
'_text': string
},
'ServiceDefinitionRefID': Array<{
'_text': string
}>
}
},
'BookingInstructions': {
'SSRCode': Array<{
'_text': string
}>,
'OSIText': Array<{
'_text': string
}>,
'Method': {
'_text': string
},
'UpgradeMethod': {
'NewClass': string,
'_text': string
},
'Text': Array<{
'_text': string
}>,
'Equipment': {
'_text': string
}
},
'Detail': {
'ServiceCombinations': {
'Combination': Array<{
'Rule': {
'_text': string
},
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>,
'ServiceReference': Array<{
'_text': string
}>
}>
},
'ServiceCoupon': {
'InstantPurchase': {
'_text': string
},
'FeeBasis': {
'_text': string
},
'CouponType': {
'_text': string
}
},
'ServiceFulfillment': {
'refs': string,
'OfferValidDates': {
'Start': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'End': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
}
},
'Provider': {
'metadata': string,
'refs': string,
'PartnerID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Name': {
'_text': string
},
'Type': {
'_text': string
},
'Fulfillments': {
'Fulfillment': Array<{
'refs': string,
'OfferValidDates': {
'Start': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'End': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
}
},
'Location': {
'AirportCode': {
'refs': string,
'_text': string
},
'StructuredAddress': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
}
}
}>
}
},
'Location': {
'AirportFulfillmentLocation': {
'refs': string,
'AirportCode': {
'refs': string,
'_text': string
}
},
'OtherFulfillmentLocation': {
'Address': {
'AddressCore': {
'refs': string,
'Address': {
'PaymentAddress': {
'refs': string,
'Street': Array<{
'_text': string
}>,
'PO_Box': {
'_text': string
},
'BuildingRoom': {
'_text': string
},
'City': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'Country': {
'_text': string
}
},
'SimpleAddress': {
'refs': string,
'AddressLine': Array<{
'_text': string
}>
},
'StructuredAddress': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
}
}
},
'AddressDetail': {
'refs': string,
'Address': {
'PaymentAddress': {
'refs': string,
'Street': Array<{
'_text': string
}>,
'PO_Box': {
'_text': string
},
'BuildingRoom': {
'_text': string
},
'City': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'Country': {
'_text': string
}
},
'SimpleAddress': {
'refs': string,
'AddressLine': Array<{
'_text': string
}>
},
'StructuredAddress': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
}
},
'Directions': {
'Direction': Array<{
'refs': string,
'Text': {
'_text': string
},
'Link': {
'_text': string
},
'Name': {
'_text': string
},
'From': {
'_text': string
},
'To': {
'_text': string
}
}>
}
}
}
}
}
},
'ServiceItemQuantityRules': {
'MinimumQuantity': {
'_text': string
},
'MaximumQuantity': {
'_text': string
}
}
}
}>
},
'TermsList': {
'refs': string,
'ListKey': string,
'Term': Array<{
'refs': string,
'AvailablePeriod': {
'Earliest': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'Latest': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
}
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
},
'GroupSize': {
'Minimum': {
'_text': string
},
'Maximum': {
'_text': string
}
},
'OrderingQuantity': {
'Minimum': {
'_text': string
},
'Maximum': {
'_text': string
}
}
}>
}
},
'Metadata': {
'Shopping': {
'ShopMetadataGroup': {
'Baggage': {
'CheckedBagMetadatas': {
'CheckedBagMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'CarryOnBagMetadatas': {
'CarryOnBagMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'BaggageDisclosureMetadatas': {
'BaggageDisclosureMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'BaggageDetailMetadata': {
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Attributes': {
'refs': string,
'Group': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
},
'SubGroup': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
},
'Desc1': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
},
'Desc2': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
}
},
'FareTariff': {
'FareType': string,
'_text': string
},
'FareRule': {
'_text': string
},
'FareCat': {
'_text': string
}
},
'BaggageQueryMetadata': {
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'TicketIssuePlace': {
'_text': string
},
'TicketIssueCountry': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'TotalPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
}
},
'Fare': {
'refs': string,
'FareCode': {
'_text': string
},
'FareDetail': {
'FareIndicatorCode': {
'FiledFareInd': string,
'_text': string
},
'PassengerRefs': Array<{
'_text': string
}>,
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareComponent': Array<{
'Parameters': {
'Quantity': string
},
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareBasis': {
'FareBasisCode': {
'refs': string,
'Code': {
'_text': string
},
'Application': {
'_text': string
}
},
'FareRulesRemarks': {
'FareRulesRemark': Array<{
'refs': string,
'Category': {
'_text': string
},
'Text': {
'_text': string
}
}>
},
'FareBasisCityPair': {
'_text': string
},
'RBD': {
'_text': string
},
'CabinType': {
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
}
},
'TicketDesig': {
'Application': string,
'_text': string
},
'FareRules': {
'refs': string,
'Penalty': {
'CancelFeeInd': string,
'ChangeFeeInd': string,
'RefundableInd': string,
'ReuseInd': string,
'UpgradeFeeInd': string,
'refs': string,
'ObjectKey': string,
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'Details': {
'Detail': Array<{
'refs': string,
'Type': {
'_text': string
},
'Application': {
'_text': string
},
'Amounts': {
'Amount': Array<{
'CurrencyAmountValue': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
},
'AmountApplication': {
'_text': string
},
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
}
}>
}
},
'CorporateFare': {
'refs': string,
'ObjectKey': string,
'PricedInd': string,
'CompanyIndex': {
'refs': string,
'_text': string
},
'Contract': {
'refs': string,
'_text': string
},
'Account': {
'refs': string,
'_text': string
}
},
'AdvanceStay': {
'AdvancePurchase': {
'Context': string,
'_text': string
},
'AdvanceTicketing': {
'AdvanceReservation': {
'Context': string,
'_text': string
},
'AdvanceDeparture': {
'Context': string,
'_text': string
}
},
'MinimumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
},
'MaximumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
}
},
'Ticketing': {
'TicketlessInd': {
'_text': string
},
'InstantPurchase': {
'_text': string
},
'Endorsements': {
'Endorsement': Array<{
'_text': string
}>
}
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'PriceClassRef': {
'_text': string
},
'SegmentRefs': Array<{
'ON_Point': string,
'OFF_Point': string,
'_text': string
}>
}>,
'FlightMileage': {
'Value': {
'_text': string
},
'Application': {
'_text': string
}
},
'TourCode': {
'_text': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}
},
'Flight': {
'FlightMetadatas': {
'FlightMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'ActionCode': {
'_text': string
},
'BindingKey': {
'_text': string
},
'FlightStatus': {
'_text': string
},
'Frequency': {
'_text': string
},
'InstantPurchase': {
'_text': string
},
'Meals': {
'Meal': Array<{
'_text': string
}>
},
'OnTimePerformance': {
'Percent': string,
'Period': string,
'Type': string,
'LatePercent': string,
'CancelledPercent': string,
'SpecialHighlightInd': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
},
'ItineraryMetadata': {
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'ActionCode': {
'_text': string
}
}
},
'Location': {
'DirectionMetadatas': {
'DirectionMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
}
},
'Offer': {
'DisclosureMetadatas': {
'DisclosureMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Timestamp': {
'_text': string
}
}>
},
'OfferMetadatas': {
'OfferMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'ATPCO': {
'Attributes': {
'refs': string,
'Group': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
},
'SubGroup': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
},
'Desc1': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
},
'Desc2': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
}
},
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
},
'Incentives': {
'Incentive': Array<{
'refs': string,
'OfferCodeID': {
'_text': string
},
'ExpirationDate': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'OfferSubCode': {
'_text': string
},
'AvailableUnits': {
'_text': string
},
'DiscountLevel': {
'DiscountAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'DiscountPercent': {
'_text': string
}
},
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'Matches': {
'Match': Array<{
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'Terms': {
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
},
'TimeLimits': {
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
},
'Rule': {
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
},
'Status': {
'Status': {
'_text': string
},
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}
}>
},
'OfferInstructionMetadatas': {
'OfferInstructionMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'OfferPenaltyMetadatas': {
'OfferPenaltyMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'OfferTermsMetadatas': {
'OfferTermsMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
}
},
'Pricing': {
'DiscountMetadatas': {
'DiscountMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Timestamp': {
'_text': string
}
}>
}
},
'Qualifier': {
'BaggagePricingQualifier': {
'FixedPrePaidInd': string,
'CommercialAgreementID': string,
'DeferralInd': string,
'refs': string,
'IncludeSettlementInd': string,
'BaggageOption': Array<{
'_text': string
}>,
'RequestAction': {
'_text': string
},
'OptionalCharges': {
'_text': string
}
},
'ExistingOrderQualifier': {
'OrderKeys': {
'refs': string,
'OrderID': {
'refs': string,
'ObjectMetaReferences': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
},
'AssociatedIDs': {
'AssociatedID': Array<{
'OrderItemID': {
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
},
'OfferItemID': {
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
},
'ServiceID': {
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}
}>
}
},
'PassengerReferences': Array<{
'_text': string
}>,
'BookingReference': {
'refs': string,
'Type': {
'_text': string
},
'ID': {
'_text': string
},
'AirlineID': {
'refs': string,
'ObjectMetaReferences': string,
'Name': string,
'_text': string
},
'OtherID': {
'refs': string,
'ObjectMetaReferences': string,
'Name': string,
'_text': string
}
}
},
'PaymentCardQualifiers': Array<{
'refs': string,
'IsPercentInd': string,
'Type': {
'_text': string
},
'IIN_Number': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
}
}>,
'ProgramQualifiers': {
'refs': string,
'ProgramQualifier': Array<{
'DiscountProgramQualifier': {
'refs': string,
'Account': {
'refs': string,
'_text': string
},
'AssocCode': {
'_text': string
},
'Name': {
'_text': string
}
},
'IncentiveProgramQualifier': {
'refs': string,
'Name': {
'_text': string
},
'AccountID': {
'refs': string,
'_text': string
},
'AirlineID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'MemberStatus': {
'refs': string,
'_text': string
}
},
'PrePaidProgramQualifier': {
'refs': string,
'PrepaidProgramDetail': {
'AirlineID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Certificate': Array<{
'refs': string,
'Number': {
'refs': string,
'_text': string
},
'Application': {
'_text': string
},
'EffectivePeriod': {
'YearPeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'YearMonthPeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'TimePeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'QuarterPeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'MonthPeriod': {
'Effective': {
'Name': string,
'_text': string
},
'Expiration': {
'Name': string,
'_text': string
}
},
'DayPeriod': {
'Effective': {
'Name': string,
'_text': string
},
'Expiration': {
'Name': string,
'_text': string
}
},
'DateTimePeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'DatePeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
}
}
}>,
'ProgramName': {
'_text': string
},
'ProgramCode': {
'_text': string
},
'Holder': {
'AgencyID': {
'refs': string,
'Owner': string,
'_text': string
},
'PartnerID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
}
}
},
'ProgramStatusQualifier': {
'refs': string,
'ProgramStatus': Array<{
'_text': string
}>
}
}>
},
'PromotionQualifiers': {
'refs': string,
'CC_IssuingCountryInd': string,
'Code': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Link': {
'_text': string
},
'Issuer': {
'AirlineID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AgencyID': {
'refs': string,
'Owner': string,
'_text': string
},
'PartnerID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'VoucherOwner': {
'_text': string
}
},
'SeatQualifier': {
'Assignment': Array<{
'refs': string,
'Location': {
'Column': {
'_text': string
},
'Row': {
'refs': string,
'Number': {
'_text': string
},
'Type': {
'_text': string
},
'SeatData': {
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
},
'UOM': {
'_text': string
},
'SeatPitchLow': {
'_text': string
},
'SeatWidthLow': {
'_text': string
},
'Keywords': {
'KeyWord': Array<{
'refs': string,
'ObjectKey': string,
'Word': {
'_text': string
},
'Value': Array<{
'_text': string
}>
}>
},
'Marketing': {
'refs': string,
'Images': Array<{
'ImageID': {
'_text': string
},
'Position': {
'Row': {
'Position': {
'_text': string
},
'Orientation': {
'_text': string
}
},
'Column': {
'Position': {
'_text': string
},
'Orientation': {
'_text': string
}
}
}
}>,
'Links': Array<{
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Position': {
'Row': {
'Position': {
'_text': string
},
'Orientation': {
'_text': string
}
},
'Column': {
'Position': {
'_text': string
},
'Orientation': {
'_text': string
}
}
}
}>,
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}
}
},
'Characteristics': {
'Characteristic': Array<{
'refs': string,
'Code': {
'_text': string
},
'Definition': {
'_text': string
},
'TableName': {
'_text': string
},
'Link': {
'_text': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
},
'Associations': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}
},
'SeatAssociation': {
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>,
'PassengerReference': Array<{
'_text': string
}>
}
}>
},
'ServiceQualifier': {
'refs': string,
'Encoding': {
'refs': string,
'RFIC': {
'_text': string
},
'Type': {
'_text': string
},
'Code': {
'_text': string
},
'SubCode': {
'_text': string
}
},
'Fulfillment': {
'refs': string,
'OfferValidDates': {
'Start': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'End': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
}
},
'Provider': {
'metadata': string,
'refs': string,
'PartnerID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Name': {
'_text': string
},
'Type': {
'_text': string
},
'Fulfillments': {
'Fulfillment': Array<{
'refs': string,
'OfferValidDates': {
'Start': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'End': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
}
},
'Location': {
'AirportCode': {
'refs': string,
'_text': string
},
'StructuredAddress': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
}
}
}>
}
},
'Location': {
'AirportFulfillmentLocation': {
'refs': string,
'AirportCode': {
'refs': string,
'_text': string
}
},
'OtherFulfillmentLocation': {
'Address': {
'AddressCore': {
'refs': string,
'Address': {
'PaymentAddress': {
'refs': string,
'Street': Array<{
'_text': string
}>,
'PO_Box': {
'_text': string
},
'BuildingRoom': {
'_text': string
},
'City': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'Country': {
'_text': string
}
},
'SimpleAddress': {
'refs': string,
'AddressLine': Array<{
'_text': string
}>
},
'StructuredAddress': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
}
}
},
'AddressDetail': {
'refs': string,
'Address': {
'PaymentAddress': {
'refs': string,
'Street': Array<{
'_text': string
}>,
'PO_Box': {
'_text': string
},
'BuildingRoom': {
'_text': string
},
'City': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'Country': {
'_text': string
}
},
'SimpleAddress': {
'refs': string,
'AddressLine': Array<{
'_text': string
}>
},
'StructuredAddress': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
}
},
'Directions': {
'Direction': Array<{
'refs': string,
'Text': {
'_text': string
},
'Link': {
'_text': string
},
'Name': {
'_text': string
},
'From': {
'_text': string
},
'To': {
'_text': string
}
}>
}
}
}
}
}
},
'Associations': {
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>,
'PassengerReferences': Array<{
'_text': string
}>
},
'Include': {
'_text': string
}
},
'SocialMediaQualifiers': {
'refs': string,
'Service': {
'_text': string
},
'User': {
'refs': string,
'_text': string
},
'EmailAddress': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'SpecialFareQualifiers': {
'refs': string,
'AirlineID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'CompanyIndex': {
'refs': string,
'_text': string
},
'Contract': {
'refs': string,
'_text': string
},
'Account': {
'refs': string,
'_text': string
}
},
'SpecialNeedQualifiers': {
'refs': string,
'Code': {
'_text': string
},
'Description': {
'_text': string
},
'DescContext': {
'_text': string
}
},
'TripPurposeQualifier': {
'_text': string
}
},
'Seat': {
'SeatMetadatas': {
'SeatMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'SeatStatus': {
'_text': string
}
}>
},
'SeatMapMetadatas': {
'SeatMapMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
}
}
}
},
'Traveler': {
'PassengerMetadata': {
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'NameDetail': {
'GivenNamePrefix': Array<{
'_text': string
}>,
'TitleSuffix': Array<{
'_text': string
}>,
'SurnamePrefix': {
'_text': string
},
'SurnameSuffix': Array<{
'_text': string
}>
}
}
},
'Other': {
'OtherMetadata': Array<{
'AddressMetadatas': {
'AddressMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
},
'AddressType': {
'_text': string
},
'AddressFields': {
'FieldName': Array<{
'Mandatory': string,
'_text': string
}>
}
}>
},
'AircraftMetadatas': {
'AircraftMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'TailNumber': {
'_text': string
},
'Name': {
'_text': string
}
}>
},
'AirportMetadatas': {
'AirportMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
},
'Directions': {
'Direction': Array<{
'refs': string,
'Text': {
'_text': string
},
'Link': {
'_text': string
},
'Name': {
'_text': string
},
'From': {
'_text': string
},
'To': {
'_text': string
}
}>
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
}>
},
'CityMetadatas': {
'CityMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Directions': {
'Direction': Array<{
'refs': string,
'Text': {
'_text': string
},
'Link': {
'_text': string
},
'Name': {
'_text': string
},
'From': {
'_text': string
},
'To': {
'_text': string
}
}>
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
}>
},
'CodesetMetadatas': {
'CodesetMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Source': {
'OwnerID': {
'Name': string,
'_text': string
},
'File': {
'_text': string
},
'Version': {
'_text': string
}
},
'OtherLanguage': {
'LanguageCode': {
'refs': string,
'ObjectKey': string,
'_text': string
},
'Description': {
'_text': string
}
}
}>
},
'ContactMetadatas': {
'ContactMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'CountryMetadatas': {
'CountryMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'ICAO_Code': {
'_text': string
},
'Name': {
'_text': string
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
}>
},
'CurrencyMetadatas': {
'CurrencyMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Application': {
'_text': string
},
'Decimals': {
'_text': string
},
'Name': {
'_text': string
}
}>
},
'DescriptionMetadatas': {
'DescriptionMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Application': {
'_text': string
},
'Topic': {
'_text': string
},
'Hint': {
'_text': string
},
'Copyright': {
'_text': string
},
'Sequence': {
'_text': string
}
}>
},
'EquivalentID_Metadatas': {
'EquivalentID_Metadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'EquivID': Array<{
'metarefs': string,
'refs': string,
'MetadataKey': string,
'EquivalentID_Key': {
'_text': string
},
'ID_Value': {
'_text': string
},
'Owner': {
'_text': string
}
}>
}>
},
'LanguageMetadatas': {
'LanguageMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Application': {
'_text': string
},
'Code_ISO': {
'_text': string
},
'Code_NLS': {
'_text': string
},
'Name': {
'_text': string
}
}>
},
'PaymentCardMetadatas': {
'PaymentCardMetadata': Array<{
'refs': string,
'MetadataKey': string,
'CardCode': {
'_text': string
},
'CardName': {
'_text': string
},
'CardType': {
'_text': string
},
'CardFields': {
'FieldName': Array<{
'Mandatory': string,
'_text': string
}>
},
'CardProductType': {
'_text': string
},
'Surcharge': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
}
}
}>
},
'PaymentFormMetadatas': {
'PaymentFormMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'CustomerFileReference': {
'_text': string
},
'ExtendedPaymentCode': {
'_text': string
},
'Text': {
'_text': string
},
'CorporateContractCode': {
'_text': string
}
}>
},
'PriceMetadatas': {
'PriceMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'RuleMetadatas': {
'RuleMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'RuleID': {
'_text': string
},
'Name': {
'_text': string
},
'Status': {
'_text': string
},
'Values': {
'Value': Array<{
'NodePath': Array<{
'Path': {
'_text': string
},
'TagName': {
'_text': string
}
}>,
'Instruction': {
'_text': string
}
}>
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
},
'StateProvMetadatas': {
'StateProvMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
}>
},
'ZoneMetadatas': {
'ZoneMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
}>
}
}>
}
},
'OtherOffers': {
'ALaCarteOffer': {
'OfferID': {
'_text': string
},
'OwnerCode': {
'_text': string
},
'OwnerTypeCode': {
'_text': string
},
'ValidatingCarrierCode': {
'_text': string
},
'RequestedDateInd': {
'_text': string
},
'WebAddressURL': {
'_text': string
},
'RedemptionInd': {
'_text': string
},
'MatchAppText': {
'_text': string
},
'MatchType': {
'_text': string
},
'MatchPercent': {
'_text': string
},
'OfferExpirationDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PaymentTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PriceGuaranteeTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'BaggageDisclosureRefID': Array<{
'_text': string
}>,
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>,
'DisclosureRefID': {
'_text': string
},
'PenaltyRefID': Array<{
'_text': string
}>,
'PTC_OfferParameters': Array<{
'PTC_PricedCode': {
'_text': string
},
'PTC_RequestedCode': {
'_text': string
},
'RequestedPaxNumber': {
'_text': string
},
'PricedPaxNumber': {
'_text': string
}
}>,
'TotalPrice': {
'TotalAmount': {
'CurCode': string,
'_text': string
},
'BaseAmount': {
'CurCode': string,
'_text': string
},
'EquivAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitName': {
'_text': string
},
'Discount': {
'DiscountAmount': {
'CurCode': string,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'PreDiscountedAmount': {
'CurCode': string,
'_text': string
},
'AppText': {
'_text': string
},
'DescText': {
'_text': string
},
'DiscountContext': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
},
'Fee': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>,
'TaxSummary': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>,
'Surcharge': Array<{
'TotalAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'Breakdown': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>
}>
},
'ALaCarteOfferItem': Array<{
'OfferItemID': {
'_text': string
},
'UnitPrice': {
'TotalAmount': {
'CurCode': string,
'_text': string
},
'BaseAmount': {
'CurCode': string,
'_text': string
},
'EquivAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitName': {
'_text': string
},
'Discount': {
'DiscountAmount': {
'CurCode': string,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'PreDiscountedAmount': {
'CurCode': string,
'_text': string
},
'AppText': {
'_text': string
},
'DescText': {
'_text': string
},
'DiscountContext': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
},
'Fee': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>,
'TaxSummary': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>,
'Surcharge': Array<{
'TotalAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'Breakdown': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>
}>
},
'Service': {
'ServiceID': {
'_text': string
},
'ServiceRefID': {
'_text': string
},
'ServiceDefinitionRefID': {
'_text': string
}
},
'Eligibility': {
'PaxRefID': Array<{
'_text': string
}>,
'PriceClassRefID': Array<{
'_text': string
}>,
'FlightAssociations': {
'PaxJourneyRefID': Array<{
'_text': string
}>,
'PaxSegmentRefID': Array<{
'_text': string
}>
}
}
}>
},
'Offer': Array<{
'OfferID': {
'_text': string
},
'OwnerCode': {
'_text': string
},
'OwnerTypeCode': {
'_text': string
},
'ValidatingCarrierCode': {
'_text': string
},
'RequestedDateInd': {
'_text': string
},
'WebAddressURL': {
'_text': string
},
'RedemptionInd': {
'_text': string
},
'MatchAppText': {
'_text': string
},
'MatchType': {
'_text': string
},
'MatchPercent': {
'_text': string
},
'OfferExpirationDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PaymentTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PriceGuaranteeTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'BaggageAllowance': Array<{
'PaxJourneyRefID': Array<{
'_text': string
}>,
'PaxRefID': Array<{
'_text': string
}>,
'BaggageAllowanceRefID': {
'_text': string
}
}>,
'BaggageDisclosureRefID': Array<{
'_text': string
}>,
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>,
'DisclosureRefID': {
'_text': string
},
'PenaltyRefID': Array<{
'_text': string
}>,
'JourneyOverview': {
'PriceClassRefID': {
'_text': string
},
'JourneyPriceClass': Array<{
'PriceClassRefID': {
'_text': string
},
'PaxJourneyRefID': {
'_text': string
}
}>
},
'PTC_OfferParameters': Array<{
'PTC_PricedCode': {
'_text': string
},
'PTC_RequestedCode': {
'_text': string
},
'RequestedPaxNumber': {
'_text': string
},
'PricedPaxNumber': {
'_text': string
}
}>,
'OfferItem': Array<{
'OfferItemID': {
'_text': string
},
'MandatoryInd': {
'_text': string
},
'ModificationProhibitedInd': {
'_text': string
},
'FareDetail': Array<{
'FareIndicatorCode': {
'FiledFareInd': string,
'_text': string
},
'PassengerRefs': Array<{
'_text': string
}>,
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareComponent': Array<{
'Parameters': {
'Quantity': string
},
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareBasis': {
'FareBasisCode': {
'refs': string,
'Code': {
'_text': string
},
'Application': {
'_text': string
}
},
'FareRulesRemarks': {
'FareRulesRemark': Array<{
'refs': string,
'Category': {
'_text': string
},
'Text': {
'_text': string
}
}>
},
'FareBasisCityPair': {
'_text': string
},
'RBD': {
'_text': string
},
'CabinType': {
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
}
},
'TicketDesig': {
'Application': string,
'_text': string
},
'FareRules': {
'refs': string,
'Penalty': {
'CancelFeeInd': string,
'ChangeFeeInd': string,
'RefundableInd': string,
'ReuseInd': string,
'UpgradeFeeInd': string,
'refs': string,
'ObjectKey': string,
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'Details': {
'Detail': Array<{
'refs': string,
'Type': {
'_text': string
},
'Application': {
'_text': string
},
'Amounts': {
'Amount': Array<{
'CurrencyAmountValue': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
},
'AmountApplication': {
'_text': string
},
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
}
}>
}
},
'CorporateFare': {
'refs': string,
'ObjectKey': string,
'PricedInd': string,
'CompanyIndex': {
'refs': string,
'_text': string
},
'Contract': {
'refs': string,
'_text': string
},
'Account': {
'refs': string,
'_text': string
}
},
'AdvanceStay': {
'AdvancePurchase': {
'Context': string,
'_text': string
},
'AdvanceTicketing': {
'AdvanceReservation': {
'Context': string,
'_text': string
},
'AdvanceDeparture': {
'Context': string,
'_text': string
}
},
'MinimumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
},
'MaximumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
}
},
'Ticketing': {
'TicketlessInd': {
'_text': string
},
'InstantPurchase': {
'_text': string
},
'Endorsements': {
'Endorsement': Array<{
'_text': string
}>
}
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'PriceClassRef': {
'_text': string
},
'SegmentRefs': Array<{
'ON_Point': string,
'OFF_Point': string,
'_text': string
}>
}>,
'FlightMileage': {
'Value': {
'_text': string
},
'Application': {
'_text': string
}
},
'TourCode': {
'_text': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>,
'Service': Array<{
'ServiceID': {
'_text': string
},
'PaxRefID': Array<{
'_text': string
}>,
'ServiceRefID': {
'_text': string
},
'ServiceAssociations': {
'PaxJourneyRefID': Array<{
'_text': string
}>,
'SeatAssignment': {
'DatedOperatingLegRefID': {
'_text': string
},
'ServiceDefinitionRefID': {
'_text': string
},
'Seat': {
'SeatRowNumber': {
'_text': string
},
'CabinColumnID': {
'_text': string
}
}
},
'ServiceDefinitionRef': {
'ServiceDefinitionRefID': {
'_text': string
},
'PaxSegmentRefID': Array<{
'_text': string
}>
}
}
}>,
'Price': {
'TotalAmount': {
'CurCode': string,
'_text': string
},
'BaseAmount': {
'CurCode': string,
'_text': string
},
'EquivAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitName': {
'_text': string
},
'Discount': {
'DiscountAmount': {
'CurCode': string,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'PreDiscountedAmount': {
'CurCode': string,
'_text': string
},
'AppText': {
'_text': string
},
'DescText': {
'_text': string
},
'DiscountContext': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
},
'Fee': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>,
'TaxSummary': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>,
'Surcharge': Array<{
'TotalAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'Breakdown': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>
}>
}
}>,
'TotalPrice': {
'TotalAmount': {
'CurCode': string,
'_text': string
},
'BaseAmount': {
'CurCode': string,
'_text': string
},
'EquivAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitName': {
'_text': string
},
'Discount': {
'DiscountAmount': {
'CurCode': string,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'PreDiscountedAmount': {
'CurCode': string,
'_text': string
},
'AppText': {
'_text': string
},
'DescText': {
'_text': string
},
'DiscountContext': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
},
'Fee': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>,
'TaxSummary': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>,
'Surcharge': Array<{
'TotalAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'Breakdown': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>
}>
}
}>
},
'Payments': Array<{
'PaymentInfo': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'ContactInfoRefID': {
'_text': string
},
'OfferAssocations': Array<{
'OfferRefID': {
'_text': string
},
'OwnerRefCode': {
'_text': string
},
'ResponseRefID': {
'_text': string
},
'OfferItemRefID': Array<{
'_text': string
}>
}>,
'PaymentMethod': {
'TypeCode': {
'_text': string
},
'BankAccount': {
'AccountID': {
'_text': string
},
'AccountType': {
'_text': string
},
'BankID': {
'_text': string
},
'OwnerName': {
'_text': string
}
},
'Cash': {
'CashInd': {
'_text': string
}
},
'Check': {
'CheckNumber': {
'_text': string
},
'PayeeName': {
'_text': string
},
'SignedDate': {
'_text': string
}
},
'DirectBill': {
'OrgID': {
'_text': string
},
'OrgName': {
'_text': string
},
'ContactInfo': {
'ContactInfoID': {
'_text': string
},
'ContactTypeText': {
'_text': string
},
'IndividualRef': {
'_text': string
},
'ContactRefusedInd': {
'_text': string
},
'Phone': Array<{
'LabelText': {
'_text': string
},
'CountryDialingCode': {
'_text': string
},
'AreaCodeNumber': {
'_text': string
},
'PhoneNumber': {
'_text': string
},
'ExtensionNumber': {
'_text': string
}
}>,
'OtherAddress': Array<{
'LabelText': {
'_text': string
},
'OtherAddressText': {
'_text': string
}
}>,
'EmailAddress': Array<{
'LabelText': {
'_text': string
},
'EmailAddressText': {
'_text': string
}
}>,
'Individual': {
'IndividualID': {
'_text': string
},
'GenderCode': {
'_text': string
},
'TitleName': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
},
'SuffixName': {
'_text': string
},
'Birthdate': {
'_text': string
},
'BirthplaceText': {
'_text': string
}
},
'PostalAddress': Array<{
'LabelText': {
'_text': string
},
'StreetText': Array<{
'_text': string
}>,
'BuildingRoomText': {
'_text': string
},
'PO_BoxCode': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CityName': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
},
'CountryName': {
'_text': string
},
'CountryCode': {
'_text': string
}
}>
}
},
'LoyaltyRedemption': {
'AccountNumber': {
'_text': string
},
'CertificateNumber': Array<{
'_text': string
}>,
'LoyaltyCurAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyProgramAccount': {
'ProgramName': {
'_text': string
},
'ProgramCode': {
'_text': string
},
'AccountNumber': {
'_text': string
},
'URL': {
'_text': string
},
'SignInID': {
'_text': string
},
'TierCode': {
'_text': string
},
'TierName': {
'_text': string
},
'TierPriorityText': {
'_text': string
},
'ProviderName': {
'_text': string
},
'Alliance': {
'AllianceCode': {
'_text': string
},
'Name': {
'_text': string
},
'URL': {
'_text': string
}
},
'Carrier': {
'AirlineDesigCode': {
'_text': string
},
'DuplicateDesigInd': {
'_text': string
},
'Name': {
'_text': string
}
}
}
},
'MiscChargeOrder': {
'TicketID': {
'_text': string
}
},
'OtherPaymentMethod': {
'Remark': Array<{
'RemarkText': {
'_text': string
},
'DisplayInd': {
'_text': string
},
'CreationDateTime': {
'TimeZoneCode': string,
'_text': string
}
}>
},
'PaymentCard': {
'CardType': {
'_text': string
},
'CreditCardVendorCode': {
'_text': string
},
'CardNumber': {
'_text': string
},
'CardHolderName': {
'_text': string
},
'CardIssuerName': {
'_text': string
},
'MaskedCardNumber': {
'_text': string
},
'TokenizedCardNumber': {
'_text': string
},
'ApprovalCode': {
'_text': string
},
'EffectiveDate': {
'_text': string
},
'ExpirationDate': {
'_text': string
},
'ReconciliationID': {
'_text': string
},
'ContactInfoRefID': {
'_text': string
}
},
'Voucher': {
'VoucherID': {
'_text': string
},
'EffectiveDate': {
'_text': string
},
'ExpirationDate': {
'_text': string
}
}
}
}>
}>,
'PricedOffer': {
'Offer': {
'OfferID': {
'_text': string
},
'OwnerCode': {
'_text': string
},
'OwnerTypeCode': {
'_text': string
},
'ValidatingCarrierCode': {
'_text': string
},
'RequestedDateInd': {
'_text': string
},
'WebAddressURL': {
'_text': string
},
'RedemptionInd': {
'_text': string
},
'MatchAppText': {
'_text': string
},
'MatchType': {
'_text': string
},
'MatchPercent': {
'_text': string
},
'OfferExpirationDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PaymentTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PriceGuaranteeTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'BaggageAllowance': Array<{
'PaxJourneyRefID': Array<{
'_text': string
}>,
'PaxRefID': Array<{
'_text': string
}>,
'BaggageAllowanceRefID': {
'_text': string
}
}>,
'BaggageDisclosureRefID': Array<{
'_text': string
}>,
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>,
'DisclosureRefID': {
'_text': string
},
'PenaltyRefID': Array<{
'_text': string
}>,
'JourneyOverview': {
'PriceClassRefID': {
'_text': string
},
'JourneyPriceClass': Array<{
'PriceClassRefID': {
'_text': string
},
'PaxJourneyRefID': {
'_text': string
}
}>
},
'PTC_OfferParameters': Array<{
'PTC_PricedCode': {
'_text': string
},
'PTC_RequestedCode': {
'_text': string
},
'RequestedPaxNumber': {
'_text': string
},
'PricedPaxNumber': {
'_text': string
}
}>,
'OfferItem': Array<{
'OfferItemID': {
'_text': string
},
'MandatoryInd': {
'_text': string
},
'ModificationProhibitedInd': {
'_text': string
},
'FareDetail': Array<{
'FareIndicatorCode': {
'FiledFareInd': string,
'_text': string
},
'PassengerRefs': Array<{
'_text': string
}>,
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareComponent': Array<{
'Parameters': {
'Quantity': string
},
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareBasis': {
'FareBasisCode': {
'refs': string,
'Code': {
'_text': string
},
'Application': {
'_text': string
}
},
'FareRulesRemarks': {
'FareRulesRemark': Array<{
'refs': string,
'Category': {
'_text': string
},
'Text': {
'_text': string
}
}>
},
'FareBasisCityPair': {
'_text': string
},
'RBD': {
'_text': string
},
'CabinType': {
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
}
},
'TicketDesig': {
'Application': string,
'_text': string
},
'FareRules': {
'refs': string,
'Penalty': {
'CancelFeeInd': string,
'ChangeFeeInd': string,
'RefundableInd': string,
'ReuseInd': string,
'UpgradeFeeInd': string,
'refs': string,
'ObjectKey': string,
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'Details': {
'Detail': Array<{
'refs': string,
'Type': {
'_text': string
},
'Application': {
'_text': string
},
'Amounts': {
'Amount': Array<{
'CurrencyAmountValue': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
},
'AmountApplication': {
'_text': string
},
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
}
}>
}
},
'CorporateFare': {
'refs': string,
'ObjectKey': string,
'PricedInd': string,
'CompanyIndex': {
'refs': string,
'_text': string
},
'Contract': {
'refs': string,
'_text': string
},
'Account': {
'refs': string,
'_text': string
}
},
'AdvanceStay': {
'AdvancePurchase': {
'Context': string,
'_text': string
},
'AdvanceTicketing': {
'AdvanceReservation': {
'Context': string,
'_text': string
},
'AdvanceDeparture': {
'Context': string,
'_text': string
}
},
'MinimumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
},
'MaximumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
}
},
'Ticketing': {
'TicketlessInd': {
'_text': string
},
'InstantPurchase': {
'_text': string
},
'Endorsements': {
'Endorsement': Array<{
'_text': string
}>
}
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'PriceClassRef': {
'_text': string
},
'SegmentRefs': Array<{
'ON_Point': string,
'OFF_Point': string,
'_text': string
}>
}>,
'FlightMileage': {
'Value': {
'_text': string
},
'Application': {
'_text': string
}
},
'TourCode': {
'_text': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>,
'Service': Array<{
'ServiceID': {
'_text': string
},
'PaxRefID': Array<{
'_text': string
}>,
'ServiceRefID': {
'_text': string
},
'ServiceAssociations': {
'PaxJourneyRefID': Array<{
'_text': string
}>,
'SeatAssignment': {
'DatedOperatingLegRefID': {
'_text': string
},
'ServiceDefinitionRefID': {
'_text': string
},
'Seat': {
'SeatRowNumber': {
'_text': string
},
'CabinColumnID': {
'_text': string
}
}
},
'ServiceDefinitionRef': {
'ServiceDefinitionRefID': {
'_text': string
},
'PaxSegmentRefID': Array<{
'_text': string
}>
}
}
}>,
'Price': {
'TotalAmount': {
'CurCode': string,
'_text': string
},
'BaseAmount': {
'CurCode': string,
'_text': string
},
'EquivAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitName': {
'_text': string
},
'Discount': {
'DiscountAmount': {
'CurCode': string,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'PreDiscountedAmount': {
'CurCode': string,
'_text': string
},
'AppText': {
'_text': string
},
'DescText': {
'_text': string
},
'DiscountContext': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
},
'Fee': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>,
'TaxSummary': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>,
'Surcharge': Array<{
'TotalAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'Breakdown': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>
}>
}
}>,
'TotalPrice': {
'TotalAmount': {
'CurCode': string,
'_text': string
},
'BaseAmount': {
'CurCode': string,
'_text': string
},
'EquivAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitName': {
'_text': string
},
'Discount': {
'DiscountAmount': {
'CurCode': string,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'PreDiscountedAmount': {
'CurCode': string,
'_text': string
},
'AppText': {
'_text': string
},
'DescText': {
'_text': string
},
'DiscountContext': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
},
'Fee': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>,
'TaxSummary': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>,
'Surcharge': Array<{
'TotalAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'Breakdown': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>
}>
}
}
},
'Processing': {
'refs': string,
'Marketing': {
'refs': string,
'Message': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'Associations': {
'refs': string,
'Association': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}>
}
},
'Promotions': {
'Promotion': Array<{
'Code': {
'_text': string
},
'OwnerID': {
'_text': string
},
'URL': {
'_text': string
},
'Remark': Array<{
'RemarkText': {
'_text': string
},
'DisplayInd': {
'_text': string
},
'CreationDateTime': {
'TimeZoneCode': string,
'_text': string
}
}>
}>
},
'ResponseParameters': {
'CurParameter': Array<{
'CurCode': {
'_text': string
},
'AppCode': {
'_text': string
},
'DecimalsAllowedNumber': {
'_text': string
}
}>,
'LangUsage': Array<{
'LangCode': {
'_text': string
},
'LangUsageType': {
'_text': string
}
}>,
'MeasurementSystemCode': {
'_text': string
},
'PricingParameter': {
'AutoExchInd': {
'_text': string
},
'AwardIncludedInd': {
'_text': string
},
'AwardOnlyInd': {
'_text': string
},
'OverrideCurCode': {
'_text': string
},
'SimplePricingInd': {
'_text': string
},
'TaxExemption': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>
}
},
'ShoppingResponse': {
'ShoppingResponseID': {
'_text': string
},
'OwnerCode': {
'_text': string
}
},
'Warning': Array<{
'Code': {
'_text': string
},
'DescText': {
'_text': string
},
'LanguageCode': {
'_text': string
},
'OwnerName': {
'_text': string
},
'StatusText': {
'_text': string
},
'TagText': {
'_text': string
},
'TypeCode': {
'_text': string
},
'URL': {
'_text': string
}
}>
}
export type NDC_OrderReshopRes = {
'ChangeFees': {
'PenaltyID': {
'_text': string
},
'UpgradeFeeInd': {
'_text': string
},
'TypeCode': {
'_text': string
},
'DescText': {
'_text': string
},
'AppCode': {
'_text': string
},
'CancelFeeInd': {
'_text': string
},
'ChangeFeeInd': {
'_text': string
},
'PenaltyAmount': {
'CurCode': string,
'_text': string
},
'PenaltyPercent': {
'_text': string
}
},
'Commission': {
'Amount': {
'CurCode': string,
'_text': string
},
'Percentage': {
'_text': string
},
'CommissionCode': {
'_text': string
},
'RemarkText': Array<{
'_text': string
}>
},
'DataLists': {
'BaggageAllowanceList': {
'BaggageAllowance': Array<{
'BaggageAllowanceID': {
'_text': string
},
'TypeCode': {
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'ApplicablePartyText': {
'_text': string
},
'DescText': Array<{
'_text': string
}>,
'WeightAllowance': Array<{
'MaximumWeightMeasure': {
'UnitCode': string,
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'ApplicablePartyText': {
'_text': string
},
'DescText': Array<{
'_text': string
}>
}>,
'BDC': {
'CarrierDesigCode': {
'_text': string
},
'CarrierName': {
'_text': string
},
'BagRuleCode': {
'_text': string
},
'BDCAnalysisResultCode': {
'_text': string
},
'BDCReasonText': {
'_text': string
}
},
'DimensionAllowance': Array<{
'BaggageDimensionCategory': {
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'MinMeasure': {
'UnitCode': string,
'_text': string
},
'ApplicablePartyText': {
'_text': string
},
'MaxMeasure': {
'UnitCode': string,
'_text': string
},
'DescText': Array<{
'_text': string
}>,
'Qty': {
'_text': string
}
}>,
'PieceAllowance': Array<{
'ApplicablePartyText': {
'_text': string
},
'TotalQty': {
'_text': string
},
'TypeText': {
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'Desc': Array<{
'_text': string
}>,
'PieceDimensionAllowance': Array<{
'BaggageDimensionCategory': {
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'MinMeasure': {
'UnitCode': string,
'_text': string
},
'ApplicablePartyText': {
'_text': string
},
'MaxMeasure': {
'UnitCode': string,
'_text': string
},
'DescText': Array<{
'_text': string
}>,
'Qty': {
'_text': string
}
}>,
'PieceWeightAllowance': Array<{
'MaximumWeightMeasure': {
'UnitCode': string,
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'ApplicablePartyText': {
'_text': string
},
'DescText': Array<{
'_text': string
}>
}>
}>
}>
},
'BaggageDisclosureList': {
'BaggageDisclosure': Array<{
'BaggageDisclosureID': {
'_text': string
},
'RuleTypeCode': {
'_text': string
},
'CommercialAgreementID': {
'_text': string
},
'DeferralInd': {
'_text': string
},
'FixedPrePaidInd': {
'_text': string
},
'CheckInChargesInd': {
'_text': string
},
'BDC': {
'CarrierDesigCode': {
'_text': string
},
'CarrierName': {
'_text': string
},
'BagRuleCode': {
'_text': string
},
'BDCAnalysisResultCode': {
'_text': string
},
'BDCReasonText': {
'_text': string
}
},
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>
}>
},
'ContactInfoList': {
'ContactInfo': Array<{
'ContactInfoID': {
'_text': string
},
'ContactTypeText': {
'_text': string
},
'IndividualRef': {
'_text': string
},
'ContactRefusedInd': {
'_text': string
},
'Phone': Array<{
'LabelText': {
'_text': string
},
'CountryDialingCode': {
'_text': string
},
'AreaCodeNumber': {
'_text': string
},
'PhoneNumber': {
'_text': string
},
'ExtensionNumber': {
'_text': string
}
}>,
'OtherAddress': Array<{
'LabelText': {
'_text': string
},
'OtherAddressText': {
'_text': string
}
}>,
'EmailAddress': Array<{
'LabelText': {
'_text': string
},
'EmailAddressText': {
'_text': string
}
}>,
'Individual': {
'IndividualID': {
'_text': string
},
'Birthdate': {
'_text': string
},
'BirthplaceText': {
'_text': string
},
'GenderCode': {
'_text': string
},
'TitleName': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
},
'SuffixName': {
'_text': string
}
},
'PostalAddress': Array<{
'LabelText': {
'_text': string
},
'StreetText': Array<{
'_text': string
}>,
'BuildingRoomText': {
'_text': string
},
'PO_BoxCode': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CityName': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
},
'CountryName': {
'_text': string
},
'CountryCode': {
'_text': string
}
}>
}>
},
'ContentSource': {
'ContentSource': Array<{
'refs': string,
'ListKey': string,
'NodePath': {
'_text': string
},
'AirlineID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'OtherID': {
'name': string,
'_text': string
}
}>
},
'DisclosureList': {
'DisclosureID': {
'_text': string
},
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>
},
'FareList': {
'FareGroup': Array<{
'refs': string,
'ListKey': string,
'Fare': {
'FareCode': {
'_text': string
},
'FareDetail': {
'FareIndicatorCode': {
'FiledFareInd': string,
'_text': string
},
'PassengerRefs': Array<{
'_text': string
}>,
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareComponent': Array<{
'Parameters': {
'Quantity': string
},
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareBasis': {
'FareBasisCode': {
'refs': string,
'Code': {
'_text': string
},
'Application': {
'_text': string
}
},
'FareRulesRemarks': {
'FareRulesRemark': Array<{
'refs': string,
'Category': {
'_text': string
},
'Text': {
'_text': string
}
}>
},
'FareBasisCityPair': {
'_text': string
},
'RBD': {
'_text': string
},
'CabinType': {
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
}
},
'TicketDesig': {
'Application': string,
'_text': string
},
'FareRules': {
'refs': string,
'Penalty': {
'CancelFeeInd': string,
'ChangeFeeInd': string,
'RefundableInd': string,
'ReuseInd': string,
'UpgradeFeeInd': string,
'refs': string,
'ObjectKey': string,
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'Details': {
'Detail': Array<{
'refs': string,
'Type': {
'_text': string
},
'Application': {
'_text': string
},
'Amounts': {
'Amount': Array<{
'CurrencyAmountValue': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
},
'AmountApplication': {
'_text': string
},
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
}
}>
}
},
'CorporateFare': {
'refs': string,
'ObjectKey': string,
'PricedInd': string,
'CompanyIndex': {
'refs': string,
'_text': string
},
'Contract': {
'refs': string,
'_text': string
},
'Account': {
'refs': string,
'_text': string
}
},
'AdvanceStay': {
'AdvancePurchase': {
'Context': string,
'_text': string
},
'AdvanceTicketing': {
'AdvanceReservation': {
'Context': string,
'_text': string
},
'AdvanceDeparture': {
'Context': string,
'_text': string
}
},
'MinimumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
},
'MaximumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
}
},
'Ticketing': {
'TicketlessInd': {
'_text': string
},
'InstantPurchase': {
'_text': string
},
'Endorsements': {
'Endorsement': Array<{
'_text': string
}>
}
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'PriceClassRef': {
'_text': string
},
'SegmentRefs': Array<{
'ON_Point': string,
'OFF_Point': string,
'_text': string
}>
}>,
'FlightMileage': {
'Value': {
'_text': string
},
'Application': {
'_text': string
}
},
'TourCode': {
'_text': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}
},
'FareBasisCode': {
'Code': {
'_text': string
},
'Application': {
'_text': string
}
}
}>
},
'InstructionsList': {
'Instruction': Array<{
'refs': string,
'ListKey': string,
'ClassOfServiceUpgrade': {
'Classes': {
'ClassOfService': Array<{
'refs': string,
'Code': {
'SeatsLeft': string,
'_text': string
},
'MarketingName': {
'CabinDesignator': string,
'_text': string
},
'FareBasisCode': {
'_text': string
}
}>
}
},
'FreeFormTextInstruction': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'SpecialBookingInstruction': {
'_text': string
}
}>
},
'MediaList': {
'Media': Array<{
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}>
},
'OriginDestList': {
'OriginDest': Array<{
'OriginDestID': {
'_text': string
},
'ArrivalStationCode': {
'_text': string
},
'DepStationCode': {
'_text': string
},
'PaxJourneyRefID': Array<{
'_text': string
}>
}>
},
'PaxJourneyList': {
'PaxJourney': Array<{
'PaxJourneyID': {
'_text': string
},
'DistanceMeasure': {
'UnitCode': string,
'_text': string
},
'Duration': {
'_text': string
},
'PaxSegmentRefID': Array<{
'_text': string
}>,
'SettlementInfo': {
'MethodCode': {
'_text': string
},
'SettlementAmount': {
'CurCode': string,
'_text': string
}
}
}>
},
'PaxList': {
'Pax': Array<{
'PaxID': {
'_text': string
},
'PTC': {
'_text': string
},
'AgeMeasure': {
'UnitCode': string,
'_text': string
},
'Birthdate': {
'_text': string
},
'ResidenceCountryCode': {
'_text': string
},
'CitizenshipCountryCode': {
'_text': string
},
'ProfileID_Text': {
'_text': string
},
'ProfileConsentInd': {
'_text': string
},
'ContactInfoRefID': {
'_text': string
},
'PaxRefID': {
'_text': string
},
'IdentityDoc': Array<{
'IdentityDocNumber': {
'_text': string
},
'IdentityDocTypeCode': {
'_text': string
},
'IssuingCountryCode': {
'_text': string
},
'TitleName': {
'_text': string
},
'CitizenshipCountryCode': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'ResidenceCountryCode': {
'_text': string
},
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
},
'SuffixName': {
'_text': string
},
'Birthdate': {
'_text': string
},
'BirthplaceText': {
'_text': string
},
'GenderCode': {
'_text': string
},
'IssueDate': {
'_text': string
},
'ExpiryDate': {
'_text': string
},
'Individual': Array<{
'IndividualID': {
'_text': string
},
'Birthdate': {
'_text': string
},
'BirthplaceText': {
'_text': string
},
'GenderCode': {
'_text': string
},
'TitleName': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
},
'SuffixName': {
'_text': string
}
}>,
'Visa': Array<{
'VisaNumber': {
'_text': string
},
'VisaTypeCode': {
'_text': string
},
'DurationOfStay': {
'_text': string
},
'EnterBeforeDate': {
'_text': string
},
'EntryQty': {
'_text': string
},
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': Array<{
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
}>
}>
}>,
'Individual': {
'IndividualID': {
'_text': string
},
'Birthdate': {
'_text': string
},
'BirthplaceText': {
'_text': string
},
'GenderCode': {
'_text': string
},
'TitleName': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
},
'SuffixName': {
'_text': string
}
},
'LoyaltyProgramAccount': Array<{
'ProgramName': {
'_text': string
},
'ProgramCode': {
'_text': string
},
'AccountNumber': {
'_text': string
},
'URL': {
'_text': string
},
'SignInID': {
'_text': string
},
'TierCode': {
'_text': string
},
'TierName': {
'_text': string
},
'TierPriorityText': {
'_text': string
},
'ProviderName': {
'_text': string
},
'Alliance': {
'AllianceCode': {
'_text': string
},
'Name': {
'_text': string
},
'URL': {
'_text': string
}
},
'Carrier': {
'AirlineDesigCode': {
'_text': string
},
'DuplicateDesigInd': {
'_text': string
},
'Name': {
'_text': string
},
'Alliance': {
'AllianceCode': {
'_text': string
},
'Name': {
'_text': string
},
'URL': {
'_text': string
}
}
}
}>,
'Remark': Array<{
'RemarkText': {
'_text': string
},
'DisplayInd': {
'_text': string
},
'CreationDateTime': {
'TimeZoneCode': string,
'_text': string
}
}>
}>
},
'PaxSegmentList': {
'PaxSegment': Array<{
'PaxSegmentID': {
'_text': string
},
'ARNK_Ind': {
'_text': string
},
'TicketlessInd': {
'_text': string
},
'SecureFlightInd': {
'_text': string
},
'Duration': {
'_text': string
},
'SegmentTypeCode': {
'_text': string
},
'CabinType': {
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
},
'Dep': {
'BoardingGateID': {
'_text': string
},
'IATA_LocationCode': {
'_text': string
},
'StationName': {
'_text': string
},
'TerminalName': {
'_text': string
},
'AircraftScheduledDateTime': {
'TimeZoneCode': string,
'_text': string
}
},
'Arrival': {
'BoardingGateID': {
'_text': string
},
'IATA_LocationCode': {
'_text': string
},
'StationName': {
'_text': string
},
'TerminalName': {
'_text': string
},
'AircraftScheduledDateTime': {
'TimeZoneCode': string,
'_text': string
}
},
'MarketingCarrierInfo': {
'CarrierDesigCode': {
'_text': string
},
'CarrierName': {
'_text': string
},
'MarketingCarrierFlightNumberText': {
'_text': string
},
'OperationalSuffixText': {
'_text': string
},
'RBD_Code': {
'_text': string
}
},
'OperatingCarrierInfo': {
'CarrierDesigCode': {
'_text': string
},
'CarrierName': {
'_text': string
},
'OperatingCarrierFlightNumberText': {
'_text': string
},
'RBD_Code': {
'_text': string
},
'OperationalSuffixText': {
'_text': string
},
'Disclosure': {
'DisclosureID': {
'_text': string
},
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>
}
},
'DatedOperatingLeg': Array<{
'DatedOperatingLegID': {
'_text': string
},
'CarrierAircraftType': {
'CarrierAircraftTypeCode': {
'_text': string
},
'CarrierAircraftTypeName': {
'_text': string
}
},
'ChangeofGaugeInd': {
'_text': string
},
'DistanceMeasure': {
'UnitCode': string,
'_text': string
},
'Dep': {
'BoardingGateID': {
'_text': string
},
'IATA_LocationCode': {
'_text': string
},
'StationName': {
'_text': string
},
'TerminalName': {
'_text': string
},
'AircraftScheduledDateTime': {
'TimeZoneCode': string,
'_text': string
}
},
'Arrival': {
'BoardingGateID': {
'_text': string
},
'IATA_LocationCode': {
'_text': string
},
'StationName': {
'_text': string
},
'TerminalName': {
'_text': string
},
'AircraftScheduledDateTime': {
'TimeZoneCode': string,
'_text': string
}
},
'IATA_AircraftType': {
'IATA_AircraftTypeCode': {
'_text': string
}
},
'OnGroundTime': {
'_text': string
}
}>,
'SettlementInfo': {
'MethodCode': {
'_text': string
},
'SettlementAmount': {
'CurCode': string,
'_text': string
}
}
}>
},
'PenaltyList': {
'Penalty': Array<{
'PenaltyID': {
'_text': string
},
'UpgradeFeeInd': {
'_text': string
},
'TypeCode': {
'_text': string
},
'DescText': {
'_text': string
},
'AppCode': {
'_text': string
},
'CancelFeeInd': {
'_text': string
},
'ChangeFeeInd': {
'_text': string
},
'PenaltyAmount': {
'CurCode': string,
'_text': string
},
'PenaltyPercent': {
'_text': string
}
}>
},
'PriceClassList': {
'PriceClass': Array<{
'PriceClassID': {
'_text': string
},
'Code': {
'_text': string
},
'Name': {
'_text': string
},
'FareBasisCode': {
'_text': string
},
'FareBasisAppText': {
'_text': string
},
'DisplayOrderText': {
'_text': string
},
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>,
'CabinType': Array<{
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
}>
}>
},
'SeatProfileList': {
'SeatProfile': Array<{
'SeatProfileID': {
'_text': string
},
'SeatWidthMeasure': {
'UnitCode': string,
'_text': string
},
'CharacteristicCode': {
'_text': string
},
'SeatPitchMeasure': {
'UnitCode': string,
'_text': string
},
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>,
'SeatKeywords': Array<{
'KeywordText': {
'_text': string
},
'KeywordValueText': Array<{
'_text': string
}>
}>
}>
},
'ServiceDefinitionList': {
'ServiceDefinition': Array<{
'ServiceDefinitionID': {
'_text': string
},
'OwnerCode': {
'_text': string
},
'Name': {
'_text': string
},
'ServiceCode': {
'_text': string
},
'ReasonForIssuanceCode': {
'_text': string
},
'ReasonForIssuanceSubCode': {
'_text': string
},
'DepositTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'NamingTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PaymentTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PriceGuaranteeTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'TicketingTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'BilateralProcessTimeLimit': Array<{
'Name': {
'_text': string
},
'TimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'DescText': {
'_text': string
}
}>,
'ValidatingCarrierCode': {
'_text': string
},
'Description': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>,
'ServiceDefinitionAssociation': {
'BaggageAllowanceRefID': {
'_text': string
},
'SeatProfileRefID': {
'_text': string
},
'ServiceBundle': {
'MaxQty': {
'_text': string
},
'ServiceDefinitionRefID': Array<{
'_text': string
}>
}
},
'BookingInstructions': {
'SSRCode': Array<{
'_text': string
}>,
'OSIText': Array<{
'_text': string
}>,
'Method': {
'_text': string
},
'UpgradeMethod': {
'NewClass': string,
'_text': string
},
'Text': Array<{
'_text': string
}>,
'Equipment': {
'_text': string
}
},
'Detail': {
'ServiceCombinations': {
'Combination': Array<{
'Rule': {
'_text': string
},
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>,
'ServiceReference': Array<{
'_text': string
}>
}>
},
'ServiceCoupon': {
'InstantPurchase': {
'_text': string
},
'FeeBasis': {
'_text': string
},
'CouponType': {
'_text': string
}
},
'ServiceFulfillment': {
'refs': string,
'OfferValidDates': {
'Start': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'End': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
}
},
'Provider': {
'metadata': string,
'refs': string,
'PartnerID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Name': {
'_text': string
},
'Type': {
'_text': string
},
'Fulfillments': {
'Fulfillment': Array<{
'refs': string,
'OfferValidDates': {
'Start': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'End': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
}
},
'Location': {
'AirportCode': {
'refs': string,
'_text': string
},
'StructuredAddress': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
}
}
}>
}
},
'Location': {
'AirportFulfillmentLocation': {
'refs': string,
'AirportCode': {
'refs': string,
'_text': string
}
},
'OtherFulfillmentLocation': {
'Address': {
'AddressCore': {
'refs': string,
'Address': {
'PaymentAddress': {
'refs': string,
'Street': Array<{
'_text': string
}>,
'PO_Box': {
'_text': string
},
'BuildingRoom': {
'_text': string
},
'City': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'Country': {
'_text': string
}
},
'SimpleAddress': {
'refs': string,
'AddressLine': Array<{
'_text': string
}>
},
'StructuredAddress': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
}
}
},
'AddressDetail': {
'refs': string,
'Address': {
'PaymentAddress': {
'refs': string,
'Street': Array<{
'_text': string
}>,
'PO_Box': {
'_text': string
},
'BuildingRoom': {
'_text': string
},
'City': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'Country': {
'_text': string
}
},
'SimpleAddress': {
'refs': string,
'AddressLine': Array<{
'_text': string
}>
},
'StructuredAddress': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
}
},
'Directions': {
'Direction': Array<{
'refs': string,
'Text': {
'_text': string
},
'Link': {
'_text': string
},
'Name': {
'_text': string
},
'From': {
'_text': string
},
'To': {
'_text': string
}
}>
}
}
}
}
}
},
'ServiceItemQuantityRules': {
'MinimumQuantity': {
'_text': string
},
'MaximumQuantity': {
'_text': string
}
}
}
}>
},
'TermsList': {
'refs': string,
'ListKey': string,
'Term': Array<{
'refs': string,
'AvailablePeriod': {
'Earliest': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'Latest': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
}
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
},
'GroupSize': {
'Minimum': {
'_text': string
},
'Maximum': {
'_text': string
}
},
'OrderingQuantity': {
'Minimum': {
'_text': string
},
'Maximum': {
'_text': string
}
}
}>
}
},
'Metadata': {
'PassengerMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'NameDetail': {
'GivenNamePrefix': Array<{
'_text': string
}>,
'TitleSuffix': Array<{
'_text': string
}>,
'SurnamePrefix': {
'_text': string
},
'SurnameSuffix': Array<{
'_text': string
}>
}
}>,
'Other': {
'OtherMetadata': Array<{
'AddressMetadatas': {
'AddressMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
},
'AddressType': {
'_text': string
},
'AddressFields': {
'FieldName': Array<{
'Mandatory': string,
'_text': string
}>
}
}>
},
'AircraftMetadatas': {
'AircraftMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'TailNumber': {
'_text': string
},
'Name': {
'_text': string
}
}>
},
'AirportMetadatas': {
'AirportMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
},
'Directions': {
'Direction': Array<{
'refs': string,
'Text': {
'_text': string
},
'Link': {
'_text': string
},
'Name': {
'_text': string
},
'From': {
'_text': string
},
'To': {
'_text': string
}
}>
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
}>
},
'CityMetadatas': {
'CityMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Directions': {
'Direction': Array<{
'refs': string,
'Text': {
'_text': string
},
'Link': {
'_text': string
},
'Name': {
'_text': string
},
'From': {
'_text': string
},
'To': {
'_text': string
}
}>
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
}>
},
'CodesetMetadatas': {
'CodesetMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Source': {
'OwnerID': {
'Name': string,
'_text': string
},
'File': {
'_text': string
},
'Version': {
'_text': string
}
},
'OtherLanguage': {
'LanguageCode': {
'refs': string,
'ObjectKey': string,
'_text': string
},
'Description': {
'_text': string
}
}
}>
},
'ContactMetadatas': {
'ContactMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'ContentMetadatas': {
'ContentMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'CountryMetadatas': {
'CountryMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'ICAO_Code': {
'_text': string
},
'Name': {
'_text': string
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
}>
},
'CurrencyMetadatas': {
'CurrencyMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Application': {
'_text': string
},
'Decimals': {
'_text': string
},
'Name': {
'_text': string
}
}>
},
'DescriptionMetadatas': {
'DescriptionMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Application': {
'_text': string
},
'Topic': {
'_text': string
},
'Hint': {
'_text': string
},
'Copyright': {
'_text': string
},
'Sequence': {
'_text': string
}
}>
},
'EquivalentID_Metadatas': {
'EquivalentID_Metadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'EquivID': Array<{
'metarefs': string,
'refs': string,
'MetadataKey': string,
'EquivalentID_Key': {
'_text': string
},
'ID_Value': {
'_text': string
},
'Owner': {
'_text': string
}
}>
}>
},
'LanguageMetadatas': {
'LanguageMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Application': {
'_text': string
},
'Code_ISO': {
'_text': string
},
'Code_NLS': {
'_text': string
},
'Name': {
'_text': string
}
}>
},
'PaymentCardMetadatas': {
'PaymentCardMetadata': Array<{
'refs': string,
'MetadataKey': string,
'CardCode': {
'_text': string
},
'CardName': {
'_text': string
},
'CardType': {
'_text': string
},
'CardFields': {
'FieldName': Array<{
'Mandatory': string,
'_text': string
}>
},
'CardProductType': {
'_text': string
},
'Surcharge': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
}
}
}>
},
'PaymentFormMetadatas': {
'PaymentFormMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'CustomerFileReference': {
'_text': string
},
'ExtendedPaymentCode': {
'_text': string
},
'Text': {
'_text': string
},
'CorporateContractCode': {
'_text': string
}
}>
},
'PriceMetadatas': {
'PriceMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'RuleMetadatas': {
'RuleMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'RuleID': {
'_text': string
},
'Name': {
'_text': string
},
'Status': {
'_text': string
},
'Values': {
'Value': Array<{
'NodePath': Array<{
'Path': {
'_text': string
},
'TagName': {
'_text': string
}
}>,
'Instruction': {
'_text': string
}
}>
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
},
'StateProvMetadatas': {
'StateProvMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
}>
},
'ZoneMetadatas': {
'ZoneMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
}>
}
}>
}
},
'OrderReshopProcessing': {
'refs': string,
'MarketingMessages': {
'MarketMessage': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'Associations': {
'OfferAssociations': {
'Shopper': {
'AllShopperInd': string,
'PassengerReference': Array<{
'_text': string
}>
},
'Flight': {
'AllSegmentInd': {
'_text': string
},
'FlightSegmentReference': Array<{
'ref': string,
'Cabin': {
'refs': string,
'CabinDesignator': {
'AllCabins': string,
'_text': string
},
'MarketingName': {
'_text': string
}
},
'ClassOfService': {
'refs': string,
'Code': {
'SeatsLeft': string,
'_text': string
},
'MarketingName': {
'CabinDesignator': string,
'_text': string
},
'FareBasisCode': {
'_text': string
}
},
'BagDetailAssociation': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'MarriedSegmentGroup': {
'_text': string
}
}>,
'AllOriginDestinationInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'AllFlightInd': {
'_text': string
},
'FlightReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'PriceClass': {
'PriceClassReference': {
'_text': string
}
},
'BagDetails': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'OfferDetails': {
'OfferInstructionReferences': {
'_text': string
},
'OfferPenaltyReferences': {
'_text': string
},
'OfferTermReferences': Array<{
'_text': string
}>
},
'Services': {
'Service': Array<{
'BundleReference': {
'_text': string
},
'ServiceReferences': Array<{
'_text': string
}>
}>
},
'Media': {
'MediaGroupreference': {
'_text': string
},
'MediaItems': Array<{
'MediaItemReference': {
'_text': string
},
'MediaLink': {
'_text': string
}
}>
},
'Other': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
},
'OrderAssociations': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}
}
}>
},
'Alerts': {
'Alert': Array<{
'InventoryDiscrepancyAlert': {
'NoInventoryInd': string,
'refs': string,
'Code': {
'_text': string
},
'Associations': {
'OfferAssociations': {
'Shopper': {
'AllShopperInd': string,
'PassengerReference': Array<{
'_text': string
}>
},
'Flight': {
'AllSegmentInd': {
'_text': string
},
'FlightSegmentReference': Array<{
'ref': string,
'Cabin': {
'refs': string,
'CabinDesignator': {
'AllCabins': string,
'_text': string
},
'MarketingName': {
'_text': string
}
},
'ClassOfService': {
'refs': string,
'Code': {
'SeatsLeft': string,
'_text': string
},
'MarketingName': {
'CabinDesignator': string,
'_text': string
},
'FareBasisCode': {
'_text': string
}
},
'BagDetailAssociation': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'MarriedSegmentGroup': {
'_text': string
}
}>,
'AllOriginDestinationInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'AllFlightInd': {
'_text': string
},
'FlightReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'PriceClass': {
'PriceClassReference': {
'_text': string
}
},
'BagDetails': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'OfferDetails': {
'OfferInstructionReferences': {
'_text': string
},
'OfferPenaltyReferences': {
'_text': string
},
'OfferTermReferences': Array<{
'_text': string
}>
},
'Services': {
'Service': Array<{
'BundleReference': {
'_text': string
},
'ServiceReferences': Array<{
'_text': string
}>
}>
},
'Media': {
'MediaGroupreference': {
'_text': string
},
'MediaItems': Array<{
'MediaItemReference': {
'_text': string
},
'MediaLink': {
'_text': string
}
}>
},
'Other': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
},
'OrderAssociations': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}
},
'AlternateOffers': {
'TotalOfferQuantity': {
'_text': string
},
'Owner': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AlternateOffer': Array<{
'OfferID': string,
'Owner': string,
'OwnerType': string,
'RequestedDateInd': string,
'WebAddressID': string,
'Parameters': {
'ApplyToAllInd': string,
'RedemptionOnlyInd': string,
'TotalItemQuantity': {
'_text': string
},
'PTC_Priced': Array<{
'refs': string,
'Requested': {
'Quantity': string,
'_text': string
},
'Priced': {
'Quantity': string,
'_text': string
}
}>
},
'ValidatingCarrier': {
'_text': string
},
'TimeLimits': {
'refs': string,
'OfferExpiration': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string,
'refs': string,
'ObjectKey': string
},
'Payment': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'OtherLimits': {
'OtherLimit': Array<{
'refs': string,
'ObjectKey': string,
'PriceGuaranteeTimeLimit': {
'refs': string
},
'TicketByTimeLimit': {
'refs': string,
'TicketBy': {
'_text': string
}
}
}>
}
},
'TotalPrice': {
'refs': string,
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
}
},
'DescriptionReferences': Array<{
'_text': string
}>,
'DisclosureRef': {
'_text': string
},
'BagDisclosureRefs': Array<{
'_text': string
}>,
'Penalty': {
'CancelFeeInd': string,
'ChangeFeeInd': string,
'RefundableInd': string,
'ReuseInd': string,
'UpgradeFeeInd': string,
'refs': string,
'ObjectKey': string,
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'Details': {
'Detail': Array<{
'refs': string,
'Type': {
'_text': string
},
'Application': {
'_text': string
},
'Amounts': {
'Amount': Array<{
'CurrencyAmountValue': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
},
'AmountApplication': {
'_text': string
},
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
}
}>
}
},
'Match': {
'refs': string,
'Application': {
'_text': string
},
'MatchResult': {
'_text': string
},
'Percentage': {
'_text': string
}
},
'AltBaggageOffer': {
'TotalPrice': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
}
},
'ValidatingCarrier': {
'_text': string
},
'BagDetails': {
'BagDetail': Array<{
'ValidatingCarrier': {
'_text': string
},
'Associations': {
'AssociatedPassenger': {
'AllPassengerInd': {
'_text': string
},
'PassengerReferences': Array<{
'_text': string
}>
},
'ApplicableFlight': {
'AllSegmentInd': {
'_text': string
},
'FlightSegmentReference': Array<{
'ref': string,
'Cabin': {
'refs': string,
'CabinDesignator': {
'AllCabins': string,
'_text': string
},
'MarketingName': {
'_text': string
}
},
'ClassOfService': {
'refs': string,
'Code': {
'SeatsLeft': string,
'_text': string
},
'MarketingName': {
'CabinDesignator': string,
'_text': string
},
'FareBasisCode': {
'_text': string
}
},
'BagDetailAssociation': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'MarriedSegmentGroup': {
'_text': string
}
}>,
'AllOriginDestinationInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'AllFlightInd': {
'_text': string
},
'FlightReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OfferDetailAssociation': {
'OfferInstructionReferences': {
'_text': string
},
'OfferPenaltyReferences': {
'_text': string
},
'OfferTermReferences': Array<{
'_text': string
}>
},
'BagDetailAssociation': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'AssociatedService': {
'BundleReference': {
'_text': string
},
'ServiceReferences': Array<{
'_text': string
}>
},
'OtherAssociation': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
},
'CheckedBags': {
'CheckedBag': Array<{
'refs': string,
'WeightAllowance': {
'refs': string,
'ApplicableParty': {
'_text': string
},
'MaximumWeight': Array<{
'refs': string,
'Value': {
'_text': string
},
'UOM': {
'_text': string
},
'SpreadOverBagsQuantity': {
'_text': string
}
}>,
'ApplicableBag': {
'_text': string
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
},
'DimensionAllowance': Array<{
'refs': string,
'ApplicableParty': {
'_text': string
},
'DimensionUOM': {
'_text': string
},
'MaxLinear': {
'refs': string,
'_text': string
},
'MinLinear': {
'refs': string,
'_text': string
},
'Dimensions': Array<{
'refs': string,
'Category': {
'_text': string
},
'MaxValue': {
'_text': string
},
'MinValue': {
'_text': string
}
}>,
'ApplicableBag': {
'_text': string
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
}
}>
},
'PieceAllowance': Array<{
'PieceAllowanceCombination': string,
'refs': string,
'ApplicableParty': {
'_text': string
},
'TotalQuantity': {
'_text': string
},
'BagType': {
'_text': string
},
'ApplicableBag': {
'_text': string
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
},
'PieceMeasurements': Array<{
'Quantity': string,
'PieceWeightAllowance': Array<{
'refs': string,
'ApplicableParty': {
'_text': string
},
'MaximumWeight': Array<{
'refs': string,
'Value': {
'_text': string
},
'UOM': {
'_text': string
},
'SpreadOverBagsQuantity': {
'_text': string
}
}>,
'ApplicableBag': {
'_text': string
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
}
}>,
'PieceDimensionAllowance': Array<{
'refs': string,
'ApplicableParty': {
'_text': string
},
'DimensionUOM': {
'_text': string
},
'MaxLinear': {
'refs': string,
'_text': string
},
'MinLinear': {
'refs': string,
'_text': string
},
'Dimensions': Array<{
'refs': string,
'Category': {
'_text': string
},
'MaxValue': {
'_text': string
},
'MinValue': {
'_text': string
}
}>,
'ApplicableBag': {
'_text': string
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
}
}>
}>
}>,
'DimensionAllowance': {
'refs': string,
'ApplicableParty': {
'_text': string
},
'DimensionUOM': {
'_text': string
},
'MaxLinear': {
'refs': string,
'_text': string
},
'MinLinear': {
'refs': string,
'_text': string
},
'Dimensions': Array<{
'refs': string,
'Category': {
'_text': string
},
'MaxValue': {
'_text': string
},
'MinValue': {
'_text': string
}
}>,
'ApplicableBag': {
'_text': string
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
}
},
'AllowanceDescription': {
'refs': string,
'Concept': string,
'ApplicableParty': {
'_text': string
},
'ApplicableBag': {
'_text': string
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
}
},
'BaggageDeterminingCarrier': {
'BDC_Reason': string,
'AirlineID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Name': {
'_text': string
},
'BagRule': {
'_text': string
},
'BDC_AnalysisResult': {
'_text': string
}
},
'BDC_AnalysisResult': {
'_text': string
}
}>
},
'CarryOnBags': {
'CarryOnBag': Array<{
'refs': string,
'WeightAllowance': {
'refs': string,
'ApplicableParty': {
'_text': string
},
'MaximumWeight': Array<{
'refs': string,
'Value': {
'_text': string
},
'UOM': {
'_text': string
},
'SpreadOverBagsQuantity': {
'_text': string
}
}>,
'ApplicableBag': {
'_text': string
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
},
'DimensionAllowance': Array<{
'refs': string,
'ApplicableParty': {
'_text': string
},
'DimensionUOM': {
'_text': string
},
'MaxLinear': {
'refs': string,
'_text': string
},
'MinLinear': {
'refs': string,
'_text': string
},
'Dimensions': Array<{
'refs': string,
'Category': {
'_text': string
},
'MaxValue': {
'_text': string
},
'MinValue': {
'_text': string
}
}>,
'ApplicableBag': {
'_text': string
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
}
}>
},
'PieceAllowance': Array<{
'PieceAllowanceCombination': string,
'refs': string,
'ApplicableParty': {
'_text': string
},
'TotalQuantity': {
'_text': string
},
'BagType': {
'_text': string
},
'ApplicableBag': {
'_text': string
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
},
'PieceMeasurements': Array<{
'Quantity': string,
'PieceWeightAllowance': Array<{
'refs': string,
'ApplicableParty': {
'_text': string
},
'MaximumWeight': Array<{
'refs': string,
'Value': {
'_text': string
},
'UOM': {
'_text': string
},
'SpreadOverBagsQuantity': {
'_text': string
}
}>,
'ApplicableBag': {
'_text': string
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
}
}>,
'PieceDimensionAllowance': Array<{
'refs': string,
'ApplicableParty': {
'_text': string
},
'DimensionUOM': {
'_text': string
},
'MaxLinear': {
'refs': string,
'_text': string
},
'MinLinear': {
'refs': string,
'_text': string
},
'Dimensions': Array<{
'refs': string,
'Category': {
'_text': string
},
'MaxValue': {
'_text': string
},
'MinValue': {
'_text': string
}
}>,
'ApplicableBag': {
'_text': string
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
}
}>
}>
}>,
'DimensionAllowance': {
'refs': string,
'ApplicableParty': {
'_text': string
},
'DimensionUOM': {
'_text': string
},
'MaxLinear': {
'refs': string,
'_text': string
},
'MinLinear': {
'refs': string,
'_text': string
},
'Dimensions': Array<{
'refs': string,
'Category': {
'_text': string
},
'MaxValue': {
'_text': string
},
'MinValue': {
'_text': string
}
}>,
'ApplicableBag': {
'_text': string
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
}
},
'AllowanceDescription': {
'refs': string,
'Concept': string,
'ApplicableParty': {
'_text': string
},
'ApplicableBag': {
'_text': string
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
}
},
'BaggageDeterminingCarrier': {
'BDC_Reason': string,
'AirlineID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Name': {
'_text': string
},
'BagRule': {
'_text': string
},
'BDC_AnalysisResult': {
'_text': string
}
},
'BDC_AnalysisResult': {
'_text': string
}
}>
},
'Disclosure': {
'refs': string,
'BagRule': {
'_text': string
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'Price': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
}
}>
},
'Associations': {
'OfferAssociations': {
'Shopper': {
'AllShopperInd': string,
'PassengerReference': Array<{
'_text': string
}>
},
'Flight': {
'AllSegmentInd': {
'_text': string
},
'FlightSegmentReference': Array<{
'ref': string,
'Cabin': {
'refs': string,
'CabinDesignator': {
'AllCabins': string,
'_text': string
},
'MarketingName': {
'_text': string
}
},
'ClassOfService': {
'refs': string,
'Code': {
'SeatsLeft': string,
'_text': string
},
'MarketingName': {
'CabinDesignator': string,
'_text': string
},
'FareBasisCode': {
'_text': string
}
},
'BagDetailAssociation': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'MarriedSegmentGroup': {
'_text': string
}
}>,
'AllOriginDestinationInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'AllFlightInd': {
'_text': string
},
'FlightReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'PriceClass': {
'PriceClassReference': {
'_text': string
}
},
'BagDetails': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'OfferDetails': {
'OfferInstructionReferences': {
'_text': string
},
'OfferPenaltyReferences': {
'_text': string
},
'OfferTermReferences': Array<{
'_text': string
}>
},
'Services': {
'Service': Array<{
'BundleReference': {
'_text': string
},
'ServiceReferences': Array<{
'_text': string
}>
}>
},
'Media': {
'MediaGroupreference': {
'_text': string
},
'MediaItems': Array<{
'MediaItemReference': {
'_text': string
},
'MediaLink': {
'_text': string
}
}>
},
'Other': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
},
'OrderAssociations': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}
}
},
'AltOtherOffer': {
'OfferPrice': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Associations': {
'OfferAssociations': {
'Shopper': {
'AllShopperInd': string,
'PassengerReference': Array<{
'_text': string
}>
},
'Flight': {
'AllSegmentInd': {
'_text': string
},
'FlightSegmentReference': Array<{
'ref': string,
'Cabin': {
'refs': string,
'CabinDesignator': {
'AllCabins': string,
'_text': string
},
'MarketingName': {
'_text': string
}
},
'ClassOfService': {
'refs': string,
'Code': {
'SeatsLeft': string,
'_text': string
},
'MarketingName': {
'CabinDesignator': string,
'_text': string
},
'FareBasisCode': {
'_text': string
}
},
'BagDetailAssociation': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'MarriedSegmentGroup': {
'_text': string
}
}>,
'AllOriginDestinationInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'AllFlightInd': {
'_text': string
},
'FlightReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'PriceClass': {
'PriceClassReference': {
'_text': string
}
},
'BagDetails': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'OfferDetails': {
'OfferInstructionReferences': {
'_text': string
},
'OfferPenaltyReferences': {
'_text': string
},
'OfferTermReferences': Array<{
'_text': string
}>
},
'Services': {
'Service': Array<{
'BundleReference': {
'_text': string
},
'ServiceReferences': Array<{
'_text': string
}>
}>
},
'Media': {
'MediaGroupreference': {
'_text': string
},
'MediaItems': Array<{
'MediaItemReference': {
'_text': string
},
'MediaLink': {
'_text': string
}
}>
},
'Other': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
},
'OrderAssociations': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}
}
},
'AltPricedFlightOffer': {
'LeadPricedInd': string,
'LeadPricingIncInd': string,
'refs': string,
'OfferPrice': {
'OfferItemID': string,
'ModificationProhibitedInd': string,
'refs': string,
'RequestedDate': {
'PriceDetail': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Discount': {
'refs': string,
'DiscountAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'Application': {
'_text': string
},
'Description': {
'_text': string
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'OriginDestinationReference': {
'_text': string
},
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>,
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'Associations': Array<{
'AssociatedPassenger': {
'AllPassengerInd': {
'_text': string
},
'PassengerReferences': Array<{
'_text': string
}>
},
'ApplicableFlight': {
'AllSegmentInd': {
'_text': string
},
'FlightSegmentReference': Array<{
'ref': string,
'Cabin': {
'refs': string,
'CabinDesignator': {
'AllCabins': string,
'_text': string
},
'MarketingName': {
'_text': string
}
},
'ClassOfService': {
'refs': string,
'Code': {
'SeatsLeft': string,
'_text': string
},
'MarketingName': {
'CabinDesignator': string,
'_text': string
},
'FareBasisCode': {
'_text': string
}
},
'BagDetailAssociation': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'MarriedSegmentGroup': {
'_text': string
}
}>,
'AllOriginDestinationInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'AllFlightInd': {
'_text': string
},
'FlightReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'PriceClass': {
'PriceClassReference': {
'_text': string
}
},
'OfferDetailAssociation': {
'OfferInstructionReferences': {
'_text': string
},
'OfferPenaltyReferences': {
'_text': string
},
'OfferTermReferences': Array<{
'_text': string
}>
},
'IncludedService': {
'BundleReference': {
'_text': string
},
'ServiceReferences': Array<{
'_text': string
}>
},
'AssociatedService': {
'BundleReference': {
'_text': string
},
'ServiceReferences': Array<{
'_text': string
}>
},
'OtherAssociation': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}>
},
'FareDetail': {
'FareIndicatorCode': {
'FiledFareInd': string,
'_text': string
},
'PassengerRefs': Array<{
'_text': string
}>,
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareComponent': Array<{
'Parameters': {
'Quantity': string
},
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareBasis': {
'FareBasisCode': {
'refs': string,
'Code': {
'_text': string
},
'Application': {
'_text': string
}
},
'FareRulesRemarks': {
'FareRulesRemark': Array<{
'refs': string,
'Category': {
'_text': string
},
'Text': {
'_text': string
}
}>
},
'FareBasisCityPair': {
'_text': string
},
'RBD': {
'_text': string
},
'CabinType': {
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
}
},
'TicketDesig': {
'Application': string,
'_text': string
},
'FareRules': {
'refs': string,
'Penalty': {
'CancelFeeInd': string,
'ChangeFeeInd': string,
'RefundableInd': string,
'ReuseInd': string,
'UpgradeFeeInd': string,
'refs': string,
'ObjectKey': string,
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'Details': {
'Detail': Array<{
'refs': string,
'Type': {
'_text': string
},
'Application': {
'_text': string
},
'Amounts': {
'Amount': Array<{
'CurrencyAmountValue': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
},
'AmountApplication': {
'_text': string
},
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
}
}>
}
},
'CorporateFare': {
'refs': string,
'ObjectKey': string,
'PricedInd': string,
'CompanyIndex': {
'refs': string,
'_text': string
},
'Contract': {
'refs': string,
'_text': string
},
'Account': {
'refs': string,
'_text': string
}
},
'AdvanceStay': {
'AdvancePurchase': {
'Context': string,
'_text': string
},
'AdvanceTicketing': {
'AdvanceReservation': {
'Context': string,
'_text': string
},
'AdvanceDeparture': {
'Context': string,
'_text': string
}
},
'MinimumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
},
'MaximumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
}
},
'Ticketing': {
'TicketlessInd': {
'_text': string
},
'InstantPurchase': {
'_text': string
},
'Endorsements': {
'Endorsement': Array<{
'_text': string
}>
}
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'PriceClassRef': {
'_text': string
},
'SegmentRefs': Array<{
'ON_Point': string,
'OFF_Point': string,
'_text': string
}>
}>,
'FlightMileage': {
'Value': {
'_text': string
},
'Application': {
'_text': string
}
},
'TourCode': {
'_text': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}
},
'FareDetail': {
'FareIndicatorCode': {
'FiledFareInd': string,
'_text': string
},
'PassengerRefs': Array<{
'_text': string
}>,
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareComponent': Array<{
'Parameters': {
'Quantity': string
},
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareBasis': {
'FareBasisCode': {
'refs': string,
'Code': {
'_text': string
},
'Application': {
'_text': string
}
},
'FareRulesRemarks': {
'FareRulesRemark': Array<{
'refs': string,
'Category': {
'_text': string
},
'Text': {
'_text': string
}
}>
},
'FareBasisCityPair': {
'_text': string
},
'RBD': {
'_text': string
},
'CabinType': {
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
}
},
'TicketDesig': {
'Application': string,
'_text': string
},
'FareRules': {
'refs': string,
'Penalty': {
'CancelFeeInd': string,
'ChangeFeeInd': string,
'RefundableInd': string,
'ReuseInd': string,
'UpgradeFeeInd': string,
'refs': string,
'ObjectKey': string,
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'Details': {
'Detail': Array<{
'refs': string,
'Type': {
'_text': string
},
'Application': {
'_text': string
},
'Amounts': {
'Amount': Array<{
'CurrencyAmountValue': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
},
'AmountApplication': {
'_text': string
},
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
}
}>
}
},
'CorporateFare': {
'refs': string,
'ObjectKey': string,
'PricedInd': string,
'CompanyIndex': {
'refs': string,
'_text': string
},
'Contract': {
'refs': string,
'_text': string
},
'Account': {
'refs': string,
'_text': string
}
},
'AdvanceStay': {
'AdvancePurchase': {
'Context': string,
'_text': string
},
'AdvanceTicketing': {
'AdvanceReservation': {
'Context': string,
'_text': string
},
'AdvanceDeparture': {
'Context': string,
'_text': string
}
},
'MinimumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
},
'MaximumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
}
},
'Ticketing': {
'TicketlessInd': {
'_text': string
},
'InstantPurchase': {
'_text': string
},
'Endorsements': {
'Endorsement': Array<{
'_text': string
}>
}
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'PriceClassRef': {
'_text': string
},
'SegmentRefs': Array<{
'ON_Point': string,
'OFF_Point': string,
'_text': string
}>
}>,
'FlightMileage': {
'Value': {
'_text': string
},
'Application': {
'_text': string
}
},
'TourCode': {
'_text': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'Associations': {
'OfferAssociations': {
'Shopper': {
'AllShopperInd': string,
'PassengerReference': Array<{
'_text': string
}>
},
'Flight': {
'AllSegmentInd': {
'_text': string
},
'FlightSegmentReference': Array<{
'ref': string,
'Cabin': {
'refs': string,
'CabinDesignator': {
'AllCabins': string,
'_text': string
},
'MarketingName': {
'_text': string
}
},
'ClassOfService': {
'refs': string,
'Code': {
'SeatsLeft': string,
'_text': string
},
'MarketingName': {
'CabinDesignator': string,
'_text': string
},
'FareBasisCode': {
'_text': string
}
},
'BagDetailAssociation': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'MarriedSegmentGroup': {
'_text': string
}
}>,
'AllOriginDestinationInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'AllFlightInd': {
'_text': string
},
'FlightReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'PriceClass': {
'PriceClassReference': {
'_text': string
}
},
'BagDetails': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'OfferDetails': {
'OfferInstructionReferences': {
'_text': string
},
'OfferPenaltyReferences': {
'_text': string
},
'OfferTermReferences': Array<{
'_text': string
}>
},
'Services': {
'Service': Array<{
'BundleReference': {
'_text': string
},
'ServiceReferences': Array<{
'_text': string
}>
}>
},
'Media': {
'MediaGroupreference': {
'_text': string
},
'MediaItems': Array<{
'MediaItemReference': {
'_text': string
},
'MediaLink': {
'_text': string
}
}>
},
'Other': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
},
'OrderAssociations': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}
}
},
'AltSeatOffer': {
'refs': string,
'Location': {
'Column': {
'_text': string
},
'Row': {
'refs': string,
'Number': {
'_text': string
},
'Type': {
'_text': string
},
'SeatData': {
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
},
'UOM': {
'_text': string
},
'SeatPitchLow': {
'_text': string
},
'SeatWidthLow': {
'_text': string
},
'Keywords': {
'KeyWord': Array<{
'refs': string,
'ObjectKey': string,
'Word': {
'_text': string
},
'Value': Array<{
'_text': string
}>
}>
},
'Marketing': {
'refs': string,
'Images': Array<{
'ImageID': {
'_text': string
},
'Position': {
'Row': {
'Position': {
'_text': string
},
'Orientation': {
'_text': string
}
},
'Column': {
'Position': {
'_text': string
},
'Orientation': {
'_text': string
}
}
}
}>,
'Links': Array<{
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Position': {
'Row': {
'Position': {
'_text': string
},
'Orientation': {
'_text': string
}
},
'Column': {
'Position': {
'_text': string
},
'Orientation': {
'_text': string
}
}
}
}>,
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}
}
},
'Characteristics': {
'Characteristic': Array<{
'refs': string,
'Code': {
'_text': string
},
'Definition': {
'_text': string
},
'TableName': {
'_text': string
},
'Link': {
'_text': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
},
'Associations': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}
},
'Details': {

},
'OfferPrice': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Associations': {
'OfferAssociations': {
'Shopper': {
'AllShopperInd': string,
'PassengerReference': Array<{
'_text': string
}>
},
'Flight': {
'AllSegmentInd': {
'_text': string
},
'FlightSegmentReference': Array<{
'ref': string,
'Cabin': {
'refs': string,
'CabinDesignator': {
'AllCabins': string,
'_text': string
},
'MarketingName': {
'_text': string
}
},
'ClassOfService': {
'refs': string,
'Code': {
'SeatsLeft': string,
'_text': string
},
'MarketingName': {
'CabinDesignator': string,
'_text': string
},
'FareBasisCode': {
'_text': string
}
},
'BagDetailAssociation': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'MarriedSegmentGroup': {
'_text': string
}
}>,
'AllOriginDestinationInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'AllFlightInd': {
'_text': string
},
'FlightReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'PriceClass': {
'PriceClassReference': {
'_text': string
}
},
'BagDetails': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'OfferDetails': {
'OfferInstructionReferences': {
'_text': string
},
'OfferPenaltyReferences': {
'_text': string
},
'OfferTermReferences': Array<{
'_text': string
}>
},
'Services': {
'Service': Array<{
'BundleReference': {
'_text': string
},
'ServiceReferences': Array<{
'_text': string
}>
}>
},
'Media': {
'MediaGroupreference': {
'_text': string
},
'MediaItems': Array<{
'MediaItemReference': {
'_text': string
},
'MediaLink': {
'_text': string
}
}>
},
'Other': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
},
'OrderAssociations': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}
}
}
}>
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'PIN_AuthenticationAlert': {
'refs': string,
'trxItemKey': string,
'AuthResponseInd': string,
'MaximumTryInd': string,
'NotProcessedInd': string,
'RetryInd': string,
'AuthRequestInd': string,
'AuthRequest': {
'AuthenticationInd': string,
'MaximumTrxInd': string,
'RetryInd': string,
'Status': {
'Context': string,
'_text': string
},
'Challenge': {
'SourceURL': {
'_text': string
},
'Parameters': {
'ChallengeQuestion': {
'_text': string
},
'PhrasePrompt': {
'_text': string
},
'Positions': {
'Position': Array<{
'_text': string
}>
}
}
},
'Associations': {
'OfferAssociations': {
'Shopper': {
'AllShopperInd': string,
'PassengerReference': Array<{
'_text': string
}>
},
'Flight': {
'AllSegmentInd': {
'_text': string
},
'FlightSegmentReference': Array<{
'ref': string,
'Cabin': {
'refs': string,
'CabinDesignator': {
'AllCabins': string,
'_text': string
},
'MarketingName': {
'_text': string
}
},
'ClassOfService': {
'refs': string,
'Code': {
'SeatsLeft': string,
'_text': string
},
'MarketingName': {
'CabinDesignator': string,
'_text': string
},
'FareBasisCode': {
'_text': string
}
},
'BagDetailAssociation': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'MarriedSegmentGroup': {
'_text': string
}
}>,
'AllOriginDestinationInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'AllFlightInd': {
'_text': string
},
'FlightReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'PriceClass': {
'PriceClassReference': {
'_text': string
}
},
'BagDetails': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'OfferDetails': {
'OfferInstructionReferences': {
'_text': string
},
'OfferPenaltyReferences': {
'_text': string
},
'OfferTermReferences': Array<{
'_text': string
}>
},
'Services': {
'Service': Array<{
'BundleReference': {
'_text': string
},
'ServiceReferences': Array<{
'_text': string
}>
}>
},
'Media': {
'MediaGroupreference': {
'_text': string
},
'MediaItems': Array<{
'MediaItemReference': {
'_text': string
},
'MediaLink': {
'_text': string
}
}>
},
'Other': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
},
'OrderAssociations': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}
},
'AuthAccount': {
'AccountID': {
'_text': string
},
'AccountName': {
'_text': string
},
'FirstName': {
'_text': string
},
'LastName': {
'_text': string
}
},
'Device': {
'Type': {
'refs': string,
'Code': {
'_text': string
},
'Definition': {
'_text': string
},
'TableName': {
'_text': string
},
'Link': {
'_text': string
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
},
'PhoneNumber': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'IP_Address': {
'_text': string
},
'MAC_Address': {
'_text': string
},
'Name': {
'_text': string
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
},
'OriginalTransactionID': {
'source': string,
'_text': string
}
},
'AuthResponse': {
'Status': {
'Context': string,
'_text': string
},
'PIN': {
'_text': string
},
'PoolQuestion': {
'_text': string
},
'AuthAccount': {
'AccountID': {
'_text': string
},
'AccountName': {
'_text': string
},
'FirstName': {
'_text': string
},
'LastName': {
'_text': string
}
},
'Device': {
'Type': {
'refs': string,
'Code': {
'_text': string
},
'Definition': {
'_text': string
},
'TableName': {
'_text': string
},
'Link': {
'_text': string
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
},
'PhoneNumber': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'IP_Address': {
'_text': string
},
'MAC_Address': {
'_text': string
},
'Name': {
'_text': string
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
},
'OriginalTransactionID': {
'source': string,
'_text': string
}
}
},
'SecurePaymentAlert': {
'refs': string,
'trxItemKey': string,
'AuthResponseInd': string,
'MaximumTryInd': string,
'NotProcessedInd': string,
'RetryInd': string,
'AuthRequestInd': string,
'PayerAuth': {
'PAReq': {
'_text': string
},
'PARes': {
'_text': string
}
},
'TransactionType': {
'Context': string,
'_text': string
},
'EnrollmentStatus': {
'Context': string,
'_text': string
},
'Airline': {
'ID': {
'Context': string,
'_text': string
},
'Name': {
'Context': string,
'_text': string
},
'CountryCode': {
'_text': string
}
},
'Reference': {
'ACS_TxnReference': {
'Context': string,
'_text': string
},
'SPM_TxnReference': {
'Context': string,
'_text': string
},
'OriginalTransactionID': {
'source': string,
'_text': string
},
'TrxTimestamp': {
'_text': string
},
'TxnDescription': {
'_text': string
},
'TxnDatas': {
'TxnDate': Array<{
'_text': string
}>
}
},
'URLs': {
'ACS_URL': {
'_text': string
},
'FailURL': {
'_text': string
},
'MerchantURL': {
'_text': string
},
'TermURL': {
'_text': string
}
},
'Details': {
'TrxTimestamp': {
'_text': string
},
'ClientType': {
'Context': string,
'_text': string
},
'CustomerDevice': {
'Type': {
'refs': string,
'Code': {
'_text': string
},
'Definition': {
'_text': string
},
'TableName': {
'_text': string
},
'Link': {
'_text': string
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
},
'PhoneNumber': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'IP_Address': {
'_text': string
},
'MAC_Address': {
'_text': string
},
'Name': {
'_text': string
},
'BowserAcceptHeader': {
'_text': string
},
'BrowserUserAgentHeader': {
'_text': string
},
'DeviceCategoryCode': {
'_text': string
}
},
'Currency': {
'InputCurrCode': {
'_text': string
},
'SettlementCurrCode': {
'_text': string
}
},
'ReservationInfo': {
'refs': string,
'Carriers': {
'Carrier': Array<{
'refs': string,
'ObjectMetaReferences': string,
'Application': string,
'_text': string
}>
},
'DateTimes': {
'DateTime': Array<{
'Date': string,
'Time': string,
'Application': string
}>
},
'ClassesOfService': {
'ClassOfService': Array<{
'refs': string,
'Code': {
'SeatsLeft': string,
'_text': string
},
'MarketingName': {
'CabinDesignator': string,
'_text': string
},
'FareBasisCode': {
'_text': string
}
}>
},
'StopLocations': {
'StopLocation': Array<{
'AirportCode': {
'refs': string,
'_text': string
}
}>
},
'FareBasisCodes': {
'FareBasisCode': Array<{
'refs': string,
'Code': {
'_text': string
},
'Application': {
'_text': string
}
}>
},
'FlightNumbers': {
'FlightNumber': Array<{
'OperationalSuffix': string,
'_text': string
}>
},
'PassengerName': {
'_text': string
},
'ResidenceCode': {
'_text': string
},
'PassengerTktNbr': {
'_text': string
},
'AgencyInfo': {
'_text': string
}
},
'TxnDatas': {
'TxnDate': Array<{
'_text': string
}>
}
},
'ProcessingInfos': {
'ProcessingInfo': Array<{
'AddrVerification': {
'Code': string,
'Text': string,
'InvalidInd': string,
'NoMatchInd': string
},
'CAVV': {
'_text': string
},
'CustomerAuthStatus': {
'Context': string,
'_text': string
},
'ECI': {
'_text': string
}
}>
}
}
}>
},
'Notices': {
'Notice': Array<{
'TaxExemptionNotice': {
'refs': string,
'trxItemKey': string,
'AuthResponseInd': string,
'MaximumTryInd': string,
'NotProcessedInd': string,
'RetryInd': string,
'AuthRequestInd': string,
'Query': {
'ExemptAllInd': string,
'Named': {
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'TaxCodes': {
'TaxCode': Array<{
'Designator': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
},
'Rules': {
'refs': string,
'RuleSetID': {
'Name': string,
'_text': string
},
'RuleValid': {
'YearPeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'YearMonthPeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'TimePeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'QuarterPeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'MonthPeriod': {
'Effective': {
'Name': string,
'_text': string
},
'Expiration': {
'Name': string,
'_text': string
}
},
'DayPeriod': {
'Effective': {
'Name': string,
'_text': string
},
'Expiration': {
'Name': string,
'_text': string
}
},
'DateTimePeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'DatePeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
}
},
'Owner': {
'_text': string
},
'Rule': Array<{
'refs': string,
'ObjectKey': string,
'RuleID': {
'_text': string
},
'Value': {
'Text': {
'_text': string
},
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
},
'Name': {
'_text': string
},
'Owner': {
'_text': string
},
'EffectivePeriod': {
'YearPeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'YearMonthPeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'TimePeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'QuarterPeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'MonthPeriod': {
'Effective': {
'Name': string,
'_text': string
},
'Expiration': {
'Name': string,
'_text': string
}
},
'DayPeriod': {
'Effective': {
'Name': string,
'_text': string
},
'Expiration': {
'Name': string,
'_text': string
}
},
'DateTimePeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'DatePeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
}
}
}>
}
},
'Results': {
'ExemptionAppliedInd': string,
'TaxExemption': Array<{
'RuleID': {
'Context': string,
'Name': string,
'_text': string
},
'Associations': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}
}>
}
},
'ServiceFilterNotice': {
'refs': string,
'trxItemKey': string,
'AuthResponseInd': string,
'MaximumTryInd': string,
'NotProcessedInd': string,
'RetryInd': string,
'AuthRequestInd': string,
'Query': {
'ServiceFilter': Array<{
'refs': string,
'GroupCode': {
'_text': string
},
'SubGroupCode': Array<{
'_text': string
}>
}>
},
'Results': {
'SrvcFilterAppliedInd': string,
'AppliedFilters': {
'AppliedFilter': Array<{
'refs': string,
'GroupCode': {
'_text': string
},
'SubGroupCode': Array<{
'_text': string
}>,
'Associations': {
'Offer': {
'Shopper': {
'AllShopperInd': string,
'PassengerReference': Array<{
'_text': string
}>
},
'Flight': {
'AllSegmentInd': {
'_text': string
},
'FlightSegmentReference': Array<{
'ref': string,
'Cabin': {
'refs': string,
'CabinDesignator': {
'AllCabins': string,
'_text': string
},
'MarketingName': {
'_text': string
}
},
'ClassOfService': {
'refs': string,
'Code': {
'SeatsLeft': string,
'_text': string
},
'MarketingName': {
'CabinDesignator': string,
'_text': string
},
'FareBasisCode': {
'_text': string
}
},
'BagDetailAssociation': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'MarriedSegmentGroup': {
'_text': string
}
}>,
'AllOriginDestinationInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'AllFlightInd': {
'_text': string
},
'FlightReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'PriceClass': {
'PriceClassReference': {
'_text': string
}
},
'BagDetails': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'OfferDetails': {
'OfferInstructionReferences': {
'_text': string
},
'OfferPenaltyReferences': {
'_text': string
},
'OfferTermReferences': Array<{
'_text': string
}>
},
'OtherAssociation': Array<{
'Type': string,
'RefValue': string
}>
},
'Order': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}
}
}>
}
}
},
'PricingParametersNotice': {
'refs': string,
'trxItemKey': string,
'AuthResponseInd': string,
'MaximumTryInd': string,
'NotProcessedInd': string,
'RetryInd': string,
'AuthRequestInd': string,
'Query': {
'AutoExchangeReqInd': string,
'IncludeAwardReqInd': string,
'AwardOnlyReqInd': string,
'SimpleReqInd': string
},
'Results': {
'AutoExchangeInd': string,
'AwardIncludedInd': string,
'AwardOnlyInd': string,
'SimpleInd': string
}
},
'PriceVarianceNotice': {
'refs': string,
'trxItemKey': string,
'AuthResponseInd': string,
'MaximumTryInd': string,
'NotProcessedInd': string,
'RetryInd': string,
'AuthRequestInd': string,
'Query': {
'VarianceRuleInd': string,
'PriceVarianceRule': Array<{
'SequenceNbr': {
'_text': string
},
'RuleID': {
'Context': string,
'Name': string,
'_text': string
},
'AcceptableVariance': {
'CurrencyAmountValue': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
}
},
'Name': {
'_text': string
},
'Owner': {
'_text': string
},
'RuleValid': {
'DatePeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'DateTimePeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'DayPeriod': {
'Effective': {
'Name': string,
'_text': string
},
'Expiration': {
'Name': string,
'_text': string
}
},
'MonthPeriod': {
'Effective': {
'Name': string,
'_text': string
},
'Expiration': {
'Name': string,
'_text': string
}
},
'QuarterPeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'TimePeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'YearMonthPeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'YearPeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
}
},
'Currencies': {
'InputCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>,
'SettlementCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Associations': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
},
'Results': {
'VarianceAppliedInd': string,
'PriceVariance': Array<{
'RuleID': {
'Context': string,
'Name': string,
'_text': string
},
'Amount': {
'CurrencyAmountValue': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
}
},
'Associations': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}
}>
}
},
'PaymentRulesNotice': {
'refs': string,
'trxItemKey': string,
'AuthResponseInd': string,
'MaximumTryInd': string,
'NotProcessedInd': string,
'RetryInd': string,
'AuthRequestInd': string,
'FormOfPayment': {
'Query': {
'ProceedOnFailureInd': string
},
'Results': {
'PaymentFailureInd': string,
'Code': {
'_text': string
},
'Associations': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}
}
}
},
'InventoryGuaranteeNotif': {
'refs': string,
'Query': {
'GuaranteeRequestInd': string
},
'Results': {
'NoGuaranteeInd': string,
'InventoryGuarantee': {
'InvGuaranteeID': {
'_text': string
},
'InventoryGuaranteeTimeLimits': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'Associations': {
'OfferAssociations': {
'Shopper': {
'AllShopperInd': string,
'PassengerReference': Array<{
'_text': string
}>
},
'Flight': {
'AllSegmentInd': {
'_text': string
},
'FlightSegmentReference': Array<{
'ref': string,
'Cabin': {
'refs': string,
'CabinDesignator': {
'AllCabins': string,
'_text': string
},
'MarketingName': {
'_text': string
}
},
'ClassOfService': {
'refs': string,
'Code': {
'SeatsLeft': string,
'_text': string
},
'MarketingName': {
'CabinDesignator': string,
'_text': string
},
'FareBasisCode': {
'_text': string
}
},
'BagDetailAssociation': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'MarriedSegmentGroup': {
'_text': string
}
}>,
'AllOriginDestinationInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'AllFlightInd': {
'_text': string
},
'FlightReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'PriceClass': {
'PriceClassReference': {
'_text': string
}
},
'BagDetails': {
'BagDisclosureReferences': Array<{
'_text': string
}>,
'CheckedBagReferences': Array<{
'_text': string
}>,
'CarryOnReferences': Array<{
'_text': string
}>
},
'OfferDetails': {
'OfferInstructionReferences': {
'_text': string
},
'OfferPenaltyReferences': {
'_text': string
},
'OfferTermReferences': Array<{
'_text': string
}>
},
'Services': {
'Service': Array<{
'BundleReference': {
'_text': string
},
'ServiceReferences': Array<{
'_text': string
}>
}>
},
'Media': {
'MediaGroupreference': {
'_text': string
},
'MediaItems': Array<{
'MediaItemReference': {
'_text': string
},
'MediaLink': {
'_text': string
}
}>
},
'Other': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
},
'OrderAssociations': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}
}
}
}
}
}>
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'PaxGroup': {
'PaxGroupID': {
'_text': string
},
'IntendedPaxQty': {
'_text': string
},
'Name': {
'_text': string
}
},
'Payments': Array<{
'PaymentInfo': Array<{
'PaymentMethod': {
'TypeCode': {
'_text': string
},
'BankAccount': {
'AccountID': {
'_text': string
},
'AccountType': {
'_text': string
},
'BankID': {
'_text': string
},
'OwnerName': {
'_text': string
}
},
'Cash': {
'ReceiptID': {
'_text': string
},
'TerminalID': {
'_text': string
},
'CollectionAddress': {
'LabelText': {
'_text': string
},
'StreetText': Array<{
'_text': string
}>,
'BuildingRoomText': {
'_text': string
},
'PO_BoxCode': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CityName': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
},
'CountryName': {
'_text': string
},
'CountryCode': {
'_text': string
}
}
},
'Check': {
'CheckNumber': {
'_text': string
},
'PayeeName': {
'_text': string
},
'SignedDate': {
'_text': string
}
},
'DirectBill': {
'OrgID': {
'_text': string
},
'OrgName': {
'_text': string
},
'ContactInfo': {
'ContactInfoID': {
'_text': string
},
'ContactTypeText': {
'_text': string
},
'IndividualRef': {
'_text': string
},
'ContactRefusedInd': {
'_text': string
},
'Phone': Array<{
'LabelText': {
'_text': string
},
'CountryDialingCode': {
'_text': string
},
'AreaCodeNumber': {
'_text': string
},
'PhoneNumber': {
'_text': string
},
'ExtensionNumber': {
'_text': string
}
}>,
'OtherAddress': Array<{
'LabelText': {
'_text': string
},
'OtherAddressText': {
'_text': string
}
}>,
'EmailAddress': Array<{
'LabelText': {
'_text': string
},
'EmailAddressText': {
'_text': string
}
}>,
'Individual': {
'IndividualID': {
'_text': string
},
'Birthdate': {
'_text': string
},
'BirthplaceText': {
'_text': string
},
'GenderCode': {
'_text': string
},
'TitleName': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
},
'SuffixName': {
'_text': string
}
},
'PostalAddress': Array<{
'LabelText': {
'_text': string
},
'StreetText': Array<{
'_text': string
}>,
'BuildingRoomText': {
'_text': string
},
'PO_BoxCode': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CityName': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
},
'CountryName': {
'_text': string
},
'CountryCode': {
'_text': string
}
}>
}
},
'LoyaltyRedemption': {
'AccountNumber': {
'_text': string
},
'CertificateNumber': Array<{
'_text': string
}>,
'LoyaltyCurAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyProgramAccount': {
'ProgramName': {
'_text': string
},
'ProgramCode': {
'_text': string
},
'AccountNumber': {
'_text': string
},
'URL': {
'_text': string
},
'SignInID': {
'_text': string
},
'TierCode': {
'_text': string
},
'TierName': {
'_text': string
},
'TierPriorityText': {
'_text': string
},
'ProviderName': {
'_text': string
},
'Alliance': {
'AllianceCode': {
'_text': string
},
'Name': {
'_text': string
},
'URL': {
'_text': string
}
},
'Carrier': {
'AirlineDesigCode': {
'_text': string
},
'DuplicateDesigInd': {
'_text': string
},
'Name': {
'_text': string
},
'Alliance': {
'AllianceCode': {
'_text': string
},
'Name': {
'_text': string
},
'URL': {
'_text': string
}
}
}
}
},
'MiscChargeOrder': {
'TicketID': {
'_text': string
}
},
'OtherPaymentMethod': {
'Remark': Array<{
'RemarkText': {
'_text': string
},
'DisplayInd': {
'_text': string
},
'CreationDateTime': {
'TimeZoneCode': string,
'_text': string
}
}>
},
'PaymentCard': {
'CardType': {
'_text': string
},
'CreditCardVendorCode': {
'_text': string
},
'CardNumber': {
'_text': string
},
'CardHolderName': {
'_text': string
},
'CardIssuerName': {
'_text': string
},
'MaskedCardNumber': {
'_text': string
},
'TokenizedCardNumber': {
'_text': string
},
'ApprovalCode': {
'_text': string
},
'EffectiveDate': {
'_text': string
},
'ExpirationDate': {
'_text': string
},
'ReconciliationID': {
'_text': string
},
'ContactInfoRefID': {
'_text': string
}
},
'Voucher': {
'VoucherID': {
'_text': string
},
'EffectiveDate': {
'_text': string
},
'ExpirationDate': {
'_text': string
},
'RemainingAmount': {
'CurCode': string,
'_text': string
}
}
},
'Promotion': Array<{
'PromotionID': {
'_text': string
},
'OwnerName': {
'_text': string
},
'URL': {
'_text': string
},
'Remark': Array<{
'RemarkText': {
'_text': string
},
'DisplayInd': {
'_text': string
},
'CreationDateTime': {
'TimeZoneCode': string,
'_text': string
}
}>,
'PromotionIssuer': {
'Carrier': {
'AirlineDesigCode': {
'_text': string
},
'DuplicateDesigInd': {
'_text': string
},
'Name': {
'_text': string
},
'Alliance': {
'AllianceCode': {
'_text': string
},
'Name': {
'_text': string
},
'URL': {
'_text': string
}
}
},
'Org': {
'OrgID': {
'_text': string
},
'Name': {
'_text': string
}
},
'TravelAgency': {
'AgencyID': {
'_text': string
},
'IATA_Number': {
'_text': string
},
'Name': {
'_text': string
},
'PseudoCityID': {
'_text': string
},
'TypeCode': {
'_text': string
},
'TravelAgent': {
'TravelAgentID': {
'_text': string
},
'TypeCode': {
'_text': string
}
}
}
}
}>
}>
}>,
'ReshopResults': {
'NoPriceChangeInd': {
'_text': string
},
'RepricedOffer': {
'OfferRefID': {
'_text': string
},
'OwnerTypeCode': {
'_text': string
},
'RequestedDateInd': {
'_text': string
},
'OwnerCode': {
'_text': string
},
'RepricedOfferItem': Array<{
'OfferItemRefID': {
'_text': string
},
'FareDetail': Array<{
'FareIndicatorCode': {
'FiledFareInd': string,
'_text': string
},
'PassengerRefs': Array<{
'_text': string
}>,
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareComponent': Array<{
'Parameters': {
'Quantity': string
},
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareBasis': {
'FareBasisCode': {
'refs': string,
'Code': {
'_text': string
},
'Application': {
'_text': string
}
},
'FareRulesRemarks': {
'FareRulesRemark': Array<{
'refs': string,
'Category': {
'_text': string
},
'Text': {
'_text': string
}
}>
},
'FareBasisCityPair': {
'_text': string
},
'RBD': {
'_text': string
},
'CabinType': {
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
}
},
'TicketDesig': {
'Application': string,
'_text': string
},
'FareRules': {
'refs': string,
'Penalty': {
'CancelFeeInd': string,
'ChangeFeeInd': string,
'RefundableInd': string,
'ReuseInd': string,
'UpgradeFeeInd': string,
'refs': string,
'ObjectKey': string,
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'Details': {
'Detail': Array<{
'refs': string,
'Type': {
'_text': string
},
'Application': {
'_text': string
},
'Amounts': {
'Amount': Array<{
'CurrencyAmountValue': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
},
'AmountApplication': {
'_text': string
},
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
}
}>
}
},
'CorporateFare': {
'refs': string,
'ObjectKey': string,
'PricedInd': string,
'CompanyIndex': {
'refs': string,
'_text': string
},
'Contract': {
'refs': string,
'_text': string
},
'Account': {
'refs': string,
'_text': string
}
},
'AdvanceStay': {
'AdvancePurchase': {
'Context': string,
'_text': string
},
'AdvanceTicketing': {
'AdvanceReservation': {
'Context': string,
'_text': string
},
'AdvanceDeparture': {
'Context': string,
'_text': string
}
},
'MinimumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
},
'MaximumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
}
},
'Ticketing': {
'TicketlessInd': {
'_text': string
},
'InstantPurchase': {
'_text': string
},
'Endorsements': {
'Endorsement': Array<{
'_text': string
}>
}
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'PriceClassRef': {
'_text': string
},
'SegmentRefs': Array<{
'ON_Point': string,
'OFF_Point': string,
'_text': string
}>
}>,
'FlightMileage': {
'Value': {
'_text': string
},
'Application': {
'_text': string
}
},
'TourCode': {
'_text': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>,
'TotalPrice': {
'TotalAmount': {
'CurCode': string,
'_text': string
},
'BaseAmount': {
'CurCode': string,
'_text': string
},
'EquivAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitName': {
'_text': string
},
'Discount': {
'DiscountAmount': {
'CurCode': string,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'PreDiscountedAmount': {
'CurCode': string,
'_text': string
},
'AppText': {
'_text': string
},
'DescText': {
'_text': string
},
'DiscountContext': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
},
'Fee': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>,
'TaxSummary': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>,
'Surcharge': Array<{
'TotalAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'Breakdown': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>
}>
},
'OriginalOrderItem': {
'OrderItemRefID': {
'_text': string
},
'FareDetail': Array<{
'FareIndicatorCode': {
'FiledFareInd': string,
'_text': string
},
'PassengerRefs': Array<{
'_text': string
}>,
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareComponent': Array<{
'Parameters': {
'Quantity': string
},
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareBasis': {
'FareBasisCode': {
'refs': string,
'Code': {
'_text': string
},
'Application': {
'_text': string
}
},
'FareRulesRemarks': {
'FareRulesRemark': Array<{
'refs': string,
'Category': {
'_text': string
},
'Text': {
'_text': string
}
}>
},
'FareBasisCityPair': {
'_text': string
},
'RBD': {
'_text': string
},
'CabinType': {
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
}
},
'TicketDesig': {
'Application': string,
'_text': string
},
'FareRules': {
'refs': string,
'Penalty': {
'CancelFeeInd': string,
'ChangeFeeInd': string,
'RefundableInd': string,
'ReuseInd': string,
'UpgradeFeeInd': string,
'refs': string,
'ObjectKey': string,
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'Details': {
'Detail': Array<{
'refs': string,
'Type': {
'_text': string
},
'Application': {
'_text': string
},
'Amounts': {
'Amount': Array<{
'CurrencyAmountValue': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
},
'AmountApplication': {
'_text': string
},
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
}
}>
}
},
'CorporateFare': {
'refs': string,
'ObjectKey': string,
'PricedInd': string,
'CompanyIndex': {
'refs': string,
'_text': string
},
'Contract': {
'refs': string,
'_text': string
},
'Account': {
'refs': string,
'_text': string
}
},
'AdvanceStay': {
'AdvancePurchase': {
'Context': string,
'_text': string
},
'AdvanceTicketing': {
'AdvanceReservation': {
'Context': string,
'_text': string
},
'AdvanceDeparture': {
'Context': string,
'_text': string
}
},
'MinimumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
},
'MaximumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
}
},
'Ticketing': {
'TicketlessInd': {
'_text': string
},
'InstantPurchase': {
'_text': string
},
'Endorsements': {
'Endorsement': Array<{
'_text': string
}>
}
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'PriceClassRef': {
'_text': string
},
'SegmentRefs': Array<{
'ON_Point': string,
'OFF_Point': string,
'_text': string
}>
}>,
'FlightMileage': {
'Value': {
'_text': string
},
'Application': {
'_text': string
}
},
'TourCode': {
'_text': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>,
'TotalPrice': {
'TotalAmount': {
'CurCode': string,
'_text': string
},
'BaseAmount': {
'CurCode': string,
'_text': string
},
'EquivAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitName': {
'_text': string
},
'Discount': {
'DiscountAmount': {
'CurCode': string,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'PreDiscountedAmount': {
'CurCode': string,
'_text': string
},
'AppText': {
'_text': string
},
'DescText': {
'_text': string
},
'DiscountContext': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
},
'Fee': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>,
'TaxSummary': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>,
'Surcharge': Array<{
'TotalAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'Breakdown': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>
}>
}
}
}>
},
'ReshopOffers': {
'ALaCarteOffer': {
'OfferID': {
'_text': string
},
'OwnerCode': {
'_text': string
},
'OwnerTypeCode': {
'_text': string
},
'ValidatingCarrierCode': {
'_text': string
},
'RequestedDateInd': {
'_text': string
},
'WebAddressURL': {
'_text': string
},
'RedemptionInd': {
'_text': string
},
'MatchAppText': {
'_text': string
},
'MatchType': {
'_text': string
},
'MatchPercent': {
'_text': string
},
'OfferExpirationDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PaymentTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PriceGuaranteeTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'BaggageDisclosureRefID': Array<{
'_text': string
}>,
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>,
'DisclosureRefID': {
'_text': string
},
'PenaltyRefID': Array<{
'_text': string
}>,
'PTC_OfferParameters': Array<{
'PTC_PricedCode': {
'_text': string
},
'PTC_RequestedCode': {
'_text': string
},
'RequestedPaxNumber': {
'_text': string
},
'PricedPaxNumber': {
'_text': string
}
}>,
'TotalPrice': {
'TotalAmount': {
'CurCode': string,
'_text': string
},
'BaseAmount': {
'CurCode': string,
'_text': string
},
'EquivAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitName': {
'_text': string
},
'Discount': {
'DiscountAmount': {
'CurCode': string,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'PreDiscountedAmount': {
'CurCode': string,
'_text': string
},
'AppText': {
'_text': string
},
'DescText': {
'_text': string
},
'DiscountContext': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
},
'Fee': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>,
'TaxSummary': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>,
'Surcharge': Array<{
'TotalAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'Breakdown': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>
}>
},
'ALaCarteOfferItem': Array<{
'OfferItemID': {
'_text': string
},
'UnitPrice': {
'TotalAmount': {
'CurCode': string,
'_text': string
},
'BaseAmount': {
'CurCode': string,
'_text': string
},
'EquivAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitName': {
'_text': string
},
'Discount': {
'DiscountAmount': {
'CurCode': string,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'PreDiscountedAmount': {
'CurCode': string,
'_text': string
},
'AppText': {
'_text': string
},
'DescText': {
'_text': string
},
'DiscountContext': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
},
'Fee': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>,
'TaxSummary': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>,
'Surcharge': Array<{
'TotalAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'Breakdown': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>
}>
},
'Service': {
'ServiceID': {
'_text': string
},
'ServiceRefID': {
'_text': string
},
'ServiceDefinitionRefID': {
'_text': string
}
},
'Eligibility': {
'PaxRefID': Array<{
'_text': string
}>,
'PriceClassRefID': Array<{
'_text': string
}>,
'FlightAssociations': {
'PaxJourneyRefID': Array<{
'_text': string
}>,
'PaxSegmentRefID': Array<{
'_text': string
}>
}
}
}>
},
'ReshopOffer': Array<{
'OfferID': {
'_text': string
},
'OwnerCode': {
'_text': string
},
'OwnerTypeCode': {
'_text': string
},
'ValidatingCarrierCode': {
'_text': string
},
'RequestedDateInd': {
'_text': string
},
'WebAddressURL': {
'_text': string
},
'RedemptionInd': {
'_text': string
},
'MatchAppText': {
'_text': string
},
'MatchType': {
'_text': string
},
'MatchPercent': {
'_text': string
},
'OfferExpirationDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PaymentTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PriceGuaranteeTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'BaggageAllowance': Array<{
'PaxJourneyRefID': Array<{
'_text': string
}>,
'PaxRefID': Array<{
'_text': string
}>,
'BaggageAllowanceRefID': {
'_text': string
}
}>,
'BaggageDisclosureRefID': Array<{
'_text': string
}>,
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>,
'DisclosureRefID': {
'_text': string
},
'PenaltyRefID': Array<{
'_text': string
}>,
'JourneyOverview': {
'PriceClassRefID': {
'_text': string
},
'JourneyPriceClass': Array<{
'PriceClassRefID': {
'_text': string
},
'PaxJourneyRefID': {
'_text': string
}
}>
},
'PTC_OfferParameters': Array<{
'PTC_PricedCode': {
'_text': string
},
'PTC_RequestedCode': {
'_text': string
},
'RequestedPaxNumber': {
'_text': string
},
'PricedPaxNumber': {
'_text': string
}
}>,
'TotalPrice': {
'TotalAmount': {
'CurCode': string,
'_text': string
},
'BaseAmount': {
'CurCode': string,
'_text': string
},
'EquivAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitName': {
'_text': string
},
'Discount': {
'DiscountAmount': {
'CurCode': string,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'PreDiscountedAmount': {
'CurCode': string,
'_text': string
},
'AppText': {
'_text': string
},
'DescText': {
'_text': string
},
'DiscountContext': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
},
'Fee': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>,
'TaxSummary': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>,
'Surcharge': Array<{
'TotalAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'Breakdown': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>
}>
},
'AddOfferItem': Array<{
'OfferItemID': {
'_text': string
},
'MandatoryInd': {
'_text': string
},
'ModificationProhibitedInd': {
'_text': string
},
'OrderItemRefID': Array<{
'_text': string
}>,
'FareDetail': Array<{
'FareIndicatorCode': {
'FiledFareInd': string,
'_text': string
},
'PassengerRefs': Array<{
'_text': string
}>,
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareComponent': Array<{
'Parameters': {
'Quantity': string
},
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareBasis': {
'FareBasisCode': {
'refs': string,
'Code': {
'_text': string
},
'Application': {
'_text': string
}
},
'FareRulesRemarks': {
'FareRulesRemark': Array<{
'refs': string,
'Category': {
'_text': string
},
'Text': {
'_text': string
}
}>
},
'FareBasisCityPair': {
'_text': string
},
'RBD': {
'_text': string
},
'CabinType': {
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
}
},
'TicketDesig': {
'Application': string,
'_text': string
},
'FareRules': {
'refs': string,
'Penalty': {
'CancelFeeInd': string,
'ChangeFeeInd': string,
'RefundableInd': string,
'ReuseInd': string,
'UpgradeFeeInd': string,
'refs': string,
'ObjectKey': string,
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'Details': {
'Detail': Array<{
'refs': string,
'Type': {
'_text': string
},
'Application': {
'_text': string
},
'Amounts': {
'Amount': Array<{
'CurrencyAmountValue': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
},
'AmountApplication': {
'_text': string
},
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
}
}>
}
},
'CorporateFare': {
'refs': string,
'ObjectKey': string,
'PricedInd': string,
'CompanyIndex': {
'refs': string,
'_text': string
},
'Contract': {
'refs': string,
'_text': string
},
'Account': {
'refs': string,
'_text': string
}
},
'AdvanceStay': {
'AdvancePurchase': {
'Context': string,
'_text': string
},
'AdvanceTicketing': {
'AdvanceReservation': {
'Context': string,
'_text': string
},
'AdvanceDeparture': {
'Context': string,
'_text': string
}
},
'MinimumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
},
'MaximumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
}
},
'Ticketing': {
'TicketlessInd': {
'_text': string
},
'InstantPurchase': {
'_text': string
},
'Endorsements': {
'Endorsement': Array<{
'_text': string
}>
}
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'PriceClassRef': {
'_text': string
},
'SegmentRefs': Array<{
'ON_Point': string,
'OFF_Point': string,
'_text': string
}>
}>,
'FlightMileage': {
'Value': {
'_text': string
},
'Application': {
'_text': string
}
},
'TourCode': {
'_text': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>,
'Service': Array<{
'ServiceID': {
'_text': string
},
'PaxRefID': Array<{
'_text': string
}>,
'ServiceRefID': {
'_text': string
},
'ServiceAssociations': {
'PaxJourneyRefID': Array<{
'_text': string
}>,
'SeatAssignment': {
'DatedOperatingLegRefID': {
'_text': string
},
'ServiceDefinitionRefID': {
'_text': string
},
'Seat': {
'SeatRowNumber': {
'_text': string
},
'CabinColumnID': {
'_text': string
}
}
},
'ServiceDefinitionRef': {
'ServiceDefinitionRefID': {
'_text': string
},
'PaxSegmentRefID': Array<{
'_text': string
}>
}
},
'SettlementInfo': {
'MethodCode': {
'_text': string
},
'SettlementAmount': {
'CurCode': string,
'_text': string
}
}
}>,
'Price': {
'TotalAmount': {
'CurCode': string,
'_text': string
},
'BaseAmount': {
'CurCode': string,
'_text': string
},
'EquivAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitName': {
'_text': string
},
'Discount': {
'DiscountAmount': {
'CurCode': string,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'PreDiscountedAmount': {
'CurCode': string,
'_text': string
},
'AppText': {
'_text': string
},
'DescText': {
'_text': string
},
'DiscountContext': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
},
'Fee': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>,
'TaxSummary': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>,
'Surcharge': Array<{
'TotalAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'Breakdown': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>
}>
},
'OriginalOrderItemDifferential': {
'Amount': {
'CurCode': string,
'_text': string
},
'TaxSummary': {
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}
},
'NewOfferItemDifferential': {
'Amount': {
'CurCode': string,
'_text': string
},
'TaxSummary': {
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}
},
'PenaltyDifferential': {
'Amount': {
'CurCode': string,
'_text': string
},
'TaxSummary': {
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}
},
'FeesDifferential': {
'Amount': {
'CurCode': string,
'_text': string
},
'TaxSummary': {
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}
},
'DifferentialAmountDue': {
'Amount': {
'CurCode': string,
'_text': string
},
'TaxSummary': {
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}
}
}>,
'DeleteOrderItem': Array<{
'OrderItemRefID': {
'_text': string
},
'OriginalOrderItemDifferential': {
'Amount': {
'CurCode': string,
'_text': string
},
'TaxSummary': {
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}
},
'NewOfferItemDifferential': {
'Amount': {
'CurCode': string,
'_text': string
},
'TaxSummary': {
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}
},
'PenaltyDifferential': {
'Amount': {
'CurCode': string,
'_text': string
},
'TaxSummary': {
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}
},
'FeesDifferential': {
'Amount': {
'CurCode': string,
'_text': string
},
'TaxSummary': {
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}
},
'DifferentialAmountDue': {
'Amount': {
'CurCode': string,
'_text': string
},
'TaxSummary': {
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}
}
}>,
'NameChangeOfferItem': {
'OfferItemID': {
'_text': string
},
'NameChangeService': {
'ServiceID': {
'_text': string
},
'ServiceDefinitionRefID': {
'_text': string
}
},
'MandatoryInd': {
'_text': string
},
'ModificationProhibitedInd': {
'_text': string
},
'Price': {
'TotalAmount': {
'CurCode': string,
'_text': string
},
'BaseAmount': {
'CurCode': string,
'_text': string
},
'EquivAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitName': {
'_text': string
},
'Discount': {
'DiscountAmount': {
'CurCode': string,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'PreDiscountedAmount': {
'CurCode': string,
'_text': string
},
'AppText': {
'_text': string
},
'DescText': {
'_text': string
},
'DiscountContext': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
},
'Fee': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>,
'TaxSummary': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>,
'Surcharge': Array<{
'TotalAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'Breakdown': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>
}>
},
'PaxNameChange': {
'PaxRefID': {
'_text': string
},
'SuffixName': {
'_text': string
},
'TitleName': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
}
}
}
}>
}
},
'ShoppingResponse': {
'ShoppingResponseID': {
'_text': string
},
'OwnerCode': {
'_text': string
}
},
'Warning': Array<{
'Code': {
'_text': string
},
'DescText': {
'_text': string
},
'LanguageCode': {
'_text': string
},
'OwnerName': {
'_text': string
},
'StatusText': {
'_text': string
},
'TagText': {
'_text': string
},
'TypeCode': {
'_text': string
},
'URL': {
'_text': string
}
}>
}
export type NDC_ServiceListRes = {
'OfferExpirationDateTime': {
'TimeZoneCode': string,
'_text': string
},
'ALaCarteOffer': {
'OfferID': {
'_text': string
},
'OwnerCode': {
'_text': string
},
'OwnerTypeCode': {
'_text': string
},
'ValidatingCarrierCode': {
'_text': string
},
'RequestedDateInd': {
'_text': string
},
'WebAddressURL': {
'_text': string
},
'RedemptionInd': {
'_text': string
},
'MatchAppText': {
'_text': string
},
'MatchType': {
'_text': string
},
'MatchPercent': {
'_text': string
},
'OfferExpirationDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PaymentTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PriceGuaranteeTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'BaggageDisclosureRefID': Array<{
'_text': string
}>,
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>,
'DisclosureRefID': {
'_text': string
},
'PenaltyRefID': Array<{
'_text': string
}>,
'PTC_OfferParameters': Array<{
'PTC_PricedCode': {
'_text': string
},
'PTC_RequestedCode': {
'_text': string
},
'RequestedPaxNumber': {
'_text': string
},
'PricedPaxNumber': {
'_text': string
}
}>,
'TotalPrice': {
'TotalAmount': {
'CurCode': string,
'_text': string
},
'BaseAmount': {
'CurCode': string,
'_text': string
},
'EquivAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitName': {
'_text': string
},
'Discount': {
'DiscountAmount': {
'CurCode': string,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'PreDiscountedAmount': {
'CurCode': string,
'_text': string
},
'AppText': {
'_text': string
},
'DescText': {
'_text': string
},
'DiscountContext': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
},
'Fee': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>,
'TaxSummary': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>,
'Surcharge': Array<{
'TotalAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'Breakdown': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>
}>
},
'ALaCarteOfferItem': Array<{
'OfferItemID': {
'_text': string
},
'UnitPrice': {
'TotalAmount': {
'CurCode': string,
'_text': string
},
'BaseAmount': {
'CurCode': string,
'_text': string
},
'EquivAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitAmount': {
'CurCode': string,
'_text': string
},
'LoyaltyUnitName': {
'_text': string
},
'Discount': {
'DiscountAmount': {
'CurCode': string,
'_text': string
},
'DiscountPercent': {
'_text': string
},
'PreDiscountedAmount': {
'CurCode': string,
'_text': string
},
'AppText': {
'_text': string
},
'DescText': {
'_text': string
},
'DiscountContext': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
},
'Fee': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>,
'TaxSummary': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>,
'Surcharge': Array<{
'TotalAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'Breakdown': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'RefundInd': {
'_text': string
},
'DescText': {
'_text': string
},
'DesigText': {
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
},
'ApproximateInd': {
'_text': string
}
}>
}>
},
'Service': {
'ServiceID': {
'_text': string
},
'ServiceRefID': {
'_text': string
},
'ServiceDefinitionRefID': {
'_text': string
}
},
'Eligibility': {
'PaxRefID': Array<{
'_text': string
}>,
'PriceClassRefID': Array<{
'_text': string
}>,
'FlightAssociations': {
'PaxJourneyRefID': Array<{
'_text': string
}>,
'PaxSegmentRefID': Array<{
'_text': string
}>
}
}
}>
},
'DataLists': {
'BaggageAllowanceList': {
'BaggageAllowance': Array<{
'BaggageAllowanceID': {
'_text': string
},
'TypeCode': {
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'ApplicablePartyText': {
'_text': string
},
'DescText': Array<{
'_text': string
}>,
'WeightAllowance': Array<{
'MaximumWeightMeasure': {
'UnitCode': string,
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'ApplicablePartyText': {
'_text': string
},
'DescText': Array<{
'_text': string
}>
}>,
'BDC': {
'CarrierDesigCode': {
'_text': string
},
'CarrierName': {
'_text': string
},
'BagRuleCode': {
'_text': string
},
'BDCAnalysisResultCode': {
'_text': string
},
'BDCReasonText': {
'_text': string
}
},
'DimensionAllowance': Array<{
'BaggageDimensionCategory': {
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'MinMeasure': {
'UnitCode': string,
'_text': string
},
'ApplicablePartyText': {
'_text': string
},
'MaxMeasure': {
'UnitCode': string,
'_text': string
},
'DescText': Array<{
'_text': string
}>,
'Qty': {
'_text': string
}
}>,
'PieceAllowance': Array<{
'ApplicablePartyText': {
'_text': string
},
'TotalQty': {
'_text': string
},
'TypeText': {
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'Desc': Array<{
'_text': string
}>,
'PieceDimensionAllowance': Array<{
'BaggageDimensionCategory': {
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'MinMeasure': {
'UnitCode': string,
'_text': string
},
'ApplicablePartyText': {
'_text': string
},
'MaxMeasure': {
'UnitCode': string,
'_text': string
},
'DescText': Array<{
'_text': string
}>,
'Qty': {
'_text': string
}
}>,
'PieceWeightAllowance': Array<{
'MaximumWeightMeasure': {
'UnitCode': string,
'_text': string
},
'ApplicableBagText': {
'_text': string
},
'ApplicablePartyText': {
'_text': string
},
'DescText': Array<{
'_text': string
}>
}>
}>
}>
},
'BaggageDisclosureList': {
'BaggageDisclosure': Array<{
'BaggageDisclosureID': {
'_text': string
},
'RuleTypeCode': {
'_text': string
},
'CommercialAgreementID': {
'_text': string
},
'DeferralInd': {
'_text': string
},
'FixedPrePaidInd': {
'_text': string
},
'CheckInChargesInd': {
'_text': string
},
'BDC': {
'CarrierDesigCode': {
'_text': string
},
'CarrierName': {
'_text': string
},
'BagRuleCode': {
'_text': string
},
'BDCAnalysisResultCode': {
'_text': string
},
'BDCReasonText': {
'_text': string
}
},
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>
}>
},
'ContactInfoList': {
'ContactInfo': Array<{
'ContactInfoID': {
'_text': string
},
'ContactTypeText': {
'_text': string
},
'IndividualRef': {
'_text': string
},
'ContactRefusedInd': {
'_text': string
},
'Phone': Array<{
'LabelText': {
'_text': string
},
'CountryDialingCode': {
'_text': string
},
'AreaCodeNumber': {
'_text': string
},
'PhoneNumber': {
'_text': string
},
'ExtensionNumber': {
'_text': string
}
}>,
'OtherAddress': Array<{
'LabelText': {
'_text': string
},
'OtherAddressText': {
'_text': string
}
}>,
'EmailAddress': Array<{
'LabelText': {
'_text': string
},
'EmailAddressText': {
'_text': string
}
}>,
'Individual': {
'IndividualID': {
'_text': string
},
'GenderCode': {
'_text': string
},
'TitleName': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
},
'SuffixName': {
'_text': string
},
'Birthdate': {
'_text': string
},
'BirthplaceText': {
'_text': string
}
},
'PostalAddress': Array<{
'LabelText': {
'_text': string
},
'StreetText': Array<{
'_text': string
}>,
'BuildingRoomText': {
'_text': string
},
'PO_BoxCode': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CityName': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
},
'CountryName': {
'_text': string
},
'CountryCode': {
'_text': string
}
}>
}>
},
'ContentSource': {
'ContentSource': Array<{
'refs': string,
'ListKey': string,
'NodePath': {
'_text': string
},
'AirlineID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'OtherID': {
'name': string,
'_text': string
}
}>
},
'DisclosureList': {
'DisclosureID': {
'_text': string
},
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>
},
'FareList': {
'FareGroup': Array<{
'refs': string,
'ListKey': string,
'Fare': {
'FareCode': {
'_text': string
},
'FareDetail': {
'FareIndicatorCode': {
'FiledFareInd': string,
'_text': string
},
'PassengerRefs': Array<{
'_text': string
}>,
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareComponent': Array<{
'Parameters': {
'Quantity': string
},
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareBasis': {
'FareBasisCode': {
'refs': string,
'Code': {
'_text': string
},
'Application': {
'_text': string
}
},
'FareRulesRemarks': {
'FareRulesRemark': Array<{
'refs': string,
'Category': {
'_text': string
},
'Text': {
'_text': string
}
}>
},
'FareBasisCityPair': {
'_text': string
},
'RBD': {
'_text': string
},
'CabinType': {
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
}
},
'TicketDesig': {
'Application': string,
'_text': string
},
'FareRules': {
'refs': string,
'Penalty': {
'CancelFeeInd': string,
'ChangeFeeInd': string,
'RefundableInd': string,
'ReuseInd': string,
'UpgradeFeeInd': string,
'refs': string,
'ObjectKey': string,
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'Details': {
'Detail': Array<{
'refs': string,
'Type': {
'_text': string
},
'Application': {
'_text': string
},
'Amounts': {
'Amount': Array<{
'CurrencyAmountValue': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
},
'AmountApplication': {
'_text': string
},
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
}
}>
}
},
'CorporateFare': {
'refs': string,
'ObjectKey': string,
'PricedInd': string,
'CompanyIndex': {
'refs': string,
'_text': string
},
'Contract': {
'refs': string,
'_text': string
},
'Account': {
'refs': string,
'_text': string
}
},
'AdvanceStay': {
'AdvancePurchase': {
'Context': string,
'_text': string
},
'AdvanceTicketing': {
'AdvanceReservation': {
'Context': string,
'_text': string
},
'AdvanceDeparture': {
'Context': string,
'_text': string
}
},
'MinimumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
},
'MaximumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
}
},
'Ticketing': {
'TicketlessInd': {
'_text': string
},
'InstantPurchase': {
'_text': string
},
'Endorsements': {
'Endorsement': Array<{
'_text': string
}>
}
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'PriceClassRef': {
'_text': string
},
'SegmentRefs': Array<{
'ON_Point': string,
'OFF_Point': string,
'_text': string
}>
}>,
'FlightMileage': {
'Value': {
'_text': string
},
'Application': {
'_text': string
}
},
'TourCode': {
'_text': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}
},
'FareBasisCode': {
'Code': {
'_text': string
},
'Application': {
'_text': string
}
}
}>
},
'InstructionsList': {
'Instruction': Array<{
'refs': string,
'ListKey': string,
'ClassOfServiceUpgrade': {
'Classes': {
'ClassOfService': Array<{
'refs': string,
'Code': {
'SeatsLeft': string,
'_text': string
},
'MarketingName': {
'CabinDesignator': string,
'_text': string
},
'FareBasisCode': {
'_text': string
}
}>
}
},
'FreeFormTextInstruction': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'SpecialBookingInstruction': {
'_text': string
}
}>
},
'MediaList': {
'Media': Array<{
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}>
},
'OriginDestList': {
'OriginDest': Array<{
'OriginDestID': {
'_text': string
},
'ArrivalStationCode': {
'_text': string
},
'DepStationCode': {
'_text': string
},
'PaxJourneyRefID': Array<{
'_text': string
}>
}>
},
'PaxJourneyList': {
'PaxJourney': Array<{
'PaxJourneyID': {
'_text': string
},
'DistanceMeasure': {
'UnitCode': string,
'_text': string
},
'Duration': {
'_text': string
},
'PaxSegmentRefID': Array<{
'_text': string
}>,
'SettlementInfo': {
'MethodCode': {
'_text': string
},
'SettlementAmount': {
'CurCode': string,
'_text': string
}
}
}>
},
'PaxList': {
'Pax': Array<{
'PaxID': {
'_text': string
},
'PTC': {
'_text': string
},
'AgeMeasure': {
'UnitCode': string,
'_text': string
},
'Birthdate': {
'_text': string
},
'ResidenceCountryCode': {
'_text': string
},
'CitizenshipCountryCode': {
'_text': string
},
'ProfileID_Text': {
'_text': string
},
'ProfileConsentInd': {
'_text': string
},
'ContactInfoRefID': {
'_text': string
},
'PaxRefID': {
'_text': string
},
'IdentityDoc': Array<{
'IdentityDocNumber': {
'_text': string
},
'IdentityDocTypeCode': {
'_text': string
},
'IssuingCountryCode': {
'_text': string
},
'TitleName': {
'_text': string
},
'CitizenshipCountryCode': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'ResidenceCountryCode': {
'_text': string
},
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
},
'SuffixName': {
'_text': string
},
'Birthdate': {
'_text': string
},
'BirthplaceText': {
'_text': string
},
'GenderCode': {
'_text': string
},
'IssueDate': {
'_text': string
},
'ExpiryDate': {
'_text': string
},
'Individual': Array<{
'IndividualID': {
'_text': string
},
'GenderCode': {
'_text': string
},
'TitleName': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
},
'SuffixName': {
'_text': string
},
'Birthdate': {
'_text': string
},
'BirthplaceText': {
'_text': string
}
}>,
'Visa': Array<{
'VisaNumber': {
'_text': string
},
'VisaTypeCode': {
'_text': string
},
'DurationOfStay': {
'_text': string
},
'EnterBeforeDate': {
'_text': string
},
'EntryQty': {
'_text': string
},
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': Array<{
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
}>
}>
}>,
'Individual': {
'IndividualID': {
'_text': string
},
'GenderCode': {
'_text': string
},
'TitleName': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
},
'SuffixName': {
'_text': string
},
'Birthdate': {
'_text': string
},
'BirthplaceText': {
'_text': string
}
},
'LoyaltyProgramAccount': Array<{
'ProgramName': {
'_text': string
},
'ProgramCode': {
'_text': string
},
'AccountNumber': {
'_text': string
},
'URL': {
'_text': string
},
'SignInID': {
'_text': string
},
'TierCode': {
'_text': string
},
'TierName': {
'_text': string
},
'TierPriorityText': {
'_text': string
},
'ProviderName': {
'_text': string
},
'Alliance': {
'AllianceCode': {
'_text': string
},
'Name': {
'_text': string
},
'URL': {
'_text': string
}
},
'Carrier': {
'AirlineDesigCode': {
'_text': string
},
'DuplicateDesigInd': {
'_text': string
},
'Name': {
'_text': string
},
'Alliance': {
'AllianceCode': {
'_text': string
},
'Name': {
'_text': string
},
'URL': {
'_text': string
}
}
}
}>,
'Remark': Array<{
'RemarkText': {
'_text': string
},
'DisplayInd': {
'_text': string
},
'CreationDateTime': {
'TimeZoneCode': string,
'_text': string
}
}>
}>
},
'PaxSegmentList': {
'PaxSegment': Array<{
'PaxSegmentID': {
'_text': string
},
'ARNK_Ind': {
'_text': string
},
'TicketlessInd': {
'_text': string
},
'SecureFlightInd': {
'_text': string
},
'Duration': {
'_text': string
},
'SegmentTypeCode': {
'_text': string
},
'CabinType': {
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
},
'Dep': {
'BoardingGateID': {
'_text': string
},
'IATA_LocationCode': {
'_text': string
},
'TerminalName': {
'_text': string
},
'AircraftScheduledDateTime': {
'TimeZoneCode': string,
'_text': string
}
},
'Arrival': {
'BoardingGateID': {
'_text': string
},
'IATA_LocationCode': {
'_text': string
},
'TerminalName': {
'_text': string
},
'AircraftScheduledDateTime': {
'TimeZoneCode': string,
'_text': string
}
},
'MarketingCarrierInfo': {
'CarrierDesigCode': {
'_text': string
},
'CarrierName': {
'_text': string
},
'MarketingCarrierFlightNumberText': {
'_text': string
},
'OperationalSuffixText': {
'_text': string
},
'RBD_Code': {
'_text': string
}
},
'OperatingCarrierInfo': {
'CarrierDesigCode': {
'_text': string
},
'CarrierName': {
'_text': string
},
'OperatingCarrierFlightNumberText': {
'_text': string
},
'RBD_Code': {
'_text': string
},
'OperationalSuffixText': {
'_text': string
},
'Disclosure': {
'DisclosureID': {
'_text': string
},
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>
}
},
'DatedOperatingLeg': Array<{
'DatedOperatingLegID': {
'_text': string
},
'CarrierAircraftType': {
'CarrierAircraftTypeCode': {
'_text': string
},
'CarrierAircraftTypeName': {
'_text': string
}
},
'ChangeofGaugeInd': {
'_text': string
},
'DistanceMeasure': {
'UnitCode': string,
'_text': string
},
'Dep': {
'BoardingGateID': {
'_text': string
},
'IATA_LocationCode': {
'_text': string
},
'TerminalName': {
'_text': string
},
'AircraftScheduledDateTime': {
'TimeZoneCode': string,
'_text': string
}
},
'Arrival': {
'BoardingGateID': {
'_text': string
},
'IATA_LocationCode': {
'_text': string
},
'TerminalName': {
'_text': string
},
'AircraftScheduledDateTime': {
'TimeZoneCode': string,
'_text': string
}
},
'IATA_AircraftType': {
'IATA_AircraftTypeCode': {
'_text': string
}
},
'OnGroundTime': {
'_text': string
}
}>,
'SettlementInfo': {
'MethodCode': {
'_text': string
},
'SettlementAmount': {
'CurCode': string,
'_text': string
}
}
}>
},
'PenaltyList': {
'Penalty': Array<{
'PenaltyID': {
'_text': string
},
'UpgradeFeeInd': {
'_text': string
},
'TypeCode': {
'_text': string
},
'DescText': {
'_text': string
},
'AppCode': {
'_text': string
},
'CancelFeeInd': {
'_text': string
},
'ChangeFeeInd': {
'_text': string
},
'PenaltyAmount': {
'CurCode': string,
'_text': string
},
'PenaltyPercent': {
'_text': string
}
}>
},
'PriceClassList': {
'PriceClass': Array<{
'PriceClassID': {
'_text': string
},
'Code': {
'_text': string
},
'Name': {
'_text': string
},
'FareBasisCode': {
'_text': string
},
'FareBasisAppText': {
'_text': string
},
'DisplayOrderText': {
'_text': string
},
'Desc': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>,
'CabinType': Array<{
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
}>
}>
},
'SeatProfileList': {
'SeatProfile': Array<{
'SeatProfileID': {
'_text': string
},
'CharacteristicCode': Array<{
'_text': string
}>,
'SeatWidthMeasure': {
'UnitCode': string,
'_text': string
},
'SeatPitchMeasure': {
'UnitCode': string,
'_text': string
},
'DescText': Array<{
'_text': string
}>,
'MarketingInformation': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>,
'SeatKeywords': Array<{
'KeyWordText': {
'_text': string
},
'ValueText': {
'_text': string
}
}>
}>
},
'ServiceDefinitionList': {
'ServiceDefinition': Array<{
'ServiceDefinitionID': {
'_text': string
},
'OwnerCode': {
'_text': string
},
'Name': {
'_text': string
},
'ServiceCode': {
'_text': string
},
'ReasonForIssuanceCode': {
'_text': string
},
'ReasonForIssuanceSubCode': {
'_text': string
},
'DepositTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'NamingTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PaymentTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'PriceGuaranteeTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'TicketingTimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'BilateralProcessTimeLimit': Array<{
'Name': {
'_text': string
},
'TimeLimitDateTime': {
'TimeZoneCode': string,
'_text': string
},
'DescText': {
'_text': string
}
}>,
'ValidatingCarrierCode': {
'_text': string
},
'Description': Array<{
'DescID': {
'_text': string
},
'DescText': {
'_text': string
},
'MarkupStyleText': {
'_text': string
},
'URL': {
'_text': string
},
'Media': {
'MediaID': {
'_text': string
},
'DescText': {
'_text': string
},
'MimeTypeText': {
'_text': string
},
'URL': {
'_text': string
},
'FileSizeMeasure': {
'UnitCode': string,
'_text': string
},
'WidthMeasure': {
'UnitCode': string,
'_text': string
},
'HeightMeasure': {
'UnitCode': string,
'_text': string
},
'HintText': {
'_text': string
},
'RenderingInstructionsText': {
'_text': string
},
'RenderingMethodText': {
'_text': string
},
'RenderingOutputFormatText': {
'_text': string
},
'RenderingOverviewText': {
'_text': string
}
}
}>,
'ServiceDefinitionAssociation': {
'BaggageAllowanceRefID': {
'_text': string
},
'SeatProfileRefID': {
'_text': string
},
'ServiceBundle': {
'MaxQty': {
'_text': string
},
'ServiceDefinitionRefID': Array<{
'_text': string
}>
}
},
'BookingInstructions': {
'SSRCode': Array<{
'_text': string
}>,
'OSIText': Array<{
'_text': string
}>,
'Method': {
'_text': string
},
'UpgradeMethod': {
'NewClass': string,
'_text': string
},
'Text': Array<{
'_text': string
}>,
'Equipment': {
'_text': string
}
},
'Detail': {
'ServiceCombinations': {
'Combination': Array<{
'Rule': {
'_text': string
},
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>,
'ServiceReference': Array<{
'_text': string
}>
}>
},
'ServiceCoupon': {
'InstantPurchase': {
'_text': string
},
'FeeBasis': {
'_text': string
},
'CouponType': {
'_text': string
}
},
'ServiceFulfillment': {
'refs': string,
'OfferValidDates': {
'Start': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'End': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
}
},
'Provider': {
'metadata': string,
'refs': string,
'PartnerID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Name': {
'_text': string
},
'Type': {
'_text': string
},
'Fulfillments': {
'Fulfillment': Array<{
'refs': string,
'OfferValidDates': {
'Start': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'End': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
}
},
'Location': {
'AirportCode': {
'refs': string,
'_text': string
},
'StructuredAddress': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
}
}
}>
}
},
'Location': {
'AirportFulfillmentLocation': {
'refs': string,
'AirportCode': {
'refs': string,
'_text': string
}
},
'OtherFulfillmentLocation': {
'Address': {
'AddressCore': {
'refs': string,
'Address': {
'PaymentAddress': {
'refs': string,
'Street': Array<{
'_text': string
}>,
'PO_Box': {
'_text': string
},
'BuildingRoom': {
'_text': string
},
'City': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'Country': {
'_text': string
}
},
'SimpleAddress': {
'refs': string,
'AddressLine': Array<{
'_text': string
}>
},
'StructuredAddress': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
}
}
},
'AddressDetail': {
'refs': string,
'Address': {
'PaymentAddress': {
'refs': string,
'Street': Array<{
'_text': string
}>,
'PO_Box': {
'_text': string
},
'BuildingRoom': {
'_text': string
},
'City': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'Country': {
'_text': string
}
},
'SimpleAddress': {
'refs': string,
'AddressLine': Array<{
'_text': string
}>
},
'StructuredAddress': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
}
},
'Directions': {
'Direction': Array<{
'refs': string,
'Text': {
'_text': string
},
'Link': {
'_text': string
},
'Name': {
'_text': string
},
'From': {
'_text': string
},
'To': {
'_text': string
}
}>
}
}
}
}
}
},
'ServiceItemQuantityRules': {
'MinimumQuantity': {
'_text': string
},
'MaximumQuantity': {
'_text': string
}
}
}
}>
},
'TermsList': {
'refs': string,
'ListKey': string,
'Term': Array<{
'refs': string,
'AvailablePeriod': {
'Earliest': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'Latest': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
}
},
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
},
'GroupSize': {
'Minimum': {
'_text': string
},
'Maximum': {
'_text': string
}
},
'OrderingQuantity': {
'Minimum': {
'_text': string
},
'Maximum': {
'_text': string
}
}
}>
}
},
'Metadata': {
'Shopping': {
'ShopMetadataGroup': {
'Baggage': {
'CheckedBagMetadatas': {
'CheckedBagMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'CarryOnBagMetadatas': {
'CarryOnBagMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'BaggageDisclosureMetadatas': {
'BaggageDisclosureMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'BaggageDetailMetadata': {
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Attributes': {
'refs': string,
'Group': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
},
'SubGroup': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
},
'Desc1': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
},
'Desc2': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
}
},
'FareTariff': {
'FareType': string,
'_text': string
},
'FareRule': {
'_text': string
},
'FareCat': {
'_text': string
}
},
'BaggageQueryMetadata': {
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'TicketIssuePlace': {
'_text': string
},
'TicketIssueCountry': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'TotalPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
}
},
'Fare': {
'refs': string,
'FareCode': {
'_text': string
},
'FareDetail': {
'FareIndicatorCode': {
'FiledFareInd': string,
'_text': string
},
'PassengerRefs': Array<{
'_text': string
}>,
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareComponent': Array<{
'Parameters': {
'Quantity': string
},
'Price': {
'TotalAmount': {
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'DetailCurrencyPrice': {
'refs': string,
'ApproxInd': string,
'RefundAllInd': string,
'TaxIncludedInd': string,
'OtherChargeInd': string,
'AutoExchangeInd': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Equivalent': {
'_text': string
},
'Details': {
'refs': string,
'Detail': Array<{
'refs': string,
'RefundInd': string,
'SubTotal': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Application': {
'_text': string
}
}>
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'Fees': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}
},
'EncodedCurrencyPrice': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FareFiledIn': {
'refs': string,
'BaseAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'NUC_Amount': {
'_text': string
},
'ExchangeRate': {
'_text': string
},
'TicketBulkMask': {
'_text': string
},
'FiledFare': {
'refs': string,
'Cat35NetFareInd': string,
'FareIndicatorCode': {
'_text': string
}
}
},
'Surcharges': {
'Surcharge': Array<{
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'refundInd': string,
'_text': string
},
'Breakdown': {
'Fee': Array<{
'refs': string,
'ApproxInd': string,
'RefundInd': string,
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Designator': {
'_text': string
},
'Description': {
'_text': string
},
'Nature': {
'_text': string
}
}>
}
}>
},
'AwardPricing': {
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
},
'CombinationPricing': {
'Partial': {
'refs': string,
'Currency': {
'EncodedCurrencyAmount': {
'refs': string,
'Code': string,
'_text': string
},
'SimpleCurrencyPrice': {
'Code': string,
'Taxable': boolean,
'_text': string
}
},
'Redemption': {
'refs': string,
'Unit': {
'refs': string,
'_text': string
},
'Quantity': {
'_text': string
},
'Conversion': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Ratio': {
'_text': string
}
}
}
}
},
'Taxes': {
'ApproxInd': string,
'CollectionInd': string,
'RefundAllInd': string,
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Breakdown': {
'refs': string,
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Qualifier': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Nation': {
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxType': {
'_text': string
},
'CollectionPoint': Array<{
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
},
'AirportAmount': {
'_text': string
},
'AirportCode': {
'refs': string,
'_text': string
}
}>,
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
},
'Conversion': {
'CurrencyAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'ConversionRate': {
'_text': string
}
},
'FiledAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'FileTaxType': {
'_text': string
},
'AddlTaxType': {
'_text': string
},
'AddlFiledTaxType': {
'_text': string
}
}>
}
},
'TaxExemption': {
'refs': string,
'Total': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Entities': {
'Entity': Array<{
'_text': string
}>
},
'Territories': {
'Territory': Array<{
'_text': string
}>
},
'Countries': {
'CountryCode': Array<{
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}>
},
'Tax': Array<{
'refs': string,
'ApproxInd': string,
'CollectionInd': string,
'RefundInd': string,
'Designator': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Nature': {
'_text': string
},
'LocalAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'Description': {
'_text': string
}
}>
},
'RequestedDate': {
'_text': string
},
'Alignment': {
'_text': string
}
},
'FareBasis': {
'FareBasisCode': {
'refs': string,
'Code': {
'_text': string
},
'Application': {
'_text': string
}
},
'FareRulesRemarks': {
'FareRulesRemark': Array<{
'refs': string,
'Category': {
'_text': string
},
'Text': {
'_text': string
}
}>
},
'FareBasisCityPair': {
'_text': string
},
'RBD': {
'_text': string
},
'CabinType': {
'CabinTypeCode': {
'_text': string
},
'CabinTypeName': {
'_text': string
}
}
},
'TicketDesig': {
'Application': string,
'_text': string
},
'FareRules': {
'refs': string,
'Penalty': {
'CancelFeeInd': string,
'ChangeFeeInd': string,
'RefundableInd': string,
'ReuseInd': string,
'UpgradeFeeInd': string,
'refs': string,
'ObjectKey': string,
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'Details': {
'Detail': Array<{
'refs': string,
'Type': {
'_text': string
},
'Application': {
'_text': string
},
'Amounts': {
'Amount': Array<{
'CurrencyAmountValue': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
},
'AmountApplication': {
'_text': string
},
'ApplicableFeeRemarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
}
}>
}
},
'CorporateFare': {
'refs': string,
'ObjectKey': string,
'PricedInd': string,
'CompanyIndex': {
'refs': string,
'_text': string
},
'Contract': {
'refs': string,
'_text': string
},
'Account': {
'refs': string,
'_text': string
}
},
'AdvanceStay': {
'AdvancePurchase': {
'Context': string,
'_text': string
},
'AdvanceTicketing': {
'AdvanceReservation': {
'Context': string,
'_text': string
},
'AdvanceDeparture': {
'Context': string,
'_text': string
}
},
'MinimumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
},
'MaximumStay': {
'DayQuantity': {
'Context': string,
'_text': string
},
'DayOfWeek': {
'_text': string
}
}
},
'Ticketing': {
'TicketlessInd': {
'_text': string
},
'InstantPurchase': {
'_text': string
},
'Endorsements': {
'Endorsement': Array<{
'_text': string
}>
}
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
},
'PriceClassRef': {
'_text': string
},
'SegmentRefs': Array<{
'ON_Point': string,
'OFF_Point': string,
'_text': string
}>
}>,
'FlightMileage': {
'Value': {
'_text': string
},
'Application': {
'_text': string
}
},
'TourCode': {
'_text': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}
},
'Flight': {
'FlightMetadatas': {
'FlightMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'ActionCode': {
'_text': string
},
'BindingKey': {
'_text': string
},
'FlightStatus': {
'_text': string
},
'Frequency': {
'_text': string
},
'InstantPurchase': {
'_text': string
},
'Meals': {
'Meal': Array<{
'_text': string
}>
},
'OnTimePerformance': {
'Percent': string,
'Period': string,
'Type': string,
'LatePercent': string,
'CancelledPercent': string,
'SpecialHighlightInd': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
},
'ItineraryMetadata': {
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'ActionCode': {
'_text': string
}
}
},
'Location': {
'DirectionMetadatas': {
'DirectionMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
}
},
'Offer': {
'DisclosureMetadatas': {
'DisclosureMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Timestamp': {
'_text': string
}
}>
},
'OfferMetadatas': {
'OfferMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'ATPCO': {
'Attributes': {
'refs': string,
'Group': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
},
'SubGroup': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
},
'Desc1': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
},
'Desc2': {
'Code': {
'_text': string
},
'Text': {
'_text': string
}
}
},
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
},
'Incentives': {
'Incentive': Array<{
'refs': string,
'OfferCodeID': {
'_text': string
},
'ExpirationDate': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'OfferSubCode': {
'_text': string
},
'AvailableUnits': {
'_text': string
},
'DiscountLevel': {
'DiscountAmount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'DiscountPercent': {
'_text': string
}
},
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'Matches': {
'Match': Array<{
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'Terms': {
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
},
'TimeLimits': {
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
},
'Rule': {
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
},
'Status': {
'Status': {
'_text': string
},
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}
}>
},
'OfferInstructionMetadatas': {
'OfferInstructionMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'OfferPenaltyMetadatas': {
'OfferPenaltyMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'OfferTermsMetadatas': {
'OfferTermsMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
}
},
'Pricing': {
'DiscountMetadatas': {
'DiscountMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Timestamp': {
'_text': string
}
}>
}
},
'Qualifier': {
'BaggagePricingQualifier': {
'FixedPrePaidInd': string,
'CommercialAgreementID': string,
'DeferralInd': string,
'refs': string,
'IncludeSettlementInd': string,
'BaggageOption': Array<{
'_text': string
}>,
'RequestAction': {
'_text': string
},
'OptionalCharges': {
'_text': string
}
},
'ExistingOrderQualifier': {
'OrderKeys': {
'refs': string,
'OrderID': {
'refs': string,
'ObjectMetaReferences': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
},
'AssociatedIDs': {
'AssociatedID': Array<{
'OrderItemID': {
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
},
'OfferItemID': {
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
},
'ServiceID': {
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}
}>
}
},
'PassengerReferences': Array<{
'_text': string
}>,
'BookingReference': {
'refs': string,
'Type': {
'_text': string
},
'ID': {
'_text': string
},
'AirlineID': {
'refs': string,
'ObjectMetaReferences': string,
'Name': string,
'_text': string
},
'OtherID': {
'refs': string,
'ObjectMetaReferences': string,
'Name': string,
'_text': string
}
}
},
'PaymentCardQualifiers': Array<{
'refs': string,
'IsPercentInd': string,
'Type': {
'_text': string
},
'IIN_Number': {
'_text': string
},
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'CurrCode': {
'id': string,
'metadata': string,
'refs': string,
'application': string,
'NumberOfDecimals': string,
'_text': string
}
}>,
'ProgramQualifiers': {
'refs': string,
'ProgramQualifier': Array<{
'DiscountProgramQualifier': {
'refs': string,
'Account': {
'refs': string,
'_text': string
},
'AssocCode': {
'_text': string
},
'Name': {
'_text': string
}
},
'IncentiveProgramQualifier': {
'refs': string,
'Name': {
'_text': string
},
'AccountID': {
'refs': string,
'_text': string
},
'AirlineID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'MemberStatus': {
'refs': string,
'_text': string
}
},
'PrePaidProgramQualifier': {
'refs': string,
'PrepaidProgramDetail': {
'AirlineID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Certificate': Array<{
'refs': string,
'Number': {
'refs': string,
'_text': string
},
'Application': {
'_text': string
},
'EffectivePeriod': {
'YearPeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'YearMonthPeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'TimePeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'QuarterPeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'MonthPeriod': {
'Effective': {
'Name': string,
'_text': string
},
'Expiration': {
'Name': string,
'_text': string
}
},
'DayPeriod': {
'Effective': {
'Name': string,
'_text': string
},
'Expiration': {
'Name': string,
'_text': string
}
},
'DateTimePeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
},
'DatePeriod': {
'Effective': {
'_text': string
},
'Expiration': {
'_text': string
}
}
}
}>,
'ProgramName': {
'_text': string
},
'ProgramCode': {
'_text': string
},
'Holder': {
'AgencyID': {
'refs': string,
'Owner': string,
'_text': string
},
'PartnerID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
}
}
},
'ProgramStatusQualifier': {
'refs': string,
'ProgramStatus': Array<{
'_text': string
}>
}
}>
},
'PromotionQualifiers': {
'refs': string,
'CC_IssuingCountryInd': string,
'Code': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Link': {
'_text': string
},
'Issuer': {
'AirlineID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AgencyID': {
'refs': string,
'Owner': string,
'_text': string
},
'PartnerID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
},
'VoucherOwner': {
'_text': string
}
},
'SeatQualifier': {
'Assignment': Array<{
'refs': string,
'Location': {
'Column': {
'_text': string
},
'Row': {
'refs': string,
'Number': {
'_text': string
},
'Type': {
'_text': string
},
'SeatData': {
'Descriptions': {
'Description': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'OriginDestinationReference': Array<{
'_text': string
}>
}>
},
'UOM': {
'_text': string
},
'SeatPitchLow': {
'_text': string
},
'SeatWidthLow': {
'_text': string
},
'Keywords': {
'KeyWord': Array<{
'refs': string,
'ObjectKey': string,
'Word': {
'_text': string
},
'Value': Array<{
'_text': string
}>
}>
},
'Marketing': {
'refs': string,
'Images': Array<{
'ImageID': {
'_text': string
},
'Position': {
'Row': {
'Position': {
'_text': string
},
'Orientation': {
'_text': string
}
},
'Column': {
'Position': {
'_text': string
},
'Orientation': {
'_text': string
}
}
}
}>,
'Links': Array<{
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Position': {
'Row': {
'Position': {
'_text': string
},
'Orientation': {
'_text': string
}
},
'Column': {
'Position': {
'_text': string
},
'Orientation': {
'_text': string
}
}
}
}>,
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}
}
},
'Characteristics': {
'Characteristic': Array<{
'refs': string,
'Code': {
'_text': string
},
'Definition': {
'_text': string
},
'TableName': {
'_text': string
},
'Link': {
'_text': string
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
},
'Associations': {
'AllPassengersInd': string,
'WholeItineraryInd': string,
'Passengers': Array<{
'PassengerReferences': {
'_text': string
}
}>,
'Group': {
'refs': string,
'ObjectKey': string,
'Name': {
'_text': string
},
'PassengerCount': {
'_text': string
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
}
},
'Flight': {
'AllFlightInd': {
'_text': string
},
'AllOriginDestinationInd': {
'_text': string
},
'AllSegmentInd': {
'_text': string
},
'OriginDestinationReferences': Array<{
'_text': string
}>,
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>
},
'OrderItems': {
'OrderItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'OfferItems': {
'OfferItemID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'WebAddressID': string,
'OwnerType': string,
'_text': string
}>
},
'Services': {
'ServiceID': Array<{
'refs': string,
'ObjectKey': string,
'Owner': string,
'_text': string
}>
},
'OtherAssociations': {
'OtherAssociation': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}
},
'SeatAssociation': {
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>,
'PassengerReference': Array<{
'_text': string
}>
}
}>
},
'ServiceQualifier': {
'refs': string,
'Encoding': {
'refs': string,
'RFIC': {
'_text': string
},
'Type': {
'_text': string
},
'Code': {
'_text': string
},
'SubCode': {
'_text': string
}
},
'Fulfillment': {
'refs': string,
'OfferValidDates': {
'Start': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'End': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
}
},
'Provider': {
'metadata': string,
'refs': string,
'PartnerID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'Name': {
'_text': string
},
'Type': {
'_text': string
},
'Fulfillments': {
'Fulfillment': Array<{
'refs': string,
'OfferValidDates': {
'Start': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
},
'End': {
'DateTime': string,
'ShortDate': string,
'Timestamp': string,
'Time': string
}
},
'Location': {
'AirportCode': {
'refs': string,
'_text': string
},
'StructuredAddress': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
}
}
}>
}
},
'Location': {
'AirportFulfillmentLocation': {
'refs': string,
'AirportCode': {
'refs': string,
'_text': string
}
},
'OtherFulfillmentLocation': {
'Address': {
'AddressCore': {
'refs': string,
'Address': {
'PaymentAddress': {
'refs': string,
'Street': Array<{
'_text': string
}>,
'PO_Box': {
'_text': string
},
'BuildingRoom': {
'_text': string
},
'City': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'Country': {
'_text': string
}
},
'SimpleAddress': {
'refs': string,
'AddressLine': Array<{
'_text': string
}>
},
'StructuredAddress': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
}
}
},
'AddressDetail': {
'refs': string,
'Address': {
'PaymentAddress': {
'refs': string,
'Street': Array<{
'_text': string
}>,
'PO_Box': {
'_text': string
},
'BuildingRoom': {
'_text': string
},
'City': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'Country': {
'_text': string
}
},
'SimpleAddress': {
'refs': string,
'AddressLine': Array<{
'_text': string
}>
},
'StructuredAddress': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
}
},
'Directions': {
'Direction': Array<{
'refs': string,
'Text': {
'_text': string
},
'Link': {
'_text': string
},
'Name': {
'_text': string
},
'From': {
'_text': string
},
'To': {
'_text': string
}
}>
}
}
}
}
}
},
'Associations': {
'SegmentReferences': Array<{
'OnPoint': string,
'OffPoint': string,
'_text': string
}>,
'PassengerReferences': Array<{
'_text': string
}>
},
'Include': {
'_text': string
}
},
'SocialMediaQualifiers': {
'refs': string,
'Service': {
'_text': string
},
'User': {
'refs': string,
'_text': string
},
'EmailAddress': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'SpecialFareQualifiers': {
'refs': string,
'AirlineID': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'CompanyIndex': {
'refs': string,
'_text': string
},
'Contract': {
'refs': string,
'_text': string
},
'Account': {
'refs': string,
'_text': string
}
},
'SpecialNeedQualifiers': {
'refs': string,
'Code': {
'_text': string
},
'Description': {
'_text': string
},
'DescContext': {
'_text': string
}
},
'TripPurposeQualifier': {
'_text': string
}
},
'Seat': {
'SeatMetadatas': {
'SeatMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'SeatStatus': {
'_text': string
}
}>
},
'SeatMapMetadatas': {
'SeatMapMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
}
}
}
},
'Passenger': {
'PassengerMetadata': {
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'NameDetail': {
'GivenNamePrefix': Array<{
'_text': string
}>,
'TitleSuffix': Array<{
'_text': string
}>,
'SurnamePrefix': {
'_text': string
},
'SurnameSuffix': Array<{
'_text': string
}>
}
}
},
'Other': {
'OtherMetadata': Array<{
'AddressMetadatas': {
'AddressMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
},
'AddressType': {
'_text': string
},
'AddressFields': {
'FieldName': Array<{
'Mandatory': string,
'_text': string
}>
}
}>
},
'AircraftMetadatas': {
'AircraftMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'TailNumber': {
'_text': string
},
'Name': {
'_text': string
}
}>
},
'AirportMetadatas': {
'AirportMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Contacts': {
'Contact': Array<{
'ContactType': string,
'AddressContact': {
'refs': string,
'Application': {
'_text': string
},
'Street': Array<{
'_text': string
}>,
'BuildingRoom': {
'_text': string
},
'PO_Box': {
'_text': string
},
'CityName': {
'_text': string
},
'StateProv': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CountryCode': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'County': {
'_text': string
}
},
'EmailContact': {
'refs': string,
'Application': {
'_text': string
},
'Address': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
}
},
'OtherContactMethod': {
'refs': string,
'Name': {
'_text': string
},
'Value': {
'_text': string
},
'GroupName': {
'_text': string
}
},
'PhoneContact': {
'refs': string,
'Application': {
'_text': string
},
'Number': Array<{
'refs': string,
'CountryCode': string,
'AreaCode': string,
'Extension': string,
'_text': string
}>
},
'Name': {
'refs': string,
'ObjectMetaReferences': string,
'Surname': {
'refs': string,
'_text': string
},
'Given': Array<{
'refs': string,
'_text': string
}>,
'Title': {
'_text': string
},
'SurnameSuffix': {
'_text': string
},
'Middle': Array<{
'refs': string,
'_text': string
}>
}
}>
},
'Directions': {
'Direction': Array<{
'refs': string,
'Text': {
'_text': string
},
'Link': {
'_text': string
},
'Name': {
'_text': string
},
'From': {
'_text': string
},
'To': {
'_text': string
}
}>
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
}>
},
'CityMetadatas': {
'CityMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Directions': {
'Direction': Array<{
'refs': string,
'Text': {
'_text': string
},
'Link': {
'_text': string
},
'Name': {
'_text': string
},
'From': {
'_text': string
},
'To': {
'_text': string
}
}>
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
}>
},
'CodesetMetadatas': {
'CodesetMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Source': {
'OwnerID': {
'Name': string,
'_text': string
},
'File': {
'_text': string
},
'Version': {
'_text': string
}
},
'OtherLanguage': {
'LanguageCode': {
'refs': string,
'ObjectKey': string,
'_text': string
},
'Description': {
'_text': string
}
}
}>
},
'ContactMetadatas': {
'ContactMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'CountryMetadatas': {
'CountryMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'ICAO_Code': {
'_text': string
},
'Name': {
'_text': string
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
}>
},
'CurrencyMetadatas': {
'CurrencyMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Application': {
'_text': string
},
'Decimals': {
'_text': string
},
'Name': {
'_text': string
}
}>
},
'DescriptionMetadatas': {
'DescriptionMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Application': {
'_text': string
},
'Topic': {
'_text': string
},
'Hint': {
'_text': string
},
'Copyright': {
'_text': string
},
'Sequence': {
'_text': string
}
}>
},
'EquivalentID_Metadatas': {
'EquivalentID_Metadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'EquivID': Array<{
'metarefs': string,
'refs': string,
'MetadataKey': string,
'EquivalentID_Key': {
'_text': string
},
'ID_Value': {
'_text': string
},
'Owner': {
'_text': string
}
}>
}>
},
'LanguageMetadatas': {
'LanguageMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Application': {
'_text': string
},
'Code_ISO': {
'_text': string
},
'Code_NLS': {
'_text': string
},
'Name': {
'_text': string
}
}>
},
'PaymentCardMetadatas': {
'PaymentCardMetadata': Array<{
'refs': string,
'MetadataKey': string,
'CardCode': {
'_text': string
},
'CardName': {
'_text': string
},
'CardType': {
'_text': string
},
'CardFields': {
'FieldName': Array<{
'Mandatory': string,
'_text': string
}>
},
'CardProductType': {
'_text': string
},
'Surcharge': {
'Amount': {
'Code': string,
'Taxable': boolean,
'_text': string
},
'PercentageValue': {
'_text': string
}
}
}>
},
'PaymentFormMetadatas': {
'PaymentFormMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'CustomerFileReference': {
'_text': string
},
'ExtendedPaymentCode': {
'_text': string
},
'Text': {
'_text': string
},
'CorporateContractCode': {
'_text': string
}
}>
},
'PriceMetadatas': {
'PriceMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
}
}>
},
'RuleMetadatas': {
'RuleMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'RuleID': {
'_text': string
},
'Name': {
'_text': string
},
'Status': {
'_text': string
},
'Values': {
'Value': Array<{
'NodePath': Array<{
'Path': {
'_text': string
},
'TagName': {
'_text': string
}
}>,
'Instruction': {
'_text': string
}
}>
},
'Remarks': {
'refs': string,
'Remark': Array<{
'DisplayInd': string,
'Timestamp': string,
'_text': string
}>
}
}>
},
'StateProvMetadatas': {
'StateProvMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
}>
},
'ZoneMetadatas': {
'ZoneMetadata': Array<{
'refs': string,
'MetadataKey': string,
'AugmentationPoint': {
'AugPoint': Array<{
'Key': string,
'Owner': string,
'Seq': string
}>,
'Lists': {
'ListToken': string,
'Owner': string,
'Seq': string,
'List': Array<{
'ListKey': string,
'ListName': string,
'Owner': string,
'Seq': string,
'AugPointAssoc': Array<{
'KeyRef': string,
'Owner': string,
'Seq': string
}>,
'NamedAssocs': {
'NamedAssoc': Array<{
'Target': string,
'KeyRef': string,
'From': string,
'Seq': string,
'Group': Array<{
'GroupKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'List': Array<{
'ListKeyRef': string,
'TokenRef': string,
'Seq': string
}>,
'UniqueKeyID': Array<{
'UniqueID_Ref': string,
'Seq': string
}>
}>
}
}>
}
},
'Position': {
'refs': string,
'Latitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Longitude': {
'Sign': string,
'Minute': string,
'Second': string,
'_text': string
},
'Altitude': {
'Unit': string,
'Context': string,
'_text': string
},
'NAC': {
'_text': string
}
}
}>
}
}>
}
},
'ResponseParameters': {
'InventoryGuaranteeInd': {
'_text': string
},
'CurParameter': Array<{
'CurCode': {
'_text': string
},
'AppCode': {
'_text': string
},
'DecimalsAllowedNumber': {
'_text': string
}
}>,
'LangUsage': Array<{
'LangCode': {
'_text': string
},
'LangUsageType': {
'_text': string
}
}>,
'MeasurementSystemCode': {
'_text': string
},
'PaxGroup': {
'GroupID': {
'_text': string
},
'Name': {
'_text': string
},
'IntendedPaxQty': {
'_text': string
},
'ContactInfo': Array<{
'ContactInfoID': {
'_text': string
},
'ContactTypeText': {
'_text': string
},
'IndividualRef': {
'_text': string
},
'ContactRefusedInd': {
'_text': string
},
'Phone': Array<{
'LabelText': {
'_text': string
},
'CountryDialingCode': {
'_text': string
},
'AreaCodeNumber': {
'_text': string
},
'PhoneNumber': {
'_text': string
},
'ExtensionNumber': {
'_text': string
}
}>,
'OtherAddress': Array<{
'LabelText': {
'_text': string
},
'OtherAddressText': {
'_text': string
}
}>,
'EmailAddress': Array<{
'LabelText': {
'_text': string
},
'EmailAddressText': {
'_text': string
}
}>,
'Individual': {
'IndividualID': {
'_text': string
},
'GenderCode': {
'_text': string
},
'TitleName': {
'_text': string
},
'GivenName': Array<{
'_text': string
}>,
'MiddleName': Array<{
'_text': string
}>,
'Surname': {
'_text': string
},
'SuffixName': {
'_text': string
},
'Birthdate': {
'_text': string
},
'BirthplaceText': {
'_text': string
}
},
'PostalAddress': Array<{
'LabelText': {
'_text': string
},
'StreetText': Array<{
'_text': string
}>,
'BuildingRoomText': {
'_text': string
},
'PO_BoxCode': {
'_text': string
},
'PostalCode': {
'_text': string
},
'CityName': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
},
'CountryName': {
'_text': string
},
'CountryCode': {
'_text': string
}
}>
}>
},
'PricingParameter': {
'AutoExchInd': {
'_text': string
},
'AwardIncludedInd': {
'_text': string
},
'AwardOnlyInd': {
'_text': string
},
'OverrideCurCode': {
'_text': string
},
'SimplePricingInd': {
'_text': string
},
'TaxExemption': Array<{
'TotalTaxAmount': {
'CurCode': string,
'_text': string
},
'AllRefundableInd': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'Tax': Array<{
'Amount': {
'CurCode': string,
'_text': string
},
'TaxCode': {
'_text': string
},
'TaxTypeCode': {
'_text': string
},
'QualifierCode': {
'_text': string
},
'DescText': {
'_text': string
},
'FiledAmount': {
'CurCode': string,
'_text': string
},
'FiledTaxCode': {
'_text': string
},
'ApproximateInd': {
'_text': string
},
'CollectionInd': {
'_text': string
},
'RefundInd': {
'_text': string
},
'AppTypeCode': {
'_text': string
},
'AdditionalTaxCode': {
'_text': string
},
'AdditionalFiledTaxCode': {
'_text': string
},
'CollectionPointTax': Array<{
'AirportAmount': {
'CurCode': string,
'_text': string
},
'IATA_StationCode': {
'_text': string
}
}>,
'Country': {
'CountryCode': {
'_text': string
},
'CountryName': {
'_text': string
}
},
'CountrySub-Division': {
'CountrySubDivisionCode': {
'_text': string
},
'CountrySubDivisionName': {
'_text': string
}
},
'CurConversion': {
'Amount': {
'CurCode': string,
'_text': string
},
'ConversionRate': {
'MultiplierValue': string,
'UnitCode': string,
'CurCode': string,
'BaseMultiplierValue': string,
'BaseUnitCode': string,
'BaseCurCode': string,
'Format': string,
'_text': string
},
'LocalAmount': {
'CurCode': string,
'_text': string
}
}
}>
}>
}
},
'ServiceListProcessing': {
'refs': string,
'Marketing': {
'refs': string,
'Message': Array<{
'refs': string,
'ObjectKey': string,
'Text': {
'refs': string,
'_text': string
},
'MarkupStyle': {
'_text': string
},
'Link': {
'_text': string
},
'Media': Array<{
'ObjectID': {
'refs': string,
'Owner': string,
'_text': string
},
'MediaLink': {
'refs': string,
'ObjectMetaReferences': string,
'_text': string
},
'AttachmentID': {
'refs': string,
'Owner': string,
'_text': string
}
}>,
'Associations': {
'refs': string,
'Association': Array<{
'Type': {
'_text': string
},
'ReferenceValue': {
'_text': string
}
}>
}
}>
}
},
'ShoppingResponse': {
'ShoppingResponseID': {
'_text': string
},
'OwnerCode': {
'_text': string
}
},
'Warning': Array<{
'Code': {
'_text': string
},
'DescText': {
'_text': string
},
'LanguageCode': {
'_text': string
},
'OwnerName': {
'_text': string
},
'StatusText': {
'_text': string
},
'TagText': {
'_text': string
},
'TypeCode': {
'_text': string
},
'URL': {
'_text': string
}
}>
}
